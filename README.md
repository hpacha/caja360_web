# CajaArequipaGestionDeTalentos

Sistema de gestion de talento Caja Arequipa

## Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Compilación para QA

-cd /home/jespinales/dev/caja-arequipa-gestion-de-talentos
-git pull
-npm i; ng build --prod --base-href /caja-gestion-de-talentos/ --c dev
-rm -rf /var/www/html/caja-gestion-de-talentos/*
-cp -r /home/jespinales/dev/caja-arequipa-gestion-de-talentos/dist/caja-arequipa-gestion-de-talentos/* /var/www/html/caja-gestion-de-talentos/


## Compilación para PROD

- git pull origin dev; npm i; ng build --prod --base-href /home/jespinales/dev/caja-arequipa-gestion-de-talentos/dist/caja-arequipa-gestion-de-talentos/ --c dev; 

- rm -f /var/www/html/caja-gestion-de-talentos/; ln -s /home/jespinales/dev/caja-arequipa-gestion-de-talentos/dist/caja-arequipa-gestion-de-talentos/ /var/www/html/caja-gestion-de-talentos/

## Rutas QA:

- FRONT https://pappstest.com/caja-gestion-de-talentos/perfil

## Usuarios de QA
-Front
codigo empleado: 005
usuario: rcontreras
contraseña: 123

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
