// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  debug: true,
  socketUrl: 'https://caja-arequipa-rrhh.pappstest.com',
  chatUrl: 'https://caja-arequipa-rrhh.pappstest.com/api/v1',
  apiurl: 'http://8a74-2800-200-f580-7883-64b1-372f-73da-e49d.ngrok.io/api/v1',
  apiurlprueba: 'http://185.166.215.240/api/v1',
  key_encrypt: 'ODI3NDZkY2FmNjJiNDE2ZGE0YjRiMWI2Mzg4ZmQ3MmM=',
  recaptcha_key: '6Ld11PYUAAAAAHLaYlfMurFnaWxUG9Nci3qpYtcB',
  google_maps_api_key: 'AIzaSyASOUaPdUBhEiG2yRWAkoObNbv1vr5v-uw',
  google_places_api_key: 'AIzaSyASOUaPdUBhEiG2yRWAkoObNbv1vr5v-uw',
  firebaseConfig: {
    apiKey: 'AIzaSyA-zPmSc5IFq8UCWPqGLymap0c4S7eiX4w',
    authDomain: 'recursos-humanos-280516.firebaseapp.com',
    databaseURL: 'https://recursos-humanos-280516.firebaseio.com',
    projectId: 'recursos-humanos-280516',
    storageBucket: 'recursos-humanos-280516.appspot.com',
    messagingSenderId: '359422243725',
    appId: '1:359422243725:web:63271693b79bb5f349c3c4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
