export const environment = {
  production: false,
  debug: true,
  socketUrl: 'https://chatcaja360.cajaarequipa.pe:7443',
  chatUrl: 'https://wscaja360.cajaarequipa.pe:4443/api/v1',
  apiurl: 'https://wscaja360.cajaarequipa.pe:4443/api/v1',
  apiurlprueba: 'http://4175-2800-200-f580-7883-d843-2f8b-c089-7706.ngrok.io/api/v1',
  key_encrypt: 'YTE4NTljOTdlYjRiMTVlNjM0YjU4NjU5OTZkOWY0ZGE=',
  recaptcha_key: '6Ld11PYUAAAAAHLaYlfMurFnaWxUG9Nci3qpYtcB',
  google_maps_api_key: 'AIzaSyASOUaPdUBhEiG2yRWAkoObNbv1vr5v-uw',
  google_places_api_key: 'AIzaSyASOUaPdUBhEiG2yRWAkoObNbv1vr5v-uw',
  firebaseConfig: {
    apiKey: 'AIzaSyA-zPmSc5IFq8UCWPqGLymap0c4S7eiX4w',
    authDomain: 'recursos-humanos-280516.firebaseapp.com',
    databaseURL: 'https://recursos-humanos-280516.firebaseio.com',
    projectId: 'recursos-humanos-280516',
    storageBucket: 'recursos-humanos-280516.appspot.com',
    messagingSenderId: '359422243725',
    appId: '1:359422243725:web:63271693b79bb5f349c3c4'
  }
};
