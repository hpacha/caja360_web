export const OAuthSettings = {
  // appId: '7cc12216-85b6-4844-954f-065cefe17dd6',
  appId: '128b084d-d51a-4421-a25e-fa68170db167',
  redirectUri: 'http://localhost:4200',
  forceRefresh: true,
  scopes: [
    "openid",
    "profile",
    "offline_access",
    "user.read",
    "calendars.readwrite"
  ]
};
