export interface Notification {
  created_at?: string;
  data: { title: string, message: string, action: string, id: number, resource_title?: string };
  id: string;
  read_at: string;
  updated_at: string;
}

export interface NotificationResponse {
  current_page: number;
  data: Notification[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url: string;
  to: number;
  total: number;
}
