import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertModule } from 'ngx-bootstrap/alert';

import { TrimCajaDirective } from './directives/trim-caja.directive';
import { OnlyNumbersDirective } from './directives/only-numbers.directive';
import { ImagePreloadDirective } from './directives/image-preload.directive';
import { CreditcardPipe } from './pipes/creditcard.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { FormatDatePipe } from './pipes/format-date.pipe';
import { FormatHourPipe } from './pipes/format-hour.pipe';
import { UrlifyPipe } from './pipes/urlify.pipe';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { SlugifyPipe } from './pipes/slugify.pipe';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

import { AlertMessageComponent } from './alert-message/alert-message.component';
import { HttpErrorComponent } from './http-error/http-error.component';
import { EmptyDataComponent } from './empty-data/empty-data.component';
import { SafePipe } from '@shared/pipes/safe.pipe';

@NgModule({
  declarations: [
    AlertMessageComponent,
    HttpErrorComponent,
    EmptyDataComponent,
    TrimCajaDirective,
    OnlyNumbersDirective,
    ImagePreloadDirective,
    CreditcardPipe,
    TruncatePipe,
    FormatDatePipe,
    FormatHourPipe,
    UrlifyPipe,
    TimeAgoPipe,
    SlugifyPipe,
    SafeHtmlPipe,
    SafePipe
  ],
  exports: [
    AlertMessageComponent,
    HttpErrorComponent,
    EmptyDataComponent,
    TrimCajaDirective,
    OnlyNumbersDirective,
    ImagePreloadDirective,
    CreditcardPipe,
    TruncatePipe,
    FormatDatePipe,
    FormatHourPipe,
    UrlifyPipe,
    TimeAgoPipe,
    SlugifyPipe,
    SafeHtmlPipe,
    SafePipe
  ],
  imports: [CommonModule, AlertModule.forRoot()]
})
export class SharedModule {}
