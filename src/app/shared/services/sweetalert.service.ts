import {Injectable} from '@angular/core';
import Swal from 'sweetalert2';

declare const swal: any;

@Injectable({
    providedIn: 'root'
})
export class SweetalertService {
    constructor() {
    }

    swal(...args) {
        let defaultArg: any = args;
        if (defaultArg.length > 1) {
            return swal(args.shift(), args.pop());
        } else {
            if (defaultArg.find(x => x).buttons) {
                defaultArg = Object.assign(defaultArg.find(x => x), {
                    buttons: ['Cerrar', 'Ok'],
                    dangerMode: true
                });
            } else {
                defaultArg = args.shift();
            }
            return swal(defaultArg);
        }
    }

    sweetAlert(icon: any, title: string, text: string) {
        return Swal.fire({
            icon,
            title,
            text,
        });
    }

    swalToast(icon: any, title: string, text: string) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

        return Toast.fire({
            icon,
            title,
            text
        });
    }

    sweetConfirm(...args) {
        return Swal.fire(...args);
    }
}
