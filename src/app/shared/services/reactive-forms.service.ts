import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SweetalertService } from './sweetalert.service';
import { environment } from '../../../environments/environment';
import { settings } from '../common/config';

@Injectable({
    providedIn: 'root'
})
export class ReactiveFormsService {
    constructor(private sweetalertService: SweetalertService) {}

    markFormGroupTouched(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach((key: any) => {
            // formGroup.controls[key].markAsTouched();
            const control: any = formGroup.get(key);
            if (!control.controls) {
                control.markAsTouched({ onlySelf: true });
            } else {
                const formArray: any = control.controls;
                formArray.forEach(field => {
                    Object.keys(field.controls).forEach(subkey => {
                        const nestedControl = field.get(subkey);
                        nestedControl.markAsTouched({ onlySelf: true });
                    });
                });
            }
        });
    }

    isValid(formControl: FormControl) {
        return formControl.touched && !formControl.valid ? 'is-invalid' : '';
    }

    swalsave() {
        return this.sweetalertService.swal({
            title: settings.messages.save.title,
            text: settings.messages.save.text,
            icon: settings.alerttype.successicon
        });
    }
}
