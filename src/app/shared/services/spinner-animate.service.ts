import { Injectable, ElementRef } from '@angular/core';
import { AnimationBuilder, style, animate } from '@angular/animations';

@Injectable({
    providedIn: 'root',
})
export class SpinnerAnimateService {
    player: any;
    spinner: any;
    opacity = '.5';
    constructor(private animationBuilder: AnimationBuilder) {
        this.spinner = document.querySelector('#spinner-wrapper');
        this.spinner.style.opacity = this.opacity;
    }

     show() {
        this.animation(true);
    }

    hide() {
        this.animation(false);
    }

    animation(status: any) {
        this.player = this.animationBuilder
            .build([
                style({ opacity: status ? '0' : this.opacity }),
                animate(
                    '400ms ease',
                    style({
                        opacity: status ? this.opacity : '0',
                        zIndex: status ? '99999' : '-10',
                    })
                ),
            ])
            .create(this.spinner);
        this.player.play();
    }
}
