export enum days {
    L = 'Lunes',
    M = 'Martes',
    Mi = 'Miércoles',
    J = 'Jueves',
    V = 'Viernes',
    S = 'Sábado',
    D = 'Domingo',
}
