export enum DocumentType {
    DNI = 'dni',
    RUC = 'ruc'
}
