import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss']
})
export class AlertMessageComponent implements OnInit {
  @Input() dismissible = false;
  @Input() alert: any;

  constructor() {}

  ngOnInit() {
    //
  }

  onClosedAlert() {
    this.alert = null;
    this.dismissible = false;
  }
}
