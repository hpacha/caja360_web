import { Pipe, PipeTransform, SecurityContext } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser'
/**
 * @ignore
 */
@Pipe({ name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value: string): any {
    return value ? String(this.sanitized.sanitize(SecurityContext.HTML, value)).replace(/<[^>]+>/gm, '') : '';
  }
}
