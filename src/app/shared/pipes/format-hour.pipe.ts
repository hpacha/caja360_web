import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatHour'
})
export class FormatHourPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value) {
      return moment(value).format('LT');
    }
  }
}
