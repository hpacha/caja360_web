import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value && !args) {
      return moment(value).format('DD/MM/YY');
    }
    if (args === 'year') {
      return moment(value).format('DD/MM/YYYY');
    }
  }
}
