export const settings = {
    pagePaths: {
    },
    alerttype: {
      danger: 'danger',
      warning: 'warning',
      success: 'success',
      successicon: './assets/img/exito.png',
      clear: ''
    },
    auth: {
      recoverPassword: {
          message: 'Solicitud enviada'
      }
    },
    messages: {
      titles: {
          success: 'Éxito'
      },
      save: {
          title: 'Registro Exitoso',
          text: 'Se registró correctamente la información.'
      },
      update: {
          title: 'Registro Exitoso',
          text: 'Se actualizó correctamente la información.'
      },
      delete: {
          title: '¿Estás seguro de eliminar?',
          text: 'Una vez eliminado, se borrará toda la información que está relacionada.',
          text2: 'Se eliminó correctamente'
      },
      savePicture: {
        title: 'Registro Exitoso',
        text: 'Se actualizó correctamente tu foto.'
      },
      error: {
          title: 'Error'
      },
      warning: {
          title: 'Advertencia'
      },
      resend: {
          title: 'Éxito',
          message: 'El correo para restablecer contraseña fue enviado correctamente.'
      },
      norecords: 'No se encontraron registros coincidentes'
    }
};
