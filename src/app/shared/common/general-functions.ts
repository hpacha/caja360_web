import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptorCaja } from '@interceptors/AppHttpInterceptorCaja';
import { AppHttpErrorElement } from '@interceptors/AppHttpErrorElement';
import { settings } from './config';

export const InterceptorCajaGeneral = {
    provide: HTTP_INTERCEPTORS,
    useClass: AppHttpInterceptorCaja,
    multi: true
};

export const InteceptorErrorContent = {
  provide: HTTP_INTERCEPTORS,
  useClass: AppHttpErrorElement,
  multi: true
};

export const handleErrors = error => {
  return {
    type: settings.alerttype.danger,
    title: settings.messages.error.title,
    msg: error
  };
};
