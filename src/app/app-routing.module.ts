import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CajaActiveGuard } from './guards/caja-active.guard';
import { CajaInactiveGuard } from './guards/caja-inactive.guard';

const routes: Routes = [
  {path: '',loadChildren: () => import('@components/dashboard/dashboard.module').then( m => m.DashboardModule ),canActivateChild: [CajaActiveGuard]},
  {path: 'auth', loadChildren: () => import('@components/authentication/authentication.module').then( m => m.AuthenticationModule ), canActivateChild: [CajaInactiveGuard]},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
