export let chatAdviserVisible = true;
export function setChatAdviserVisible(show: boolean) {
  chatAdviserVisible = show;
}
export let loadingVisible = true;
export function setLoadingVisible(show: boolean) {
  loadingVisible = show;
}
