import * as CryptoJS from 'crypto-js';
import { environment } from '@environments/environment';

export class CipherUtil {
  private static keyEnc = environment.key_encrypt; // btoa
  private static key = CryptoJS.enc.Utf8.parse(atob( CipherUtil.keyEnc)); // CipherUtil.getKey()
  private static iv = CryptoJS.enc.Utf8.parse(String.fromCharCode(0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0)); // '1012CJQ@CFCHPWLO'

  public static decrypt(data: string) {
    return new Promise<string>((resolve, reject) => {
      try {
        const decrypted = CryptoJS.AES.decrypt(data, this.key, {iv: this.iv});
        if (decrypted.toString()) {
          resolve(decrypted.toString(CryptoJS.enc.Latin1));
        }
        reject('MSG.ERROR');
      } catch (e) {
        reject(e);
      }
    });
  }

  public static encrypt( data: any ) {
    return new Promise((resolve, reject) => {
      try {
        const encrypted = CryptoJS.AES.encrypt(data, this.key, {iv: this.iv});
        if (encrypted.toString()) {
          resolve(encrypted.toString());
        }
        reject('MSG.ERROR');
      } catch (e) {
        reject(e);
      }
    });
  }
}
