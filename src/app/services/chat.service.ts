import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable( {
  providedIn: 'root'
})
export class ChatService {
  private chatSource = new BehaviorSubject('default message');
  eventChatId = new Subject<any>();
  currentMessage = this.chatSource.asObservable();

  constructor(
    private http: HttpClient
  ) {}

  changeSource(message: string) {
    console.log(message);
    this.chatSource.next(message)
  }

  getChatRooms(page) {
    return this.http.get(`${environment.chatUrl}/chat/rooms?page=${page}`);
  }

  putChatRooms(params) {
    return this.http.put(`${environment.chatUrl}/chat/rooms`, params);
  }

  patchChatRooms(room_id, params) {
    return this.http.post(`${environment.chatUrl}/chat/rooms/${room_id}`, params);
  }

  delChatRooms(room_id) {
    return this.http.delete(`${environment.chatUrl}/chat/rooms/${room_id}`);
  }

  showChatRooms(room_id) {
    return this.http.get(`${environment.chatUrl}/chat/rooms/${room_id}`);
  }

  searchChatRooms(params) {
    return this.http.post(`${environment.chatUrl}/chat/rooms/search`, params);
  }

  leaveChatRooms(room_id) {
    return this.http.post(`${environment.chatUrl}/chat/rooms/${room_id}/leave`, {});
  }

  getChatRoomsMessages(room_id, page) {
    return this.http.get(`${environment.chatUrl}/chat/rooms/${room_id}/messages?page=${page}`);
  }

  delChatRoomsMessages(room_id) {
    return this.http.delete(`${environment.chatUrl}/chat/messages/${room_id}`);
  }

  getChatRoomsFiles(room_id) {
    return this.http.get(`${environment.chatUrl}/chat/rooms/${room_id}/files`);
  }

  putChatRoomsMessages(room_id, params) {
    return this.http.post(`${environment.chatUrl}/chat/rooms/${room_id}/messages`, params);
  }

  delChatMessages(message_id) {
    return this.http.delete(`${environment.chatUrl}/chat/messages/${message_id}`);
  }
}
