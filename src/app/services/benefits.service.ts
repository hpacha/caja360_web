import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable( {
  providedIn: 'root'
})
export class BenefitsService {
  private messageSource = new BehaviorSubject(null);
  currentMessage = this.messageSource.asObservable();

  constructor(
    private http: HttpClient
  ) {}

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  getBenefits() {
    return this.http.get(`${environment.apiurl}/benefits`);
  }

  getBenefitsId(benefitId) {
    return this.http.get(`${environment.apiurl}/benefits/${benefitId}`);
  }

  postStoreBenefitsFile(params) {
    return this.http.post(`${environment.apiurl}/benefits`, params);
  }

  delStoreBenefitsFile(benefitId, params) {
    let httpParams = new HttpParams().set('action', params.action);
    return this.http.delete(`${environment.apiurl}/benefits/${benefitId}`, { params: httpParams });
  }
}
