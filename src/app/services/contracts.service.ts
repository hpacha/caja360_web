import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable( {
  providedIn: 'root'
})
export class ContractService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getListContracts(type, page, parameters) {
    if(parameters)
      return this.httpClient.post(`${environment.apiurl}/contracts/${type}`, parameters);
    return this.httpClient.post(`${environment.apiurl}/contracts/${type}?page=${page}`, parameters);
  }

  postStoreContract(parameters: any) {
    return this.httpClient.post(`${environment.apiurl}/contracts/store`, parameters);
  }

  putDestroyContract(file_id: any) {
    return this.httpClient.put(`${environment.apiurl}/contracts/destroy`, file_id);
  }

  putRejectContract(parameters: any) {
    return this.httpClient.put(`${environment.apiurl}/contracts/reject`, parameters);
  }

  putApproveContract(parameters: any) {
    return this.httpClient.put(`${environment.apiurl}/contracts/approve`, parameters);
  }
}
