import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
})

export class AdviserService {
  constructor(
    private http: HttpClient,
  ) {}

  getChatAdviserOptions() {
    return this.http.get(`${environment.chatUrl}/chat/adviser/options`);
  }

  getChatAdviserOptionsChild(adviserId) {
    return this.http.get(`${environment.chatUrl}/chat/adviser/options/${adviserId}/child`);
  }
}
