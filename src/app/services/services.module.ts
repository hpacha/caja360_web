import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { UserService } from './user.service';
import { NotificationService } from './notification.service';
import { NewnessNewsService } from './newness-news.service';
import { NewnessActivityService } from './newness-activity.service';
import { ContactsService } from './contacts.service';
import { InteceptorErrorContent, InterceptorCajaGeneral } from '@shared/common/general-functions';
import { FrequentQuestionsService } from './frequent-questions.service';
import { CardService } from './card.service';
import { ReceiptsService } from './receipts.service';
import { WebsocketService } from './websocket.service';
import { ChatService } from './chat.service';
import { AdviserService } from './adviser.service';
import { ChallengesService } from '@services/challenges.service';
import { RecognitionSystemService } from '@services/recognition-system.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    AuthenticationService,
    UserService,
    NotificationService,
    NewnessActivityService,
    NewnessNewsService,
    ContactsService,
    FrequentQuestionsService,
    CardService,
    ReceiptsService,
    WebsocketService,
    ChatService,
    AdviserService,
    ChallengesService,
    RecognitionSystemService,
    InterceptorCajaGeneral,
    /*{
      provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorCaja, multi: true,
      provide: HTTP_INTERCEPTORS, useClass: AppHttpErrorElement, multi: true,
    }*/
  ]
})

export class ServicesModule {}
