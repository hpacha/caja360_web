import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import Echo from 'laravel-echo';
@Injectable( {
  providedIn: 'root'
})
export class WebsocketService {
  echo: any;

  constructor(
    private authenticationService: AuthenticationService
  ) {}

  connect() {
    const xser = this.authenticationService.getUser();
    const token = `Bearer ${xser.data.access_token}`;
    this.echo = new Echo({
      broadcaster: 'socket.io',
      host: environment.socketUrl,
      auth: {
        headers: {
          'Authorization': token
        }
      },
    });
    this.echo.connect();
  }

  disconnect(chatRoom, userId) {
    const thisIn = this;
    if(chatRoom) {
      thisIn.echo.leave(`presence-chat.${chatRoom.id}`);
      thisIn.echo.leaveChannel(`presence-chat.${chatRoom.id}`);
    }
    thisIn.echo.leave(`private-rooms-observe.${userId}`);
    thisIn.echo.leaveChannel(`private-rooms-observe.${userId}`);
    thisIn.echo.disconnect();
  }

  getNewRoom(userId) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`private-rooms-observe.${userId}`)
        .listen('NewRoom', (res) => {
          observer.next(res);
        });
    });
  }
  getUpdatedRoom(userId) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`private-rooms-observe.${userId}`)
        .listen('UpdatedRoom', (res) => {
          observer.next(res);
        });
    });
  }

  getDeletedRoom(userId) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`private-rooms-observe.${userId}`)
        .listen('DeletedRoom', (res) => {
          observer.next(res);
        });
    });
  }

  getLeaveRoom(userId) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`private-rooms-observe.${userId}`)
        .listen('LeaveRoom', (res) => {
          observer.next(res);
        });
    });
  }
  getNewMessageRoom(userId) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`private-rooms-observe.${userId}`)
        .listen('NewMessageRoom', (res) => {
          observer.next(res);
        });
    });
  }

  getNewMessage(chatRoom) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`presence-chat.${chatRoom.id}`)
        .listen('NewMessage', (res) => {
          observer.next(res);
        });
    });
  }

  getDeletedMessage(chatRoom) {
    const thisIn = this;
    return Observable.create((observer) => {
      thisIn.echo.channel(`presence-chat.${chatRoom.id}`)
        .listen('DeletedMessage', (res) => {
          observer.next(res);
        });
    });
  }

  leaveNewMessage(chatRoom) {
    const thisIn = this;
    thisIn.echo.leave(`presence-chat.${chatRoom.id}`);
    thisIn.echo.leaveChannel(`presence-chat.${chatRoom.id}`);
  }
}
