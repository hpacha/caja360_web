import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class RecognitionSystemService {
  constructor( private http: HttpClient ) {}

  getMyRecognitions() {
    return this.http.get( `${environment.apiurl}/user/recognitions` );
  }

  getMyRecognitionSent() {
    return this.http.get( `${environment.apiurl}/user/recognitions/sent` );
  }

  getCategories() {
    return this.http.get( `${environment.apiurl}/recognitions/categories` );
  }

  getEventByCategory( id: number ) {
    return this.http.get( `${environment.apiurl}/recognitions/categories/${id}/events` );
  }

  getEventByPhoto( id: number ) {
    return this.http.get( `${environment.apiurl}/recognitions/events/${id}/templates` );
  }

  getProducts() {
    return this.http.get( `${environment.apiurl}/recognitions/products` );
  }

  getSearchUsers( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/recognitions/users/search`, { params: parameters } );
  }

  getSearchGroups( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/recognitions/groups/search`, { params: parameters } );
  }

  getPoints() {
    return this.http.get( `${environment.apiurl}/user/recognitions/points` );
  }

  postRedeemProduct( params: any ) {
    return this.http.post( `${environment.apiurl}/user/recognitions/products`, params );
  }

  postPreview( params: any ) {
    return this.http.post( `${environment.apiurl}/recognitions/preview`, params );
  }

  postRecognitionSend( params: any ) {
    return this.http.post( `${environment.apiurl}/user/recognitions/send`, params );
  }

  getVideo() {
    return environment.apiurl + '/storage/client/resources/videos/recognitions/laestrella.mp4';
  }
}
