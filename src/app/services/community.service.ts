import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

@Injectable( {
  providedIn: 'root'
} )
export class CommunityService {
  private listCategories: any;

  constructor(
    private httpClient: HttpClient
  ) {}

  getCompetencesCategories() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.listCategories ) {
        resolve( this.listCategories );
      } else {
        return this.httpClient.get( `${environment.apiurl}/competences/categories` )
        .subscribe( ( res: any ) => {
          this.listCategories = res.data;
          resolve( this.listCategories );
        } );
      }
    } );
  }

  getUserCompetences( parameters, type, page ) {
    return this.httpClient.post( `${environment.apiurl}/user/competences/${type}?page=${page}`, parameters );
  }

  getUserCompetencesPerformance() {
    return this.httpClient.get( `${environment.apiurl}/user/competences/performance` );
  }

  getShowCompetenceParticiping( competenceId ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/participating/${competenceId}` );
  }

  getShowCompetenceEvaluations( competenceId ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/participating/${competenceId}/evaluations` );
  }

  postSearchMentors( competence, parameters ) {
    return this.httpClient.post( `${environment.apiurl}/competences/${competence}/mentors`, parameters );
  }

  postApplyOtherMentor( parameters, oldMentorRequest ) {
    return this.httpClient.post( `${environment.apiurl}/user/competences/mentor-request/${oldMentorRequest}/another`, parameters );
  }

  postApplyMentor( competenceId, mentorId ) {
    return this.httpClient.post( `${environment.apiurl}/user/competences/${competenceId}/mentors/${mentorId}`, {} );
  }

  getShowCompetence( competenceId ) {
    return this.httpClient.get( `${environment.apiurl}/competences/${competenceId}` );
  }

  delMentorRequest( requestId ) {
    return this.httpClient.delete( `${environment.apiurl}/user/competences/mentor-request/${requestId}` );
  }

  postMentorAcceptSuggest( requestId ) {
    return this.httpClient.post( `${environment.apiurl}/user/competences/mentor-request/${requestId}/accept-suggested`, {} );
  }

  getUserCompetencesMentoring( parameters, page ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/mentoring?page=${page}`, parameters );
  }

  getUserCompetencesMentees( mentoringId: number, page ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/${mentoringId}/mentees?page=${page}` );
  }

  getUserCompetencesMentoringRequest( mentoringId: number, page ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/${mentoringId}/mentor-request?page=${page}` );
  }

  patchRequestAcceptReject( competenceId, requestId, parameters ) {
    return this.httpClient.patch( `${environment.apiurl}/user/competences/${competenceId}/mentor-request/${requestId}`, parameters );
  }

  getShowMentorEvaluations( competenceId, menteesId ) {
    return this.httpClient.get( `${environment.apiurl}/user/competences/${competenceId}/mentees/${menteesId}/evaluations` );
  }

  patchUpdateEvaluations( competenceId, menteesId, evaluationId, parameters ) {
    return this.httpClient.patch( `${environment.apiurl}/user/competences/${competenceId}/mentees/${menteesId}/evaluations/${evaluationId}`, parameters );
  }
}
