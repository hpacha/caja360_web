import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class HeadquarterService {
  private listHeadquarters: any;
  private listAreas: any;
  private listOccupations: any;

  constructor(
    private httpClient: HttpClient
  ) {}

  getHeadquarters() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.listHeadquarters) {
        resolve(this.listHeadquarters);
      } else {
        return this.httpClient.get(`${environment.apiurl}/headquarters`)
          .subscribe((res: any) => {
            this.listHeadquarters = res.data;
            this.listHeadquarters.unshift( { id: '', title: 'Todos' } );
            resolve(this.listHeadquarters);
          });
      }
    });
  }

  getAreas() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.listAreas) {
        resolve(this.listAreas);
      } else {
        return this.httpClient.get(`${environment.apiurl}/areas`)
          .subscribe((res: any) => {
            this.listAreas = res.data;
            resolve(this.listAreas);
          });
      }
    });
  }

  getOccupations() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.listOccupations) {
        resolve(this.listOccupations);
      } else {
        return this.httpClient.get(`${environment.apiurl}/occupations`)
          .subscribe((res: any) => {
            this.listOccupations = res.data;
            resolve(this.listOccupations);
          });
      }
    });
  }
}
