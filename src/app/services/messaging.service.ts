import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  currentMessage = new BehaviorSubject(null);

  constructor(
    private angularFireMessaging: AngularFireMessaging
  ) {}

  // onReceiveMessage(event) {
  //   if (event.data != null) {
  //     const type = event.data.firebaseMessagingType;
  //     if (type === 'push-msg-received') {
  //       const firebaseMessagingData = event.data.firebaseMessagingData;
  //       const from = firebaseMessagingData.from;
  //       const payload = firebaseMessagingData.notification;
  //       if (from === environment.firebaseConfig.messagingSenderId) {
  //         // console.log( 'message received from firebase', payload );
  //       }
  //     }
  //   }
  // }

  requestPermission() {
    localStorage.removeItem('fcm');
    this.angularFireMessaging.requestToken
    .subscribe((token) => {
      console.log(token);
      localStorage.setItem('fcm', btoa(JSON.stringify(token)));
    }, (error) => {
      console.error(error);
    });
  }

  // receiveMessage() {
  //   this.angularFireMessaging.messaging.subscribe((payload) => {
  //     this.currentMessage.next(payload);
  //   });
  // }

  listen() {
    return this.angularFireMessaging.messages;
  }
}
