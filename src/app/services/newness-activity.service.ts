import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class NewnessActivityService {
  private listActivities: any;

  constructor(
    private httpClient: HttpClient
  ) {}

  getActivities() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.listActivities) {
        resolve(this.listActivities);
      } else {
        return this.httpClient.get(`${environment.apiurl}/newness/activity-types`)
          .subscribe((res: any) => {
            this.listActivities = res.data;
            this.listActivities.unshift( { id: '', name: 'Seleccionar' } );
            resolve(this.listActivities);
          });
      }
    });
  }

  getListActivities(page) {
    return this.httpClient.get(`${environment.apiurl}/newness/activity?page=${page}`);
  }

  getSearchPost(params: any) {
    return this.httpClient.post(`${environment.apiurl}/newness/activity/search`, params);
  }

  putSchedulePost(params: any) {
    return this.httpClient.put(`${environment.apiurl}/newness/activity/schedule`, params);
  }

  /******************/

  // getActivityId
  getPostId( id: number ) {
    return this.httpClient.get( `${environment.apiurl}/newness/activity/${id}` );
  }

// getSearchActivity


// putScheduleActivity

  deleteSchedule( id: number ) {
    return this.httpClient.delete( `${environment.apiurl}/newness/activity/${id}/schedule` );
  }

  // ACTIVITIES - COMMENT

  putSaveComment( params: any ) {
    return this.httpClient.put( `${environment.apiurl}/newness/activity/comment`, params );
  }

  deleteComment( id: number ) {
    return this.httpClient.delete( `${environment.apiurl}/newness/activity/comment/${id}` );
  }

// ACTIVITIES - REACTION

  putCreateReaction( params: any ) {
    return this.httpClient.put( `${environment.apiurl}/newness/activity/reaction`, params );
  }

  deleteReaction( id: number ) {
    return this.httpClient.delete( `${environment.apiurl}/newness/activity/reaction/${id}` );
  }

}
