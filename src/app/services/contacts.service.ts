import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class ContactsService {

  constructor( private http: HttpClient ) {}

  getContacts() {
    return this.http.get( `${environment.apiurl}/user/contacts` );
  }

  // CREATE OR EDIT USER PROFILE
  postContacts( params: any ) {
    return this.http.post( `${environment.apiurl}/user/contacts`, params );
  }
}
