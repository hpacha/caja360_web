import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';

import { environment } from '@environments/environment';
import { Notification, NotificationResponse } from '@interfaces/notification.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notificationSubject = new BehaviorSubject<NotificationResponse>(null);
  private notificationPendingSubject = new BehaviorSubject<boolean>(false);
  private notificationsResponse: NotificationResponse;
  currentMessage = new BehaviorSubject(null);

  constructor(
    private httpClient: HttpClient
  ) {}

  getNotification() {
    return this.notificationSubject.asObservable();
  }

  fetchNotification( parameters?: any ) {
    this.httpClient.get(`${environment.apiurl}/user/notifications`, {params: parameters}).subscribe((res: any) => {
      this.notificationsResponse = res.data;
      console.log(res.data);
      this.notificationSubject.next(res.data);
      this.checkPending();
    });
  }

  checkPending() {
    let pending = false;
    for (let i = 0; i < this.notificationsResponse.data.length; i++) {
      if (this.notificationsResponse.data[i].read_at === null) {
        pending = true;
        break;
      }
    }
    this.notificationPendingSubject.next(pending);
  }

  getPending() {
    return this.notificationPendingSubject.asObservable();
  }

  setNotification( notification: Notification ) {
    console.log(notification);
    if (this.notificationsResponse.data.length === 10) {
      this.notificationsResponse.data.pop();
    }
    this.notificationsResponse.data.unshift(notification);
    console.log(this.notificationsResponse);
    this.notificationSubject.next(this.notificationsResponse);
  }

  deleteNotification(id: number) {
    return this.httpClient.delete(`${environment.apiurl}/user/notifications/${id}`);
  }

  postSendViewed(id: number) {
    return this.httpClient.post(`${environment.apiurl}/user/notifications/${id}/viewed`, null);
  }
}
