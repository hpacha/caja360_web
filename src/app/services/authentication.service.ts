import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) {}

  postLogin(params: any) {
    return this.httpClient.post(`${environment.apiurl}/auth/login`, params).pipe(
      tap((res: any) => {
        console.log(res);
        localStorage.setItem('xser', btoa( JSON.stringify(res)));
      })
    );
  }

  postLogout(params: any) {
    return this.httpClient.post(`${environment.apiurl}/auth/logout`, params);
  }

  logout() {
    localStorage.removeItem('fcm');
    localStorage.removeItem('user_is_boss');
    localStorage.removeItem('user');
    localStorage.removeItem('xser');
    localStorage.removeItem('tk');
    localStorage.removeItem( 'cart' );
    localStorage.removeItem( 'cart-products' );
    localStorage.removeItem( 'cart-index' );
    window.location.reload();
    this.router.navigate(['/auth']);
  }

  getUser() {
    return localStorage.getItem('xser') ? JSON.parse(atob(localStorage.getItem('xser'))) : null;
  }

  getTk() {
    return localStorage.getItem('tk') ? JSON.parse(atob(localStorage.getItem('tk'))) : null;
  }

  getBoos() {
    return this.httpClient.get(`${environment.apiurl}/user/is-boss`);
  }
}
