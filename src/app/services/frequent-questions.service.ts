import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FrequentQuestionsService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getFrequentQuestion(page) {
    return this.httpClient.get( `${environment.apiurl}/frequent-question?page=${page}`);
  }
}
