import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {
  constructor(
    private httpClient: HttpClient
  ) {}

  getDocuments(page, parameters = null) {
    if (parameters)
      return this.httpClient.post(`${environment.apiurl}/files/search/`, parameters);
    else
      return this.httpClient.get(`${environment.apiurl}/files?page=${page}`);
  }
}
