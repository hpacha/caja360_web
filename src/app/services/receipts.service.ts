import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReceiptsService {
  private listReceipsDatesYears: any;

  constructor(
    private httpClient: HttpClient
  ) {}

  getReceipts(parameters = null) {
    console.log(Object.keys(parameters).length);
    if(Object.keys(parameters).length)
      return this.httpClient.get(`${environment.apiurl}/receipts/filter/${parameters.year}/${parameters.month}`);
    else
      return this.httpClient.get(`${environment.apiurl}/receipts`);
  }

  getReceiptsDates() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.listReceipsDatesYears) {
        resolve(this.listReceipsDatesYears);
      } else {
        return this.httpClient.get(`${environment.apiurl}/receipts/years`)
          .subscribe((res: any) => {
            this.listReceipsDatesYears = res.data;
            resolve(this.listReceipsDatesYears);
          });
      }
    });
  }

  getReceiptsDatesMonths(year) {
    return this.httpClient.get(`${environment.apiurl}/receipts/years/${year}`)
  }
}
