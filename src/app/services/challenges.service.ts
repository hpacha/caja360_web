import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable( {
  providedIn: 'root'
} )

export class ChallengesService {
  stepQuestion = new Subject<any>();
  eventComment = new Subject<any>();
  eventLiked = new Subject<any>();
  option = new Subject<any>();
  isCorrect = new Subject<any>();

  constructor( private http: HttpClient ) {}

  getChallenges() {
    return this.http.get( `${environment.apiurl}/challenges` );
  }

  getChallengesValues() {
    return this.http.get( `${environment.apiurl}/challenges/values` );
  }

  getExperiencesOwn( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/experiences/own`, { params: parameters } );
  }

  getExperiences( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/experiences`, { params: parameters } );
  }

  getExperienceId( id: number ) {
    return this.http.get( `${environment.apiurl}/challenges/experiences/show/${id}` );
  }

  // EXPERIENCE - COMMENT

  postSavePublicationExperience( params: any ) {
    return this.http.post( `${environment.apiurl}/challenges/experiences/store`, params );
  }

  postSaveCommentExperience( params: any ) {
    return this.http.post( `${environment.apiurl}/challenges/experiences/comments`, params );
  }

  putLikExperience( params: any ) {
    return this.http.put( `${environment.apiurl}/challenges/experiences/like`, params );
  }
  putDeleteCommentExp( params: any ) {
    return this.http.put( `${environment.apiurl}/challenges/destroy-comments`, params );
  }

  // LIST DISCUSSION
  getDiscussionType( id: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/discussion/${id}`, { params: parameters } );
  }

  putLikeDiscussion( params: any ) {
    return this.http.put( `${environment.apiurl}/challenges/discussion/like`, params );
  }

  getDiscussionId( id: number ) {
    return this.http.get( `${environment.apiurl}/challenges/discussion/show/${id}` );
  }

  postSaveCommentDiscussions( params: any ) {
    return this.http.post( `${environment.apiurl}/challenges/discussion/comments`, params );
  }

  // RECEIVE KNOWLEDGE
  getKnowledgeReceive( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/recognition`, { params: parameters } );
  }

  getKnowledgeGive( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/recognition/give`, { params: parameters } );
  }

  getKnowledgeId( id: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/recognition/show/${id}`, { params: parameters } );
  }

  getKnowledgeCategories( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/categorys`, { params: parameters } );
  }

  getKnowledgeEvents( parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/events`, { params: parameters } );
  }

  postSaveRecognition( params: any ) {
    return this.http.post( `${environment.apiurl}/challenges/recognition/store`, params );
  }

  // LISTADO DE FORO

  getForumsBox( id: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/event/${id}`, { params: parameters } );
  }

  // QUESTIONS
  getQuestions( id: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/discussion/show-question/${id}`, { params: parameters } );
  }

  postCheckQuestion( params: any ) {
    return this.http.post( `${environment.apiurl}/challenges/discussion/check-question`, params );
  }

  getFinalizeChallenge( id: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/discussion/finish-challenge/${id}`, { params: parameters } );
  }

  // CONSULT POINTS
  getPointsReceived( id: number, resourceId: number, parameters?: any ) {
    return this.http.get( `${environment.apiurl}/challenges/${id}/points-received?resource_id=${resourceId}`,
      { params: parameters } );
  }

  putNoModal( params: any ) {
    return this.http.put( `${environment.apiurl}/challenges/modal`, params );
  }
}
