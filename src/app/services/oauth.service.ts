import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { MsalService, BroadcastService } from '@azure/msal-angular';
import { OAuthSettings } from '../../oauth';

@Injectable( {
  providedIn: 'root'
})
export class OAuthService {
  public authenticated: boolean;

  constructor(
    private http: HttpClient,
    private msalService: MsalService,
  ) {
    this.authenticated = false;
  }

  async signIn(): Promise<void> {
    let result = await this.msalService.loginPopup(OAuthSettings)
      .catch((reason) => {
        console.log('Login failed', JSON.stringify(reason, null, 2));
      });

    if (result) {
      this.authenticated = true;
      console.log(result);
    }
  }

  signOut(): void {
    this.msalService.logout();
    this.authenticated = false;
  }

  async getAccessToken(): Promise<string> {
    let result = await this.msalService.acquireTokenSilent(OAuthSettings)
      .catch((reason) => {
        console.log('Get token failed', JSON.stringify(reason, null, 2));
      });
    if (result) {
      console.log('Token acquired', result.accessToken);
      return result.accessToken;
    }
    this.authenticated = false;
    return null;
  }
}
