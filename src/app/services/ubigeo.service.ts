import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
})
export class UbigeoService {
  private departments: any;

  constructor(
    private httpClient: HttpClient
  ) {}

  getDepartments() {
    return new Promise<any[]>((resolve, reject) => {
      if (this.departments) {
        resolve(this.departments);
      } else {
        return this.httpClient.get(`${environment.apiurl}/ubigeo/department`)
        .subscribe((res: any) => {
          this.departments = res.data;
          resolve(this.departments);
        });
      }
    });
  }

  getProvinces(id: number) {
    return this.httpClient.get(`${environment.apiurl}/ubigeo/department/${id}/province`);
  }

  getDistricts(id: number) {
    return this.httpClient.get(`${environment.apiurl}/ubigeo/province/${id}/district`);
  }
}
