import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )
export class UserService {
  private documentType: any;
  private maritalStatus: any;
  private professions: any;
  private academicDegrees: any;
  private userProfile: any;
  eventChangeImage: EventEmitter<any> = new EventEmitter();

  constructor(
    private httpClient: HttpClient
  ) {}

  getUserProfile() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.userProfile ) {
        resolve( this.userProfile );
      } else {
        return this.httpClient.get( `${environment.apiurl}/user/profile` )
        .subscribe( ( res: any ) => {
          this.userProfile = res.data;
          resolve( this.userProfile );
        } );
      }
    } );
  }

  updateUserProfileData( data: any ) {
    this.userProfile = data;
  }

  updateUserProfileDataContacts( data: any ) {
    this.userProfile.contacts = data;
  }

  getDocumenType() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.documentType ) {
        resolve( this.documentType );
      } else {
        return this.httpClient.get( `${environment.apiurl}/user/profile/document-types` )
        .subscribe( ( res: any ) => {
          this.documentType = res.data;
          resolve( this.documentType );
        } );
      }
    } );
  }

  getMaritalStatus() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.maritalStatus ) {
        resolve( this.maritalStatus );
      } else {
        return this.httpClient.get( `${environment.apiurl}/user/profile/marital-status` )
        .subscribe( ( res: any ) => {
          this.maritalStatus = res.data;
          resolve( this.maritalStatus );
        } );
      }
    } );
  }

  getAcademicDegrees() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.academicDegrees ) {
        resolve( this.academicDegrees );
      } else {
        return this.httpClient.get( `${environment.apiurl}/academic-degrees` )
        .subscribe( ( res: any ) => {
          this.academicDegrees = res.data;
          resolve( this.academicDegrees );
        } );
      }
    } );
  }

  getProfessions() {
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.professions ) {
        resolve( this.professions );
      } else {
        return this.httpClient.get( `${environment.apiurl}/professions` )
        .subscribe( ( res: any ) => {
          this.professions = res.data;
          resolve( this.professions );
        } );
      }
    } );
  }

  getPictureProfile( id: any ) {
    return this.httpClient.get( `${environment.apiurl}/storage/profile/${id}` );
  }

  getSearchUsers( parameters?: any , page = null) {
    if ( page ) {
      return this.httpClient.post( `${environment.chatUrl}/users/search?page=${page}`, parameters );
    }
    return this.httpClient.post( `${environment.apiurl}/users/search`, parameters );
  }

  getProfileUserById( id ) {
    return this.httpClient.get( `${environment.apiurl}/users/${id}` );
  }

  postFavoriteUsers( parameters: any ) {
    return this.httpClient.post( `${environment.apiurl}/user/favorites`, parameters );
  }
  postPanic( parameters: any ) {
    return this.httpClient.post( `${environment.apiurl}/panic/emergency`, parameters );
  }

  getListPanic( ) {
    return this.httpClient.get( `${environment.apiurl}/panic/emergency_types` );
  }

  delFavoriteUsers( userId: any ) {
    return this.httpClient.delete( `${environment.apiurl}/user/favorites/${userId}` );
  }

  /* UPDATE IMAGE */
  updateImage( data: any ) {
    this.eventChangeImage.emit( data );
  }


  // DOCUMENT TYPE


  // PROFESSIONS

  // PROFESSIONS


  // CREATE OR EDIT USER PROFILE
  postUserProfile( params: any ) {
    return this.httpClient.post( `${environment.apiurl}/user/profile/personal-data`, params );
  }

  // EDIT INFORMATION ACADEMIC
  postInfoAcademic( params: any ) {
    return this.httpClient.post( `${environment.apiurl}/user/profile/academic-data`, params );
  }

  // EDIT INFORMATION PHYSICAL DATA
  postPhysicalData( params: any ) {
    return this.httpClient.post( `${environment.apiurl}/user/profile/physical-data`, params );
  }

  //  Business-card-share
  postBusinessCardShare( params: any ) {
    return this.httpClient.post( `${environment.apiurl}/user/qr/business-card-share`, params );
  }

  // UPDATE PROFILE PICTURE
  postProfilePicture( params ) {
    return this.httpClient.post( `${environment.apiurl}/user/profile/picture`, params );
  }

  // QR
  getQR() {
    return this.httpClient.get( `${environment.apiurl}/user/qr/business-card` );
  }


}
