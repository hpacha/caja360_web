import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class NewnessNewsService {
  constructor(
    private http: HttpClient
  ) {}

  getListNews(page) {
    return this.http.get(`${environment.apiurl}/newness/news?page=${page}`);
  }

  getSearchPost(params: any) {
    return this.http.post(`${environment.apiurl}/newness/news/search`, params);
  }

  /******************/

  // getNewId
  getPostId( id: number ) {
    return this.http.get( `${environment.apiurl}/newness/news/${id}` );
  }

  // NEWS - COMMENT

  putSaveComment( params: any ) {
    return this.http.put( `${environment.apiurl}/newness/news/comment`, params );
  }

  deleteComment( id: number ) {
    return this.http.delete( `${environment.apiurl}/newness/news/comment/${id}` );
  }

// NEWS - REACTION

  putCreateReaction( params: any ) {
    return this.http.put( `${environment.apiurl}/newness/news/reaction`, params );
  }

  deleteReaction( id: number ) {
    return this.http.delete( `${environment.apiurl}/newness/news/reaction/${id}` );
  }

}
