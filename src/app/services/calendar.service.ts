import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable( {
  providedIn: 'root'
} )
export class CalendarService {
  private settingsSubj = new BehaviorSubject<any[]>( [] );
  private settings = [];

  constructor(
    private httpClient: HttpClient
  ) {
    this.getSettings();
  }

  // PRODUCTS LINES

  getSettingsFilter() {
    if ( this.settings && sessionStorage.getItem( 'settings' ) ) {
      this.settings = JSON.parse( sessionStorage.getItem( 'settings' ) );
      this.settingsSubj.next( this.settings );
    }
    const url = `${environment.apiurl}/settings`;
    return this.httpClient.get( url ).subscribe( ( res: any ) => {
      sessionStorage.setItem( 'settings', JSON.stringify( res.data ) );
      this.settingsSubj.next( res.data );
      this.settings = res.data;
    } );
  }

  getSettings() {
    this.getSettingsFilter();
    return this.settingsSubj.asObservable();
  }


  getItems( parameters ) {
    return this.httpClient.get( `${environment.apiurl}/calendar`, { params: parameters } );
  }

  setSynchronize( tokenBearer ) {
    const customHeaders = new HttpHeaders( {
      'Authorization': tokenBearer,
      'Access-Control-Allow-Origin': 'true'
    } );
    return this.httpClient.get( `https://caja-arequipa-rrhh.pappstest.com/signin`, { observe: 'response', headers: customHeaders } );
  }

  getBoss() {
    return this.httpClient.get( `${environment.apiurl}/vacation/boss` );
  }

  /*  getSettings() {
   return this.httpClient.get( `${environment.apiurl}/settings` ).subscribe( ( res: any ) => {
   localStorage.setItem( 'calendar-settings', JSON.stringify( res ) );
   } );
   }*/

  getHistorial( page, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.get( `${environment.apiurl}/telecommuting?page=${page}` );
    }
    return this.httpClient.get( `${environment.apiurl}/vacation?page=${page}` );
  }

  getSlopesCount( entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.get( `${environment.apiurl}/telecommuting/slopes/count` );
    }
    return this.httpClient.get( `${environment.apiurl}/vacation/slopes/count` );
  }

  getSlopes( page, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.get( `${environment.apiurl}/telecommuting/slopes?page=${page}` );
    }
    return this.httpClient.get( `${environment.apiurl}/vacation/slopes?page=${page}` );
  }

  sendApply( params, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.post( `${environment.apiurl}/telecommuting/apply`, params );
    }
    return this.httpClient.post( `${environment.apiurl}/vacation/apply`, params );
  }

  approveApply( params, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.put( `${environment.apiurl}/telecommuting/approve`, params );
    }
    return this.httpClient.put( `${environment.apiurl}/vacation/approve`, params );
  }

  rejectApply( params, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.put( `${environment.apiurl}/telecommuting/reject`, params );
    }
    return this.httpClient.put( `${environment.apiurl}/vacation/reject`, params );
  }

  updateApply( params, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.put( `${environment.apiurl}/telecommuting/update`, params );
    }
    return this.httpClient.put( `${environment.apiurl}/vacation/update`, params );
  }

  deleteApply( params, entity: string ) {
    if ( entity === 'telecommuting' ) {
      return this.httpClient.put( `${environment.apiurl}/telecommuting/destroy`, params );
    }
    return this.httpClient.put( `${environment.apiurl}/vacation/destroy`, params );
  }


  // HYBRID WORK

  getItemsHybridWork( parameters?: any ) {
    return this.httpClient.get( `${environment.apiurl}/services/appointment/get-consult`, { params: parameters } );
  }
  postShifting( params ) {
    return this.httpClient.post( `${environment.apiurl}/services/appointment/request-shifting`, params );
  }

  deleteItem( id, params? ) {
    return this.httpClient.delete( `${environment.apiurl}/services/appointment/delete/${id}`, params );
  }
}
