import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable( {
  providedIn: 'root'
} )

export class MeetingService {
  constructor(private http: HttpClient) {  }

  getMeeting(id) {
    return this.http.get( `${environment.apiurl}/meetings/${id}`);
  }

  putMeeting(parameters) {
    return this.http.put( `${environment.apiurl}/meetings`, parameters);
  }

  deleteMeeting(id) {
    return this.http.delete( `${environment.apiurl}/meetings/${id}`);
  }

  updateMeeting(id, parameters) {
    return this.http.patch( `${environment.apiurl}/meetings/${id}`, parameters);
  }
}
