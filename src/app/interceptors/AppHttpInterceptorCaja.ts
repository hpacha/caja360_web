import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AppHttpInterceptorCaja implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const xser = this.authenticationService.getUser();
    const tk = this.authenticationService.getTk();
    if ( xser ) {
      req = req.clone({
        setHeaders: {
          Authorization:  'Bearer ' +  xser.data.access_token,
        },
      });
    }
    return next.handle(req);
  }
}
