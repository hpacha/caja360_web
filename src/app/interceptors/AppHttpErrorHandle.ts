import { Injectable, ErrorHandler } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AppHttpErrorHandle implements ErrorHandler {
    static readonly REFRESH_PAGE_ON_TOAST_CLICK_MESSAGE: string =
        'An error occurred: Please click this message to refresh';
    static readonly DEFAULT_ERROR_TITLE: string = 'Something went wrong';
    constructor(private router: Router) {}
    handleError(error: any) {
        // console.error(error);
    }
}
