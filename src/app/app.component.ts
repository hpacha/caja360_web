import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BnNgIdleService } from 'bn-ng-idle';
import { environment } from '../environments/environment';

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html'
} )
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private bnIdle: BnNgIdleService,
    private router: Router
  ) {
    this.bnIdle.startWatching( 18000 ).subscribe( ( res ) => {
      if ( res ) {
        localStorage.removeItem( 'xser' );
        localStorage.removeItem( 'tk' );
        localStorage.removeItem( 'cart' );
        localStorage.removeItem( 'cart-products' );
        localStorage.removeItem( 'cart-index' );
        this.router.navigate( ['/auth'] );
      }
    } ).unsubscribe();
  }

  ngOnInit() {
    this.setGooglePlacesApi();
  }

  @HostListener( 'window:beforeunload' )
  ngOnDestroy() {

  }

  setGooglePlacesApi() {
    const script = document.createElement( 'script' );
    script.type = 'text/javascript';
    script.src = `https://maps.googleapis.com/maps/api/js?key=${environment.google_places_api_key}&libraries=places&language=es&sessiontoken=${this.uuidv4()}`;
    script.async = true;
    document.head.appendChild( script );
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace( /[xy]/g, ( c ) => {
      const r = Math.random() * 16 | 0;
      const v = c === 'x' ? r : ( r & 0x3 | 0x8 );
      return v.toString( 16 );
    } );
  }
}
