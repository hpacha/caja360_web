import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BenefitsComponent } from '@components/dashboard/benefits/benefits.component';
import { DetailComponent } from '@components/dashboard/benefits/detail/detail.component';

const routes: Routes = [
  {path: '', component: BenefitsComponent},
  {path: ':id/:name', component: DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitsRoutingModule {}
