import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';

import { BenefitsService } from '@services/benefits.service';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss'],
  providers: [SlugifyPipe]
})
export class BenefitsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  listBenefits: any;
  preload: boolean;
  defaultImage: string;

  constructor(
    private router: Router,
    private benefitsService: BenefitsService,
    private slugifyPipe: SlugifyPipe
  ) {}

  async ngOnInit() {
    setTimeout(() => { this.globals.setLoadingVisible(true); });
    this.defaultImage = './assets/img/placeholder-h.png';
    await this.getLisBenefits();
    setTimeout(() => { this.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getLisBenefits() {
    return new Promise((resolve, reject) => {
      this.preload = true;
      this.benefitsService.getBenefits()
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => {
            this.preload = false;
            resolve();
          }),
        ).subscribe((res: any) => {
          this.listBenefits = res.data;
        }, err => {
          this.listBenefits = [];
        });
    });
  }

  onClickDetail(item) {
    this.router.navigate([`beneficios/${item.id}/${this.slugifyPipe.transform(item.name)}`]);
  }
}
