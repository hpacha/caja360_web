import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-compensation',
  templateUrl: './compensation.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class CompensationComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
    console.log(this.benefit);
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
  }
}
