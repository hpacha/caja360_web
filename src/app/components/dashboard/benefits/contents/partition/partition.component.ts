import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-partition',
  templateUrl: './partition.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class PartitionComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
  }
}
