import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class TaxComponent implements OnInit {
  @Input() benefit: any;

  constructor() { }

  ngOnInit(): void {
  }
}
