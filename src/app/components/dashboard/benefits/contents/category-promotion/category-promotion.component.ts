import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-category-promotion',
  templateUrl: './category-promotion.component.html',
  styleUrls: ['./category-promotion.component.scss']
})
export class CategoryPromotionComponent implements OnInit {

  @Input() benefit: any;
  isOpen = true;
  isOpen2 = false;
  isOpen3 = false;
  isOpen4 = false;
  isOpen5 = false;
  constructor() { }

  ngOnInit(): void {
    console.log(this.benefit);
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
    if (type === 2) {
      this.isOpen2 = event === true;
    }
    if (type === 3) {
      this.isOpen3 = event === true;
    }
    if (type === 4) {
      this.isOpen4 = event === true;
    }
    if (type === 5) {
      this.isOpen5 = event === true;
    }
  }

}
