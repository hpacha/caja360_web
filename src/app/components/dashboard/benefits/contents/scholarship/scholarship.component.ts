import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scholarship',
  templateUrl: './scholarship.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class ScholarshipComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;
  constructor() { }

  ngOnInit(): void {
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
  }
}
