import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gratification',
  templateUrl: './gratification.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class GratificationComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;
  isOpen2 = false;
  isOpen3 = false;
  isOpen4 = false;
  constructor() { }

  ngOnInit(): void {
    console.log(this.benefit);
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
    if (type === 2) {
      this.isOpen2 = event === true;
    }
    if (type === 3) {
      this.isOpen3 = event === true;
    }
    if (type === 4) {
      this.isOpen4 = event === true;
    }
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }
}
