import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bond',
  templateUrl: './bond.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class BondComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;

  constructor() {}

  ngOnInit(): void {
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    }
  }
}
