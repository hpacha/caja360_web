import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['../../detail/detail.component.scss']
})
export class AgreementComponent implements OnInit {
  @Input() benefit: any;
  isOpen = true;
  isOpen2 = true;
  isOpen3 = true;

  constructor() {}

  ngOnInit(): void {
  }

  log(event: boolean, type: number) {
    if (type === 1) {
      this.isOpen = event === true;
    } else if (type === 2) {
      this.isOpen2 = event === true;
    } else {
      this.isOpen3 = event === true;
    }
  }
}
