import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { SpinnerModule } from '@shared/spinner/spinner.module';
import { BenefitsRoutingModule } from './benefits-routing.module';
import { BenefitsComponent } from './benefits.component';
import { DetailComponent } from './detail/detail.component';
import { AgreementComponent } from './contents/agreement/agreement.component';
import { BondComponent } from './contents/bond/bond.component';
import { CanastaComponent } from './contents/canasta/canasta.component';
import { CompensationComponent } from './contents/compensation/compensation.component';
import { CtsComponent } from './contents/cts/cts.component';
import { GratificationComponent } from './contents/gratification/gratification.component';
import { LifeComponent } from './contents/life/life.component';
import { PartitionComponent } from './contents/partition/partition.component';
import { ProcessComponent } from './contents/process/process.component';
import { OtherComponent } from './contents/other/other.component';
import { ScholarshipComponent } from './contents/scholarship/scholarship.component';
import { TaxComponent } from './contents/tax/tax.component';
import { UniformComponent } from './contents/uniform/uniform.component';
import { CategoryPromotionComponent } from './contents/category-promotion/category-promotion.component';

@NgModule({
  declarations: [
    AgreementComponent,
    BenefitsComponent,
    BondComponent,
    CanastaComponent,
    CompensationComponent,
    CtsComponent,
    DetailComponent,
    GratificationComponent,
    LifeComponent,
    PartitionComponent,
    ProcessComponent,
    OtherComponent,
    ScholarshipComponent,
    TaxComponent,
    UniformComponent,
    CategoryPromotionComponent
  ],
  imports: [
    AccordionModule.forRoot(),
    BenefitsRoutingModule,
    CommonModule,
    LazyLoadImageModule,
    PerfectScrollbarModule,
    SpinnerModule,
    TabsModule.forRoot(),
  ]
})
export class BenefitsModule {}
