import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { BenefitsService } from '@services/benefits.service';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  benefits: any;
  defaultImage: string;
  @ViewChild('fileInputBenefit') fileInputBenefit: ElementRef;

  constructor(
    private benefitsService: BenefitsService,
    private activatedRoute: ActivatedRoute,
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    this.defaultImage = './assets/img/placeholder-h.png';
    if (this.activatedRoute.snapshot.params['id']) {
      await this.getBenefitsId(this.activatedRoute.snapshot.params['id']);
    }
    setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getBenefitsId(id) {
    const thisIn = this;
    return new Promise((resolve, reject) => {
      thisIn.benefitsService.getBenefitsId(id)
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => {
          resolve();
        }),
      ).subscribe((res:any) => {
        thisIn.benefits = res.data;
        console.log(this.benefits);
      }, error => {
        thisIn.benefits = [];
      });
    });
  }

  onFileUpload(fileInput: any) {
    const thisIn = this;
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    if (fileInput.target.files && fileInput.target.files[0]) {
      const myReader = new FileReader();
      myReader.onloadend = (e) => {
        let file = <string>myReader.result;
        const formData = new FormData();
        formData.append("benefit_id", this.benefits.id);
        formData.append("file", thisIn.fileInputBenefit.nativeElement.files[0]);
        formData.append("action", "upload");
        (<HTMLInputElement>document.getElementById('uploadFile')).value = "";
        thisIn.benefitsService.postStoreBenefitsFile(formData)
        .pipe(takeUntil(thisIn.destroyed$),finalize(() => {setTimeout(() => { thisIn.globals.setLoadingVisible(false);});})).subscribe((res: any) => {
          thisIn.benefits = res.data;
          Swal.fire({ title: 'Adjuntar documento', text: 'Tu documento se envió exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Adjuntar documento', text: 'Ocurrió un error al enviar el documento', icon: 'error', iconHtml: null, timer: 4000 })
        });
      };
      myReader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  onDeleteFile() {
    const thisIn = this;
    const parameters: any = {}
    parameters.action = "document";
    Swal.fire({
      title: 'Eliminar documento',
      text: '¿Estás seguro que deseas eliminar el documento?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
        thisIn.benefitsService.delStoreBenefitsFile(thisIn.benefits.id, parameters)
        .pipe(takeUntil(thisIn.destroyed$),finalize(() => {setTimeout(() => { thisIn.globals.setLoadingVisible(false);});})).subscribe((res: any) => {
          thisIn.benefits = res.data;
          Swal.fire({ title: 'Eliminar documento', text: 'Tu documento se eliminó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Eliminar documento', text: 'Ocurrió un error al eliminar el documento', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }

  applyBenefit() {
    const thisIn = this;
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    const params: any = {};
    params.action = 'solicit';
    params.benefit_id = this.benefits.id;
    this.benefitsService.postStoreBenefitsFile(params)
    .pipe(takeUntil(thisIn.destroyed$),finalize(() => {setTimeout(() => { thisIn.globals.setLoadingVisible(false);});})).subscribe((res: any) => {
      this.benefits = res.data;
      Swal.fire({ title: 'Solicitar beneficio', text: 'Tu solicitud se envió exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
    }, error => {
      Swal.fire({ title: 'Solicitar beneficio', text: 'Ocurrió un error al enviar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
    });
  }

  cancelBenefit() {
    const thisIn = this;
    const parameters: any = {}
    parameters.action = 'solicitude';
    Swal.fire({
      title: 'Cancelar solicitud',
      text: '¿Estás seguro que deseas eliminar la solicitud?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
        thisIn.benefitsService.delStoreBenefitsFile(thisIn.benefits.id, parameters)
        .pipe(takeUntil(thisIn.destroyed$),finalize(() => {setTimeout(() => { thisIn.globals.setLoadingVisible(false);});})).subscribe((res: any) => {
          thisIn.benefits = res.data;
          Swal.fire({ title: 'Cancelar solicitud', text: 'Tu solicitud se canceló exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Cancelar solicitud', text: 'Ocurrió un error al cancelar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }
}
