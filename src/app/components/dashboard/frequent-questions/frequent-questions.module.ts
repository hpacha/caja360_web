import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrequentQuestionsRoutingModule } from './frequent-questions-routing.module';
import { FrequentQuestionsComponent } from './frequent-questions.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SpinnerModule } from '../../../shared/spinner/spinner.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SharedModule } from '../../../shared/shared.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';


@NgModule({
  declarations: [FrequentQuestionsComponent],
  imports: [
    CommonModule,
    FrequentQuestionsRoutingModule,
    PerfectScrollbarModule,
    SpinnerModule,
    PaginationModule.forRoot(),
    AccordionModule.forRoot(),
    SharedModule
  ]
})
export class FrequentQuestionsModule { }
