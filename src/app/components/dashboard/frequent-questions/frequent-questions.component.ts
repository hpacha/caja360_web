import { Component, OnDestroy, OnInit, ViewChild, NgZone } from '@angular/core';

import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import 'rxjs/add/operator/finally';

import { FrequentQuestionsService } from '@services/frequent-questions.service';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-frequent-questions',
  templateUrl: './frequent-questions.component.html',
  styleUrls: ['./frequent-questions.component.scss']
})
export class FrequentQuestionsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  frequentsQuestions: any;
  preload: boolean;
  preloadPage: boolean;
  currentPage = 1;
  lastPage = 0;
  question: string;
  answer: string;
  @ViewChild('questionPS', {static: false}) chatPS: PerfectScrollbarComponent;

  constructor(
    private zone: NgZone,
    private frequentQuestionsService: FrequentQuestionsService
  ) {}

  async ngOnInit() {
    setTimeout(() => { this.globals.setLoadingVisible(true); });
    await this.getFrequentQuestions();
    setTimeout(() => { this.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onClickDetail(item: any) {
    const thisIn = this;
    thisIn.question = item.question;
    thisIn.answer = item.answer;
    setTimeout(function(){
      thisIn.chatPS.directiveRef.scrollToTop(0, 0);
      thisIn.chatPS.directiveRef.update();
    });
  }

  getFrequentQuestions() {
    const thisIn = this;
    return new Promise((resolve, reject) => {
      if(thisIn.currentPage === 1) thisIn.preload = true; else thisIn.preloadPage = true;
      thisIn.frequentQuestionsService.getFrequentQuestion(thisIn.currentPage).pipe(
        takeUntil(this.destroyed$),
        finalize(() => {
          if(thisIn.currentPage === 1) thisIn.preload = false; else thisIn.preloadPage = false;
          resolve();
        }),
      ).subscribe((res: any) => {
        thisIn.frequentsQuestions = (thisIn.frequentsQuestions) ? (thisIn.frequentsQuestions).concat(res.data.data) : res.data.data;
        thisIn.lastPage = res.data.last_page;
      }, error => {
        thisIn.frequentsQuestions = [];
        thisIn.lastPage = 0;
      });
    });
  }

  async onScrolledDownEnd() {
    console.log('onScrolledDownEnd');
    this.zone.run(async () => {
      this.currentPage = this.currentPage + 1;
      if(this.currentPage <= this.lastPage)
        await this.getFrequentQuestions();
    });
  }
}
