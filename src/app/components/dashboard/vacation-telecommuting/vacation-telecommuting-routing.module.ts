import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacationTelecommutingComponent } from '@components/dashboard/vacation-telecommuting/vacation-telecommuting.component';
import { VacationComponent } from '@components/dashboard/vacation-telecommuting/vacation/vacation.component';
import { TelecommutingComponent } from '@components/dashboard/vacation-telecommuting/telecommuting/telecommuting.component';


const routes: Routes = [
  // {
  //   path: '',
  //   component: VacationTelecommutingComponent
  // },
  // {
  //   path: 'vacaciones',
  //   component: VacationComponent
  // },
  // {
  //   path: 'teletrabajo',
  //   component: TelecommutingComponent
  // }
  {
    path: '',
    component: VacationTelecommutingComponent,
    children: [
     { path: 'vacaciones', component: VacationComponent },
     { path: 'teletrabajo', component: TelecommutingComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacationTelecommutingRoutingModule { }
