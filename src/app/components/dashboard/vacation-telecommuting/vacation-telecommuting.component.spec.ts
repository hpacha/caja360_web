import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationTelecommutingComponent } from './vacation-telecommuting.component';

describe('VacationTelecommutingComponent', () => {
  let component: VacationTelecommutingComponent;
  let fixture: ComponentFixture<VacationTelecommutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacationTelecommutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacationTelecommutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
