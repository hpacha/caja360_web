import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelecommutingComponent } from './telecommuting.component';

describe('TelecommutingComponent', () => {
  let component: TelecommutingComponent;
  let fixture: ComponentFixture<TelecommutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelecommutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelecommutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
