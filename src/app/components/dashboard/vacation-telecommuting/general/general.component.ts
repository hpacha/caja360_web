import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatCalendar, MatCalendarCellCssClasses } from '@angular/material/datepicker';

import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import 'moment/locale/es';
import Swal from 'sweetalert2';

import { CalendarService } from '@services/calendar.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
} )
export class GeneralComponent implements OnInit {
  tillMonth = new Date( new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() ); // moment().format();
  tillWeek = new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 7 ); // moment().format();
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  isActive: boolean = false;
  preload: boolean;
  currentOption: string;
  listItems: any;
  listItemsFilter: any;
  listItemsTotal: 0;
  listDatesToHighlight: [];
  listMonths = moment.localeData( 'es' ).months();
  selectedDate = new Date( new Date() );
  selectedDateFrom: any;
  selectedDateTo: any;
  selectedDates: Array<any> = [];
  selectedDateView: any;
  selectedDateStatus: string = 'pending';
  childComponent: any;
  childComponentItem: any;

  CALENDAR_ENABLED_RANGE: any;
  CALENDAR_SELECTION_TYPE: any;
  settings: any;
  @ViewChild( MatCalendar ) calendar: MatCalendar<Date>;

  constructor(
    private router: Router,
    private renderer: Renderer2,
    private calendarService: CalendarService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
    if ( thisIn.router.url.split( '/' ).length === 3 ) {
      thisIn.isActive = true;
      thisIn.currentOption = this.router.url.split( '/' )[ 2 ] === 'vacaciones' ? 'vacation' : 'telecommuting';
    }
    setTimeout( function() {
      thisIn.onCalendarView();
    }, 0 );
    setTimeout( () => { thisIn.globals.setLoadingVisible( false ); } );
    this.getSettings();
    console.log( this.tillMonth );
    console.log( this.tillWeek );
  }

  setCurrentOption( option ) {
    this.isActive = true;
    this.currentOption = option;
    this.onStartSelectApply();
  }

  onCalendarView() {
    const thisIn = this;
    thisIn.calendar.updateTodaysDate();
    thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    const buttonsPrev = document.querySelector( '.mat-calendar-previous-button' );
    const buttonsNext = document.querySelector( '.mat-calendar-next-button' );
    thisIn.renderer.listen( buttonsPrev, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
    thisIn.renderer.listen( buttonsNext, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
  }

  onSelectedChange( event ) {
    console.log( event );
    event = event.value;
    const thisIn = this;
    thisIn.selectedDate = event;
    let todayDate = new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() );
    if ( !( new Date( thisIn.selectedDate.getFullYear(), thisIn.selectedDate.getMonth(), thisIn.selectedDate.getDate() ) >= todayDate ) ) {
      return false;
    }
    if ( thisIn.currentOption === 'vacation' ) {
      thisIn.logicSelectionVacations( event );
    } else if ( thisIn.currentOption === 'telecommuting' ) {
      thisIn.logicSelectionTelecommuting( event );
    }
    setTimeout( function() {
      thisIn.calendar.updateTodaysDate();
      thisIn.dateClass();
    }, 0 );
  }

  logicSelectionVacations( event ) {
    const thisIn = this;
    if ( !thisIn.selectedDateFrom ) {
      thisIn.selectedDateFrom = event;
    } else if ( thisIn.selectedDateFrom > event.getTime() || thisIn.selectedDateTo ) {
      thisIn.selectedDateFrom = event;
      thisIn.selectedDateTo = null;
    } else {
      thisIn.selectedDateTo = event;
    }
    if ( thisIn.selectedDateFrom && thisIn.selectedDateTo ) {
      thisIn.childComponent.scrollMin = false;
    }
  }

  logicSelectionTelecommuting( event ) {
    console.log( event );
    const thisIn = this;
    let dateTempmoment = moment( event ).format( 'YYYY-MM-DD' );
    const index = thisIn.selectedDates && thisIn.selectedDates.findIndex( date => date === dateTempmoment );
    console.log( index );
    if ( this.CALENDAR_SELECTION_TYPE === 'unique' ) {
      thisIn.selectedDates = [];
      thisIn.selectedDates = [dateTempmoment];
    } else {
      if ( index > -1 ) {
        thisIn.selectedDates.splice( index, 1 );
      } else {
        thisIn.selectedDates.push( dateTempmoment );
      }
    }
    /*if ( index > -1 ) {
      thisIn.selectedDates.splice( index, 1 );
    } else {
      thisIn.selectedDates.push( dateTempmoment );
    }*/
    console.log( this.selectedDates );
  }

  onMonthSelected( event ) {
    console.log( event );
    this.selectedDateView = this.listMonths[ event.getMonth() ] + ' ' + event.getFullYear();
    console.log( this.selectedDateView );
  }

  myDateFilter = ( d: Date ): boolean => {
    const day = d.getDay();
    return day !== -1;
  };

  dateClass() {
    const thisIn = this;
    if ( thisIn.currentOption === 'vacation' ) {
      return ( date: Date ): MatCalendarCellCssClasses => {
        let dateFrom = this.selectedDateFrom ? new Date( this.selectedDateFrom.getFullYear(), this.selectedDateFrom.getMonth(), this.selectedDateFrom.getDate() ) : null;
        let dateTo = this.selectedDateTo ? new Date( this.selectedDateTo.getFullYear(), this.selectedDateTo.getMonth(), this.selectedDateTo.getDate() ) : null;
        let currentDate = new Date( date.getFullYear(), date.getMonth(), date.getDate() );
        let todayDate = new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() );
        if ( thisIn.childComponentItem ) {
          return ( currentDate < dateFrom || currentDate > dateTo ) ? 'pass-date' : ( +currentDate === +dateFrom || +currentDate === +dateTo ) ? `pivot-date ${thisIn.selectedDateStatus}` : ( currentDate < dateFrom || currentDate > dateTo ) ? '' : ( currentDate > dateTo ) ? '' : `internal-date ${thisIn.selectedDateStatus}`;
        } else if ( dateFrom || dateTo ) {
          return ( currentDate < todayDate ) ? 'pass-date' : ( +currentDate === +dateFrom || +currentDate === +dateTo ) ? `pivot-date ${thisIn.selectedDateStatus}` : ( currentDate < dateFrom || currentDate > dateTo ) ? '' : ( currentDate > dateTo ) ? '' : `internal-date ${thisIn.selectedDateStatus}`;
        }
        return ( currentDate < todayDate ) ? 'pass-date' : ( currentDate > todayDate ) ? 'future-date' : 'current-date';
      };
    } else if ( thisIn.currentOption === 'telecommuting' ) {
      return ( date: Date ): MatCalendarCellCssClasses => {
        const highlightDate = thisIn.selectedDates && thisIn.selectedDates
        .map( strDate => new Date( strDate + 'T00:00:00' ) )
        .some( d => d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear() );
        let currentDate = new Date( date.getFullYear(), date.getMonth(), date.getDate() );
        let todayDate = new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() );
        // console.log(highlightDate + ' - ' + currentDate + ' - ' +  todayDate);
        if ( highlightDate ) {
          console.log( 'entra' );
          return highlightDate ? `pivot-date ${thisIn.selectedDateStatus}` : 'pass-date';
        } else if ( this.childComponentItem ) {
          return 'pass-date';
        }
        // console.log(todayDate + '-' + this.tillMonth);
        if ( this.CALENDAR_ENABLED_RANGE && this.CALENDAR_ENABLED_RANGE === 'month' ) {
          // console.log(todayDate >= this.tillMonth);
          return ( ( currentDate < todayDate ) || ( currentDate > this.tillMonth ) ) ? 'pass-date' : ( currentDate > todayDate ) ? 'future-date' : 'current-date';
        } else {
          return ( ( currentDate < todayDate ) || ( currentDate > this.tillWeek ) ) ? 'pass-date' : ( currentDate > todayDate ) ? 'future-date' : 'current-date';
        }
        // return ( currentDate < todayDate ) ? 'pass-date' : ( currentDate > todayDate ) ? 'future-date' : 'current-date';
      };
    } else {
      return ( date: Date ): MatCalendarCellCssClasses => {
        let currentDate = new Date( date.getFullYear(), date.getMonth(), date.getDate() );
        let todayDate = new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() );
        return ( currentDate < todayDate ) ? 'pass-date' : ( currentDate > todayDate ) ? 'future-date' : 'current-date';
      };
    }
  }

  onActivate( componentReference ) {
    this.childComponent = componentReference;
    componentReference.eventEmitter.subscribe( ( data ) => {
      if ( data.function === 'onViewCalendar' ) {
        this.onViewCalendar( data.item );
      } else if ( data.function === 'onViewTelecommuting' ) {
        this.onViewTelecommuting( data.item );
      } else if ( data.function === 'onStartSelectApply' ) {
        this.onStartSelectApply();
      }
    } );
  }

  onViewTelecommuting( item: any ) {
    const thisIn = this;
    thisIn.childComponentItem = item;
    this.selectedDates = item.dates.map( ( item ) => {
      return item.date;
    } );
    thisIn.selectedDateStatus = item.status_type;
    setTimeout( function() {
      thisIn.calendar.updateTodaysDate();
      thisIn.dateClass();
      thisIn.calendar._goToDateInView( new Date( item.start + 'T00:00:00' ), 'month' );
      thisIn.selectedDateView = thisIn.listMonths[ new Date( item.start + 'T00:00:00' ).getMonth() ] + ' ' + new Date( item.start + 'T00:00:00' ).getFullYear();
    }, 0 );
  }

  onViewCalendar( item: any ) {
    const thisIn = this;
    thisIn.childComponentItem = item;
    thisIn.selectedDateFrom = new Date( item.start + 'T00:00:00' );
    thisIn.selectedDateTo = new Date( item.end + 'T00:00:00' );
    thisIn.selectedDateStatus = item.status_type;
    setTimeout( function() {
      thisIn.calendar.updateTodaysDate();
      thisIn.dateClass();
      thisIn.calendar._goToDateInView( new Date( item.start + 'T00:00:00' ), 'month' );
      thisIn.selectedDateView = thisIn.listMonths[ new Date( item.start + 'T00:00:00' ).getMonth() ] + ' ' + new Date( item.start + 'T00:00:00' ).getFullYear();
    }, 0 );
  }

  onStartSelectApply() {
    const thisIn = this;
    if ( thisIn.childComponent ) {
      thisIn.childComponent.scrollMin = false;
    }
    thisIn.childComponentItem = null;
    thisIn.selectedDateFrom = null;
    thisIn.selectedDateTo = null;
    thisIn.selectedDates = [];
    thisIn.selectedDateStatus = 'pending';
    setTimeout( function() {
      thisIn.calendar.updateTodaysDate();
      thisIn.dateClass();
      thisIn.calendar._goToDateInView( new Date(), 'month' );
      thisIn.selectedDateView = thisIn.listMonths[ new Date().getMonth() ] + ' ' + new Date().getFullYear();
    }, 0 );
  }

  onApplyVacation() {
    const thisIn = this;
    if ( thisIn.childComponentItem && thisIn.childComponentItem.status === 3 ) {
      thisIn.onUpdateApply();
    } else if ( thisIn.childComponentItem ) {
      thisIn.onStartSelectApply();
    } else {
      if ( thisIn.selectedDateFrom && thisIn.selectedDateTo ) {
        setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
        const params: any = {};
        params.start = moment.utc( thisIn.selectedDateFrom ).format( 'YYYY-MM-DD' );
        params.end = moment.utc( thisIn.selectedDateTo ).format( 'YYYY-MM-DD' );
        thisIn.calendarService.sendApply( params, 'vacation' )
        .pipe( takeUntil( thisIn.destroyed$ ), finalize( () => {setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );} ) ).subscribe( ( res: any ) => {
          this.childComponent.receiveAfterApply( res.data );
          Swal.fire( {
            title: 'Solicitar Vacaciones', text: 'Tu solicitud se envió exitosamente', icon: 'success', iconHtml: null, timer: 4000
          } );
        }, error => {
          Swal.fire( {
            title: 'Solicitar Vacaciones', html: `Ocurrió un error al enviar la solicitud<br/>${error.error.message}`, icon: 'error',
            iconHtml: null, timer: 4000
          } );
        } );
      }
    }
  }

  onApplyTelecommuting() {
    const thisIn = this;
    if ( thisIn.childComponentItem && thisIn.childComponentItem.status === 3 ) {
      thisIn.onUpdateApply();
    } else if ( thisIn.childComponentItem ) {
      thisIn.onStartSelectApply();
    } else {
      if ( thisIn.selectedDates ) {
        setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
        const params: any = {};
        params.dates = thisIn.selectedDates;
        thisIn.calendarService.sendApply( params, 'telecommuting' )
        .pipe( takeUntil( thisIn.destroyed$ ), finalize( () => {setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );} ) ).subscribe( ( res: any ) => {
          thisIn.childComponent.receiveAfterApply( res.data );
          Swal.fire( {
            title: 'Solicitar Teletrabajo', text: 'Tu solicitud se envió exitosamente', icon: 'success', iconHtml: null, timer: 4000
          } );
        }, error => {
          Swal.fire( {
            title: 'Solicitar Teletrabajo', html: `Ocurrió un error al enviar la solicitud<br/>${error.error.message}`, icon: 'error',
            iconHtml: null, timer: 4000
          } );
        } );
      }
    }
  }

  onUpdateApply() {
    const thisIn = this;
    setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
    const params: any = {};
    params.id = thisIn.childComponentItem.id;
    thisIn.calendarService.updateApply( params, thisIn.currentOption )
    .pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
        setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );
      } )
    ).subscribe( ( res: any ) => {
      thisIn.childComponent.receiveAfterUpdateApply( res.data );
      Swal.fire( {
        title: 'Solicitar Vacaciones', text: 'Tu solicitud se envió exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      Swal.fire( {
        title: 'Solicitar Vacaciones', html: `Ocurrió un error al enviar la solicitud<br/>${error.error.message}`, icon: 'error',
        iconHtml: null, timer: 4000
      } );
    } );
  }

  getSettings() {

    this.calendarService.getSettings().subscribe( ( res: any ) => this.settings = res );
    this.CALENDAR_ENABLED_RANGE = this.settings.filter( x => x.key === 'CALENDAR_ENABLED_RANGE' )[ 0 ].value;
    this.CALENDAR_SELECTION_TYPE = this.settings.filter( x => x.key === 'CALENDAR_SELECTION_TYPE' )[ 0 ].value;
    console.log( this.CALENDAR_ENABLED_RANGE );
    console.log( this.CALENDAR_SELECTION_TYPE );

    /*this.calendarService.getSettings().then( ( res: any ) => {
     this.settings = res;
     console.log(res);
     this.CALENDAR_ENABLED_RANGE = res.data.filter(x => x.key === 'CALENDAR_ENABLED_RANGE')[0].value;
     this.CALENDAR_SELECTION_TYPE = res.data.filter(x => x.key === 'CALENDAR_SELECTION_TYPE')[0].value;
     console.log(this.CALENDAR_ENABLED_RANGE);
     console.log(this.CALENDAR_SELECTION_TYPE);
     } );*/
  }
}
