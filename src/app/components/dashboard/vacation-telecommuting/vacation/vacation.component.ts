import { Component, OnInit,EventEmitter, Output, NgZone, OnDestroy, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import 'moment/locale/es';

import { VtRequestsToApproveComponent } from '@components/modals/vt-requests-to-approve/vt-requests-to-approve.component';
import { CalendarService } from '@services/calendar.service';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-vacation',
  templateUrl: './vacation.component.html',
  styleUrls: ['./vacation.component.scss']
})
export class VacationComponent implements OnInit, OnDestroy {
  @Output() eventEmitter: EventEmitter<any> = new EventEmitter();
  @ViewChild('perfectScrollbar', {static: false}) perfectScrollbar: PerfectScrollbarComponent;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  preload: boolean;
  preloadPage: boolean = false;
  listItems: any;
  listCurrentPage = 1;
  listLastPage = 0;
  scrollMin: boolean = false;
  viewDetail: any;
  listMonths = moment.localeData('es').months();
  isBoss: boolean = false;
  pendingCount: number = 0;
  entity = 'vacation';

  constructor(
    private zone: NgZone,
    private modalService: BsModalService,
    private calendarService: CalendarService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    await this.getBoss();
    await this.getSlopesCount();
    await this.getHistorial();
    setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getBoss() {
    return new Promise((resolve, reject) => {
    const thisIn = this;
    thisIn.calendarService.getBoss()
      .pipe(
        finalize(() => {
          resolve();
        }),
        takeUntil(this.destroyed$),
      ).subscribe((res: any) => {
        this.isBoss = res.data.is_boss;
      }, error => {
        this.isBoss = false;
      });
    });
  }

  getSlopesCount() {
    return new Promise((resolve, reject) => {
      const thisIn = this;
      thisIn.calendarService.getSlopesCount(thisIn.entity)
      .pipe(
        finalize(() => {
          resolve();
        }),
        takeUntil(this.destroyed$),
      ).subscribe((res: any) => {
        this.pendingCount = res.data.slopes_count;
      }, error => {
        this.pendingCount = 0;
      });
    });
  }

  getHistorial() {
    return new Promise((resolve, reject) => {
    const thisIn = this;
    if(thisIn.listCurrentPage === 1) thisIn.preload = true; else thisIn.preloadPage = true;
    thisIn.calendarService.getHistorial(thisIn.listCurrentPage, thisIn.entity)
      .pipe(
        finalize(() => {
          if(thisIn.listCurrentPage === 1) thisIn.preload = false; else thisIn.preloadPage = false;
          resolve();
        }),
        takeUntil(this.destroyed$),
      ).subscribe((res: any) => {
        res.data.data = thisIn.formatData(res.data.data);
        thisIn.listItems = (thisIn.listItems) ? (thisIn.listItems).concat(res.data.data) : res.data.data;
        thisIn.listLastPage = res.data.last_page;
      }, error => {
        thisIn.listItems = [];
        thisIn.listLastPage = 0;
      });
    });
  }

  onScrolledDownEnd() {
    if(this.listCurrentPage <= this.listLastPage && this.preloadPage === false) {
      this.zone.run(() => {
        this.listCurrentPage = this.listCurrentPage + 1;
        if(this.listCurrentPage <= this.listLastPage)
          this.getHistorial();
      });
    }
  }

  formatData(listItems) {
    return listItems = listItems.map((item) => {
      item.start_day = moment(item.start + 'T00:00:00').format("DD");
      item.start_month = this.listMonths[new Date(item.start + 'T00:00:00').getMonth()];
      item.end_day = moment(item.end + 'T00:00:00').format("DD");
      item.end_month = this.listMonths[new Date(item.end + 'T00:00:00').getMonth()];
      item.start_format = moment(item.start + 'T00:00:00').format("DD/MM/YYYY");
      item.end_format = moment(item.end + 'T00:00:00').format("DD/MM/YYYY");
      item.status_type = (item.status === 1) ? 'pending' : (item.status === 2) ? 'approve' : 'not_approve';
      return item;
    });
  }

  receiveAfterApply(data) {
    data = this.formatData([data]);
    this.onViewDetail(data[0]);
    this.listItems.unshift(data[0]);
    this.perfectScrollbar.directiveRef.scrollToTop(0, 0);
    this.perfectScrollbar.directiveRef.update();
  }

  receiveAfterUpdateApply(data) {
    data = this.formatData([data]);
    this.listItems = this.listItems.filter(function(el){
      return (el.id !== data[0].id)
    });
    this.listItems.unshift(data[0]);
    this.onViewDetail(data[0]);
    this.perfectScrollbar.directiveRef.scrollToTop(0, 0);
    this.perfectScrollbar.directiveRef.update();
  }

  receiveData(){
    this.scrollMin = true;
  }

  emitData() {
    this.eventEmitter.emit(Math.random());
  }

  onViewDetail(item) {
    this.scrollMin = true;
    this.viewDetail = item;
    this.eventEmitter.emit({function: 'onViewCalendar', item: item});
  }

  onRejectVacation(item) {
    const thisIn = this;
    const parameters: any = {}
    parameters.id = item.id;
    Swal.fire({
      title: `Eliminar solicitud`,
      text: '¿Estás seguro que deseas eliminar tu solicitud?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
        thisIn.calendarService.deleteApply(parameters, thisIn.entity)
        .pipe(
          finalize(() => {
            setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
          }),
          takeUntil(thisIn.destroyed$),
        ).subscribe((res: any) => {
          thisIn.listItems = thisIn.listItems.filter(function(el){
            return (el.id !== res.data.id)
          });
          thisIn.eventEmitter.emit({function: 'onStartSelectApply'});
          Swal.fire({ title: 'Eliminar solicitud', text: 'La solicitud fue eliminada exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Eliminar solicitud', text: 'Ocurrió un error al eliminar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }

  onViewSlopes() {
    const thisIn = this;
    if(thisIn.pendingCount) {
      const initialState = { entity: 'vacation' };
      const modal = thisIn.modalService.show(VtRequestsToApproveComponent, {
        class: 'ModalRequestApprove modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      });
      modal.content.eventClosed.subscribe(async (res: boolean) => {
        if (res) {
          await thisIn.getSlopesCount()
        }
      });
    } else {
      Swal.fire({ title: 'Solicitudes por aprobar', text: 'No existen solicitudes por aprobar', icon: 'info', iconHtml: null, timer: 4000 })
    }
  }

  trackBy(index: number, item: any) {
    return item.id;
  }
}
