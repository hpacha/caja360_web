import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacationTelecommutingRoutingModule } from './vacation-telecommuting-routing.module';
import { VacationTelecommutingComponent } from './vacation-telecommuting.component';
import { GeneralComponent } from './general/general.component';
import { VacationComponent } from './vacation/vacation.component';
import { TelecommutingComponent } from './telecommuting/telecommuting.component';
import { SharedModule } from '../../../shared/shared.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule, DateRange } from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';

defineLocale( 'es', esLocale );

@NgModule({
  declarations: [VacationTelecommutingComponent, GeneralComponent, VacationComponent, TelecommutingComponent],
  imports: [
    CommonModule,
    VacationTelecommutingRoutingModule,
    SharedModule,
    PerfectScrollbarModule,
    BsDatepickerModule.forRoot(),
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatInputModule
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' }]
})
export class VacationTelecommutingModule { }
