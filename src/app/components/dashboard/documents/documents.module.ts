import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgSelectModule } from '@ng-select/ng-select';

import { SpinnerModule } from '@shared/spinner/spinner.module';
import { SharedModule } from '@shared/shared.module';
import { DocumentsRoutingModule } from './documents-routing.module';
import { DocumentsComponent } from './documents.component';

@NgModule({
  declarations: [
    DocumentsComponent
  ],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    FormsModule,
    NgSelectModule,
    PaginationModule.forRoot(),
    PerfectScrollbarModule,
    ReactiveFormsModule,
    SharedModule,
    SpinnerModule
  ]
})
export class DocumentsModule {}
