import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';

import { DocumentsService } from '@services/documents.service';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  providers: [DatePipe]
})
export class DocumentsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  user_is_boss: boolean;
  listDocuments: any;
  preload: boolean;
  preloadPage: boolean;
  currentPage = 1;
  lastPage = 0;
  queryField: FormControl = new FormControl();
  timeout: any = null;
  forbidden: boolean;

  constructor(
    private zone: NgZone,
    private documentsService: DocumentsService,
    public datepipe: DatePipe,
  ) {}

  async ngOnInit() {
    const thisIn = this;
   /* thisIn.user_is_boss = JSON.parse(localStorage.getItem('user_is_boss'));
    if(!thisIn.user_is_boss) {
      thisIn.forbidden = true;
      return false;
    }*/
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    await this.getDocuments();
    setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getDocuments() {
    const thisIn = this;
    return new Promise<any[]>((resolve, reject) => {
      if(thisIn.currentPage === 1) thisIn.preload = true; else thisIn.preloadPage = true;
      thisIn.documentsService.getDocuments(thisIn.currentPage).pipe(
        takeUntil(thisIn.destroyed$),
        finalize(() => {
          if(thisIn.currentPage === 1) thisIn.preload = false; else thisIn.preloadPage = false;
          resolve();
        }),
      ).subscribe((res: any) => {
        thisIn.listDocuments = (thisIn.listDocuments) ? (thisIn.listDocuments).concat(res.data.data) : res.data.data;
        thisIn.lastPage = res.data.last_page;
      }, error => {
        thisIn.listDocuments = [];
        thisIn.lastPage = 0;
      });
    });
  }

  async onScrolledDownEnd() {
    this.zone.run(async () => {
      this.currentPage = this.currentPage + 1;
      if(this.currentPage <= this.lastPage)
        await this.getDocuments();
    });
  }

  searchDocuments(queryText) {
    const thisIn = this;
    if(thisIn.timeout) {
      window.clearTimeout(thisIn.timeout)
    }
    thisIn.timeout = window.setTimeout(function() {
      thisIn.timeout = null;
      if (queryText != null && queryText.length >= 3) {
        thisIn.currentPage = 1;
        thisIn.preload = true;
        thisIn.documentsService.getDocuments(thisIn.currentPage, {'hint': queryText})
          .takeUntil(thisIn.destroyed$)
          .subscribe((res:any) => {
            thisIn.listDocuments = res.data.data;
            thisIn.preload = false;
          }, (err:any) => {
            thisIn.listDocuments = [];
            thisIn.preload = false;
          });
      } else if (queryText != null && queryText.length === 0) {
        thisIn.listDocuments = [];
        thisIn.currentPage = 1;
        thisIn.getDocuments();
      }
    }, 250);
  }
}
