import { Component, OnInit } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import * as Globals from '@utils/globals';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@services/user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CommunityService } from '@services/community.service';
import { finalize, takeUntil } from 'rxjs/operators';
import { ScheduleComponent } from '@components/modals/schedule/schedule.component';
import { MeetingComponent } from '@components/modals/meeting/meeting.component';
import { SendFeedbackComponent } from '@components/modals/send-feedback/send-feedback.component';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-mentor-detail',
  templateUrl: './mentor-detail.component.html',
  styleUrls: ['./mentor-detail.component.scss']
} )
export class MentorDetailComponent implements OnInit {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  competenceId: any;
  menteesId: any;
  competence: any;
  evaluations: any;
  showData: Promise<boolean>;
  mentor: any;
  loading: boolean;
  results: any;

  constructor(
    private bsModalService: BsModalService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private communityService: CommunityService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    if ( this.activatedRoute.snapshot.params.id ) {
      thisIn.competenceId = this.activatedRoute.snapshot.params.id;
      thisIn.menteesId = this.activatedRoute.snapshot.params.idd;
      console.log( this.menteesId );
      if ( localStorage.getItem( 'competenceId' ) && localStorage.getItem( 'mentor' ) ) {
        this.competence = JSON.parse( localStorage.getItem( 'competenceId' ) ).competence;
        this.mentor = JSON.parse( localStorage.getItem( 'mentor' ) ).mentor;
        console.log( this.competence );
      }
      await this.getCompetenceEvaluations();
    }
    setTimeout( () => { this.globals.setLoadingVisible( false ); } );
  }

  onClickAcceptReject( item, type: string ) {
    console.log( item, type );
    if ( type === 'aceptar' ) {
      const parameters: any = {};
      parameters.status = 'aprobado';
      this.loading = true;
      this.communityService.patchUpdateEvaluations( this.competenceId, this.menteesId, item.id, parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.loading = false;
        Swal.fire( {
          title: 'Evaluación', text: 'La evaluación fue actualizada', icon: 'success', iconHtml: null, timer: 4000
        } ).then( () => {
          this.getCompetenceEvaluations();
        } );
      }, error => {
        Swal.fire( {
          title: 'Evaluación', html: `Ocurrió un error al actualizar la evaluación<br/>${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        } );
        this.loading = false;
      } );
    } else {
      const thisIn = this;
      const initialState = { competenceId: this.competenceId, menteesId: this.menteesId, evaluationId: item.id };
      const modal = thisIn.bsModalService.show( SendFeedbackComponent, {
        class: 'ModalSendFeedback modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      } );
      modal.content.eventClosed.subscribe( async ( res: any ) => {
        console.log( res );
        if ( res ) {
          this.getCompetenceEvaluations();
        }
      } );
    }
  }

  openSchedule( mentor: any ) {
    const thisIn = this;
    const initialState = { mentor };
    const modal = thisIn.bsModalService.show( ScheduleComponent, {
      class: 'ModalSchedule modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      console.log( res );
      mentor[ 'activity' ] = res.data;
      const initialState = { mentor };
      const modal = thisIn.bsModalService.show( MeetingComponent, {
        class: 'ModalMeeting modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      } );
    } );
  }

  getCompetenceEvaluations() {
    const thisIn = this;
    this.loading = true;
    return new Promise( ( resolve, reject ) => {
      thisIn.communityService.getShowMentorEvaluations( this.competenceId, this.menteesId )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        this.results = res.data;
        this.evaluations = res.data.evaluations;
        console.log( this.evaluations );
      }, error => {
        this.loading = false;
        this.evaluations = [];
      } );
    } );
  }

}
