import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { UserService } from '@services/user.service';
import { ChatService } from '@services/chat.service';
import { CommunityService } from '@services/community.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
} )
export class CompleteComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  loading: boolean;
  detail: any;
  competenceId: any;
  showData: Promise<boolean>;
  tabCompetenceComplete: any = { type: 'participating', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };

  constructor(
    private router: Router,
    private bsModalService: BsModalService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private chatService: ChatService,
    private communityService: CommunityService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    await this.getUserCompetences( this.tabCompetenceComplete );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  onClickDetailCompetence( id ) {
    this.router.navigate( ['comunidad/competencia/' + id] );
  }

  onClickDetail( item: any ) {
    const thisIn = this;
    this.loading = true;
    return new Promise((resolve, reject) => {
      thisIn.communityService.getShowCompetenceParticiping(item.id)
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => {
          this.loading = false;
          resolve();
        }),
      ).subscribe((res:any) => {
        this.loading = false;
        this.detail = res.data;
        console.log(this.detail);
      }, error => {
        this.loading = false;
        this.detail = [];
      });
    });
  }

  getUserCompetences( tabOption: any ) {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      if ( tabOption.currentPage === 1 ) {
        tabOption.preload = true;
      } else {
        tabOption.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = '';
      parameters.per_page = 24;
      parameters.status = 'completado';
      thisIn.communityService.getUserCompetences( parameters, tabOption.type, tabOption.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( tabOption.currentPage === 1 ) {
            tabOption.preload = false;
          } else {
            tabOption.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        tabOption.items = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
        tabOption.lastPage = res.data.last_page;
      }, error => {
        tabOption.items = [];
        tabOption.currentPage = 1;
        tabOption.lastPage = 0;
      } );
    } );
  }

  openSchedule() {

  }
}
