import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgSelectModule } from '@ng-select/ng-select';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { SharedModule } from '@shared/shared.module';
import { ServicesModule } from '@services/services.module';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { CommunityRoutingModule } from './community-routing.module';
import { CommunityComponent } from './community.component';
import { DetailComponent } from './detail/detail.component';
import { CompetenceComponent } from './competence/competence.component';
import { CompleteComponent } from './complete/complete.component';
import { MentorDetailComponent } from './mentor-detail/mentor-detail.component';

@NgModule({
  declarations: [
    CommunityComponent,
    CompleteComponent,
    CompetenceComponent,
    DetailComponent,
    MentorDetailComponent,
  ],
  imports: [
    CommonModule,
    CommunityRoutingModule,
    HttpClientModule,
    FormsModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule,
    PaginationModule.forRoot(),
    PerfectScrollbarModule,
    SharedModule,
    ServicesModule,
    SpinnerModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
  ]
})
export class CommunityModule {}
