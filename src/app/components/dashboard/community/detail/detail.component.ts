import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ActivatedRoute} from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { ChatService } from '../../../../services/chat.service';
import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import Swal from 'sweetalert2';
import * as Globals from '../../../../utils/globals';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  detail: any;
  detailId: any;
  showData: Promise<boolean>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private chatService: ChatService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    if (this.activatedRoute.snapshot.params['id']) {
      this.detailId = this.activatedRoute.snapshot.params['id'];
      await this.getBenefitsId(this.detailId);
      this.showData = Promise.resolve(true);
    }
    setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getBenefitsId(id) {
    const thisIn = this;
    return new Promise((resolve, reject) => {
      thisIn.userService.getProfileUserById(id)
      .pipe(
        takeUntil(this.destroyed$),
        finalize(() => {
          resolve();
        }),
      ).subscribe((res:any) => {
        thisIn.detail = res.data;
      }, error => {
        thisIn.detail = [];
      });
    });
  }

  openChat() {
    this.router.navigate([`chat/${this.detailId}`]);
  }
}
