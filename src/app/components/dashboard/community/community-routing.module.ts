import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommunityComponent } from '@components/dashboard/community/community.component';
import { DetailComponent } from '@components/dashboard/community/detail/detail.component';
import { CompetenceComponent } from '@components/dashboard/community/competence/competence.component';
import { CompleteComponent } from '@components/dashboard/community/complete/complete.component';
import { MentorDetailComponent } from '@components/dashboard/community/mentor-detail/mentor-detail.component';

const routes: Routes = [
  {path: '', component: CommunityComponent},
  {path: 'detail/:id', component: DetailComponent},
  {path: 'competencia/:id', component: CompetenceComponent},
  {path: 'completados', component: CompleteComponent},
  {path: 'mentor/:id/:idd', component: MentorDetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule {}
