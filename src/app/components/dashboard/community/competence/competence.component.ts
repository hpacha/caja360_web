import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { ApplyMentorComponent } from '@components/modals/mentor/apply-mentor/apply-mentor.component';
import { ConfirmMentorComponent } from '@components/modals/mentor/confirm-mentor/confirm-mentor.component';
import { MeetingComponent } from '@components/modals/meeting/meeting.component';
import { ScheduleComponent } from '@components/modals/schedule/schedule.component';
import { UserService } from '@services/user.service';
import { CommunityService } from '@services/community.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-competence',
  templateUrl: './competence.component.html',
  styleUrls: ['./competence.component.scss']
} )
export class CompetenceComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  competenceId: any;
  competence: any;
  evaluations: any;
  showData: Promise<boolean>;

  constructor(
    private bsModalService: BsModalService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private communityService: CommunityService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    if ( this.activatedRoute.snapshot.params[ 'id' ] ) {
      thisIn.competenceId = this.activatedRoute.snapshot.params[ 'id' ];
      await this.getCompetence();
      await this.getCompetenceEvaluations();
    }
    setTimeout( () => { this.globals.setLoadingVisible( false ); } );
  }

  getCompetence() {
    const thisIn = this;
    return new Promise( ( resolve, reject ) => {
      thisIn.communityService.getShowCompetenceParticiping( this.competenceId )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.competence = res.data;
        console.log( this.competence );
      }, error => {
        this.competence = [];
      } );
    } );
  }

  getCompetenceEvaluations() {
    const thisIn = this;
    return new Promise( ( resolve, reject ) => {
      thisIn.communityService.getShowCompetenceEvaluations( this.competenceId )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.evaluations = res.data;
        console.log( this.evaluations );
      }, error => {
        this.evaluations = [];
      } );
    } );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  openApplyMentorModal() {
    const thisIn = this;
    let initialState: any = { competenceId: this.competence.id, menetorRequest: this.competence };
    let modal = this.bsModalService.show( ApplyMentorComponent, {
      class: 'ModalApplyMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      console.log(res);
      if ( res && res.new ) {
        this.competence = res.new.data;
        const initialState2 = { mentor: this.competence.mentor_requests[ 0 ].mentor };
        this.bsModalService.show( ConfirmMentorComponent, {
          class: 'ModalConfirmMentor modal-dialog-centered',
          initialState: initialState2,
          backdrop: 'static',
          keyboard: false
        } );
      }
    } );
  }

  openSchedule( mentor: any ) {
    const thisIn = this;
    const initialState = { mentor };
    const modal = thisIn.bsModalService.show( ScheduleComponent, {
      class: 'ModalSchedule modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      console.log( res );
      mentor[ 'activity' ] = res.data;
      const initialState = { mentor };
      const modal = thisIn.bsModalService.show( MeetingComponent, {
        class: 'ModalMeeting modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      } );
    } );
  }

  onFavoriteUsers( event, item ) {
    event.stopPropagation();
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    thisIn.userService.postFavoriteUsers( { user_id: item.id } ).pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
        setTimeout( () => { this.globals.setLoadingVisible( false ); } );
      } ),
    ).subscribe( ( res: any ) => {
      this.competence.mentor_requests[ 0 ].mentor.is_favorite = true;
      Swal.fire( {
        title: 'Usuario favorito', text: 'Usuario favorito agregado exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      Swal.fire( {
        title: 'Usuario favorito', text: 'Ocurrió un error al agregar al usuario favorito', icon: 'error', iconHtml: null, timer: 4000
      } );
    } );
  }

  onDeleteFavoriteUsers( event, item ) {
    event.stopPropagation();
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    thisIn.userService.delFavoriteUsers( item.id ).pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
        setTimeout( () => { this.globals.setLoadingVisible( false ); } );
      } ),
    ).subscribe( ( res: any ) => {
      this.competence.mentor_requests[ 0 ].mentor.is_favorite = false;
      Swal.fire( {
        title: 'Usuario favorito', text: 'Usuario favorito removido exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      Swal.fire( {
        title: 'Usuario favorito', text: 'Ocurrió un error al remover al usuario favorito', icon: 'error', iconHtml: null, timer: 4000
      } );
    } );
  }
}
