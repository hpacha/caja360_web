import { Component, EventEmitter, NgZone, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import 'moment/locale/es';
import 'rxjs/add/operator/finally';
import Swal from 'sweetalert2';

import { ApplyCompetenceComponent } from '@components/modals/mentor/apply-competence/apply-competence.component';
import { ApplyStatusComponent } from '@components/modals/mentor/apply-status/apply-status.component';
import { ConfirmMentorComponent } from '@components/modals/mentor/confirm-mentor/confirm-mentor.component';
import { HeadquarterService } from '@services/headquarter.service';
import { UserService } from '@services/user.service';
import { CommunityService } from '@services/community.service';
import * as Globals from '@utils/globals';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { MentorRequestComponent } from '@components/modals/mentor-request/mentor-request.component';
import { MentorRejectRequestComponent } from '@components/modals/mentor-reject-request/mentor-reject-request.component';
import { SuggestMentorComponent } from '@components/modals/suggest-mentor/suggest-mentor.component';

@Component( {
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
} )
export class CommunityComponent implements OnInit {
  loading: boolean;
  typeOfService: any;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  listHeadquarters: [];
  listAreas: [];
  listOccupations: [];
  userProfile: any;
  selectedHeadquarter: number;
  selectedArea: number;
  selectedOccupation: number;
  showData: Promise<boolean>;
  tabUsers: any = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listUser: null };
  typeAheadListUsers = new EventEmitter<string>();
  queryField: FormControl = new FormControl();
  timeout: any = null;
  myPerformance: any;
  listCategories: [];
  selectedCategory: string = '';
  tabCompetenceMentoring: any = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
  tabCompetenceMentees: any = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
  tabCompetenceOld: any = { type: 'participating', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
  tabCompetenceNew: any = { type: 'not-participating', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
  tabCurrent: any;
  tabActive = 0;
  requestMentees: any;
  @ViewChild( 'staticTabs', { static: false } ) staticTabs: TabsetComponent;

  constructor(
    private zone: NgZone,
    private router: Router,
    private bsModalService: BsModalService,
    private headquarterService: HeadquarterService,
    private userService: UserService,
    private communityService: CommunityService
  ) {}

  async ngOnInit() {
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    await this.getListHeadquarters();
    await this.getListAreas();
    await this.getOccupations();
    await this.getUserProfile();
    await this.getUserSearch();
    this.showData = Promise.resolve( true );
    setTimeout( () => { this.globals.setLoadingVisible( false ); } );
    await this.getUserCompetencesPerformance();
    await this.getCompetencesCategories();
    await this.getUserCompetences( this.tabCompetenceOld );
    await this.getUserCompetences( this.tabCompetenceNew );
    await this.getUserCompetencesMentoring( this.tabCompetenceMentoring );
  }

  selectTab( tabId: number ) {
    this.staticTabs.tabs[ tabId ].active = true;
    this.tabActive = tabId;
  }

  getListHeadquarters() {
    return new Promise<any[]>( ( resolve, reject ) => {
      this.headquarterService.getHeadquarters().then( ( res: any ) => {
        this.listHeadquarters = res;
        resolve();
      } );
    } );
  }

  getListAreas() {
    return new Promise<any[]>( ( resolve, reject ) => {
      this.headquarterService.getAreas().then( ( res: any ) => {
        this.listAreas = res;
        resolve();
      } );
    } );
  }

  getOccupations() {
    return new Promise<any[]>( ( resolve, reject ) => {
      this.headquarterService.getOccupations().then( ( res: any ) => {
        this.listOccupations = res;
        resolve();
      } );
    } );
  }

  getCompetencesCategories() {
    const thisIn = this;
    this.loading = true;
    return new Promise<any[]>( ( resolve, reject ) => {
      thisIn.communityService.getCompetencesCategories().then( ( res: any ) => {
        this.loading = false;
        thisIn.listCategories = res.reduce( ( acc: any, item ) => {
          acc.push( { id: item, name: item } );
          return acc;
        }, [] ).concat( { id: '', name: 'todos' } );
        resolve();
      }, error => { this.loading = false;} );
    } );
  }

  getUserProfile() {
    return new Promise<any[]>( ( resolve, reject ) => {
      this.userService.getUserProfile().then( ( res: any ) => {
        this.userProfile = res;
        this.selectedHeadquarter = this.userProfile.headquarter_id;
        this.selectedArea = this.userProfile.area_id;
        this.selectedOccupation = this.userProfile.occupation_id;
        resolve();
      } );
    } );
  }

  async onSelectHeadquarter( event ) {
    this.selectedHeadquarter = event.id;
    await this.getUserSearch();
  }

  async onSelectArea( event ) {
    this.selectedArea = event.id;
    await this.getUserSearch();
  }

  async onSelectOccupation( event ) {
    this.selectedOccupation = event.id;
    await this.getUserSearch();
  }

  getUserSearch( queryText = null ) {
    const thisIn = this;
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( thisIn.tabUsers.currentPage === 1 ) {
        thisIn.tabUsers.preload = true;
      } else {
        thisIn.tabUsers.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = ( queryText ) ? queryText : null;
      parameters.headquarter = [thisIn.selectedHeadquarter];
      parameters.area = [thisIn.selectedArea];
      parameters.occupation = [thisIn.selectedOccupation];
      thisIn.userService.getSearchUsers( parameters, thisIn.tabUsers.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( thisIn.tabUsers.currentPage === 1 ) {
            thisIn.tabUsers.preload = false;
          } else {
            thisIn.tabUsers.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        thisIn.tabUsers.listUser = ( thisIn.tabUsers.listUser ) ? ( thisIn.tabUsers.listUser ).concat( res.data.data ) : res.data.data;
        thisIn.tabUsers.lastPage = res.data.last_page;
      }, error => {
        thisIn.tabUsers.listUser = [];
        thisIn.tabUsers.lastPage = 0;
      } );
    } );
  }

  onSearchUsers( queryText ) {
    const thisIn = this;
    if ( thisIn.timeout ) {
      window.clearTimeout( thisIn.timeout );
    }
    thisIn.timeout = window.setTimeout( function() {
      thisIn.timeout = null;
      if ( queryText != null && queryText.length >= 3 ) {
        thisIn.tabUsers.preload = true;
        const parameters: any = {};
        parameters.hint = ( queryText ) ? queryText : null;
        parameters.headquarter = [thisIn.selectedHeadquarter];
        parameters.area = [thisIn.selectedArea];
        parameters.occupation = [thisIn.selectedOccupation];
        parameters.hint = queryText;
        thisIn.userService.getSearchUsers( parameters, 1 )
        .pipe( takeUntil( thisIn.destroyed$ ) )
        .subscribe( ( res: any ) => {
          thisIn.tabUsers.listUser = ( res.data.data );
          thisIn.tabUsers.preload = false;
        }, ( err: any ) => {
          thisIn.tabUsers.preload = false;
        } );
      } else if ( queryText != null && queryText.length === 0 ) {
        thisIn.tabUsers.listUser = [];
        thisIn.tabUsers.currentPage = 1;
        thisIn.getUserSearch();
      }
    }, 250 );
  }

  onScrolledDownEnd() {
    this.zone.run( async () => {
      this.tabUsers.currentPage = this.tabUsers.currentPage + 1;
      if ( this.tabUsers.currentPage <= this.tabUsers.lastPage ) {
        await this.getUserSearch();
      }
    } );
  }

  onClickDetail( item ) {
    this.router.navigate( ['comunidad/detail/' + item.id] );
  }

  onClickMenteesDetail( item ) {
    console.log( item.id, this.requestMentees.id );
    localStorage.setItem( 'mentor', JSON.stringify( { mentor: item } ) );
    // this.router.navigate( ['comunidad/mentor/' + item.id] );
    this.router.navigate( [`comunidad/mentor/${this.requestMentees.id}/${item.id}`] );

  }

  onClickGetMentees( item ) {
    console.log( item );
    localStorage.setItem( 'competenceId', JSON.stringify( { competence: item } ) );
    this.requestMentees = { id: item.id, awaiting_requests: item.awaiting_requests };
    this.getUserCompetencesMentees( item.id, this.tabCompetenceMentees );
  }

  getUserCompetencesMentees( mentoringId: number, tabOption: any ) {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      if ( tabOption.currentPage === 1 ) {
        tabOption.preload = true;
      } else {
        tabOption.preloadPage = true;
      }
      // const parameters: any = {};
      this.loading = true;
      thisIn.communityService.getUserCompetencesMentees( mentoringId, tabOption.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( tabOption.currentPage === 1 ) {
            tabOption.preload = false;
          } else {
            tabOption.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        tabOption.items = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
        console.log( res );
        console.log( tabOption );
        tabOption.lastPage = res.data.last_page;
      }, error => {
        this.loading = false;
        tabOption.items = [];
        tabOption.currentPage = 1;
        tabOption.lastPage = 0;
      } );
    } );
  }

  onClickRequest( id: number ) {
    const initialState: any = { mentoringId: id };
    const modal = this.bsModalService.show( MentorRequestComponent, {
      class: 'ModalMentorRequest modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      console.log( res );
      if ( res && res.status === 'aceptado' ) {
        this.tabCompetenceMentoring = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
        this.tabCompetenceMentees = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
        this.requestMentees = null;
        this.getUserCompetencesMentoring( this.tabCompetenceMentoring );
      } else {
        this.modalRejectRequest( res.data );
      }
    } );
  }

  modalRejectRequest( data ) {
    const initialState = { data };
    const modal2 = this.bsModalService.show( MentorRejectRequestComponent, {
      class: 'ModalMentorRejectRequest modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal2.content.eventClosed.subscribe( async ( res: any ) => {
      if ( res && res.status ) {
        console.log( res );
        this.modalSuggestMentor( res.data, res.reason_rejection );
      }
    } );
  }

  modalSuggestMentor( data, reasonRejection ) {
    const initialState = { data, reasonRejection };
    const modal2 = this.bsModalService.show( SuggestMentorComponent, {
      class: 'ModalSuggestMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal2.content.eventClosed.subscribe( async ( res: any ) => {
      if ( res ) {
        this.tabCompetenceMentoring = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
        this.tabCompetenceMentees = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
        this.requestMentees = null;
        this.getUserCompetencesMentoring( this.tabCompetenceMentoring );
      }
    } );
  }

  onFavoriteUsers( event, item ) {
    event.stopPropagation();
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    thisIn.userService.postFavoriteUsers( { user_id: item.id } ).pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
        setTimeout( () => { this.globals.setLoadingVisible( false ); } );
      } ),
    ).subscribe( ( res: any ) => {
      thisIn.tabUsers.listUser = thisIn.tabUsers.listUser.map( ( el: any ) => {
        if ( ( el.id === res.data.id ) ) {
          el = res.data;
        }
        return el;
      } );
      Swal.fire( {
        title: 'Usuario favorito', text: 'Usuario favorito agregado exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      Swal.fire( {
        title: 'Usuario favorito', text: 'Ocurrió un error al agregar al usuario favorito', icon: 'error', iconHtml: null, timer: 4000
      } );
    } );
  }

  onDeleteFavoriteUsers( event, item ) {
    event.stopPropagation();
    const thisIn = this;
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    thisIn.userService.delFavoriteUsers( item.id ).pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
        setTimeout( () => { this.globals.setLoadingVisible( false ); } );
      } ),
    ).subscribe( ( res: any ) => {
      thisIn.tabUsers.listUser = thisIn.tabUsers.listUser.map( ( el: any ) => {
        if ( ( el.id === res.data.id ) ) {
          el = res.data;
        }
        return el;
      } );
      Swal.fire( {
        title: 'Usuario favorito', text: 'Usuario favorito removido exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      Swal.fire( {
        title: 'Usuario favorito', text: 'Ocurrió un error al remover al usuario favorito', icon: 'error', iconHtml: null, timer: 4000
      } );
    } );
  }

  async onSelectCategory( event ) {
    this.selectedCategory = event.id;
    this.tabCompetenceOld.items = [];
    this.tabCompetenceOld.currentPage = 1;
    await this.getUserCompetences( this.tabCompetenceOld );
    this.tabCompetenceNew.items = [];
    this.tabCompetenceNew.currentPage = 1;
    await this.getUserCompetences( this.tabCompetenceNew );
  }

  getUserCompetencesPerformance() {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      this.loading = true;
      thisIn.communityService.getUserCompetencesPerformance().pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        thisIn.myPerformance = res.data;
      }, error => {
        this.loading = false;
        thisIn.myPerformance = [];
      } );
    } );
  }

  getUserCompetences( tabOption: any ) {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      if ( tabOption.currentPage === 1 ) {
        tabOption.preload = true;
      } else {
        tabOption.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = thisIn.selectedCategory;
      parameters.per_page = 24;
      this.loading = true;
      thisIn.communityService.getUserCompetences( parameters, tabOption.type, tabOption.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( tabOption.currentPage === 1 ) {
            tabOption.preload = false;
          } else {
            tabOption.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        tabOption.items = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
        console.log( res );
        console.log( tabOption );
        tabOption.lastPage = res.data.last_page;
      }, error => {
        this.loading = false;
        tabOption.items = [];
        tabOption.currentPage = 1;
        tabOption.lastPage = 0;
      } );
    } );
  }

  getUserCompetencesMentoring( tabOption: any ) {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      if ( tabOption.currentPage === 1 ) {
        tabOption.preload = true;
      } else {
        tabOption.preloadPage = true;
      }
      const parameters: any = {};
      parameters.per_page = 24;
      this.loading = true;
      thisIn.communityService.getUserCompetencesMentoring( parameters, tabOption.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( tabOption.currentPage === 1 ) {
            tabOption.preload = false;
          } else {
            tabOption.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        console.log( 'mentoring', res );
        tabOption.items = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
        tabOption.lastPage = res.data.last_page;
      }, error => {
        this.loading = false;
        tabOption.items = [];
        tabOption.currentPage = 1;
        tabOption.lastPage = 0;
      } );
    } );
  }

  onScrolledCompetencesDownEnd( tabOption: any, type?: string ) {
    if ( type === 'mentoring' ) {
      this.zone.run( async () => {
        tabOption.currentPage = tabOption.currentPage + 1;
        if ( tabOption.currentPage <= tabOption.lastPage ) {
          await this.getUserCompetencesMentoring( tabOption );
        }
      } );
    } else {
      this.zone.run( async () => {
        tabOption.currentPage = tabOption.currentPage + 1;
        if ( tabOption.currentPage <= tabOption.lastPage ) {
          await this.getUserCompetences( tabOption );
        }
      } );
    }
  }

  onClickCompetence( item ) {
    this.router.navigate( ['comunidad/competencia/' + item.id] );
  }

  onClickApplyCompetence( item ) {
    const thisIn = this;
    let initialState: any = { competence: item };
    const modal = thisIn.bsModalService.show( ApplyCompetenceComponent, {
      class: 'ModalApplyMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      console.log( res );
      if ( res ) {
        thisIn.tabCompetenceOld.items.push( res.new.data );
        thisIn.tabCompetenceNew.items = thisIn.tabCompetenceNew.items.filter( ( el: any ) => {
          return ( el.id !== res.old.id );
        } );
        initialState = { mentor: res.new.data.mentor_requests[ 0 ].mentor, res };
        const modal2 = thisIn.bsModalService.show( ConfirmMentorComponent, {
          class: 'ModalConfirmMentor modal-dialog-centered',
          initialState,
          backdrop: 'static',
          keyboard: false
        } );
        modal2.content.eventClosed.subscribe( async ( response: any ) => {
          console.log( response );
          if ( response && response.res ) {
            initialState = { res: response.res };
            thisIn.bsModalService.show( ApplyStatusComponent, {
              class: 'ModalApplyStatus modal-dialog-centered',
              initialState,
              backdrop: 'static',
              keyboard: false
            } );
          }
        } );
      }
    } );
  }

  onClickApplyCompetenceStatus( item ) {
    const thisIn = this;
    thisIn.communityService.getShowCompetenceParticiping( item.id ).pipe(
      takeUntil( thisIn.destroyed$ ),
      finalize( () => {
      } ),
    ).subscribe( ( res: any ) => {
      console.log( res.data );
      if ( res.data.mentor_requests[ 0 ].status === 'Pendiente' || res.data.mentor_requests[ 0 ].status === 'Rechazado' ) {
        let initialState: any = { competence: res.data };
        const modal = thisIn.bsModalService.show( ApplyStatusComponent, {
          class: 'ModalApplyStatus modal-dialog-centered',
          initialState,
          backdrop: 'static',
          keyboard: false
        } );
        modal.content.eventClosed.subscribe( async ( res: any ) => {
          console.log( res );
          if ( res.type === 'delete' ) {
            thisIn.tabCompetenceOld.items = thisIn.tabCompetenceOld.items.filter( ( el: any ) => {
              return ( el.id !== res.competence.id );
            } );
          } else if ( res.type === 'accept' ) {
            thisIn.tabCompetenceOld.items = thisIn.tabCompetenceOld.items.map( ( el: any ) => {
              if ( el.id === res.competence.id ) {
                el = res.competence;
              }
              return el;
            } );
          }
        } );
      }
    } );
  }
}
