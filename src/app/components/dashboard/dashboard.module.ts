import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { environment } from '@environments/environment';
import { ModalsModule } from '@components/modals/modals.module';
import { ToolsModule } from '@components/tools/tools.module';
import { ServicesModule } from '@services/services.module';
import { InteceptorErrorContent } from '@shared/common/general-functions';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    AgmCoreModule.forRoot({apiKey: environment.google_maps_api_key}),
    BsDatepickerModule.forRoot(),
    CommonModule,
    DashboardRoutingModule,
    ModalsModule,
    ModalModule.forRoot(),
    ServicesModule,
    SpinnerModule,
    ToolsModule,
  ],
  providers: [
    InteceptorErrorContent
  ],
})
export class DashboardModule {}
