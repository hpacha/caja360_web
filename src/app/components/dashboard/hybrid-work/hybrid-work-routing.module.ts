import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HybridWorkComponent } from '@components/dashboard/hybrid-work/hybrid-work.component';


const routes: Routes = [
  {path: '', component: HybridWorkComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HybridWorkRoutingModule { }
