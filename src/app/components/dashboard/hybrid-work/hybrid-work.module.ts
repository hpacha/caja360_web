import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HybridWorkRoutingModule } from './hybrid-work-routing.module';
import { HybridWorkComponent } from './hybrid-work.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { SharedModule } from '@shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { ControlMessagesModule } from '@shared/control-messages/control-messages.module';
import { MatFormFieldModule } from '@angular/material/form-field';

defineLocale( 'es', esLocale );

@NgModule( {
  declarations: [HybridWorkComponent],
  imports: [
    CommonModule,
    HybridWorkRoutingModule,
    ControlMessagesModule,
    FormsModule,
    LazyLoadImageModule,
    PerfectScrollbarModule,
    SpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    NgSelectModule,
    BsDatepickerModule.forRoot(),
    SharedModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-PE' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        useUtc: true,
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    },
  ]
} )
export class HybridWorkModule {}
