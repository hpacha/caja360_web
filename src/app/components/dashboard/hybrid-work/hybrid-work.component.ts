import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatCalendar, MatCalendarCellCssClasses } from '@angular/material/datepicker';

import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { ReplaySubject } from 'rxjs';
import { finalize } from 'rxjs/internal/operators';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import 'moment/locale/es';

import { CalendarService } from '@services/calendar.service';
import * as Globals from '@utils/globals';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ConstancyComponent } from '@components/modals/constancy/constancy.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetalertService } from '@shared/services/sweetalert.service';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-hybrid-work',
  templateUrl: './hybrid-work.component.html',
  styleUrls: ['./hybrid-work.component.scss'],
  providers: [DatePipe]
} )
export class HybridWorkComponent implements OnInit, OnDestroy {
  locale = 'es';
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  @ViewChild( PerfectScrollbarDirective, { static: false } ) perfectScrollbarDirectiveRef?: PerfectScrollbarDirective;
  @ViewChild( MatCalendar ) calendar: MatCalendar<Date>;
  listMonths = moment.localeData( 'es' ).months();
  currentDate: string;
  selectedDate2: string;
  selectedDate = new Date( new Date() );
  selectedDateView: any;

  loading: boolean;
  preload: boolean;

  listItems: any;
  listItemsFilter: any;
  listItemsTotal: 0;
  listDatesToHighlight: [];


  range = [
    { id: 'M,T,W,Th,F', name: 'De Lunes a viernes' },
    { id: 'M,W,F', name: 'Lunes, miércoles y viernes' },
    { id: 'T,Th', name: 'Martes y jueves' },
  ];
  office = [
    { id: 1, name: 'Fibra' },
    { id: 2, name: 'La Pampilla' },
    { id: 3, name: 'La Merced' },
    { id: 4, name: 'Umacollo' },
  ];
  showRequest = false;

  form: FormGroup;
  minDate: Date;
  minDate2: Date;
  showDay = false;

  myFilter = ( d: Date | null ): boolean => {
    const day = ( d || new Date() ).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
  };

  constructor( private fb: FormBuilder, private localeService: BsLocaleService, private renderer: Renderer2,
               private calendarService: CalendarService, private sweetalertService: SweetalertService,
               private router: Router, public datepipe: DatePipe, private modalService: BsModalService ) {
    this.localeService.use( this.locale );
    this.minDate = new Date( new Date() );
    this.minDate2 = new Date( new Date() );
    this.currentDate = moment( new Date() ).format( 'YYYY-MM-DD' );
    this.selectedDate2 = moment( new Date() ).format( 'YYYY-MM-DD' );
  }

  async ngOnInit() {
    const thisIn = this;
    await thisIn.createForm();
    setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
    await thisIn.getData();
    // this.onSelectedChange( this.selectedDate );
    setTimeout( () => { thisIn.globals.setLoadingVisible( false ); } );
    if ( this.form.controls.type_request.value === 'single_day' ) {
      this.form.controls.date_start.setValue( this.selectedDate );
    }
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  onClickShow() {
    this.showDay = false;
    this.showRequest = !this.showRequest;
  }

  onClickChangeOption( event ) {
    this.form.controls.date_start.setValue( event.target.value === 'ranges' ? null : this.selectedDate );
    this.form.controls.headquarter_id.reset();
    this.form.controls.day_ranges.updateValueAndValidity();
    this.form.controls.date_end.updateValueAndValidity();

    this.form.controls.type_request.setValue( event.target.value );
    this.form.controls.day_ranges.setValidators( event.target.value === 'ranges' ? Validators.required : null );
    this.form.controls.date_end.setValidators( event.target.value === 'ranges' ? Validators.required : null );

    this.form.controls.day_ranges.updateValueAndValidity();
    this.form.controls.date_end.updateValueAndValidity();
  }

  onClickViewConstancy() {
    const initialState = {
      data: this.listItems
    };
    const modal = this.modalService.show( ConstancyComponent, {
      class: 'ModalConstancy modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onCalendarView() {
    const thisIn = this;
    thisIn.calendar.updateTodaysDate();
    thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    const buttonsPrev = document.querySelector( '.mat-calendar-previous-button' );
    const buttonsNext = document.querySelector( '.mat-calendar-next-button' );
    thisIn.renderer.listen( buttonsPrev, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
    thisIn.renderer.listen( buttonsNext, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
  }

  onSelectedChange( event ) {
    this.form.reset( {
      type_request: 'single_day',
      day_ranges: null,
      date_start: null,
      date_end: null,
      headquarter_id: null,
      // worker_code_user: 1234,
    } );
    if ( this.form.controls.type_request.value === 'single_day' ) {
      this.form.controls.date_start.setValue( event );
    }
    this.showRequest = false;
    this.showDay = true;
    this.selectedDate = event;
    this.selectedDate2 = moment( event ).format( 'YYYY-MM-DD' );
    var result = event.toLocaleDateString( 'sv-SE', { year: 'numeric', month: '2-digit', day: '2-digit' } );
    this.listItems = this.listItemsFilter.filter( ( el: any ) => {
      return moment( el.date ).format( 'YYYY-MM-DD' ) === result;
    } );
    this.listItemsTotal = this.listItems.length;
  }

  onMonthSelected( event ) {
    this.selectedDateView = this.listMonths[ event.getMonth() ] + ' ' + event.getFullYear();
  }

  dateClass() {
    return ( date: Date ): MatCalendarCellCssClasses => {
      const highlightDate = this.listDatesToHighlight
      .map( strDate => new Date( strDate + 'T00:00:00' ) )
      .some( d => d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear() );
      return highlightDate ? 'special-date' : ( new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() ) <= new Date( date.getFullYear(), date.getMonth(), date.getDate() ) ) ? 'future-date' : 'pass-date';
    };
  }

  renderDate( date: Date, type: string ) {
    if ( type === 'name' ) {
      return moment( date ).format( 'dddd' );
    } else if ( type === 'day' ) {
      return moment( date ).format( 'DD' );
    } else if ( type === 'month' ) {
      return moment( date ).format( 'MMMM' );
    } else if ( type === 'year' ) {
      return moment( date ).format( 'YYYY' );
    }
    return '';
  }

  onSave() {
    this.form.markAllAsTouched();
    if ( this.form.valid ) {
      this.loading = true;
      const params = {
        type_request: this.form.value.type_request,
        day_ranges: this.form.value.day_ranges ? this.form.value.day_ranges : null,
        date_start: this.form.value.date_start ? moment( this.form.value.date_start ).format( 'YYYY-MM-DD' ) : null,
        date_end: this.form.value.date_end ? moment( this.form.value.date_end ).format( 'YYYY-MM-DD' ) : null,
        headquarter_id: this.form.value.headquarter_id,
        // worker_code_user: this.form.value.worker_code_user,
      };

      this.calendarService.postShifting( params ).subscribe( ( res: any ) => {
        this.loading = false;
        // '<p class="gotham-book color-blue">Se guardaron satisfactoriamente los turnos seleccionados.</p>' +
        Swal.fire( {
          title: this.form.controls.type_request.value !== 'single_day' ? 'Turnos guardados' : 'Turno guardado',
          // text: 'Se guardaron satisfactoriamente los turnos seleccionados.',
          html: this.form.controls.type_request.value !== 'single_day' ? this.renderLegend( res ) + '</div>' : 'Se guardó satisfactoriamente el turno seleccionado.',
          icon: 'success',
          customClass: { container: 'swal2-hybrid' }, iconHtml: '<img src="./assets/img/svg/save.svg" class="img-fluid" alt="">',
        } ).then( () => {
          this.form.reset( {
            type_request: 'single_day',
            day_ranges: null,
            date_start: null,
            date_end: null,
            headquarter_id: null,
            // worker_code_user: 1234,
          } );
          this.showRequest = false;
          this.getData();
        } );
      }, error => {
        console.log( error );
        this.loading = false;
        Swal.fire( {
          title: '¡Error!',
          text: error?.error?.message,
          icon: 'warning',
          customClass: { container: 'swal2-hybrid swal2-hybrid-error' }, iconHtml: '<img src="./assets/img/svg/error.svg" class="img-fluid" alt="">',
        } );
      } );
    }
  }

  renderLegend( res: any ) {
    return '<div class="content">' +
      '<p class="gotham-book color-blue mb-0">' +
      res?.data?.dates.cantidad_dias_guardados_exitosamente + ' ' +
      ( res?.data?.dates.cantidad_dias_guardados_exitosamente === 1 ? 'cupo ' : 'cupos ' ) + 'registrados exitosamente <br>' +
      res?.data?.dates?.cantidad_dias_sin_cupos_disponibles + ' ' +
      ( res?.data?.dates?.cantidad_dias_sin_cupos_disponibles === 1 ? 'día ' : 'días ' ) + 'no hay cupo en la sede indicada' + '<br>' +
      res?.data?.dates?.cantidad_dias_cita_reservada + ' ' +
      ( res?.data?.dates?.cantidad_dias_cita_reservada === 1 ? 'día ' : 'días ' ) + 'ya tenías un turno registrado</p>' + '</div>';
  }

  onValueChange( event ) {
    if ( this.form.controls.date_start.value !== null ) {
      this.form.controls.date_end.reset();
      this.form.controls.date_end.enable();
      const date = moment( event );
      this.minDate2 = new Date( date.add( 1, 'day' ).format() );
    }
  }

  onClickDelete() {
    this.listItems = this.listItemsFilter.filter( ( el: any ) => {
      return moment( el.date ).format( 'YYYY-MM-DD' ) === moment( this.selectedDate ).format( 'YYYY-MM-DD' );
    } );
    const id = this.listItems[ 0 ].id;
    // const workercodeuser = 1234;
    this.loading = true;
    this.calendarService.deleteItem( id ).subscribe( ( res: any ) => {
      this.loading = false;
      this.form.controls.date_end.disable();
      Swal.fire( {
        title: 'Turno eliminado',
        text: 'Se eliminó el turno seleccionado.',
        icon: 'success',
        customClass: { container: 'swal2-hybrid' }, iconHtml: '<img src="./assets/img/svg/delete.svg" class="img-fluid" alt="">',
      } ).then( () => {
        this.getData();
      } );
    }, error => {
      this.loading = false;
      Swal.fire( {
        title: '¡Error!',
        text: error?.error?.message,
        icon: 'warning',
        customClass: { container: 'swal2-hybrid swal2-hybrid-error' }, iconHtml: '<img src="./assets/img/svg/error.svg" class="img-fluid" alt="">',
      } ).then( () => {
        this.getData();
      } );
    } );
  }

  getData() {
    return new Promise( ( resolve, reject ) => {
      const thisIn = this;
      thisIn.loading = true;
      thisIn.calendarService.getItemsHybridWork()
      .pipe(
        finalize( () => {
          this.loading = false;
          setTimeout( () => {
            thisIn.onCalendarView();
            thisIn.dateClass();
          }, 0 );
          resolve();
        } ),
        takeUntil( this.destroyed$ ),
      ).subscribe( ( res: any ) => {
        thisIn.listItems = res.data.dates;
        thisIn.listItemsFilter = res.data.dates;
        thisIn.listDatesToHighlight = res.data.dates.map( x => moment( x.date ).format( 'YYYY-MM-DD' ) ).filter( x => moment( x ).format( 'YYYY-MM-DD' ) >= this.currentDate );
        thisIn.listItemsTotal = thisIn.listItems.length;
        this.onSelectedChange( this.selectedDate );
      }, error => {
        thisIn.listItems = [];
        thisIn.listItemsFilter = [];
        thisIn.listDatesToHighlight = [];
        thisIn.listItemsTotal = 0;
      } );
    } );
  }

  private createForm() {
    this.form = this.fb.group( {
      type_request: ['single_day', Validators.required],
      day_ranges: [null, null],
      date_start: [null, Validators.required],
      date_end: [null, null],
      headquarter_id: [null, Validators.required],
      // worker_code_user: [1234, Validators.required],
    } );
  }

}
