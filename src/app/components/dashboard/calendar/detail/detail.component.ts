import { Component, OnInit } from '@angular/core';
import { NewnessActivityService } from '../../../../services/newness-activity.service';
import { ActivatedRoute } from '@angular/router';

@Component( {
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
} )
export class DetailComponent implements OnInit {
  detail: any;
  detailDate = [];
  detailType: any;
  detailImage = [];
  detailVideos = [];
  id: number;
  loading: boolean;
  typeOfService: any;

  constructor(
    private newnessActivityService: NewnessActivityService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    try {
      this.id = this.activatedRoute.snapshot.params.id;
      this.typeOfService = this.newnessActivityService;
      this.getActivityOrNewId( this.id );
    } catch (e) {
      console.log(e);
    }
  }
 
  private getActivityOrNewId( id: number ) {
    this.loading = true;
    this.typeOfService.getPostId(id).subscribe((res: any) => {
        this.detail = res.data;
        this.detailDate = res.data.activity_date;
        this.detailType = res.data.activity_type;
        this.detailImage = res.data.activity_image;
        this.detailVideos = res.data.activity_videos;
        this.loading = false;
      }, error => (this.loading = false));
  }
}
