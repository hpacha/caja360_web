import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatNativeDateModule, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';

import { NgSelectModule } from '@ng-select/ng-select';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { DetailComponent } from './detail/detail.component';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { SharedModule } from '@shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CalendarComponent,
    DetailComponent
  ],
    imports: [
        CommonModule,
        CalendarRoutingModule,
        CarouselModule.forRoot(),
        MatDatepickerModule,
        MatNativeDateModule,
        MatGridListModule,
        NgSelectModule,
        PaginationModule.forRoot(),
        PerfectScrollbarModule,
        SharedModule,
        SpinnerModule,
        FormsModule
    ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        useUtc: true,
        parse: {
          dateInput: ['l', 'LL'],
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        },
      },
    }
  ]
})
export class CalendarModule {}
