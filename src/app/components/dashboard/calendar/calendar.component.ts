import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatCalendar, MatCalendarCellCssClasses } from '@angular/material/datepicker';

import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import * as moment from 'moment';
import 'moment/locale/es';

import { BirthdayComponent } from '@components/modals/birthday/birthday.component';
import { MeetingComponent } from '@components/modals/meeting/meeting.component';
import { ScheduleComponent } from '@components/modals/schedule/schedule.component';
import { AuthenticationService } from '@services/authentication.service';
import { CalendarService } from '@services/calendar.service';
import { HeadquarterService } from '@services/headquarter.service';
import { NewnessActivityService } from '@services/newness-activity.service';
import { MeetingService } from '@services/meeting.service';
import { OAuthService } from '@services/oauth.service';
import { SweetalertService } from '@shared/services/sweetalert.service';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [DatePipe, SlugifyPipe]
} )
export class CalendarComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  @ViewChild( PerfectScrollbarDirective, { static: false } ) perfectScrollbarDirectiveRef?: PerfectScrollbarDirective;
  @ViewChild( MatCalendar ) calendar: MatCalendar<Date>;
  loading: boolean;
  preload: boolean;
  listHeadquarters: [];
  listActivities: [];
  SelectlistActivities: any;
  SelectlistHeadquarter: any;
  listItems: any;
  listItemsFilter: any;
  listItemsTotal: 0;
  listDatesToHighlight: [];
  listMonths = moment.localeData( 'es' ).months();
  selectedHeadquarter: number;
  selectedActivity: number;
  selectedActivityName = 'Todos';
  selectedDate = new Date( new Date() );
  selectedDateView: any;
  showData: Promise<boolean>;

  constructor(
    private headquarterService: HeadquarterService,
    private newnessActivityService: NewnessActivityService,
    private authenticationService: AuthenticationService,
    private calendarService: CalendarService,
    private oauthService: OAuthService,
    private meetingService: MeetingService,
    private sweetalertService: SweetalertService,
    private router: Router,
    private modalService: BsModalService,
    private renderer: Renderer2,
    private slugifyPipe: SlugifyPipe,
    public datepipe: DatePipe
  ) {}

  async ngOnInit() {
    const thisIn = this;
    setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
    const xser = this.authenticationService.getUser();
    this.SelectlistHeadquarter = xser.data.user.headquarter.id;
    this.selectedHeadquarter = xser.data.user.headquarter.id;
    console.log( xser );
    await thisIn.getListHeadquarters();
    await thisIn.getListActivities();
    await thisIn.getItems();
    thisIn.showData = Promise.resolve( true );
    setTimeout( () => { thisIn.globals.setLoadingVisible( false ); } );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  async setSynchronize() {
    const xser = this.authenticationService.getUser();
    const tokenBearer = `Bearer ${xser.data.access_token}`;
    await this.oauthService.signIn();
    if ( this.oauthService.authenticated ) {
      let token = await this.oauthService.getAccessToken();
    }
  }

  getListHeadquarters() {
    return new Promise( ( resolve, reject ) => {
      this.headquarterService.getHeadquarters().then( ( res: any ) => {
        this.listHeadquarters = res;
        // @ts-ignore
        /*this.listHeadquarters.unshift( { id: '', title: 'Todos' } );*/
        resolve();
      } );
    } );
  }

  getListActivities() {
    return new Promise( ( resolve, reject ) => {
      this.newnessActivityService.getActivities().then( ( res: any ) => {
        this.listActivities = res;
        // this.SelectlistActivities = '';
        resolve();
      } );
    } );
  }

  onclickClear(event) {
    console.log(event);
  }

  getItems() {
    return new Promise( ( resolve, reject ) => {
      const thisIn = this;
      thisIn.preload = true;
      const parameters: any = {};
      if ( thisIn.selectedHeadquarter ) {
        parameters.headquarter = thisIn.selectedHeadquarter || this.SelectlistHeadquarter;
      }
      if ( thisIn.selectedActivity ) {
        parameters.activity_type = thisIn.selectedActivity;
      }
      parameters.month = thisIn.datepipe.transform( thisIn.selectedDate, 'L' );
      parameters.year = thisIn.datepipe.transform( thisIn.selectedDate, 'yyyy' );
      thisIn.calendarService.getItems( parameters )
      .pipe(
        finalize( () => {
          this.preload = false;
          setTimeout( function() {
            thisIn.onCalendarView();
            thisIn.dateClass();
          }, 0 );
          resolve();
        } ),
        takeUntil( this.destroyed$ ),
      ).subscribe( ( res: any ) => {
        thisIn.listItems = res.data.data;
        thisIn.listItemsFilter = res.data.data;
        thisIn.listDatesToHighlight = res.data.dates;
        thisIn.listItemsTotal = thisIn.listItems.length;
      }, error => {
        thisIn.listItems = [];
        thisIn.listItemsFilter = [];
        thisIn.listDatesToHighlight = [];
        thisIn.listItemsTotal = 0;
      } );
    } );
  }

  async setHeadquarter( event ) {
    const thisIn = this;
    thisIn.selectedHeadquarter = event.id;
    await thisIn.getItems();
  }

  async setActivity( event ) {
    const thisIn = this;
    thisIn.selectedActivity = ( event.id ) ? event.id : 0;
    thisIn.selectedActivityName = ( event.name ) ? event.name : 'Actividades';
    await thisIn.getItems();
  }

  onCalendarView() {
    const thisIn = this;
    thisIn.calendar.updateTodaysDate();
    thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    const buttonsPrev = document.querySelector( '.mat-calendar-previous-button' );
    const buttonsNext = document.querySelector( '.mat-calendar-next-button' );
    thisIn.renderer.listen( buttonsPrev, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
    thisIn.renderer.listen( buttonsNext, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendar.activeDate.getMonth() ] + ' ' + thisIn.calendar.activeDate.getFullYear();
    } );
  }

  onSelectedChange( event ) {
    this.selectedDate = event;
    var result = event.toLocaleDateString( 'sv-SE', { year: 'numeric', month: '2-digit', day: '2-digit' } );
    this.listItems = this.listItemsFilter.filter( ( el: any ) => {
      return el.date === result;
    } );
    this.listItemsTotal = this.listItems.length;
  }

  onMonthSelected( event ) {
    this.selectedDateView = this.listMonths[ event.getMonth() ] + ' ' + event.getFullYear();
  }

  myDateFilter = ( d: Date ): boolean => {
    const day = d.getDay();
    return day !== -1;
  };

  dateClass() {
    return ( date: Date ): MatCalendarCellCssClasses => {
      const highlightDate = this.listDatesToHighlight
      .map( strDate => new Date( strDate + 'T00:00:00' ) )
      .some( d => d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear() );
      return highlightDate ? 'special-date' : ( new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() ) <= new Date( date.getFullYear(), date.getMonth(), date.getDate() ) ) ? 'future-date' : 'pass-date';
    };
  }

  openSchedule( activity = null ) {
    const thisIn = this;
    const initialState = { activity };
    const modal = thisIn.modalService.show( ScheduleComponent, {
      class: 'ModalSchedule modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      if ( res && res !== 'delete' ) {
        const initialState = { activity: res.data, current: true };
        const modal = thisIn.modalService.show( MeetingComponent, {
          class: 'ModalMeeting modal-dialog-centered',
          initialState,
          backdrop: 'static',
          keyboard: false
        } );
      }
      await thisIn.getItems();
    } );
  }

  openActivity( activity ) {
    if ( activity.action === 'activity' ) {
      this.router.navigate( [`actualidad/actividades/${activity.id}/${this.slugifyPipe.transform( activity.title )}`] );
    } else if ( activity.action === 'meeting' ) {
      const start = activity.start;
      const end = activity.end;
      this.meetingService.getMeeting( activity.id ).subscribe( ( res: any ) => {
        activity = res.data;
        if ( activity.is_editable ) {
          this.openSchedule( activity );
        } else {
          const initialState = { activity, start, end };
          const modal = this.modalService.show( MeetingComponent, {
            class: 'ModalMeeting modal-dialog-centered',
            initialState,
            backdrop: 'static',
            keyboard: false
          } );
        }
      }, error => ( console.log( error ) ) );
    } else {
      const initialState = { activity };
      const modal = this.modalService.show( BirthdayComponent, {
        class: 'ModalBirthday modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      } );
    }
  }

  onDeleteMeeting( event, item ) {
    event.stopPropagation();
    this.sweetalertService
    .sweetConfirm( {
      title: 'Eliminar Reunión',
      text: '¿Estas seguro que deseas eliminar esta reunión?, se desagendará',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!'
    } ).then( ( result ) => {
      if ( result.isConfirmed ) {
        this.deleteMeeting( item.id );
      }
    } );
  }

  deleteMeeting( meetingId ) {
    this.loading = true;
    this.meetingService.deleteMeeting( meetingId ).pipe( takeUntil( this.destroyed$ ) ).subscribe( ( res: any ) => {
      this.getItems();
      this.loading = false;
      this.sweetalertService
      .swal( {
        title: 'Eliminar Reunión',
        text: res.message,
        icon: 'success'
      } );
    }, error => {
      this.loading = false;
      this.sweetalertService
      .swal( {
        title: 'Eliminar Reunión',
        text: error.error.message,
        icon: 'warning'
      } );
    } );
  }
}
