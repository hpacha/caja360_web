import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarComponent } from '@components/dashboard/calendar/calendar.component';
import { DetailComponent } from '@components/dashboard/calendar/detail/detail.component';

const routes: Routes = [
  {path: '', component: CalendarComponent},
  {path: 'detail/:id', component: DetailComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule {}
