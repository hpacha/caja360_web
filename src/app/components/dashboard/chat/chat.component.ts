import { Component, ElementRef, EventEmitter, NgZone, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { ReplaySubject, Subject } from 'rxjs';
import { delay, finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import 'moment/locale/es';
import Swal from 'sweetalert2';

import { DeleteChatComponent } from '@components/modals/chat/delete-chat/delete-chat.component';
import { DeleteGroupComponent } from '@components/modals/chat/delete-group/delete-group.component';
import { EditGroupComponent } from '@components/modals/chat/edit-group/edit-group.component';
import { NewGroupComponent } from '@components/modals/chat/new-group/new-group.component';
import { SharedFilesComponent } from '@components/modals/chat/shared-files/shared-files.component';
import { ChatService } from '@services/chat.service';
import { WebsocketService } from '@services/websocket.service';
import * as Globals from '@utils/globals';
import { AuthenticationService } from '@services/authentication.service';

@Component( {
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  providers: [DatePipe]
} )
export class ChatComponent implements OnInit, OnDestroy {
  currentDay = moment().format( 'DD/MM/YYYY' );
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  loading: boolean;
  preload: boolean = true;
  preloadChat: boolean = true;
  myVar: boolean = false;
  userInfo: any;
  listChatRooms: any;
  listChatRoomsTotal: number;
  roomsCurrentPage = 0;
  roomsLastPage = 0;
  chatRoom: any;
  chatRoomMessage: any[] = [];
  chatRoomMessage2: any[] = [];
  chatLastPage = 0;
  chatCurrentPage = 0;
  chatCurrentHeight: any;
  chatScrollingHeight: any;
  chatScrollButton: boolean = false;
  chatMessagesUnread = 0;
  chatResponse: boolean;
  textMessage: FormControl = new FormControl();
  @Output() closeModal: EventEmitter<any> = new EventEmitter();
  @ViewChild( 'chatPS', { static: false } ) chatPS: PerfectScrollbarComponent;
  @ViewChild( 'roomsPs', { static: false } ) roomsPs: PerfectScrollbarComponent;
  @ViewChild( 'fileInputMessage' ) fileInputMessage: ElementRef;
  formGroupMessage: FormGroup;
  typeAheadListUsers = new EventEmitter<string>();
  destroy$: Subject<boolean> = new Subject<boolean>();
  queryField: FormControl = new FormControl();
  timeout: any = null;
  userId: any;

  constructor(
    private modalService: BsModalService,
    private chatService: ChatService,
    private websocketService: WebsocketService,
    private zone: NgZone,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public datepipe: DatePipe,
    private auth: AuthenticationService
  ) {
    Globals.setChatAdviserVisible( false );
  }

  ngOnInit(): void {
    if ( JSON.parse( localStorage.getItem( 'chatId' ) ) ) {
      console.log( JSON.parse( localStorage.getItem( 'chatId' ) ) );
      const chatId = JSON.parse( localStorage.getItem( 'chatId' ) );
      const parameters: any = {};
      parameters.users = [chatId.id];
      parameters.is_group = false;
      this.chatService.putChatRooms( parameters ).subscribe( ( res: any ) => {
        console.log( 'rrrrrrrrrrrres', res );
        this.onClickChat( res.data );
        this.resetChat();
        localStorage.removeItem( 'chatId' );
      } );
      // JSON.parse(localStorage.getItem('user'))
    }
    if ( this.activatedRoute.snapshot.params[ 'id' ] ) {
      this.openChatId();
    }
    this.userId = this.auth.getUser().data.user.id;
    console.log( this.userId );
    const thisIn = this;
    thisIn.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    thisIn.getListChatRooms();
    thisIn.createFormMessage();
    thisIn.websocketService.connect();
    thisIn.socketsWorked();
  }

  ngOnDestroy() {
    Globals.setChatAdviserVisible( true );
    this.websocketService.disconnect( this.chatRoom, this.userInfo );
  }

  openChatId() {
    const parameters: any = {};
    parameters.users = [this.activatedRoute.snapshot.params[ 'id' ]];
    parameters.is_group = false;
    this.chatService.putChatRooms( parameters ).subscribe( ( res: any ) => {
      this.onClickChat( res.data );
    }, error => {
      console.log( error );
    } );
  }

  socketsWorked() {
    const thisIn = this;
    thisIn.websocketService.getNewRoom( thisIn.userInfo.id ).subscribe( ( newRoom ) => {
      console.log( 'getNewRoom', newRoom );
      thisIn.zone.run( () => {
        newRoom.messages = [];
        newRoom.static_date = moment.utc( newRoom.created_at ).local().format();
        thisIn.listChatRooms.unshift( newRoom );
      } );
    } );
    thisIn.websocketService.getUpdatedRoom( thisIn.userInfo.id ).subscribe( ( newRoom ) => {
      console.log( 'getUpdatedRoom', newRoom );
      thisIn.zone.run( () => {
        const el = document.getElementById( newRoom.id );
        if ( el ) {
          el.remove();
          thisIn.listChatRooms = thisIn.listChatRooms.filter( function( el ) {
            return ( el.id !== newRoom.id );
          } );
        }
        ;
        if ( newRoom.created_by.id === thisIn.userInfo.id ) {
          newRoom.unread_messages = 0;
        }
        newRoom.static_date = moment.utc( newRoom.updated_at ).local().format();
        let newMessage = { 'message': 'Grupo editado', 'updater_at': newRoom.static_date, 'created_by': newRoom.created_by };
        newRoom[ 'messages' ] = [];
        newRoom[ 'messages' ].push( newMessage );
        thisIn.listChatRooms.unshift( newRoom );
      } );
    } );
    thisIn.websocketService.getDeletedRoom( thisIn.userInfo.id ).subscribe( ( newRoom ) => {
      console.log( 'getDeletedRoom', newRoom );
      thisIn.zone.run( () => {
        const el = document.getElementById( newRoom.id );
        if ( el ) {
          el.remove();
          thisIn.listChatRooms = thisIn.listChatRooms.filter( function( el ) {
            return ( el.id !== newRoom.id );
          } );
        }
        ;
      } );
    } );
    thisIn.websocketService.getLeaveRoom( thisIn.userInfo.id ).subscribe( ( newRoom ) => {
      console.log( 'getLeaveRoom', newRoom );
      thisIn.zone.run( () => {
        // newRoom.messages = []
        // thisIn.listChatRooms.push(newRoom);
      } );
    } );
    thisIn.websocketService.getNewMessageRoom( thisIn.userInfo.id ).subscribe( ( newRoom ) => {
      console.log( 'getNewMessageRoom', newRoom );
      thisIn.zone.run( () => {
        const room = newRoom.room;
        const el = document.getElementById( room.id );
        if ( el ) {
          el.remove();
          thisIn.listChatRooms = thisIn.listChatRooms.filter( function( el ) {
            return ( el.id !== room.id );
          } );
        }
        ;
        if ( newRoom.created_by.id === thisIn.userInfo.id ) {
          room.unread_messages = 0;
        }
        room.static_date = moment.utc( newRoom.created_at ).local().format();
        room[ 'messages' ] = [];
        room[ 'messages' ].push( newRoom );
        thisIn.listChatRooms.unshift( room );
      } );
    } );
  }

  getListChatRooms( page = 1 ) {
    const thisIn = this;
    if ( page === 1 ) {
      this.preload = true;
    }
    this.chatService.getChatRooms( page ).pipe(
      delay( 25 ),
      finalize( () => {
        setTimeout( function() {
          thisIn.preloadChat = false;
          thisIn.preload = false;
        }, 0 );
      } ),
    ).subscribe( ( res: any ) => {
      this.listChatRooms = ( this.listChatRooms ) ? ( res.data.data ).concat( this.listChatRooms ) : ( res.data.data );
      this.listChatRooms.sort( function( a, b ) {
        let c = ( a.messages && a.messages[ 0 ] ) ? new Date( a.messages[ 0 ].updater_at ) : new Date( a.updated_at );
        let d = ( b.messages && b.messages[ 0 ] ) ? new Date( b.messages[ 0 ].updater_at ) : new Date( b.updated_at );
        return c < d ? 1 : -1;
      } );
      this.listChatRooms.map( function( a ) {
        if ( a.messages && a.messages[ 0 ] ) {
          a.static_date = moment.utc( a.messages[ 0 ].updater_at ).local().format();
        } else {
          a.static_date = moment.utc( a.updated_at ).local().format();
        }
        return a;
      } );
      this.listChatRoomsTotal = res.data.total;
      this.roomsLastPage = res.data.last_page;
      this.roomsCurrentPage = page;
    }, error => {
      this.preloadChat = false;
      this.preload = false;
    } );
  }

  getSearchChats( queryText ) {
    const thisIn = this;
    if ( thisIn.timeout ) {
      window.clearTimeout( thisIn.timeout );
    }
    thisIn.timeout = window.setTimeout( function() {
      thisIn.timeout = null;
      if ( queryText.length >= 3 ) {
        this.preload = true;
        thisIn.chatService.searchChatRooms( { 'hint': queryText } )
        .takeUntil( thisIn.destroy$ )
        .subscribe( ( res: any ) => {
          thisIn.zone.run( () => {
            thisIn.listChatRooms = ( res.data.data );
            thisIn.listChatRooms.sort( function( a, b ) {
              let c = ( a.messages && a.messages[ 0 ] ) ? new Date( a.messages[ 0 ].updater_at ) : new Date( a.updated_at );
              let d = ( b.messages && b.messages[ 0 ] ) ? new Date( b.messages[ 0 ].updater_at ) : new Date( b.updated_at );
              return c < d ? 1 : -1;
            } );
            thisIn.listChatRooms.map( function( a ) {
              if ( a.messages && a.messages[ 0 ] ) {
                a.static_date = moment.utc( a.messages[ 0 ].updater_at ).local().format();
              } else {
                a.static_date = moment.utc( a.updated_at ).local().format();
              }
              return a;
            } );
            thisIn.listChatRoomsTotal = res.data.total;
            thisIn.preload = false;
          } );
        }, ( err: any ) => {
          thisIn.zone.run( () => {
            thisIn.preload = false;
          } );
        } );
      } else if ( queryText.length === 0 ) {
        thisIn.getListChatRooms();
      }
    }, 250 );
  }

  onScrollEvent( event: any ): void {
    this.chatScrollingHeight = event.target.scrollTop;
    if ( ( this.chatCurrentHeight - this.chatScrollingHeight ) > 60 ) {
      document.getElementById( 'chatScrollButton' ).classList.remove( 'hidden' );
    } else {
      document.getElementById( 'chatScrollButton' ).classList.add( 'hidden' );
    }
  }

  getChatRoomsMessages( itemChatRoom, page = 1 ) {
    if ( itemChatRoom.unread_messages ) {
      this.zone.run( () => {
        this.listChatRooms.map( function( chat ) {
          if ( chat.id === itemChatRoom.id ) {
            chat.unread_messages = 0;
          }
          return chat;
        } );
      } );
    }
    const thisIn = this;
    this.chatService.getChatRoomsMessages( itemChatRoom.id, page ).pipe(
      delay( 25 ),
      finalize( () => {
        thisIn.chatRoom = itemChatRoom;
        setTimeout( function() {
          if ( page === 1 ) {
            thisIn.chatPS.directiveRef.scrollToBottom( 0, 0 );
            thisIn.chatPS.directiveRef.update();
            thisIn.chatCurrentHeight = thisIn.chatPS.directiveRef.position( true ).y;
            thisIn.websocketService.getNewMessage( thisIn.chatRoom ).subscribe( ( newMessage ) => {
              console.log( 'getNewMessage', newMessage );
              if ( newMessage.room.id === thisIn.chatRoom.id ) {
                const exists = thisIn.chatRoomMessage2.find( message => message.id === newMessage.id );
                if ( !exists ) {
                  thisIn.zone.run( () => {
                    thisIn.chatRoomMessage2.push( thisIn.formatMessage( newMessage ) );
                  } );

                  const date = thisIn.chatRoomMessage.find( x => x.date === moment( newMessage.created_at ).format( 'DD/MM/YYYY' ) );
                  if ( date ) {
                    date.messages.push( newMessage );
                  } else {
                    thisIn.chatRoomMessage.push( {
                      date: moment( newMessage.created_at ).format( 'DD/MM/YYYY' ),
                      actualName: ( moment( newMessage.created_at ).format( 'DD/MM/YYYY' ) === thisIn.currentDay ?
                        'Hoy' : moment( newMessage.created_at ).format( 'DD/MM/YYYY' ) ), messages: [newMessage]
                    } );
                  }
                  console.log(thisIn.chatRoomMessage);
                  console.log(thisIn.chatRoomMessage2);
                }
              }
            } );
            thisIn.websocketService.getDeletedMessage( thisIn.chatRoom ).subscribe( ( deletedMessage ) => {
              console.log( 'getDeletedMessage', deletedMessage );
              if ( deletedMessage.room.id === thisIn.chatRoom.id ) {
                thisIn.zone.run( () => {
                  thisIn.chatRoomMessage = thisIn.chatRoomMessage.filter( function( el ) {
                    return ( el.id !== deletedMessage.id );
                  } );
                  thisIn.chatPS.directiveRef.update();
                } );
              }
            } );
          }
          thisIn.preloadChat = false;
        }, 0 );
      } ),
    ).subscribe( ( res: any ) => {
      console.log('subscribe', res);
      this.chatRoomMessage2 = ( this.chatRoomMessage2 ) ? ( res.data.data ).reverse().concat( this.chatRoomMessage2 ) : ( res.data.data ).reverse();
      console.log( this.chatRoomMessage2 );
      console.log( this.currentDay );
      this.chatRoomMessage = [];
      // const messChat = res.data.data ? ( res.data.data ).reverse().concat( res.data.data ) : ( res.data.data ).reverse();
      this.chatRoomMessage2.forEach( message => {
        const date = this.chatRoomMessage.find( x => x.date === moment( message.created_at ).format( 'DD/MM/YYYY' ) );
        if ( date ) {
          date.messages.push( message );
        } else {
          this.chatRoomMessage.push( {
            date: moment( message.created_at ).format( 'DD/MM/YYYY' ),
            actualName: ( moment( message.created_at ).format( 'DD/MM/YYYY' ) === this.currentDay ?
              'Hoy' : moment( message.created_at ).format( 'DD/MM/YYYY' ) ), messages: [message]
          } );
        }
      } );
      console.log( this.chatRoomMessage );
      // this.chatRoomMessage = ( this.chatRoomMessage ) ? ( res.data.data ).reverse().concat( this.chatRoomMessage ) : ( res.data.data ).reverse();
      console.log( 'this.chatRoomMessage', this.chatRoomMessage );
      this.chatLastPage = res.data.last_page;
      this.chatCurrentPage = page;
      this.chatRoomMessage && this.chatRoomMessage.map( function( b ) {
        return thisIn.formatMessage( b );
      } );
    }, error => console.log( error ) );
  }

  onClickChat( item: any ) {
    this.resetChat();
    this.zone.run( () => {
      this.chatRoom = null;
      this.chatRoomMessage = [];
      this.preloadChat = true;
      this.chatResponse = true;
      this.getChatRoomsMessages( item );
    } );
  }

  onClickBack() {
    this.chatRoom = null;
    this.chatResponse = false;
  }

  onClickDeleteChat() {
    const initialState = {
      chatRoom: this.chatRoom
    };
    const modal = this.modalService.show( DeleteChatComponent, {
      class: 'ModalDeleteChat modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: boolean ) => {
      if ( res ) {
        const el = document.getElementById( this.chatRoom.id );
        el.style.opacity = '0';
        setTimeout( function() {el.parentNode.removeChild( el );}, 500 );
        this.chatRoom = null;
      }
    } );
  }

  onClickNewGroup( is_group, chatRoom = null, picture?: string ) {
    const thisIn = this;
    const initialState = {
      chatRoom,
      is_group,
      picture
    };
    const modal = this.modalService.show( NewGroupComponent, {
      class: 'ModalNewGroup modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      if ( res ) {
        this.onClickChat( res );
        if ( chatRoom ) {
          this.resetChat();
        }
      }
    } );
  }

  onClickDeleteGroup() {
    const initialState = {
      chatRoom: this.chatRoom
    };
    const modal = this.modalService.show( DeleteGroupComponent, {
      class: 'ModalDeleteGroup modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: boolean ) => {
      if ( res ) {
        const el = document.getElementById( this.chatRoom.id );
        el.style.opacity = '0';
        setTimeout( function() {el.parentNode.removeChild( el );}, 500 );
        this.chatRoom = null;
      }
    } );
  }

  onClickLeaveGroup() {
    const thisIn = this;
    Swal.fire( {
      title: 'Salir del grupo',
      text: '¿Estás seguro que deseas salir del grupo?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Salir`,
      cancelButtonText: `Cancelar`,
    } ).then( ( result ) => {
      if ( result.isConfirmed ) {
        setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
        thisIn.chatService.leaveChatRooms( thisIn.chatRoom.id )
        .pipe( takeUntil( thisIn.destroyed$ ), finalize( () => {setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );} ) ).subscribe( ( res: any ) => {
          const el = document.getElementById( thisIn.chatRoom.id );
          el.style.opacity = '0';
          setTimeout( function() {el.parentNode.removeChild( el );}, 500 );
          thisIn.chatRoom = null;
          Swal.fire( { title: 'Salir del grupo', text: 'Saliste del grupo exitosamente', icon: 'success', iconHtml: null, timer: 4000 } );
        }, error => {
          Swal.fire( {
            title: 'Salir del grupo', text: 'Ocurrió un error al salir del grupo', icon: 'error', iconHtml: null, timer: 4000
          } );
        } );
      }
    } );
  }

  onClickSharedFiles() {
    const initialState = {
      chatRoom: this.chatRoom
    };
    const modal = this.modalService.show( SharedFilesComponent, {
      class: 'ModalSharedFiles modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickInfoGroup() {
    const thisIn = this;
    const initialState = {
      chatRoom: this.chatRoom,
      userId: this.userId
    };
    const modal = this.modalService.show( EditGroupComponent, {
      class: 'ModalEditGroup modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( res, res.chatRoom, res.picture );
      if ( res && res.chatRoom ) {
        if ( res.picture ) {
          console.log( 'exist' );
          thisIn.onClickNewGroup( true, thisIn.chatRoom, 'picture' );
        } else {
          console.log( 'not exist' );
          thisIn.onClickNewGroup( true, thisIn.chatRoom );
        }
      }
    } );
  }

  deleteMessage( messageItem ) {
    this.chatService.delChatMessages( messageItem.id ).subscribe( ( res: any ) => {
    }, error => console.log( error ) );
  }

  copyMessage( item ) {
    var copyText = ( <HTMLInputElement> document.getElementById( 'message-' + item.id ) );
    var range = document.createRange();
    range.selectNode( copyText );
    window.getSelection().removeAllRanges();
    window.getSelection().addRange( range );
    document.execCommand( 'copy' );
  }

  onScrolledUp() {
    this.zone.run( () => {
      this.chatCurrentPage = this.chatCurrentPage + 1;
      if ( this.chatCurrentPage <= this.chatLastPage ) {
        this.getChatRoomsMessages( this.chatRoom, this.chatCurrentPage );
      }
    } );
  }

  onScrolledDown() {
    const thisIn = this;
    thisIn.zone.run( () => {
      thisIn.listChatRooms.map( function( chat ) {
        if ( chat.id === thisIn.chatRoom.id ) {
          chat.unread_messages = 0;
        }
        return chat;
      } );
    } );
  }

  onScrolledDownRooms() {
    this.zone.run( () => {
      this.roomsCurrentPage = this.roomsCurrentPage + 1;
      if ( this.roomsCurrentPage <= this.roomsLastPage ) {
        this.getListChatRooms( this.roomsCurrentPage );
      }
    } );
  }

  formatMessage( itemFormat ) {
    if ( itemFormat.chat_file && itemFormat.chat_file.mime ) {
      switch ( true ) {
        case ( ( /image/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'image';
          itemFormat.chat_file.footer = false;
        }
          break;
        case ( ( /video/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'video';
          itemFormat.chat_file.footer = false;
        }
          break;
        case ( ( /word/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'icon-word.svg';
          itemFormat.chat_file.footer = true;
        }
          break;
        case ( ( /presentation/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'icon-power.svg';
          itemFormat.chat_file.footer = true;
        }
          break;
        case ( ( /pdf/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'icon-filer.svg';
          itemFormat.chat_file.footer = true;
        }
          break;
        case ( ( /sheet/.exec( itemFormat.chat_file.mime ) ) !== null ): {
          itemFormat.chat_file.icon = 'icon-excel.svg';
          itemFormat.chat_file.footer = true;
        }
          break;
        default: {
          itemFormat.chat_file.icon = 'upload-file.svg';
          itemFormat.chat_file.footer = true;
        }
          break;
      }
    }
    return itemFormat;
  }

  createFormMessage() {
    this.formGroupMessage = this.formBuilder.group( {
      message: [null],
      file: [null],
    } );
  }

  onSendMessage() {
    const thisIn = this;
    if ( thisIn.formGroupMessage.value.message ) {
      const formData = new FormData();
      formData.append( 'message', thisIn.formGroupMessage.value.message );
      formData.append( '_method', 'put' );
      thisIn.formGroupMessage.reset();
      ( <HTMLInputElement> document.getElementById( 'uploadFile' ) ).value = '';
      this.chatService.putChatRoomsMessages( this.chatRoom.id, formData ).subscribe( ( res: any ) => {
      }, error => console.log( error ) );
    }
  }

  onFileChange( fileInput: any ) {
    const thisIn = this;
    if ( fileInput.target.files && fileInput.target.files[ 0 ] ) {
      const myReader = new FileReader();
      myReader.onloadend = ( e ) => {
        let file = <string> myReader.result;
        thisIn.formGroupMessage.get( 'file' ).setValue( file );
        const formData = new FormData();
        formData.append( 'file', thisIn.fileInputMessage.nativeElement.files[ 0 ] );
        formData.append( '_method', 'put' );
        thisIn.formGroupMessage.reset();
        ( <HTMLInputElement> document.getElementById( 'uploadFile' ) ).value = '';
        thisIn.chatService.putChatRoomsMessages( thisIn.chatRoom.id, formData ).subscribe( ( res: any ) => {
        }, error => console.log( error ) );
      };
      myReader.readAsDataURL( fileInput.target.files[ 0 ] );
    }
  }

  resetChat() {
    if ( this.chatRoom ) {
      this.websocketService.leaveNewMessage( this.chatRoom );
    }
    this.myVar = false;
    this.chatRoomMessage = null;
    this.chatRoom = null;
  }

  chatScrollBottom() {
    this.chatPS.directiveRef.scrollToBottom( 0, 0 );
    this.chatPS.directiveRef.update();
  }
}
