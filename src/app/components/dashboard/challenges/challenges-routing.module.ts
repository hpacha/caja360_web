import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChallengesComponent } from '@components/dashboard/challenges/challenges.component';
import { ShareExperiencesComponent } from '@components/dashboard/challenges/share-experiences/share-experiences.component';
import { DiscussionComponent } from '@components/dashboard/challenges/discussion/discussion.component';
import { DiscussionDetailComponent } from '@components/dashboard/challenges/discussion-detail/discussion-detail.component';
import { ReceiveGiveKnowledgeComponent } from '@components/dashboard/challenges/receive-give-knowledge/receive-give-knowledge.component';
import { ForumsTalksConferencesComponent } from '@components/dashboard/challenges/forums-talks-conferences/forums-talks-conferences.component';
import { MeaningfulBoxComponent } from '@components/dashboard/challenges/meaningful-box/meaningful-box.component';

const routes: Routes = [
  { path: '', component: ChallengesComponent },
  { path: 'compartir-experiencias', component: ShareExperiencesComponent },

  { path: 'discusion-de-videos/:id', component: DiscussionComponent },
  { path: 'discusion-de-videos-detail/:id/:idd/:title', component: DiscussionDetailComponent },

  { path: 'discusion-de-lectura/:id', component: DiscussionComponent },
  { path: 'discusion-de-lectura-detail/:id/:idd/:title', component: DiscussionDetailComponent },

  { path: 'trivias/:id', component: DiscussionComponent },
  { path: 'trivias-detail/:id/:idd/:title', component: DiscussionDetailComponent },

  { path: 'recibir-conocimiento/:id', component: ReceiveGiveKnowledgeComponent },
  { path: 'dar-conocimiento/:id', component: ReceiveGiveKnowledgeComponent },

  { path: 'foros-charlas-y-conferencias', component: ForumsTalksConferencesComponent },

  { path: 'caja-con-sentido', component: MeaningfulBoxComponent },
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule]
} )
export class ChallengesRoutingModule {}
