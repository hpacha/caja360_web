import { Component, OnInit } from '@angular/core';
import { ChallengesService } from '@services/challenges.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationService } from '@services/authentication.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';
import { ChallengeValuesComponent } from '@components/modals/challenge-values/challenge-values.component';

@Component( {
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./../challenges.component.scss', './discussion.component.scss'],
  providers: [SlugifyPipe]
} )
export class DiscussionComponent implements OnInit {
  form: FormGroup;
  loading: boolean;
  discussions: any;
  listDiscussions: any;
  listDiscussionsResponse: any;
  page: any;
  discussionId: number;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService,
               private auth: AuthenticationService, private bsModalService: BsModalService,
               private router: Router,
               private activatedRoute: ActivatedRoute,
               private slugifyPipe: SlugifyPipe ) {
    this.createForm();
  }

  async ngOnInit() {
    if ( this.activatedRoute.snapshot.params.id ) {
      console.log( this.activatedRoute.snapshot.params );
      this.discussionId = Number( this.activatedRoute.snapshot.params.id );
      await this.getDiscussions( this.discussionId );
      await this.getListDiscussion( this.discussionId );
    }

  }

  openChallengeValues() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeValuesComponent, {
      class: 'ModalChallengeValues modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickLiked( item: any, e?: any ) {
    console.log( item, e );
    if ( item ) {
      this.form.controls.discussions_id.setValue( item.id );
      this.challengesService.putLikeDiscussion( this.form.value ).subscribe( ( res: any ) => {
        console.log( res );
        this.getListDiscussion( this.page );
      }, error => {
      } );
    }
  }

  onClickDetail( item ) {
    console.log( item );
    if ( item ) {
      if ( this.discussionId === 2 ) {
        this.router.navigate( [`cultura-caja-con-sentido/discusion-de-videos-detail/${this.discussionId}/${item.id}/${this.slugifyPipe.transform( item.title )}`] );

      } else if ( this.discussionId === 3 ) {
        this.router.navigate( [`cultura-caja-con-sentido/discusion-de-lectura-detail/${this.discussionId}/${item.id}/${this.slugifyPipe.transform( item.title )}`] );

      } else {
        this.router.navigate( [`cultura-caja-con-sentido/trivias-detail/${this.discussionId}/${item.id}/${this.slugifyPipe.transform( item.title )}`] );

      }
    }
  }

  pageChanged( e: any ) {
    console.log( e );
    this.page = e.page;
    this.getListDiscussion( this.discussionId, e.page );
  }

  trackByVideoId( index: number, item: any ) {
    return item.id;
  }

  private getDiscussions( id: number ) {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.discussions = res.data.challenges.filter( x => x.id === id );
      this.loading = false;
      console.log( 'getChallenges', this.discussions );
    }, error => {
      this.loading = false;
    } );
  }

  private getListDiscussion( id: number, pageNumber?: any ) {
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    this.challengesService.getDiscussionType( id, parameters ).subscribe( ( res: any ) => {
      console.log( 'getListDiscussion', res );
      this.listDiscussionsResponse = res;
      this.listDiscussions = res.data.data;
      console.log(this.listDiscussionsResponse);
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      discussions_id: [null, null]
    } );
  }

}
