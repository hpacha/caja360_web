import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { AuthenticationService } from '@services/authentication.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-open',
  templateUrl: './open.component.html',
  styleUrls: ['./open.component.scss']
} )
export class OpenComponent implements OnInit {


  @Input() urlPage: any;
  @Input() index: any;
  @Input() totalQuestions: any;
  @Input() questions: any;
  @Input() discussionTypeId: any;
  @Output() back: EventEmitter<any> = new EventEmitter();
  user: any;
  stepquestion = 0;
  questionId: number;
  loading: boolean;
  form: FormGroup;


  constructor( private fb: FormBuilder, private challengesService: ChallengesService,
               private auth: AuthenticationService,
               private bsModalService: BsModalService ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.user = this.auth.getUser()?.data.user;


    if ( this.questions ) {
      if ( this.questions.intent !== 3 && this.questions.reply.is_correct !== true ) {
        this.stepquestion = this.index;
        this.challengesService.stepQuestion.next( {step: this.index} );
      }
      // tslint:disable-next-line:prefer-for-of
      /*for ( let i = 0; i < this.questions.length; i++ ) {
        if (this.questions[ i ].reply.detail === null) {
          this.stepquestion = i;
          this.questionId = this.questions[ i ].id;
          break;
        }
      }*/
    }
  }

  onClickBack() {
    this.back.emit(false);
  }

  onClickVerify( index: number, type?: string ) {
    // return console.log(this.answer);
    // console.log( this.stepquestion, type );
    if ( this.form.valid ) {
      this.loading = true;
      this.challengesService.postCheckQuestion( this.form.value ).subscribe( ( res: any ) => {
        // console.log( res );
        this.loading = false;
        // this.challengesService.isCorrect.next( res.data );
        if ( type === 'next' && this.stepquestion < this.totalQuestions) {
          this.stepquestion = this.stepquestion + 1;
          this.challengesService.stepQuestion.next( {step: this.stepquestion} );
        }
        // console.log( this.stepquestion );
        this.form.controls.detail.reset();
        if ( type === 'finalize' ) {
          this.showChallengeMessage( res.data );
        }
      }, error => this.loading = false );
    }
  }

  showChallengeMessage( data: any, ) {
    const initialState = { user: this.user, data, urlPage: this.urlPage };
    const modal = this.bsModalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      question_id: [this.questionId ? this.questionId : null, Validators.required],
      detail: [null, Validators.required],
      type: [3, Validators.required],
    } );

    this.form.valueChanges.subscribe( ( res: any ) => {
      // console.log( this.form.value );
      // this.challengesService.option.next( this.form.value );
    } );
  }

}
