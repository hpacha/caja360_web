import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { AuthenticationService } from '@services/authentication.service';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.scss']
} )
export class SimpleComponent implements OnInit {
  @Input() urlPage: any;
  @Input() questions: any;
  @Input() index: any;
  @Input() totalQuestions: any;
  @Input() discussionTypeId: any;
  @Output() back: EventEmitter<any> = new EventEmitter();
  user: any;
  stepquestion = 0;
  loading: boolean;
  form: FormGroup;

  isCorrect: any;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService,
               private auth: AuthenticationService,
               private bsModalService: BsModalService ) {
  }

  ngOnInit(): void {
    this.user = this.auth.getUser()?.data.user;
    this.createForm();
    console.log(this.questions);
    if ( this.questions ) {
      if ( this.questions.intent !== 3 && this.questions.reply.is_correct !== true ) {
        this.stepquestion = this.index + 1;
        this.challengesService.stepQuestion.next( {step: this.index} );
      }
      /*// tslint:disable-next-line:prefer-for-of
      for ( let i = 0; i < this.questions.length; i++ ) {
        if ( this.questions[ i ].intent !== 3 && this.questions[ i ].reply.is_correct !== true ) {
          this.stepquestion = i + 1;
          this.challengesService.stepQuestion.next( {step: i} );
          break;
        }
      }*/
    }
  }
  onClickBack() {
    this.back.emit(false);
  }

  onClickVerify() {
    // console.log( this.stepquestion );
    if ( this.form.valid ) {
      this.loading = true;
      this.isCorrect = null;
      this.challengesService.postCheckQuestion( this.form.value ).subscribe( ( res: any ) => {
        // console.log( res );
        this.loading = false;
        this.isCorrect = res.data;
        this.renderAnswer( res.data );
        /*if (res.data.is_correct) {
          this.challengesService.stepQuestion.next( {step: this.stepquestion} );
        }*/
      }, error => this.loading = false );
    }
  }

  onClickFinalize() {
    this.loading = true;
    this.showChallengeMessage( this.isCorrect );
    this.loading = false;
  }

  showChallengeMessage( data: any, ) {
    const initialState = { user: this.user, data, urlPage: this.urlPage };
    const modal = this.bsModalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickNext( index: number ) {
    this.challengesService.stepQuestion.next( {step: this.stepquestion} );
    this.stepquestion = this.stepquestion + 1;
    this.isCorrect = null;
  }

  onClickTryAgain( items ) {
    this.form.controls.item_id.reset();
    this.form.controls.question_id.reset();
    this.isCorrect = null;

    // tslint:disable-next-line:prefer-for-of
    for ( let i = 0; i < items.length; i++ ) {
      document.getElementById( items[ i ].id ).classList.remove( 'active' );
      document.getElementById( items[ i ].id ).classList.remove( 'is-correct' );
      document.getElementById( items[ i ].id ).classList.remove( 'is-incorrect' );
    }
  }

  renderAnswer( values ) {
    // console.log( values );
    if ( values && values.item && values.item[ 0 ] ) {
      // console.log( values.item && values.item[ 0 ] );
      if ( values.item[ 0 ].question_id === values.id ) {
        // console.log( values.is_correct ? 'is-correct' : 'is-incorrect' );
        if ( values.is_correct === true ) {
          document.getElementById( values.item[ 0 ].id ).classList.add( 'is-correct' );
        } else {
          document.getElementById( values.item[ 0 ].id ).classList.add( 'is-incorrect' );
        }
      }
      if ( values.intent === 3 && values.item[ 1 ] && values.item[ 1 ].question_id === values.id ) {
        if ( values.is_correct ) {
          if ( values.item[ 0 ].is_correct === true ) {
            document.getElementById( values.item[ 0 ].id ).classList.add( 'is-correct' );
          } else {
            document.getElementById( values.item[ 0 ].id ).classList.add( 'is-incorrect' );
          }
        } else {
          if ( values.item[ 1 ].is_correct === true ) {
            document.getElementById( values.item[ 1 ].id ).classList.add( 'is-correct' );
          } else {
            document.getElementById( values.item[ 1 ].id ).classList.add( 'is-incorrect' );
          }
        }
      } else {
        if ( values.is_correct ) {
          if ( values.item[ 0 ].is_correct === true ) {
            document.getElementById( values.item[ 0 ].id ).classList.add( 'is-correct' );
          } else {
            document.getElementById( values.item[ 0 ].id ).classList.add( 'is-incorrect' );
          }
        }
      }
    }
  }

  onClickSelect( item ) {
    this.form.patchValue( {
      item_id: [item.id],
      question_id: item.question_id
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      item_id: [null, Validators.required],
      question_id: [null, Validators.required],
      type: [1, Validators.required],
    } );
  }

}
