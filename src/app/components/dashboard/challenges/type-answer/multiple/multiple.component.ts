import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';
import { AuthenticationService } from '@services/authentication.service';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component( {
    selector: 'app-multiple',
    templateUrl: './multiple.component.html',
    styleUrls: ['./multiple.component.scss']
} )
export class MultipleComponent implements OnInit {
    @Input() urlPage: any;
    @Input() questions: any;
    @Input() index: any;
    @Input() totalQuestions: any;
    @Input() discussionTypeId: any;
    @Output() back: EventEmitter<any> = new EventEmitter();
    user: any;
    stepquestion = 0;
    loading: boolean;
    form: FormGroup;

    isCorrect: any;

    answerList: any;
    arrQuestions: any[] = [];
    arrReply: any[] = [];

    constructor( private fb: FormBuilder, private auth: AuthenticationService,
                 private challengesService: ChallengesService,
                 private bsModalService: BsModalService ) {

    }

    ngOnInit(): void {
        this.user = this.auth.getUser()?.data.user;
        this.createForm();
        console.log( this.questions );
        if ( this.questions ) {
            if ( this.questions.intent !== 3 && this.questions.reply.is_correct !== true ) {
                this.stepquestion = this.index + 1;
                this.challengesService.stepQuestion.next( { step: this.index } );
            }
            /* // tslint:disable-next-line:prefer-for-of
             for ( let i = 0; i < this.questions.length; i++ ) {
             if ( this.questions[ i ].intent !== 3 && this.questions[ i ].reply.is_correct !== true ) {
             this.stepquestion = i + 1;
             break;
             }
             }*/
        }
    }

    onClickBack() {
        this.back.emit( false );
    }

    onClickVerify() {
        // console.log( this.stepquestion );
        if ( this.form.valid ) {
            this.loading = true;
            this.isCorrect = null;
            this.challengesService.postCheckQuestion( this.form.value ).subscribe( ( res: any ) => {
                // console.log( res );
                this.loading = false;
                this.isCorrect = res.data;
                this.renderAnswer( res.data );
                this.arrQuestions = [];
                /* if (res.data.is_correct) {
                 this.challengesService.stepQuestion.next( {step: this.stepquestion} );
                 }*/
                /* this.challengesService.isCorrect.next( res.data );*/
            }, error => this.loading = false );
        }
    }

    onClickNext( index: number ) {
        this.challengesService.stepQuestion.next( { step: this.stepquestion } );
        this.stepquestion = this.stepquestion + 1;
        this.isCorrect = null;
        this.form.controls.item_id.setValue( null );
    }

    onClickTryAgain( items ) {
        this.form.controls.item_id.reset();
        this.form.controls.question_id.reset();
        this.isCorrect = null;

        // tslint:disable-next-line:prefer-for-of
        for ( let i = 0; i < items.length; i++ ) {
            document.getElementById( items[ i ].id ).classList.remove( 'active' );
            document.getElementById( items[ i ].id ).classList.remove( 'is-correct' );
            document.getElementById( items[ i ].id ).classList.remove( 'is-incorrect' );
        }
    }

    onClickFinalize() {
        this.loading = true;
        this.showChallengeMessage( this.isCorrect );
        this.loading = false;
    }

    showChallengeMessage( data: any, ) {
        const initialState = { user: this.user, data, urlPage: this.urlPage };
        const modal = this.bsModalService.show( ChallengeMessageComponent, {
            class: 'ModalChallengeMessage modal-dialog-centered',
            initialState,
            backdrop: 'static',
            keyboard: false
        } );
    }


    selectQuestions( id: number, questionId: number, countSelect?: number ) {
        console.log( id, questionId, countSelect );
        this.form.controls.question_id.setValue( questionId );
        const i = this.arrQuestions.indexOf( id );
        // console.log( this.arrQuestions.length );
        if ( i === -1 ) {
            if ( this.arrQuestions.length < countSelect ) {
                this.arrQuestions.push( id );
            }
            // console.log( 'push', this.arrQuestions );
            this.form.controls.item_id.setValue( this.arrQuestions.length === countSelect ? this.arrQuestions : null );
        } else {
            this.arrQuestions.splice( i, 1 );
            // console.log( 'splice', this.arrQuestions );
            this.form.controls.item_id.setValue( this.arrQuestions.length === countSelect ? this.arrQuestions : null );
        }
    }

    renderAnswer( values ) {
        // console.log( values );
        if ( values && values.item && values.item[ 0 ] ) {
            // tslint:disable-next-line:prefer-for-of
            for ( let i = 0; i < values.item.length; i++ ) {
                if ( i !== 2 ) {
                    if ( values.item[ i ].question_id === values.id ) {
                        if ( values.item[ i ].is_correct === true ) {
                            document.getElementById( values.item[ i ].id ).classList.add( 'is-correct' );
                        } else {
                            document.getElementById( values.item[ i ].id ).classList.add( 'is-incorrect' );
                        }
                    }
                }
            }
        }
        if ( values && values.intent === 3 ) {
            console.log( 'entro 2' );
            if ( values && values.item && values.item[ 0 ] ) {
                // console.log( values.item && values.item[ 0 ] );
                // tslint:disable-next-line:prefer-for-of
                for ( let i = 0; i < values.item.length; i++ ) {
                    if ( values.item[ i ].question_id === values.id ) {
                        if ( values.item[ i ].is_correct === true ) {
                            document.getElementById( values.item[ i ].id ).classList.add( 'is-correct' );
                        } /*else {
                            document.getElementById( values.item[ i ].id ).classList.add( 'is-incorrect' );
                        }*/
                    }
                }
            }
        }
    }

    createForm() {
        this.form = this.fb.group( {
            item_id: [this.arrQuestions.length === 3 ? this.arrQuestions : null, Validators.required],
            question_id: [null, Validators.required],
            type: [2, Validators.required],
        } );

        this.form.valueChanges.subscribe( ( res: any ) => {
            console.log( this.form.value );
        } );
    }
}
