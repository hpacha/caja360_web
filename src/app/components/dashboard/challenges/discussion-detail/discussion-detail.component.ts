import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Globals from '@utils/globals';
import { ReplaySubject, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';
import { AuthenticationService } from '@services/authentication.service';
import { finalize, takeUntil } from 'rxjs/operators';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-discussion-detail',
  templateUrl: './discussion-detail.component.html',
  styleUrls: ['./../challenges.component.scss', './discussion-detail.component.scss']
} )
export class DiscussionDetailComponent implements OnInit, OnDestroy {
  globals = Globals;

  sub: Subscription;
  sub2: Subscription;
  loading: boolean;
  loading2: boolean;
  loading3: boolean;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  discussionTypeId: number;
  detailDiscussion: any;
  detailDiscussionId: any;

  form: FormGroup;
  formQuestions: FormGroup;
  user: any;
  questions = [];
  arrQuestions: any[] = [];
  stepquestion = 0;
  activeForm: boolean;
  answer: any;
  isCorrect: any;
  start: boolean;
  disabled: boolean;
  urlPage: string;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService, private router: Router,
               private activatedRoute: ActivatedRoute, private auth: AuthenticationService, private bsModalService: BsModalService ) {
    this.createForm();
    this.createFormQuestions();
  }

  async ngOnInit() {
    if ( this.activatedRoute.snapshot.params.id ) {
      console.log( 'DiscussionDetailComponent', this.activatedRoute.snapshot.url[ 0 ].path );
      this.urlPage = this.activatedRoute.snapshot.url[ 0 ].path;
      this.discussionTypeId = Number( this.activatedRoute.snapshot.params.id );
      this.detailDiscussionId = Number( this.activatedRoute.snapshot.params.idd );
      console.log( this.discussionTypeId );
      await this.getDiscussionId( this.detailDiscussionId );
      await this.getQuestions();
    }
    this.user = this.auth.getUser()?.data.user;
    console.log( this.isCorrect );
    /* this.showChallengeMessage({}, this.urlPage); */

    this.sub = this.challengesService.option.asObservable().subscribe( values => {
      console.log( 'option', values );
      if ( values ) {
        this.activeForm = true;
        this.answer = values;
      } else {
        this.activeForm = false;
      }
    } );
    this.sub2 = this.challengesService.stepQuestion.asObservable().subscribe( values => {
      console.log( 'stepQuestion', values );
      this.stepquestion = values.step;
    } );
    console.log( this.stepquestion );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
  }

  selectQuestions( id: number ) {
    const i = this.arrQuestions.indexOf( id );
    console.log( this.arrQuestions.length );
    if ( i === -1 ) {
      if ( this.arrQuestions.length < 2 ) {
        this.arrQuestions.push( id );
      }
      console.log( 'push', this.arrQuestions.length );
      this.formQuestions.controls.questions.setValue( this.arrQuestions );
    } else {
      this.arrQuestions.splice( i, 1 );
      console.log( 'splice', this.arrQuestions.length );
      this.formQuestions.controls.questions.setValue( this.arrQuestions );
    }
  }

  onEventBack( event ) {
    console.log( event );
    if ( !event ) {
      this.start = false;
    }
  }

  onClickStart() {
    this.start = true;
  }

  onClickVerify( index?: number, type?: string ) {
    // return console.log(this.answer);
    if ( this.answer ) {
      this.loading2 = true;
      this.isCorrect = null;
      this.challengesService.postCheckQuestion( this.answer ).subscribe( ( res: any ) => {
        console.log( res );
        this.loading2 = false;
        this.isCorrect = res.data;
        this.challengesService.isCorrect.next( res.data );
        if ( type === 'open' ) {
          this.stepquestion = index + 1;
          this.isCorrect = null;
        }
        if ( type === 'finalize' ) {
          this.showChallengeMessage( res.data );
        }
      }, error => this.loading2 = false );
    }
  }

  onTryAgain() {
    this.isCorrect = null;
  }

  onClickNext( index: number ) {
    this.stepquestion = index + 1;
    this.isCorrect = null;
  }

  onClickFinalize() {
    this.loading3 = true;
    this.showChallengeMessage( this.isCorrect );
    this.loading3 = false;
    /* this.challengesService.getFinalizeChallenge( this.detailDiscussionId ).subscribe( ( res: any ) => {
     console.log( res );
     this.loading3 = false;
     }, error => this.loading3 = false );*/
  }

  onClickBack() {
    if ( this.discussionTypeId === 2 ) {
      this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-videos/2' );
    } else if ( this.discussionTypeId === 3 ) {
      this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-lectura/3' );
    } else {
      this.router.navigateByUrl( '/cultura-caja-con-sentido/trivias/4' );
    }
    //
  }

  showChallengeMessage( data: any, ) {
    const initialState = { user: this.user, data, urlPage: this.urlPage };
    const modal = this.bsModalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  sendMessage() {
    if ( this.form.controls.comments.value.trim().length >= 1 ) {
      setTimeout( () => { this.globals.setLoadingVisible( true ); } );
      if ( this.form.valid ) {
        const params: any = {};
        params.discussions_id = this.detailDiscussion.id;
        params.comments = this.form.value.comments;
        this.challengesService.postSaveCommentDiscussions( params ).pipe(
          takeUntil( this.destroyed$ ),
          finalize( () => {
            setTimeout( () => { this.globals.setLoadingVisible( false ); } );
          } ),
        ).subscribe( async ( res: any ) => {
            console.log( res );
            this.getDiscussionId( this.detailDiscussion.id );
            this.form.controls.comments.reset();
          },
          error => {
            this.form.controls.comments.reset();
          } );
      }
    }
  }

  onClickLiked( item: any, e?: any ) {
    console.log( item, e );
    if ( item ) {
      this.form.controls.discussions_id.setValue( item.id );
      this.challengesService.putLikeDiscussion( { discussions_id: this.form.value.discussions_id } ).subscribe( ( res: any ) => {
        console.log( res );
        this.getDiscussionId( this.detailDiscussion.id );
      }, error => {
      } );
    }
  }

  getDiscussionId( id ) {
    return new Promise( ( resolve, reject ) => {
      this.challengesService.getDiscussionId( id )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        console.log( res );
        this.detailDiscussion = res.data;
        console.log( this.detailDiscussion );
      }, error => {
        this.detailDiscussion = [];
      } );
    } );
  }

  getQuestions() {
    this.challengesService.getQuestions( this.detailDiscussionId ).subscribe( ( res: any ) => {
      console.log( res.data );
      this.questions = res.data;
      if ( this.questions ) {
        // tslint:disable-next-line:prefer-for-of
        for ( let i = 0; i < this.questions.length; i++ ) {
          if ( this.questions[ i ].intent !== 3 && this.questions[ i ].reply.is_correct !== true ) {
            this.stepquestion = i;
            console.log(this.stepquestion);
            this.challengesService.stepQuestion.next( { step: i } );
            break;
          }
        }
      }

      console.log(  this.stepquestion );
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      comments: [null, Validators.required],
      discussions_id: [null, null]
    } );
  }

  createFormQuestions() {
    this.formQuestions = this.fb.group( {
      questions: [this.arrQuestions, Validators.required],
    } );

    this.formQuestions.valueChanges.subscribe( ( res: any ) => {
      console.log( this.formQuestions.value );
    } );
  }

}
