import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { BsModalRef, ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { ChallengesRoutingModule } from './challenges-routing.module';
import { ChallengesComponent } from './challenges.component';
import { ServicesModule } from '@services/services.module';
import { SharedModule } from '@shared/shared.module';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { ShareExperiencesComponent } from './share-experiences/share-experiences.component';
import { CommentsComponent } from './common/comments/comments.component';
import { ExperienceComponent } from './common/experience/experience.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { DiscussionDetailComponent } from './discussion-detail/discussion-detail.component';
import { ReceiveGiveKnowledgeComponent } from './receive-give-knowledge/receive-give-knowledge.component';
import { CardKnowledgeComponent } from './common/card-knowledge/card-knowledge.component';
import { MeaningfulBoxComponent } from './meaningful-box/meaningful-box.component';
import { ForumsTalksConferencesComponent } from './forums-talks-conferences/forums-talks-conferences.component';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { SimpleComponent } from './type-answer/simple/simple.component';
import { MultipleComponent } from './type-answer/multiple/multiple.component';
import { OpenComponent } from './type-answer/open/open.component';

@NgModule({
  declarations: [
    ChallengesComponent,
    ShareExperiencesComponent,
    CommentsComponent,
    ExperienceComponent,
    DiscussionComponent,
    DiscussionDetailComponent,
    CardKnowledgeComponent,
    ReceiveGiveKnowledgeComponent,
    MeaningfulBoxComponent,
    ForumsTalksConferencesComponent,
    SimpleComponent,
    MultipleComponent,
    OpenComponent
  ],
  imports: [
    ChallengesRoutingModule,
    CarouselModule.forRoot(),
    CommonModule,
    HttpClientModule,
    FormsModule,
    LazyLoadImageModule,
    ModalModule.forRoot(),
    PerfectScrollbarModule,
    PaginationModule.forRoot(),
    ReactiveFormsModule,
    ServicesModule,
    SharedModule,
    SpinnerModule,
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
  ],
  providers: [ BsModalRef]
})
export class ChallengesModule {}
