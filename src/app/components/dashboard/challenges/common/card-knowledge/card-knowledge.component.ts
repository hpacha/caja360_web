import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card-knowledge',
  templateUrl: './card-knowledge.component.html',
  styleUrls: ['./card-knowledge.component.scss']
})
export class CardKnowledgeComponent implements OnInit {
  @Input() item: any;
  @Input() active: any;
  @Output() showdetail: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
    // console.log(this.item);
  }

  onClickDetail() {
    this.showdetail.emit( true );
  }

}
