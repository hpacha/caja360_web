import { Component, Input, OnInit } from '@angular/core';
import { ChallengesService } from '@services/challenges.service';
import { NewsDeleteCommentComponent } from '@components/modals/news-delete-comment/news-delete-comment.component';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
} )
export class CommentsComponent implements OnInit {
  @Input() expId: any;
  @Input() userId: any;
  @Input() item: any;

  constructor( private challengesService: ChallengesService,
               private modalService: BsModalService, ) { }

  ngOnInit(): void {
    // console.log(this.item);
    console.log(this.item);
    console.log(this.expId);
  }

  onClickDelete( id: number ) {
    // this.challengesService.eventComment.next( id );

    const initialState = { deleteComment: true, idComment: id, expId: this.expId };
    const modal = this.modalService.show( NewsDeleteCommentComponent, {
      class: 'ModalDeleteComment modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: boolean ) => {
      if ( res ) {
        console.log(res);
        this.challengesService.eventComment.next( {value: true, expId: this.expId} );
      }
    } );
  }
}
