import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
} )
export class ExperienceComponent implements OnInit {
  form: FormGroup;
  @Input() item: any;
  @Output() showdetail: EventEmitter<any> = new EventEmitter();
  @Input() active: any;
  @Input() own: any;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService ) {
    this.createForm();
  }

  ngOnInit(): void {

  }

  onClickDetail() {
    this.showdetail.emit( true );
  }

  onClickLiked( item: any, e: any ) {
    console.log( 'onClickLiked-hijo', item, e );
    this.challengesService.eventLiked.next( item );
  }

  private createForm() {
    this.form = this.fb.group( {
      experiences_id: [null, null],
    } );
  }

}
