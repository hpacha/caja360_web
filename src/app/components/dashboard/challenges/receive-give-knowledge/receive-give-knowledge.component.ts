import { Component, OnInit } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import * as Globals from '@utils/globals';
import { ChallengesService } from '@services/challenges.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ChallengeValuesComponent } from '@components/modals/challenge-values/challenge-values.component';
import { ActivatedRoute } from '@angular/router';
import { ChallengeChooseKnowledgeComponent } from '@components/modals/challenge-choose-knowledge/challenge-choose-knowledge.component';
import { ChallengeGiveKnowledgeComponent } from '@components/modals/challenge-give-knowledge/challenge-give-knowledge.component';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { AuthenticationService } from '@services/authentication.service';

@Component( {
  selector: 'app-receive-give-knowledge',
  templateUrl: './receive-give-knowledge.component.html',
  styleUrls: ['./receive-give-knowledge.component.scss']
} )
export class ReceiveGiveKnowledgeComponent implements OnInit {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  loading: boolean;
  knowledge: any;
  knowledgeDetail: any;
  listKnowledge: any;
  listKnowledgeResponse: any;
  type: any;
  page: number;
  active: any;
  receivegiveId: number;
  urlPage: any;
  user: any;

  constructor( private challengesService: ChallengesService, private auth: AuthenticationService,
               private bsModalService: BsModalService,
               private activatedRoute: ActivatedRoute, public bsModalRef: BsModalRef ) { }

  async ngOnInit() {
    this.user = this.auth.getUser()?.data.user;
    if ( this.activatedRoute.snapshot.params.id ) {
      console.log( this.activatedRoute.snapshot.params );
      this.receivegiveId = Number( this.activatedRoute.snapshot.params.id );
      this.urlPage = this.activatedRoute.snapshot.url[ 0 ].path;
      console.log( this.urlPage );
      await this.getKnowledge( this.receivegiveId );
      await this.getListKnowledge();
    }

  }

  onClickBack() {
    this.knowledgeDetail = null;
  }

  openModalChallengeChooseKnowledge() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeChooseKnowledgeComponent, {
      class: 'ModalChallengeChoose fadeIn modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( 'res', res );
      if ( res.value === true ) {
        setTimeout( () => {
          this.openModalChallengeGiveKnowledge( res.item );
        }, 1100 );
      }
    } );
  }

  openModalChallengeGiveKnowledge( item ) {
    const initialState = {
      // urlPage: this.urlPage,
      user: item
    };
    this.bsModalRef.hide();
    const modal = this.bsModalService.show( ChallengeGiveKnowledgeComponent, {
      class: 'ModalChallengeGive modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( 'res', res );
      if ( res.value === true ) {
        setTimeout( () => {
          this.showChallengeMessage( res.data );
        }, 1200 );
      }
    } );
  }

  showChallengeMessage( data: any, ) {
    const initialState = { user: this.user, data, urlPage: this.urlPage };
    const modal = this.bsModalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( 'res', res );
      if ( res) {
        setTimeout( () => {
          this.getKnowledge( this.receivegiveId );
        }, 1000 );
      }
    } );
  }

  onShowDetail( event, id: number ) {
    console.log( 'click', event, id );
    if ( id ) {
      this.onClickDetail( id );
      this.active = { id, value: true };
    }
  }

  onClickDetail( id: number ) {
    this.loading = true;
    this.challengesService.getKnowledgeId( id ).subscribe( ( res: any ) => {
      this.loading = false;
      console.log( res );
      this.knowledgeDetail = res.data;
      console.log( this.knowledgeDetail );
    }, error => {this.loading = false;} );
  }

  openChallengeValues() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeValuesComponent, {
      class: 'ModalChallengeValues modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  trackById( index: number, item: any ) {
    return item.id;
  }

  pageChanged( e: any ) {
    this.page = e.page;
    this.getListKnowledge( e.page );
  }

  private getListKnowledge( pageNumber?: any ) {
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    if ( this.receivegiveId === 5 ) {
      this.challengesService.getKnowledgeReceive( parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.listKnowledgeResponse = res.data;
        this.listKnowledge = res.data.data;
        console.log( this.listKnowledgeResponse );
      } );
    } else {
      this.challengesService.getKnowledgeGive( parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.listKnowledgeResponse = res.data;
        this.listKnowledge = res.data.data;
        console.log( this.listKnowledgeResponse );
      } );
    }

  }

  private getKnowledge( id: number ) {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.knowledge = res.data.challenges.filter( x => x.id === id );
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

}
