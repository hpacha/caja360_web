import { Component, EventEmitter, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';

import { ChallengeInfoComponent } from '@components/modals/challenge-info/challenge-info.component';
import { NewnessActivityService } from '@services/newness-activity.service';
import { NewnessNewsService } from '@services/newness-news.service';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';
import * as Globals from '@utils/globals';
import { ChallengeCategorieComponent } from '@components/modals/challenge-categorie/challenge-categorie.component';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-news',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.scss'],
  providers: [SlugifyPipe],
  encapsulation: ViewEncapsulation.None
} )
export class ChallengesComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  preload: boolean;
  preloadPage = false;
  defaultImage: string;
  form: FormGroup;
  activities: any;
  activitiesResponse: any;
  news: any;
  newsResponse: any;
  listNews: Array<any> = [];
  listNewsCurrentPage = 1;
  listNewsLastPage = 0;
  listActivities: Array<any> = [];
  listActivitiesCurrentPage = 1;
  listActivitiesLastPage = 0;
  loading: boolean;
  newnessService: any;
  newnessType = 'activity';
  typeAheadListUsers = new EventEmitter<string>();
  queryField: FormControl = new FormControl();
  timeout: any = null;

  progress = {};
  myProgress = [];
  myProgress2 = [
    {
      id: 1, name: 'Certificado', text: 'Acumula 300 puntos para ganar 1 estrella y certificarte',
      image: './assets/img/svg/star-cup.svg',
      description: 'Lorem ipsum dolor sit amet, conetur sadipscing elitr, sed diam nonumy en eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam. At vero eos et accusam et a justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.  Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.'
    },
    {
      id: 2, name: 'Lorem ipsum dolor', text: 'Acumula 200 puntos para ganar 1 estrella y certificarte',
      image: './assets/img/svg/trophy.svg',
      description: 'Lorem ipsum dolor sit amet, conetur sadipscing elitr, sed diam nonumy en eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam. At vero eos et accusam et a justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.  Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.'
    },
    {
      id: 3, name: 'Embajadores de cultura', text: 'Acumula 100 puntos para ganar 1 estrella y certificarte',
      image: './assets/img/svg/icon-achievement.svg',
      description: 'Lorem ipsum dolor sit amet, conetur sadipscing elitr, sed diam nonumy en eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam. At vero eos et accusam et a justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.  Stet clita kasd gubergren, no sea takimata sactus est Lorem ipsum dolor sit amet.'
    }
  ];
  categories = [];
  categories2 = [
    {
      id: 1, router: '/cultura-caja-con-sentido/compartir-experiencias',
      modalText: 'Lee e interactúa con las experiencias de tus compañeros. Gana 15 pts por cada experiencia que compartas.',
      name: 'Compartir experiencias', points: '15', image: './assets/img/svg/a-turquoise.svg'
    },
    { id: 2, router: '/cultura-caja-con-sentido', modalText: 'Lee', name: 'Discusión de videos', points: '0', image: './assets/img/svg/a-coral.svg' },
    {
      id: 3, router: '/cultura-caja-con-sentido', modalText: 'Lee', name: 'Discusión de lecturas', points: '0', image: './assets/img/svg/a-lightgreen.svg'
    },
    { id: 4, router: '/cultura-caja-con-sentido', modalText: '', name: 'Trivias', points: '0', image: './assets/img/svg/a-orange.svg' },
    { id: 5, router: '/cultura-caja-con-sentido', modalText: '', name: 'Recibir reconocimiento', points: '0', image: './assets/img/svg/a-red.svg' },
    { id: 6, router: '/cultura-caja-con-sentido', modalText: '', name: 'Dar reconocimiento', points: '0', image: './assets/img/svg/a-lightblue.svg' },
    { id: 7, router: '/cultura-caja-con-sentido', modalText: '', name: 'Caja con Sentido', points: '0', image: './assets/img/svg/a-blue.svg' },
    {
      id: 8, router: '/cultura-caja-con-sentido', modalText: '', name: 'Foros, charlas y conferencias', points: '0', image: './assets/img/svg/a-green.svg'
    },
  ];

  constructor(
    private zone: NgZone,
    private bsModalService: BsModalService,
    private newnessActivityService: NewnessActivityService,
    private newnessNewsService: NewnessNewsService,
    private slugifyPipe: SlugifyPipe,
    private router: Router,
    private challengesService: ChallengesService
  ) {}

  async ngOnInit() {
    this.getListChallenges();
    // setTimeout(() => { this.globals.setLoadingVisible(true); });
    // this.defaultImage = './assets/img/placeholder-h.png';
    // this.newnessService = this.newnessType === 'activity' ? this.newnessActivityService : this.newnessNewsService;
    // await this.getListActivities();
    // await this.getListNews();
    // setTimeout(() => { this.globals.setLoadingVisible(false); });
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  /*change( event ) {
   const input = document.getElementById( 'input' );
   console.log( input );
   this.progress = 20;
   }*/

  openChallengeInfo( challengeItem ) {
    const initialState = { challengeItem };
    const modal = this.bsModalService.show( ChallengeInfoComponent, {
      class: 'ModalChallengeInfo modal-dialog-centered',
      initialState,
      backdrop: 'static',
      // keyboard: false
    } );
  }

  openChallengeCategorie( challengeCategorie, color: string ) {
    console.log( challengeCategorie );
    if ( challengeCategorie.modal === true ) {
      const initialState = { challengeCategorie, color };
      const modal = this.bsModalService.show( ChallengeCategorieComponent, {
        class: 'ModalChallengeCategorie modal-dialog-centered',
        initialState,
        backdrop: 'static',
        // keyboard: false
      } );
    } else {
      switch ( color ) {
        case 'turquoise':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/compartir-experiencias' );
        case 'coral':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-videos/2' );
        case 'lightgreen':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-lectura/3' );
        case 'orange':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/trivias/4' );
        case 'red':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/recibir-conocimiento/5' );
        case 'lightblue':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/dar-conocimiento/6' );
        case 'green':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/foros-charlas-y-conferencias' );
        case 'blue':
          return this.router.navigateByUrl( '/cultura-caja-con-sentido/caja-con-sentido' );
      }
    }
  }

  renderColor( id: number ) {
    let color = {};
    switch ( id ) {
      case 1:
        color = { image: './assets/img/svg/a-turquoise.svg', color: 'turquoise' };
        return color;
      case 2:
        color = { image: './assets/img/svg/a-coral.svg', color: 'coral' };
        return color;
      case 3:
        color = { image: './assets/img/svg/a-lightgreen.svg', color: 'lightgreen' };
        return color;
      case 4:
        color = { image: './assets/img/svg/a-orange.svg', color: 'orange' };
        return color;
      case 5:
        color = { image: './assets/img/svg/a-red.svg', color: 'red' };
        return color;
      case 6:
        color = { image: './assets/img/svg/a-lightblue.svg', color: 'lightblue' };
        return color;
      case 7:
        color = { image: './assets/img/svg/a-green.svg', color: 'green' };
        return color;
      case 8:
        color = { image: './assets/img/svg/a-blue.svg', color: 'blue' };
        return color;
    }
  }

  private getListChallenges() {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.progress = {
        percentage: res.data.percentage,
        points: res.data.points,
        total: res.data.total,
      };
      console.log( res.data, this.progress );
      this.myProgress = res.data.detail;
      this.categories = res.data.challenges;
      console.log(this.categories);
      this.loading = false;
    } );
  }
}
