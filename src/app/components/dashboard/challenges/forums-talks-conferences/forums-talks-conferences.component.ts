import { Component, OnInit, ViewChild } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import * as Globals from '@utils/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { ChallengesService } from '@services/challenges.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '@services/authentication.service';
import { ChallengeValuesComponent } from '@components/modals/challenge-values/challenge-values.component';

@Component( {
  selector: 'app-forums-talks-conferences',
  templateUrl: './forums-talks-conferences.component.html',
  styleUrls: ['./../challenges.component.scss', './forums-talks-conferences.component.scss']
} )
export class ForumsTalksConferencesComponent implements OnInit {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;


  forums = [];
  listForums = [];
  listForumsResponse: any;
  page: any;
  loading: boolean;
  form: FormGroup;
  id: number;
  active: any;
  user: any;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService, private bsModalService: BsModalService,
               private auth: AuthenticationService ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getForums();
    this.getListForumsBox();
    this.user = this.auth.getUser()?.data.user;
  }

  openChallengeValues() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeValuesComponent, {
      class: 'ModalChallengeValues modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  pageChanged( e: any ) {
    console.log( e );
    this.page = e.page;
    this.getListForumsBox( e.page );
  }

  private getForums() {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.forums = res.data.challenges.filter( x => x.id === 7 );
      this.loading = false;
      console.log( this.forums );
    }, error => {
      this.loading = false;
    } );
  }

  private getListForumsBox( pageNumber?: any ) {
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    this.loading = true;
    this.challengesService.getForumsBox( 7, parameters ).subscribe( ( res: any ) => {
      console.log( res );
      this.listForumsResponse = res;
      this.listForums = res.data.data;
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      comments: [null, Validators.required],
      experiences_id: [null, null]
    } );
  }

}
