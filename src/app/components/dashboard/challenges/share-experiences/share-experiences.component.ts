import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChallengesService } from '@services/challenges.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import * as Globals from '../../../../utils/globals';
import { ReplaySubject, Subscription } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
// tslint:disable-next-line:max-line-length
import { ChallengeCreatePublicationComponent } from '@components/modals/challenge-create-publication/challenge-create-publication.component';
import { AuthenticationService } from '@services/authentication.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { ChallengeValuesComponent } from '@components/modals/challenge-values/challenge-values.component';

@Component( {
  selector: 'app-share-experiences',
  templateUrl: './share-experiences.component.html',
  styleUrls: ['./share-experiences.component.scss']
} )
export class ShareExperiencesComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;

  sub: Subscription;
  sub2: Subscription;

  sharExperience = [];
  experiences = [];
  experiencesResponse: any;
  page: any;
  experienceDetail: any;
  loading: boolean;
  form: FormGroup;
  id: number;
  expGeneral = 'general';
  expOwn: any;
  experiencesOwn = [];
  experiencesOwnResponse: any;
  active: any;
  user: any;

  @ViewChild( 'staticTabs', { static: false } ) staticTabs: TabsetComponent;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService, private bsModalService: BsModalService,
               private auth: AuthenticationService ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.sub = this.challengesService.eventLiked.asObservable().subscribe( values => {
      console.log( 'eventLiked', values );
      this.onClickLiked( values );
    } );
    this.sub2 = this.challengesService.eventComment.asObservable().subscribe( values => {
      console.log( 'eventComment', values );
      if ( values.value ) {
        this.onClickDetail(values.expId);
      }
      //  this.onClickLiked( values );
    } );
    this.getSharExperience();
    this.getExperiences( 'general' );
    this.user = this.auth.getUser()?.data.user;
    console.log( this.user );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  selectTab( tabId: number ) {
    this.staticTabs.tabs[ tabId ].active = true;
  }

  onSelect( type: string ) {
    this.page = 1;
    this.experienceDetail = null;
    if ( type === 'general' ) {
      this.expGeneral = 'general';
      this.expOwn = null;
      this.experiencesOwnResponse = null;
      this.experiencesOwn = null;
    } else {
      this.expOwn = 'own';
      this.expGeneral = null;
      this.experiencesResponse = null;
      this.experiences = null;
    }
    this.getExperiences( type );
    this.active = null;
  }

  openChallengeValues() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeValuesComponent, {
      class: 'ModalChallengeValues modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onShowDetail( event, id: number ) {
    console.log( 'click', event, id );
    if ( id ) {
      this.onClickDetail( id );
      this.active = { id, value: true };
    }
  }

  onClickDetail( id: number ) {
    this.id = id;
    this.loading = true;
    this.challengesService.getExperienceId( id ).subscribe( ( res: any ) => {
      this.experienceDetail = res.data;
      console.log( this.experienceDetail );
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

  onClickDetailNull() {
    this.experienceDetail = null;
  }

  onClickLiked( item: any, e?: any ) {
    console.log( item, e );
    if ( item ) {
      this.form.controls.experiences_id.setValue( item.id );
      this.challengesService.putLikExperience( { experiences_id: this.form.value.experiences_id } ).subscribe( ( res: any ) => {
        this.getExperiences( this.expGeneral || this.expOwn, this.page );
        if ( this.experienceDetail && ( this.experienceDetail.id === item.id ) ) {
          this.onClickDetail( res.data.id );
        }
      }, error => {
      } );
    }
  }

  openChallengeCreatePublication( type: string ) {
    const initialState = { user: this.user, type };
    const modal = this.bsModalService.show( ChallengeCreatePublicationComponent, {
      class: 'ModalChallengeCreatePublication modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( 'res', res );
      if ( res.value === true ) {
        this.getExperiences( 'own', this.page );
        this.selectTab( 1 );
      }
    } );
  }

  pageChanged( e: any, type?: string ) {
    console.log( e, type );
    this.page = e.page;
    this.getExperiences( type, e.page );
  }

  sendMessage() {
    if ( this.form.controls.comments.value.trim().length >= 1 ) {
      setTimeout( () => { this.globals.setLoadingVisible( true ); } );
      if ( this.form.valid ) {
        const params: any = {};
        params.experiences_id = this.id;
        params.comments = this.form.value.comments;
        this.challengesService.postSaveCommentExperience( params ).pipe(
          takeUntil( this.destroyed$ ),
          finalize( () => {
            setTimeout( () => { this.globals.setLoadingVisible( false ); } );
          } ),
        ).subscribe( async ( res: any ) => {
            this.form.controls.comments.reset();
            this.onClickDetail( this.id );
            this.getExperiences( this.expGeneral || this.expOwn, this.page );
          },
          error => {
            this.form.controls.comments.reset();
          } );
      }
    }
  }

  trackByExperiencesId( index: number, item: any ) {
    return item.id;
  }

  private getSharExperience() {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.sharExperience = res.data.challenges.filter( x => x.id === 1 );
      this.loading = false;
      console.log( this.sharExperience );
    }, error => {
      this.loading = false;
    } );
  }

  private getExperiences( type?: string, pageNumber?: any ) {
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    this.loading = true;
  /*  if ( type === 'general' ) {
      this.challengesService.getExperiences( parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.experiencesResponse = res;
        this.experiences = res.data.data;
        this.loading = false;
      }, error => {
        this.loading = false;
      } );
    } else {
      this.challengesService.getExperiencesOwn( parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.experiencesOwnResponse = res;
        this.experiencesOwn = res.data.data;
        this.loading = false;
      }, error => {
        this.loading = false;
      } );
    }*/
    this.challengesService.getExperiences( parameters ).subscribe( ( res: any ) => {
      console.log( res );
      this.experiencesResponse = {};
      this.experiences = [];
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
    this.challengesService.getExperiencesOwn( parameters ).subscribe( ( res: any ) => {
      console.log( res );
      this.experiencesOwnResponse = res;
      this.experiencesOwn = res.data.data;
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      comments: [null, Validators.required],
      experiences_id: [null, null]
    } );
  }

}
