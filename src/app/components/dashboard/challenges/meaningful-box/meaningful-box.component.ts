import { Component, OnInit, ViewChild } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import * as Globals from '@utils/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { ChallengesService } from '@services/challenges.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '@services/authentication.service';
import { ChallengeValuesComponent } from '@components/modals/challenge-values/challenge-values.component';

@Component( {
  selector: 'app-meaningful-box',
  templateUrl: './meaningful-box.component.html',
  styleUrls: ['./../challenges.component.scss', './meaningful-box.component.scss']
} )
export class MeaningfulBoxComponent implements OnInit {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;


  meaningFulBox = [];
  listBox = [];
  listBoxResponse: any;
  page: any;
  loading: boolean;
  form: FormGroup;
  id: number;
  active: any;
  user: any;

  constructor( private fb: FormBuilder, private challengesService: ChallengesService, private bsModalService: BsModalService,
               private auth: AuthenticationService ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getMeaningFulBox();
    this.getListMeaningFulBox();
    this.user = this.auth.getUser()?.data.user;
  }

  openChallengeValues() {
    const initialState = {};
    const modal = this.bsModalService.show( ChallengeValuesComponent, {
      class: 'ModalChallengeValues modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  pageChanged( e: any ) {
    console.log( e );
    this.page = e.page;
    this.getListMeaningFulBox( e.page );
  }

  private getMeaningFulBox() {
    this.loading = true;
    this.challengesService.getChallenges().subscribe( ( res: any ) => {
      this.meaningFulBox = res.data.challenges.filter( x => x.id === 8 );
      this.loading = false;
      console.log( this.meaningFulBox );
    }, error => {
      this.loading = false;
    } );
  }

  private getListMeaningFulBox( pageNumber?: any ) {
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    this.loading = true;
    this.challengesService.getForumsBox( 8, parameters ).subscribe( ( res: any ) => {
      console.log( res );
      this.listBoxResponse = res;
      this.listBox = res.data.data;
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      comments: [null, Validators.required],
      experiences_id: [null, null]
    } );
  }
}
