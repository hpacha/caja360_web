import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationsRoutingModule } from './notifications-routing.module';
import { NotificationsComponent } from './notifications.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SharedModule } from '../../../shared/shared.module';
import { SpinnerModule } from '../../../shared/spinner/spinner.module';


@NgModule({
  declarations: [NotificationsComponent],
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    PerfectScrollbarModule,
    PaginationModule.forRoot(),
    SharedModule,
    SpinnerModule,
  ]
})
export class NotificationsModule { }
