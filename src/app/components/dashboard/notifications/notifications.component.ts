import { ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';
import { SweetalertService } from '../../../shared/services/sweetalert.service';
import { settings } from '../../../shared/common/config';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BirthdayComponent } from '@components/modals/birthday/birthday.component';
import { HappyBirthdayComponent } from '@components/modals/happy-birthday/happy-birthday.component';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';
import * as moment from 'moment';

@Component( {
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
  providers: [SlugifyPipe],
} )
export class NotificationsComponent implements OnInit, OnChanges {
  currentDay = moment().format( 'DD/MM/YYYY' );
  loading: boolean;
  notificationsResponse: any;
  notifications: any;
  page = {
    page: 1
  };
  urlPage: string;

  constructor( private notification: NotificationService, private changeDetectorRef: ChangeDetectorRef,
               private sweetalertService: SweetalertService, private router: Router,
               private slugifyPipe: SlugifyPipe,
               private modalService: BsModalService ) { }

  ngOnInit(): void {
    this.getListNotification();
  }
  ngOnChanges( changes: SimpleChanges ): void {
    this.getListNotification();
  }

  onClickNotification( item: any ) {
    console.log( item );
    this.notification.postSendViewed( item.id ).subscribe( ( res: any ) => {
      console.log( 'postSendViewed', res );
      switch ( item.data.action ) {
        case 'news':
          console.log( 'news' );
          break;
        case 'new_activity':
          this.router.navigate( [`actualidad/actividades/${item.data.id}/${this.slugifyPipe.transform( item.data.resource_title )}`] );
          /*  this.router.navigate( [
           'actualidad/details/' + item.data.id
           ], { queryParams: { data: btoa( JSON.stringify( { type: 'activity' } ) ) } } );*/
          break;
        case 'activity':
          console.log( 'news' );
          break;
        case 'new_news':
          this.router.navigate( [`actualidad/noticias/${item.data.id}/${this.slugifyPipe.transform( item.data.resource_title )}`] );
          /* this.router.navigate( [
           'actualidad/details/' + item.data.id
           ], { queryParams: { data: btoa( JSON.stringify( { type: 'news' } ) ) } } );*/
          break;
        case 'friend_birthday_card':
          console.log( 'friend_birthday_card' );
          this.onClickModalBirthday( item.data.id );
          this.pageChanged( this.page );
          break;
        case 'my_birthday_card':
          console.log( 'my_birthday_card' );
          this.onClickModalMyBirthday();
          this.pageChanged( this.page );
          break;
        case 'attended_caja_with_sense':
          console.log( 'attended_caja_with_sense' );
          this.urlPage = 'caja-con-sentido';
          this.showChallengeMessage( item, this.urlPage );
          this.pageChanged( this.page );
          break;
        case 'attended_forum':
          console.log( 'attended_forum' );
          this.urlPage = 'foros-charlas-y-conferencias';
          this.showChallengeMessage( item, this.urlPage );
          this.pageChanged( this.page );
          break;
        case 'recognition_received':
          console.log( 'recognition_received' );
          this.urlPage = 'recibir-conocimiento';
          this.showChallengeMessage( item, this.urlPage );
          this.pageChanged( this.page );
          break;
        default:
          this.pageChanged( this.page );
          break;
      }
    });
  }

  showChallengeMessage( notification: any, urlPage: string ) {
    const initialState = { notification, urlPage };
    const modal = this.modalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickDelete( id: number ) {
    // this.loading = true;
    this.sweetalertService
    .swal( {
      title: settings.messages.delete.title,
      // text: settings.messages.delete.text,
      icon: settings.alerttype.clear,
      buttons: true,
      dangerMode: true,
    } )
    .then( result => {
      if ( result ) {
        this.loading = true;
        this.notification.deleteNotification( id ).subscribe( ( res: any ) => {
            this.loading = false;
            this.sweetalertService
            .swal( {
              title: settings.messages.save.title,
              text: settings.messages.delete.text2,
            } )
            .then( () => {
              /* this.getListNotification();*/
              this.notification.fetchNotification( { page: 1 } );
              // console.log( res );
            } );
          },
          error => (
            ( this.loading = false )
          ) );
      }
    } );
    // return console.log( id );
  }

  pageChanged( e: any ) {
    this.page = {
      page: e.page
    };
    this.loading = true;
    // this.getListNotification( e.page );
    this.notification.fetchNotification( { page: e.page } );
  }

  trackByFn( index, item ) {
    return item.id;
  }

  onClickModalBirthday( id: any ) {
    const initialState = {
      id
      // title: '',
    };
    const modal = this.modalService.show( BirthdayComponent, {
      class: 'ModalBirthday modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickModalMyBirthday() {
    const initialState = {
      // title: '',
    };
    const modal = this.modalService.show( HappyBirthdayComponent, {
      class: 'ModalHappyBirthday modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  private getListNotification( pageNumber?: number ) {
    this.loading = true;
    const parameters: any = {};
    parameters.page = pageNumber ? pageNumber : 1;
    // this.notification.fetchNotification({page: 1});
    this.notification.getNotification().subscribe( ( res: any ) => {
      // console.log( 'res', res );
      if ( res ) {
        this.loading = false;
        this.changeDetectorRef.detectChanges();
        this.notificationsResponse = res;
        this.notifications = res.data;
        console.log('notifications', this.notifications);
        console.log( 'getListNotification', res );
      }
    } );
  }

}
