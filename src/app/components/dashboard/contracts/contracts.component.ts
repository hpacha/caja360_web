import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';
import { ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { ContractService } from '@services/contracts.service';
import { NewContractComponent } from '@components/modals/contracts/new-contract/new-contract.component';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-contracts',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  user_is_boss: boolean;
  tabOne: any = {type: 'new', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listContracts: null};
  tabTwo: any = {type: 'evaluation-test', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listContracts: null};
  tabThree: any = {type: 'evaluation-renovation', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listContracts: null};
  tabFour: any = {type: 'renovation', preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listContracts: null};
  tabCurrent: any;
  queryField: FormControl = new FormControl();
  timeout: any = null;
  forbidden: boolean;

  constructor(
    private zone: NgZone,
    private modalService: BsModalService,
    private contractService: ContractService
  ) {}

  async ngOnInit() {
    const thisIn = this;
    thisIn.user_is_boss = JSON.parse(localStorage.getItem('user_is_boss'));
    if(!thisIn.user_is_boss) {
      thisIn.forbidden = true;
      return false;
    }
    setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
    await this.getListContracts(thisIn.tabOne);
    this.tabCurrent = this.tabOne;
    setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
    await this.getListContracts(thisIn.tabTwo);
    await this.getListContracts(thisIn.tabThree);
    await this.getListContracts(thisIn.tabFour);
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getListContracts(tabOption: any, queryText = null) {
    const thisIn = this;
    return new Promise<any[]>((resolve, reject) => {
      if(tabOption.currentPage === 1) tabOption.preload = true; else tabOption.preloadPage = true;
      const parameters: any = {};
      parameters.hint = (queryText) ? queryText : null;
      thisIn.contractService.getListContracts(tabOption.type, tabOption.currentPage, parameters).pipe(
        takeUntil(thisIn.destroyed$),
        finalize(() => {
          if(tabOption.currentPage === 1) tabOption.preload = false; else tabOption.preloadPage = false;
          resolve();
        }),
      ).subscribe((res: any ) => {
        tabOption.listContracts = (tabOption.listContracts) ? (tabOption.listContracts).concat(res.data.data) : res.data.data;
        tabOption.lastPage = res.data.last_page;
      },error => {
        tabOption.listContracts = [];
        tabOption.lastPage = 0;
      });
    });
  }

  async onScrolledDownEnd(tabOption) {
    this.zone.run(async () => {
      tabOption.currentPage = tabOption.currentPage + 1;
      if(tabOption.currentPage <= tabOption.lastPage)
        await this.getListContracts(tabOption);
    });
  }

  onClickNewContract(item: any, tabOption: any, title:string) {
    const thisIn = this;
    const initialState = {
      contractId: item.id,
      title: title,
    };
    const modal = this.modalService.show(NewContractComponent, {
      class: 'ModalNewContract modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    });
    modal.content.eventClosed.subscribe((res: any) => {
      if(res) {
        tabOption.listContracts = tabOption.listContracts.map((el: any) => {
          if((el.contract.id === res.data.id))
            el.contract = res.data
          return el;
        });
      }
    });
  }

  onClickDeleteContract(item: any, tabOption: any) {
    const thisIn = this;
    const parameters: any = {}
    parameters.action = "document";
    Swal.fire({
      title: 'Eliminar contrato',
      text: '¿Estás seguro que deseas eliminar el contrato?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
        thisIn.contractService.putDestroyContract({id_file: item.file_id})
        .pipe(takeUntil(thisIn.destroyed$),finalize(() => {setTimeout(() => { thisIn.globals.setLoadingVisible(false);});})).subscribe((res: any) => {
          tabOption.listContracts = tabOption.listContracts.map((el: any) => {
            if((el.contract.id === res.data.id))
              el.contract = res.data
            return el;
          });
          Swal.fire({ title: 'Eliminar contrato', text: 'El contrato se eliminó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Eliminar contrato', text: 'Ocurrió un error al eliminar el contrato', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }

  onClickApproveContract(item: any, tabOption: any) {
    const thisIn = this;
    const parameters: any = {}
    parameters.id = item.id;
    Swal.fire({
      title: `<span class="positive">Aprobar solicitud<span>`,
      text: '¿Estás seguro que deseas aprobar la solicitud?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Aprobar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
        thisIn.contractService.putApproveContract(parameters)
        .pipe(
          finalize(() => {
            setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
          }),
          takeUntil(thisIn.destroyed$)
        ).subscribe((res: any) => {
          tabOption.listContracts = tabOption.listContracts.filter((el: any) => {
            return (el.contract.id !== res.data.id);
          });
          Swal.fire({ title: 'Aprobar solicitud', text: 'La solicitud fue aprobada exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Aprobar solicitud', text: 'Ocurrió un error al aprobar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }

  async onClickNotApproveContract(item: any, tabOption: any) {
    const thisIn = this;
    const { value: subject } = await Swal.fire({
      title: `Rechazar solicitud`,
      text: '¿Estás seguro que no deseas aprobar la solicitud?',
      icon: 'question',
      iconHtml: null,
      input: 'text',
      inputPlaceholder: '¿Cuál es el motivo?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Rechazar`,
      cancelButtonText: `Cancelar`,
      inputValidator: (value) => {
        if (!value) {
          return 'Necesitas ingresar el motivo'
        }
      }
    })

    if (subject) {
      const parameters: any = {}
      parameters.id = item.id;
      parameters.reason = subject;
      setTimeout(() => { thisIn.globals.setLoadingVisible(true); });
      thisIn.contractService.putRejectContract(parameters)
        .pipe(
          finalize(() => {
            setTimeout(() => { thisIn.globals.setLoadingVisible(false); });
          }),
          takeUntil(thisIn.destroyed$)
        ).subscribe((res: any) => {
          tabOption.listContracts = tabOption.listContracts.filter((el: any) => {
            return (el.contract.id !== res.data.id);
          });
          Swal.fire({ title: 'Rechazar solicitud', text: 'La solicitud fue rechazada exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Rechazar solicitud',  html: `Ocurrió un error al rechazar la solicitud<br/>${error.error.message}`, icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  onSelectType(tabOption) {
    this.tabCurrent = tabOption;
    this.queryField.reset();
  }

  onSearchContracts(queryText) {
    const thisIn = this;
    if(thisIn.timeout) {
      window.clearTimeout(thisIn.timeout)
    }
    thisIn.timeout = window.setTimeout(function() {
      thisIn.timeout = null;
      if (queryText != null && queryText.length >= 3) {
        thisIn.tabCurrent.preload = true;
        thisIn.contractService.getListContracts(thisIn.tabCurrent.type, null, {'hint': queryText})
          .pipe(takeUntil(thisIn.destroyed$))
          .subscribe((res:any) => {
            thisIn.tabCurrent.listContracts = (res.data.data);
            thisIn.tabCurrent.preload = false;
          }, (err:any) => {
            thisIn.tabCurrent.preload = false;
          });
      } else if (queryText != null && queryText.length === 0) {
        thisIn.tabCurrent.listContracts = [];
        thisIn.tabCurrent.currentPage = 1;
        thisIn.getListContracts(thisIn.tabCurrent);
      }
    }, 250);
  }
}
