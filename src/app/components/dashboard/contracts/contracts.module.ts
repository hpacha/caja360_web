import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { SharedModule } from '@shared/shared.module';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { ContractsRoutingModule } from './contracts-routing.module';
import { ContractsComponent } from './contracts.component';

@NgModule({
  declarations: [
    ContractsComponent
  ],
  imports: [
    CommonModule,
    ContractsRoutingModule,
    FormsModule,
    HttpClientModule,
    PerfectScrollbarModule,
    ReactiveFormsModule,
    SharedModule,
    SpinnerModule,
    TabsModule.forRoot(),
  ]
})
export class ContractsModule {}
