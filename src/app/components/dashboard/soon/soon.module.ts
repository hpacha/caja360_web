import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SoonRoutingModule } from './soon-routing.module';
import { SoonComponent } from './soon.component';
import { HttpClientModule } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SharedModule } from '../../../shared/shared.module';


@NgModule({
  declarations: [SoonComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SoonRoutingModule,
    TabsModule.forRoot(),
    SharedModule
  ]
})
export class SoonModule { }
