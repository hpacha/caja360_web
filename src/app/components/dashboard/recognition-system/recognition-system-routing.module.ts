import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecognitionSystemComponent } from '@components/dashboard/recognition-system/recognition-system.component';
import { DetailComponent } from '@components/dashboard/recognition-system/detail/detail.component';


const routes: Routes = [
  {path: '', component: RecognitionSystemComponent},
  {path: ':name', component: DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecognitionSystemRoutingModule { }
