import { Component, OnInit } from '@angular/core';

@Component( {
  selector: 'app-recognition-system',
  templateUrl: './recognition-system.component.html',
  styleUrls: ['./recognition-system.component.scss']
} )
export class RecognitionSystemComponent implements OnInit {

  items = [
    { id: 1, name: 'Tutorial', icon: './assets/img/svg/sr-tutorial.svg', router: 'tutorial' },
    { id: 2, name: 'Mis Reconocimientos', icon: './assets/img/svg/sr-misr.svg', router: 'mis-reconocimientos' },
    { id: 3, name: 'Enviar Reconocimientos', icon: './assets/img/svg/sr-enviar.svg', router: 'enviar-reconocimientos' },
    { id: 4, name: 'Bazar', icon: './assets/img/svg/sr-bazar.svg', router: 'bazar' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
