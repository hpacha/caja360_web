import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RecognitionSystemService } from '@services/recognition-system.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-bazaar',
  templateUrl: './bazaar.component.html',
  styleUrls: ['./../../recognition-system.component.scss', './bazaar.component.scss']
})
export class BazaarComponent implements OnInit {

  gifts = [];

  points: number;
  loading: boolean;
  increment = 0;
  defaultImage: string;

  products = [];
  productsIndex = [];

  constructor( private router: Router, private recognitionService: RecognitionSystemService ) { }

  ngOnInit(): void {
    this.getPoints();
    this.listProducts();
  }

  listProducts() {
    this.loading = true;
    this.recognitionService.getProducts().subscribe( ( res: any ) => {
      console.log( res );
      this.loading = false;
      this.gifts = res.data.map( ( x, i ) => {
        return {
          index: i,
          code: x.code,
          image: x.image,
          points: x.points,
          quantity: x.quantity,
          title: x.title,
          counter: 1
        };
      } );
      if ( localStorage.getItem( 'cart' ) && localStorage.getItem( 'cart-products' ) ) {
        this.increment = Number( localStorage.getItem( 'cart' ) );
        this.products = JSON.parse( localStorage.getItem( 'cart-products' ) );
        this.productsIndex = JSON.parse( localStorage.getItem( 'cart-index' ) );
        this.defaultImage = './assets/img/placeholder-h.png';
      }
    }, error => this.loading = false );
  }

  getPoints() {
    this.loading = true;
    this.recognitionService.getPoints().subscribe( ( res: any ) => {
      this.loading = false;
      this.points = res.data;
    }, error => this.loading = false );
  }

  onClickAdd( item: any, index: number ) {
    console.log( item, index );
    console.log( this.products );

    Swal.fire( {
      title: 'Producto agregado a la bolsa de compras',
      icon: 'success',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Seguir canjeando`,
      cancelButtonText: `Ver bolsa`,
      customClass: { container: 'swal2-recognition swal2-recognition-bazar' }, iconHtml: '<img src="./assets/img/svg/icon-bolsa.svg" class="img-fluid" alt="">',
    } ).then( ( res: any ) => {
      this.products.push( item );
      this.productsIndex.push( index );
      this.increment = this.products.length;

      localStorage.setItem( 'cart', this.increment.toString() );
      localStorage.setItem( 'cart-products', JSON.stringify( this.products ) );
      localStorage.setItem( 'cart-index', JSON.stringify( this.productsIndex ) );

      if ( res.dismiss ) {
        this.router.navigate( ['/sistema-de-reconocimientos/resumen'] );
      }
    } );
  }

}
