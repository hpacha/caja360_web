import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { RecognitionSystemService } from '@services/recognition-system.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary-bazaar',
  templateUrl: './summary-bazaar.component.html',
  styleUrls: ['./../../recognition-system.component.scss', './summary-bazaar.component.scss']
})
export class SummaryBazaarComponent implements OnInit {

  products = [];
  productsIndex = [];
  defaultImage: string;
  loading: boolean;
  points = 0;
  total: any;
  balance = 0;

  constructor( private recognitionService: RecognitionSystemService, private router: Router ) { }

  ngOnInit(): void {
    if ( localStorage.getItem( 'cart-products' ) ) {
      this.products = JSON.parse( localStorage.getItem( 'cart-products' ) );
      this.productsIndex = JSON.parse( localStorage.getItem( 'cart-index' ) );
      this.defaultImage = './assets/img/placeholder-h.png';
      this.renderTotal();
    }

    if (this.products && this.products.length >= 1) {
      this.getPoints();
    }
  }

  renderTotal() {
    const numbers = this.products.map( x => Number( x.points ) );
    this.total = numbers.reduce( ( a, b ) => a + b, 0 );
  }

  clearItem( item, index ) {
    const i = this.products.indexOf( item );
    const e = this.productsIndex.indexOf( index );
    this.products.splice( i, 1 );
    this.productsIndex.splice( e, 1 );

    this.renderTotal();

    localStorage.setItem( 'cart', this.products.length.toString() );
    localStorage.setItem( 'cart-products', JSON.stringify( this.products ) );
    localStorage.setItem( 'cart-index', JSON.stringify( this.productsIndex ) );
  }


  redeemPoints() {
    if ( this.points >= this.total ) {
      if ( this.products && this.products.length >= 1 ) {

        const products = this.products.map( ( x ) => ( { code: x.code, quantity: Number( x.counter ) } ) );

        let reduceProducts = products.reduce( ( a, b ) => ( a[ b.code ] = ( a[ b.code ] || 0 ) + b.quantity, a ), {} );
        reduceProducts = Object.entries( reduceProducts ).map( ( [code, quantity] ) => ( { code, quantity } ) );

        this.loading = true;
        this.recognitionService.postRedeemProduct( { products: reduceProducts } ).subscribe( ( res: any ) => {
          this.loading = false;
          Swal.fire( {
            title: 'Gracias por usar la plataforma',
            html: `<div class="">
        <p class="mb-0 gotham-book color-blue mb-3">Pronto le enviaremos los artículos a su sede.</p>
        <p class="mb-0 gotham-book color-blue"> Si desea hacer alguna consulta o tiene alguna duda, contáctenos a este correo: <span>ediazc@cajaarequipa.pe</span></p>
      </div>`,
            icon: 'success',
            showConfirmButton: true,
            confirmButtonText: `Aceptar`,
            // timer: 4000,
            customClass: { container: 'swal2-recognition swal2-recognition-summary' }, iconHtml: '<img src="./assets/img/svg/icon-canje.svg" class="img-fluid" alt="">',
          } ).then( ( response: any ) => {
            console.log( response );
            localStorage.removeItem( 'cart' );
            localStorage.removeItem( 'cart-products' );
            localStorage.removeItem( 'cart-index' );
            this.router.navigate( ['/sistema-de-reconocimientos/bazar'] );
          } );
        }, error => this.loading = false );
      }
    } else {
      Swal.fire( {
        title: 'Canjear puntos',
        html:
          'No puedes canjear porque </br> <b>tus puntos no son suficientes</b>, ',
        icon: 'info',
        iconHtml: null,
        showConfirmButton: true,
        showCancelButton: false,
        confirmButtonText: `Aceptar`,
      } );
    }

  }

  getPoints() {
    this.loading = true;
    this.recognitionService.getPoints().subscribe( ( res: any ) => {
      this.loading = false;
      this.points = res.data;
    }, error => this.loading = false );
  }

}
