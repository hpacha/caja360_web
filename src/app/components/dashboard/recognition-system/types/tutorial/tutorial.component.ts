import { Component, OnInit } from '@angular/core';
import { RecognitionSystemService } from '@services/recognition-system.service';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./../../recognition-system.component.scss', './tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  constructor(private recognitionService: RecognitionSystemService) { }

  ngOnInit(): void {
  }

  getUrlVideo() {
    return this.recognitionService.getVideo();
  }

}
