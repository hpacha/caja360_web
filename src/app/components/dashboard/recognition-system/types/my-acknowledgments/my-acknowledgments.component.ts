import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { RecognitionSystemService } from '@services/recognition-system.service';

@Component({
  selector: 'app-my-acknowledgments',
  templateUrl: './my-acknowledgments.component.html',
  styleUrls: ['./../../recognition-system.component.scss', './my-acknowledgments.component.scss']
})
export class MyAcknowledgmentsComponent implements OnInit, OnChanges  {

  @Input() head = true;
  @Input() data = [];
  acknowlegments = [];
  loading: boolean;

  constructor( private recognitionService: RecognitionSystemService ) { }

  ngOnInit(): void {
    if ( this.head === true ) {
      this.listMyRecognition();
    } else {
      if ( this.data ) {
        this.acknowlegments = this.data;
      }
    }
  }

  ngOnChanges( changes: SimpleChanges ): void {
    if ( this.data ) {
      this.acknowlegments = this.data;
    }
  }

  listMyRecognition() {
    this.loading = true;
    this.recognitionService.getMyRecognitions().subscribe( ( res: any ) => {
      this.loading = false;
      this.acknowlegments = res.data;
    }, error => this.loading = false );
  }

}
