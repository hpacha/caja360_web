import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SelectPersonGroupComponent } from '@components/modals/select-person-group/select-person-group.component';
import { RecognitionSystemService } from '@services/recognition-system.service';
import { DefaultImagesComponent } from '@components/modals/default-images/default-images.component';
import { UserService } from '@services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsService } from '@shared/services/reactive-forms.service';
import { PreviewComponent } from '@components/modals/preview/preview.component';

@Component( {
  selector: 'app-send-acknowledgments',
  templateUrl: './send-acknowledgments.component.html',
  styleUrls: ['./../../recognition-system.component.scss', './send-acknowledgments.component.scss']
} )
export class SendAcknowledgmentsComponent implements OnInit {
  form: FormGroup;
  categories = [];
  events = [];
  eventId: number;
  eventsLoading: boolean;

  text: string;
  option = 'person';
  loading: boolean;
  acknowlegments = [];

  dataEmails: any[] = [];
  direct: any;
  copy: any;
  template: any;


  photo: any;
  audio: any;
  @ViewChild( 'fileInputImage' ) fileInputImage: ElementRef;
  @ViewChild( 'fileInputAudio' ) fileInputAudio: ElementRef;

  constructor( public bsModalService: BsModalService, private recognitionService: RecognitionSystemService,
               private usersService: UserService, private fb: FormBuilder,
               private reactiveFormsService: ReactiveFormsService ) { }

  ngOnInit(): void {
    this.createForm();
    this.getListCategories();
    this.listSendRecognitions();

  }

  onClickChangeOption( event ) {
    this.option = event.target.value;
    this.dataEmails = [];
    this.direct = null;
    this.copy = null;

    this.form.controls.received.setValue( null );
    this.form.controls.received_group.setValue( null );
    this.form.controls.received_copy.setValue( null );
    this.form.controls.received_group_copy.setValue( null );
  }

  onSelectCategory( event ) {
    this.eventsLoading = true;
    this.recognitionService.getEventByCategory( event.id ).subscribe( ( res: any ) => {
      this.events = res.data;
      this.eventsLoading = false;
    }, error => this.eventsLoading = false );
  }

  onSelectEvent( event ) {
    this.template = null;
    this.eventId = event.id;
  }

  renderText( type: string ) {
    if ( type === 'title' ) {
      if ( this.option === 'person' ) {
        return 'Colaborador a reconocer';
      } else {
        return 'Grupo de Colaboradores';
      }
    }
    if ( type === 'description' ) {
      if ( this.option === 'person' ) {
        return 'Ingrese un email o nombre';
      } else {
        return 'Seleccione uno o más grupos';
      }
    }

    if ( type === 'direct' ) {
      return this.direct && this.direct.items?.length >= 1 ?
        this.direct.items.map( x => this.option === 'person' ? x.username : x.groupname ).join( ', ' ) : ( this.option === 'person' ? 'Ingrese un email o nombre' : 'Seleccione uno o más grupos' );
    }
    if ( type === 'copy' ) {
      return this.copy && this.copy.items?.length >= 1 ?
        this.copy.items.map( x => this.option === 'person' ? x.username : x.groupname ).join( ', ' ) : ( this.option === 'person' ? 'Ingrese un email o nombre' : 'Seleccione uno o más grupos' );
    }
  }

  onClickSave() {
    this.reactiveFormsService.markFormGroupTouched( this.form );
    if ( this.form.valid ) {
      const formData: FormData = new FormData();
      formData.append( 'received', this.form.controls.received.value );
      formData.append( 'received_copy', this.form.controls.received_copy.value );
      formData.append( 'received_group', this.form.controls.received_group.value );
      formData.append( 'received_group_copy', this.form.controls.received_group_copy.value );
      formData.append( 'category_id', this.form.controls.category_id.value );
      formData.append( 'event_id', this.form.controls.event_id.value );
      formData.append( 'template_id', this.form.controls.template_id.value );
      formData.append( 'message', this.form.controls.message.value );
      formData.append( 'name', this.form.controls.name.value );
      formData.append( 'image', this.photo );
      formData.append( 'audio', this.audio );

      console.log( this.photo, this.audio );

      this.loading = true;
      this.recognitionService.postRecognitionSend( formData ).subscribe( ( res: any ) => {
        this.loading = false;
        Swal.fire( {
          title: 'Enviado',
          text: 'El reconocimiento fue enviado correctamente.',
          icon: 'success',
          customClass: { container: 'swal2-recognition' },
          iconHtml: '<img src="./assets/img/svg/icon-enviar.svg" class="img-fluid" alt="">',
        } ).then( () => {
          this.onClearData();
        } );
      }, error => {
        this.loading = false;
        Swal.fire( {
          title: 'Oops!',
          html: error.message,
          icon: 'error',
          iconHtml: null,
          showConfirmButton: true,
          showCancelButton: false,
          confirmButtonText: 'Aceptar',
        } );
      } );
    }
  }

  onSelect( typeSelect: string ) {
    const initialState = { type: this.option, typeSelect, items: typeSelect === 'direct' ? this.direct?.items : this.copy?.items };
    const modal = this.bsModalService.show( SelectPersonGroupComponent, {
      class: 'ModalPersonGroup modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( ( res: any ) => {
      console.log( 'res', res );
      if ( res.value === true ) {
        setTimeout( () => {
          this.dataEmails = [];
          if ( res.type === 'direct' ) {
            this.direct = res; // items.map( x => x.email ).join();
          } else {
            this.copy = res; // items.map( x => x.email ).join();
          }
          this.dataEmails.push( this.direct ? this.direct : [] );
          this.dataEmails.push( this.copy ? this.copy : [] );


          if ( this.option === 'person' ) {
            this.form.controls.received.setValue( this.direct && this.direct.items?.length >= 1 ?
              this.direct.items.map( x => x.email ).join() : null );
            this.form.controls.received_copy.setValue( this.copy && this.copy.items?.length >= 1 ?
              this.copy.items.map( x => x.email ).join() : null );
          } else {
            this.form.controls.received_group.setValue( this.direct && this.direct.items?.length >= 1 ?
              this.direct.items.map( x => x.groupname ).join() : null );
            this.form.controls.received_group_copy.setValue( this.copy && this.copy.items?.length >= 1 ?
              this.copy.items.map( x => x.groupname ).join() : null );
          }
        }, 1100 );
      }
    } );
  }

  onPreview() {
    if ( this.form.controls.template_id.value === null || this.form.controls.name.value === null || this.form.controls.name.value === '' ) {
      Swal.fire( {
        title: 'Previsualizar',
        html: 'No puedes previsualizar </br> porque aún no haz agregado el nombre de la persona y no haz seleccionado una plantilla.',
        icon: 'info',
        iconHtml: null,
        showConfirmButton: true,
        showCancelButton: false,
        confirmButtonText: 'Aceptar',
      } );
    } else {
      const formData: FormData = new FormData();
      formData.append( 'template_id', this.form.controls.template_id.value );
      formData.append( 'message', this.form.controls.name.value );
      formData.append( 'image', this.photo );

      this.loading = true;
      this.recognitionService.postPreview( formData ).subscribe( ( res: any ) => {
        this.loading = false;
        const initialState = { data: res?.data };
        const modal = this.bsModalService.show( PreviewComponent, {
          class: 'ModalPreview modal-dialog-centered',
          initialState,
          backdrop: 'static',
          keyboard: true
        } );
      }, error => this.loading = false );
    }
  }

  openDefaultImages() {
    if ( this.form.controls.event_id.value === null ) {
      Swal.fire( {
        title: 'Plantilla',
        html:
          'No puedes elegir una plantilla </br> hasta que selecciones un evento.',
        icon: 'info',
        iconHtml: null,
        showConfirmButton: true,
        showCancelButton: false,
        confirmButtonText: `Aceptar`,
      } );
    } else {

      const initialState = { eventId: this.eventId, template: this.template };
      const modal = this.bsModalService.show( DefaultImagesComponent, {
        class: 'ModalDefaultImages modal-dialog-centered',
        initialState,
        backdrop: 'static',
        keyboard: false
      } );
      modal.content.eventClosed.subscribe( ( res: any ) => {
        console.log( 'res', res );
        if ( res.value === true ) {
          this.template = res.template;
          this.form.controls.template_id.setValue( this.template.id );
        }
      } );

    }
  }

  delete( type: string ) {
    if ( type === 'template' ) {
      this.template = null;
    } else if ( type === 'image' ) {
      this.photo = null;
    } else {
      this.audio = null;
    }
  }

  onFileUpload( evt, type: string ) {
    console.log( evt );
    const file = evt.target.files[ 0 ];
    /*const reader = new FileReader();
     if ( event.target.files && event.target.files.length > 0 ) {
     const file = event.target.files[ 0 ];
     reader.readAsDataURL( file );
     reader.onload = () => {
     this.photo = reader.result;
     console.log( this.photo );
     };
     }*/
    const reader = new FileReader();
    reader.readAsDataURL( file );
    reader.onload = () => {
      if ( type === 'image' ) {
        this.photo = this.fileInputImage.nativeElement.files[ 0 ];
        console.log( this.photo );
      } else {
        this.audio = this.fileInputAudio.nativeElement.files[ 0 ];
        console.log( this.audio );
      }
    };
  }

  onClearData() {
    this.form.reset();
    this.template = null;
    this.direct = null;
    this.copy = null;
    this.photo = null;
    this.audio = null;
  }

  private getListCategories() {
    this.recognitionService.getCategories().subscribe( ( res: any ) => {
      console.log( res.data );
      this.categories = res.data;
    } );
  }

  private listSendRecognitions() {
    this.loading = true;
    this.recognitionService.getMyRecognitionSent().subscribe( ( res: any ) => {
      this.loading = false;
      this.acknowlegments = res.data;
      console.log( this.acknowlegments, res.data );
    }, error => this.loading = false );
  }

  private createForm() {
    this.form = this.fb.group( {
      received: [null, this.option === 'person' ? Validators.required : null],
      received_copy: [null, null],
      received_group: [null, this.option === 'group' ? Validators.required : null],
      received_group_copy: [null, null],
      name: [null, Validators.required],
      category_id: [null, Validators.required],
      event_id: [null, Validators.required],
      template_id: [null, Validators.required],
      message: [null, Validators.required],
      image: [null, null],
      audio: [null, null],
    } );

    this.form.valueChanges.subscribe( ( res: any ) => {
      console.log( res );
      this.renderText( 'direct' );
      this.renderText( 'copy' );
    } );
  }

}
