import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component( {
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
} )
export class DetailComponent implements OnInit {

  type: string;

  constructor( private activatedRoute: ActivatedRoute ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      ( params: Params ) => {
        if ( this.activatedRoute.snapshot.params.name ) {
          this.type = this.activatedRoute.snapshot.params.name;
        }
      } );
  }

}
