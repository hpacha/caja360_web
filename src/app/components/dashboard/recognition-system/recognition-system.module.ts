import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecognitionSystemRoutingModule } from './recognition-system-routing.module';
import { RecognitionSystemComponent } from './recognition-system.component';
import { ControlMessagesModule } from '@shared/control-messages/control-messages.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { SharedModule } from '@shared/shared.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { DetailComponent } from './detail/detail.component';
import { BazaarComponent } from './types/bazaar/bazaar.component';
import { MyAcknowledgmentsComponent } from './types/my-acknowledgments/my-acknowledgments.component';
import { SendAcknowledgmentsComponent } from './types/send-acknowledgments/send-acknowledgments.component';
import { SummaryBazaarComponent } from './types/summary-bazaar/summary-bazaar.component';
import { TutorialComponent } from './types/tutorial/tutorial.component';


@NgModule( {
  declarations: [
    RecognitionSystemComponent, DetailComponent, BazaarComponent, MyAcknowledgmentsComponent,
    SendAcknowledgmentsComponent, SummaryBazaarComponent, TutorialComponent
  ],
  imports: [
    CommonModule,
    RecognitionSystemRoutingModule,
    ControlMessagesModule,
    FormsModule,
    LazyLoadImageModule,
    PerfectScrollbarModule,
    SpinnerModule,
    SharedModule,
    ReactiveFormsModule,
    AccordionModule.forRoot(),
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgSelectModule,
    TypeaheadModule.forRoot(),
  ]
} )
export class RecognitionSystemModule {}
