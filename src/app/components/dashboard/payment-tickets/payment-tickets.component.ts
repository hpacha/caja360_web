import { Component, OnDestroy, OnInit } from '@angular/core';

import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import 'moment/locale/es';

import { ReceiptsService } from '@services/receipts.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-payment-tickets',
  templateUrl: './payment-tickets.component.html',
  styleUrls: ['./payment-tickets.component.scss']
} )
export class PaymentTicketsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  preload: boolean;
  listReceipts: any;
  listMonths = moment.localeData( 'es' ).months();
  listOptionsMonths = [];
  listOptionsYears = [];
  selectedYear = moment().year();
  selectedMonth: number;
  showData: Promise<boolean>;

  constructor(
    private receiptsService: ReceiptsService
  ) {}

  async ngOnInit() {
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    await this.getReceiptsDatesYears();
    await this.getReceiptsDatesMonths();
    await this.getTickets();
    this.showData = Promise.resolve( true );
    setTimeout( () => { this.globals.setLoadingVisible( false ); } );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getReceiptsDatesYears() {
    return new Promise( ( resolve, reject ) => {
      const thisIn = this;
      thisIn.receiptsService.getReceiptsDates().then( ( res: any ) => {
        console.log(res);
        console.log(this.selectedYear);
        res.forEach( element => {
         /* if ( !this.selectedYear ) {
            this.selectedYear = element;
            console.log(this.selectedYear);
          }*/
          thisIn.listOptionsYears.push( { id: element, name: element } );
        } );
        console.log(this.listOptionsYears);
        resolve();
      } );
    } );
  }

  getReceiptsDatesMonths() {
    return new Promise( ( resolve, reject ) => {
      const thisIn = this;
      thisIn.receiptsService.getReceiptsDatesMonths( this.selectedYear ).pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        ( res.data ).forEach( element => {
          thisIn.listOptionsMonths.push( { id: element, name: this.listMonths[ element - 1] } );
          console.log(this.listOptionsMonths);
        } );
      }, error => {
        this.listReceipts = [];
      } );
    } );
  }

  getTickets() {
    return new Promise( ( resolve, reject ) => {
      const thisIn = this;
      thisIn.preload = true;
      const parameters: any = {};
      if ( thisIn.selectedYear && thisIn.selectedMonth ) {
        parameters.year = thisIn.selectedYear;
        parameters.month = thisIn.selectedMonth;
      }
      this.receiptsService.getReceipts( parameters ).pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          thisIn.preload = false;
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        if ( Object.keys( parameters ).length ) {
          this.listReceipts = Array( res.data );
        } else {
          this.listReceipts = res.data;
        }
      }, error => {
        this.listReceipts = [];
      } );
    } );
  }

  async onSelectYear( event ) {
    console.log(event);
    this.selectedYear = event;
    this.getReceiptsDatesMonths();
    if ( this.selectedMonth ) {
      await this.getTickets();
    }
  }

  async onSelectMonth( event ) {
    console.log(event);
    this.selectedMonth = event;
    await this.getTickets();
  }
}
