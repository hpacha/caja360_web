import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgSelectModule } from '@ng-select/ng-select';

import { SharedModule } from '@shared/shared.module';
import { SpinnerModule } from '@shared/spinner/spinner.module';
import { ControlMessagesModule } from '@shared/control-messages/control-messages.module';
import { PaymentTicketsRoutingModule } from './payment-tickets-routing.module';
import { PaymentTicketsComponent } from './payment-tickets.component';

@NgModule({
  declarations: [
    PaymentTicketsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaymentTicketsRoutingModule,
    TabsModule.forRoot(),
    NgSelectModule,
    SharedModule,
    SpinnerModule,
    ControlMessagesModule
  ]
})
export class PaymentTicketsModule {}
