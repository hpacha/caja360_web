import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentTicketsComponent } from '@components/dashboard/payment-tickets/payment-tickets.component';


const routes: Routes = [
  {path: '', component: PaymentTicketsComponent}
];

@NgModule({
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule]
})
export class PaymentTicketsRoutingModule {}
