import { Component, EventEmitter, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NewnessActivityService } from '../../../services/newness-activity.service';
import { NewnessNewsService } from '../../../services/newness-news.service';
import { SlugifyPipe } from '../../../shared/pipes/slugify.pipe';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import * as Globals from '../../../utils/globals';
import 'rxjs/add/operator/finally';
import Swal from "sweetalert2";

@Component( {
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  providers: [SlugifyPipe],
  encapsulation: ViewEncapsulation.None
} )
export class NewsComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  preload: boolean;
  preloadPage: boolean = false;
  defaultImage: string;
  form: FormGroup;
  activities: any;
  activitiesResponse: any;
  news: any;
  newsResponse: any;
  listNews: Array<any> = [];
  listNewsCurrentPage = 1;
  listNewsLastPage = 0;
  listActivities: Array<any> = [];
  listActivitiesCurrentPage = 1;
  listActivitiesLastPage = 0;
  loading: boolean;
  newnessService: any;
  newnessType = 'activity';
  typeAheadListUsers = new EventEmitter<string>();
  queryField: FormControl = new FormControl();
  timeout: any = null;

  constructor(
    private zone: NgZone,
    private newnessActivityService: NewnessActivityService,
    private newnessNewsService: NewnessNewsService,
    private slugifyPipe: SlugifyPipe,
    private router: Router
  ) {}

  async ngOnInit() {
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    this.defaultImage = './assets/img/placeholder-h.png';
    this.newnessService = this.newnessType === 'activity' ? this.newnessActivityService : this.newnessNewsService;
    await this.getListActivities();
    await this.getListNews();
    setTimeout( () => { this.globals.setLoadingVisible( false ); } );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getListActivities() {
    return new Promise( ( resolve, reject ) => {
      if ( this.listActivitiesCurrentPage === 1 ) {
        this.preload = true;
      } else {
        this.preloadPage = true;
      }
      this.newnessActivityService.getListActivities( this.listActivitiesCurrentPage )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          if ( this.listActivitiesCurrentPage === 1 ) {
            this.preload = false;
          } else {
            this.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.listActivities = ( this.listActivities ) ? ( this.listActivities ).concat( res.data.data ) : res.data.data;
        this.listActivitiesLastPage = res.data.last_page;
      }, error => {
        this.listActivities = [];
      } );
    } );
  }

  getListNews() {
    return new Promise( ( resolve, reject ) => {
      if ( this.listActivitiesCurrentPage === 1 ) {
        this.preload = true;
      } else {
        this.preloadPage = true;
      }
      this.newnessNewsService.getListNews( this.listNewsCurrentPage )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          if ( this.listActivitiesCurrentPage === 1 ) {
            this.preload = false;
          } else {
            this.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.listNews = ( this.listNews ) ? ( this.listNews ).concat( res.data.data ) : res.data.data;
        this.listNewsLastPage = res.data.last_page;
      }, error => {
        this.listNews = [];
      } );
    } );
  }

  onSearchActivities( queryText ) {
    const thisIn = this;
    if ( thisIn.timeout ) {
      window.clearTimeout( thisIn.timeout );
    }
    thisIn.timeout = window.setTimeout( function() {
      thisIn.timeout = null;
      if ( queryText.length >= 3 ) {
        thisIn.preload = true;
        thisIn.newnessService.getSearchPost( { 'trace': queryText } )
        .pipe( takeUntil( thisIn.destroyed$ ) )
        .subscribe( ( res: any ) => {
          if ( thisIn.newnessType === 'activity' ) {
            thisIn.listActivities = ( res.data.data );
          } else {
            thisIn.listNews = ( res.data.data );
          }
          thisIn.preload = false;
        }, ( err: any ) => {
          thisIn.preload = false;
        } );
      } else if ( queryText.length === 0 ) {
        if ( thisIn.newnessType === 'activity' ) {
          thisIn.listActivities = [];
          thisIn.listActivitiesCurrentPage = 1;
          thisIn.getListActivities();
        } else {
          thisIn.listNews = [];
          thisIn.listNewsCurrentPage = 1;
          thisIn.getListNews();
        }
      }
    }, 250 );
  }

  onClickRouterLink( item, type ) {
    this.router.navigate( [`actualidad/${type}/${item.id}/${this.slugifyPipe.transform( item.title )}`] );
  }

  async onSelect( type: string ) {
    this.newnessType = type;
    this.newnessService = this.newnessType === 'activity' ? this.newnessActivityService : this.newnessNewsService;
    if ( type === 'news' ) {
      this.newnessType = 'news';
    } else {
      this.newnessType = 'activity';
    }
  }

  onClickLiked( item: any, e: any ) {
    e.stopPropagation();
    const thisIn = this;
    setTimeout( () => { thisIn.globals.setLoadingVisible( true ); } );
    if ( item.my_reaction_id === null ) {
      item.my_reaction_id = 1;
      const params: any = {};
      params.activity_id = this.newnessType === 'activity' ? item.id : null;
      params.news_id = this.newnessType === 'news' ? item.id : null;
      params.type_reaction = 'like';
      this.newnessService.putCreateReaction( params )
      .pipe( takeUntil( this.destroyed$ ), finalize( () => {setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );} ) ).subscribe( ( res: any ) => {
        item.my_reaction_id = res.data.id;
        item.reactions = item.reactions + 1;
      }, error => {
        console.log(error);
        item.my_reaction_id = null;
        Swal.fire( {
          html: `${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        });
      });
    } else {
      this.newnessService.deleteReaction( item.my_reaction_id )
      .pipe( takeUntil( this.destroyed$ ), finalize( () => {setTimeout( () => { thisIn.globals.setLoadingVisible( false );} );} ) ).subscribe( ( res: any ) => {
        item.my_reaction_id = null;
        item.reactions = item.reactions - 1;
      }, error => {
        console.log(error);
        Swal.fire( {
          html: `${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        }); });
    }
  }

  onScrolledDownEnd( type ) {
    if ( type === 'activities' ) {
      if ( this.listActivitiesCurrentPage <= this.listActivitiesLastPage && this.preloadPage === false && !this.queryField.value ) {
        this.zone.run( async () => {
          this.listActivitiesCurrentPage = this.listActivitiesCurrentPage + 1;
          if ( this.listActivitiesCurrentPage <= this.listActivitiesLastPage ) {
            await this.getListActivities();
          }
        } );
      }
    } else {
      if ( this.listNewsCurrentPage <= this.listNewsLastPage && this.preloadPage === false && !this.queryField.value ) {
        this.zone.run( async () => {
          this.listNewsCurrentPage = this.listNewsCurrentPage + 1;
          if ( this.listNewsCurrentPage <= this.listNewsLastPage ) {
            await this.getListNews();
          }
        } );
      }
    }
  }
}
