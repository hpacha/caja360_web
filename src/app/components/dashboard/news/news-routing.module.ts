import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from '@components/dashboard/news/news.component';
import { DetailComponent } from '@components/dashboard/news/detail/detail.component';


const routes: Routes = [
  {
    path: '',
    component: NewsComponent
  },
  {
    path: ':type/:id/:name',
    component: DetailComponent
  }
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule]
} )
export class NewsRoutingModule {}
