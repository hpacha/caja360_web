import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { DatePipe, Location } from '@angular/common';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NewsEditDateComponent } from '@components/modals/news-edit-date/news-edit-date.component';
import { NewsDeleteCommentComponent } from '@components/modals/news-delete-comment/news-delete-comment.component';
import { NewnessActivityService } from '../../../../services/newness-activity.service';
import { NewnessNewsService } from '../../../../services/newness-news.service';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import * as Globals from '../../../../utils/globals';
import 'rxjs/add/operator/finally';
import * as moment from 'moment';
import 'moment/locale/es';
import { DetailCarouselComponent } from '@components/modals/detail-carousel/detail-carousel.component';
import Swal from 'sweetalert2';
import { animate, style, transition, trigger } from '@angular/animations';

@Component( {
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [DatePipe],
  animations: [
    trigger( 'fadeIn', [
      transition( ':enter', [
        style( { opacity: 0 } ),
        animate( 300, style( { opacity: 1 } ) ),
      ] ),
      transition( ':leave', [animate( 300, style( { opacity: 0 } ) )] ),
    ] ),
  ],
} )
export class DetailComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  form: FormGroup;
  userProfile: any;
  detail: any;
  detailComments = [];
  detailDate = [];
  disagree: boolean;
  disagree2: boolean;
  detailType: any;
  detailImage = [];
  detailVideos = [];
  data: any;
  id: number;
  type: string;
  loading: boolean;
  typeOfService: any;
  @Output() closeModal: EventEmitter<any> = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private modalService: BsModalService,
    private user: UserService,
    private newnessActivityService: NewnessActivityService,
    private newnessNewsService: NewnessNewsService,
    private activatedRoute: ActivatedRoute,
    private _location: Location
  ) {}

  async ngOnInit() {
    setTimeout( () => { this.globals.setLoadingVisible( true ); } );
    this.createForm();
    this.activatedRoute.params.subscribe(
      async ( params: Params ) => {
        console.log( 'sadfgh', params );
        if ( params && params[ 'id' ] ) {
          this.getUserProfile();
          this.typeOfService = params[ 'type' ] === 'actividades' ? this.newnessActivityService : this.newnessNewsService;
          this.type = params[ 'type' ];
          this.id = params[ 'id' ];
          await this.getActivityOrNewId( params[ 'id' ] );
          setTimeout( () => { this.globals.setLoadingVisible( false ); } );
        }
      } );
    /* if ( this.activatedRoute.snapshot.params[ 'id' ] ) {
     this.getUserProfile();
     this.typeOfService = this.activatedRoute.snapshot.params[ 'type' ] === 'actividades' ? this.newnessActivityService : this.newnessNewsService;
     this.type = this.activatedRoute.snapshot.params[ 'type' ];
     this.id = this.activatedRoute.snapshot.params[ 'id' ];
     await this.getActivityOrNewId( this.activatedRoute.snapshot.params[ 'id' ] );
     setTimeout( () => { this.globals.setLoadingVisible( false ); } );
     } */
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getBackLocation() {
    this._location.back();
  }

  getUserProfile() {
    this.user.getUserProfile().then( ( res: any ) => {
      this.userProfile = res;
      console.log( this.userProfile );
    } );
  }

  sendMessage() {
    if ( this.form.controls.comment.value.trim().length >= 1 ) {
      setTimeout( () => { this.globals.setLoadingVisible( true ); } );
      if ( this.form.valid ) {
        const params: any = {};
        params.activity_id = this.type === 'actividades' ? this.id : null;
        params.news_id = this.type === 'noticias' ? this.id : null;
        params.comment = this.form.value.comment;
        this.typeOfService.putSaveComment( params ).pipe(
          takeUntil( this.destroyed$ ),
          finalize( () => {
            setTimeout( () => { this.globals.setLoadingVisible( false ); } );
          } ),
        ).subscribe( async ( res: any ) => {
            this.form.controls.comment.reset();
            await this.getActivityOrNewId( this.id );
          },
          error => {
            this.form.controls.comment.reset();
          } );
      }
    }
  }

  onClickSchedule( schedule: any ) {
    const initialState = { id: this.detail.id, schedule, type: this.type };
    const modal = this.modalService.show( NewsEditDateComponent, {
      class: 'ModalEditSchedule modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.pipe( takeUntil( this.destroyed$ ) ).subscribe( async ( res: boolean ) => {
      if ( res ) {
        await this.getActivityOrNewId( this.id );
        this.disagree = true;
        this.disagree2 = true;
        setTimeout( () => { this.disagree2 = false; }, 3000 );
      }
    } );
  }

  onClickDisagree() {
    Swal.fire( {
      title: 'Desagendar cita',
      text: '¿Estás seguro que deseas desagendar?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Aceptar`,
      cancelButtonText: `Cancelar`,
    } ).then( ( result ) => {
      this.newnessActivityService.deleteSchedule( this.detailDate[ 0 ]?.activity_id ).pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          this.loading = false;
        } ),
      ).subscribe( ( res: any ) => {
        Swal.fire( {
          title: 'Desagendar Actividad', text: `La actividad se guardo correctamente`, icon: 'success', iconHtml: null, timer: 4000
        } ).then( () => {
          this.getActivityOrNewId( this.id );
        } );
      }, error => {
        console.log( error );
        Swal.fire( {
          title: 'Desagendar Actividad', html: `Ocurrió un error al desagendar actividad. <br/> ${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        } ).then( () => {
          //
        } );
      } );
      /*if ( result.isConfirmed ) {
       Swal.fire( 'Saved!', 'asdfghj', 'success' );
       } else if ( result.isDismissed ) {
       Swal.fire( 'Changes are not saved', '213456', 'info' );
       }*/
    } );
  }

  onClickDelete( id: number ) {
    const initialState = { id, type: this.type };
    const modal = this.modalService.show( NewsDeleteCommentComponent, {
      class: 'ModalDeleteComment modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.pipe( takeUntil( this.destroyed$ ) ).subscribe( async ( res: boolean ) => {
      if ( res ) {
        await this.getActivityOrNewId( this.id );
      }
    } );
  }

  onClickDetailPictures( images, videos ) {
    const initialState = { images, videos };
    const modal = this.modalService.show( DetailCarouselComponent, {
      class: 'ModalDetailCarousel modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  getActivityOrNewId( id: number ) {
    return new Promise( ( resolve, reject ) => {
      this.loading = true;
      this.typeOfService.getPostId( id )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.detail = res.data;
        console.log( this.detail );
        this.detailComments = this.type === 'actividades' ? res.data.activity_comment : res.data.news_comment;
        this.detailDate = this.type === 'actividades' ? res.data.activity_date : res.data.news_date;
        if ( this.type === 'actividades' ) {
          const a = this.detailDate.filter( x => x.my_activity_event_id !== null );
          this.disagree = a.length >= 1;
        }
        console.log( this.detailDate );
        console.log( this.disagree );
        this.detailType = this.type === 'actividades' ? res.data.activity_type : res.data.news_type;
        this.detailImage = this.type === 'actividades' ? res.data.activity_image : res.data.news_image;
        this.detailVideos = this.type === 'actividades' ? res.data.activity_videos : res.data.news_videos;
        this.detailComments && this.detailComments.map( function( item ) {
          item.created_at = moment.utc( item.created_at ).local().format();
          return item;
        } );
        console.log( this.detailDate );
      }, error => {
      } );
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      comment: [null, Validators.required]
    } );
  }
}
