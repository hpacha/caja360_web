import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '@components/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'perfil' },
      { path: 'actualidad', loadChildren: () => import('@components/dashboard/news/news.module').then( m => m.NewsModule ) },
      // {path: 'comunidad', loadChildren: () => import('@components/dashboard/community/community.module').then( m => m.CommunityModule )},
      { path: 'comunidad', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule ) },
      // {path: 'beneficios', loadChildren: () => import('@components/dashboard/benefits/benefits.module').then(m => m.BenefitsModule)},
      { path: 'beneficios', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule ) },
      { path: 'notificaciones', loadChildren: () => import('@components/dashboard/notifications/notifications.module').then( m => m.NotificationsModule ) },
      { path: 'consulta-y-descarga-boletas-de-pago', loadChildren: () => import('@components/dashboard/payment-tickets/payment-tickets.module').then( m => m.PaymentTicketsModule ) },
      { path: 'perfil', loadChildren: () => import('@components/dashboard/profile/profile.module').then( m => m.ProfileModule ) },
      // {path: 'consulta-solicita-vacaciones', loadChildren: () => import('@components/dashboard/vacation-telecommuting/vacation-telecommuting.module').then(m => m.VacationTelecommutingModule)},
      { path: 'consulta-solicita-vacaciones', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule ) },
      { path: 'covid-19', loadChildren: () => import('@components/dashboard/frequent-questions/frequent-questions.module').then( m => m.FrequentQuestionsModule ) },
      { path: 'mi-agenda', loadChildren: () => import('@components/dashboard/calendar/calendar.module').then( m => m.CalendarModule ) },
      // {path: 'cultura-caja-con-sentido', loadChildren: () => import('@components/dashboard/challenges/challenges.module').then(m => m.ChallengesModule)},
      { path: 'cultura-caja-con-sentido', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule ) },
      {
        path: 'chat', children: [
          { path: ':id', loadChildren: () => import('@components/dashboard/chat/chat.module').then( m => m.ChatModule ) },
          { path: '', loadChildren: () => import('@components/dashboard/chat/chat.module').then( m => m.ChatModule ) },
        ]
      },
      { path: 'politicas-reglamentos', loadChildren: () => import('@components/dashboard/documents/documents.module').then( m => m.DocumentsModule ) },
      // {path: 'politicas-reglamentos', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule )},
      // {path: 'contratos', loadChildren: () => import('@components/dashboard/contracts/contracts.module').then(m => m.ContractsModule)},
      { path: 'contratos', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule ) },
      // {path: 'gestiona-tu-turno-presencial', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule )},
      { path: 'gestiona-tu-turno-presencial', loadChildren: () => import('@components/dashboard/hybrid-work/hybrid-work.module').then( m => m.HybridWorkModule ) },
      // {path: 'sistema-de-reconocimientos', loadChildren: () => import('@components/dashboard/soon/soon.module').then( m => m.SoonModule )},
      { path: 'sistema-de-reconocimientos', loadChildren: () => import('@components/dashboard/recognition-system/recognition-system.module').then( m => m.RecognitionSystemModule ) },
    ]
  }
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule]
} )
export class DashboardRoutingModule {}
