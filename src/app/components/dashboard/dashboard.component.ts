import { Component, OnDestroy, OnInit } from '@angular/core';

import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MessagingService } from '@services/messaging.service';
import { NotificationService } from '@services/notification.service';
import { UserService } from '@services/user.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  loading: boolean;
  showChatBox: boolean;
  globals = Globals;
  userProfile: any;
  showTridentData:Promise<boolean>;

  constructor(
    private messagingService: MessagingService,
    private notification: NotificationService,
    private userService: UserService,
  ) {}

  ngOnInit(): void {
    this.globals.setLoadingVisible(true);
    this.getUserProfile();
    this.messagingService.listen()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((message: any) => {
        console.log(message);
        // const data = JSON.parse(message.data['gcm.notification.data']);
        this.notification.setNotification(message);
        this.notification.fetchNotification({page: 1});
      });
    this.notification.fetchNotification({page: 1});
    this.globals.setLoadingVisible(false);
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onClickChatBox() {
    this.showChatBox = !this.showChatBox;
  }

  showEventCloseChat(event: boolean) {
    this.showChatBox = !(event && event === true);
  }

  showEventLogout(event) {
    this.loading = event && event === true;
  }

  getUserProfile() {
    this.userService.getUserProfile().then((res: any) => {
      this.userProfile = res;
      this.showTridentData = Promise.resolve(true);
    });
  }
}
