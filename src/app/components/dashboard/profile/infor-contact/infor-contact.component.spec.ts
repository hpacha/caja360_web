import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InforContactComponent } from './infor-contact.component';

describe('InforContactComponent', () => {
  let component: InforContactComponent;
  let fixture: ComponentFixture<InforContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InforContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
