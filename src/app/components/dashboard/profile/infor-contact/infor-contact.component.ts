import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactsService } from '../../../../services/contacts.service';
import { UserService } from '../../../../services/user.service';
import { ReactiveFormsService } from '../../../../shared/services/reactive-forms.service';
import { Router } from '@angular/router';
import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import 'rxjs/add/operator/finally';
import Swal from 'sweetalert2';
import * as Globals from '../../../../utils/globals';

@Component( {
  selector: 'app-infor-contact',
  templateUrl: './infor-contact.component.html',
  styleUrls: ['./infor-contact.component.scss']
} )
export class InforContactComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  formGroup: FormGroup;
  contacts: any;
  loading: boolean;
  userProfile: any;
  addMore = 0;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactsService,
    private userService: UserService,
    private reactiveFormsService: ReactiveFormsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getListContacts();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onClickSave() {
    this.reactiveFormsService.markFormGroupTouched(this.formGroup);
    if (this.formGroup.valid) {
      setTimeout(() => { this.globals.setLoadingVisible(true); });
      this.contactService.postContacts(this.formGroup.value)
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => {
            setTimeout(() => { this.globals.setLoadingVisible(false); });
          }),
        )
        .subscribe((res: any) => {
          Swal.fire({ title: 'Información de contacto', text: 'Tu información se guardó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
          .then(()=>{
            console.log(res.data);
            this.userService.updateUserProfileDataContacts(res.data)
            this.router.navigate(['/perfil']);
          })
        }, error => {
          Swal.fire({ title: 'Información  de contacto', text: 'Ocurrió un error al guardar la información', icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  getArrayFamiliar() {
    const formarray: any = this.formGroup.get('contacts') as FormArray;
    this.addMore = this.formGroup.get('contacts').value.length;
    return formarray.controls;
  }

  onClickFamiliareAdd(value?: any) {
    const items = this.formGroup.get('contacts') as FormArray;
    items.push(this.familiarCreateItem());
    this.contacts = this.formGroup.value.contacts;
  }

  onClickFamiliarRemove(index: number) {
    const formarray = this.formGroup.get('contacts') as FormArray;
    formarray.removeAt(index);
    this.contacts = this.formGroup.value.contacts;
  }

  private getListContacts() {
    this.contactService.getContacts().subscribe((res: any) => {
      this.contacts = res.data;
      const data = res.data.map(x => ({ names: x.names, surname: x.surname, phone: x.phone }));
      for (let i = 0; i < data.length; i++) {
        this.onClickFamiliareAdd();
      }
      this.formGroup.controls.contacts.patchValue(data);
    });
  }

  private createForm() {
    this.formGroup = this.formBuilder.group({
      contacts: this.formBuilder.array([])
    });
  }

  private familiarCreateItem(c?: any): FormGroup {
    return this.formBuilder.group( {
      names: [null, [Validators.required]],
      surname: [null, [Validators.required]],
      phone: [null, [Validators.required, Validators.minLength( 1 ), Validators.maxLength( 10 )]],
    });
  }
}
