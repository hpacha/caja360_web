import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { ReactiveFormsService } from '../../../../shared/services/reactive-forms.service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { UbigeoService } from '../../../../services/ubigeo.service';
import { Router } from '@angular/router';
import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import 'rxjs/add/operator/finally';
import Swal from 'sweetalert2';
import * as Globals from '../../../../utils/globals';

@Component({
  selector: 'app-infor-personal',
  templateUrl: './infor-personal.component.html',
  styleUrls: ['./infor-personal.component.scss']
})
export class InforPersonalComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  ChildrenCounts = 1;
  formGroup: FormGroup;
  privacyConfig = [{id: false, name: 'Todos', icon: './assets/img/svg/icon-users.svg'}, {id: true, name: 'Solo yo', icon: './assets/img/svg/icon-candado.svg'}];
  googlePlaceOptions: any = {componentRestrictions: {country: 'pe'}};
  coords = [];
  maritalStatus = [];
  documentTypes = [];
  userProfile: any;
  departments: any;
  provinces: any;
  districts: any;
  childrenQuestion: boolean;
  typeOfDocument: any;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private reactiveFormsService: ReactiveFormsService,
    private ubigeoService: UbigeoService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
    this.getDocumentType();
    this.getListMaritalStatus();
    this.getDepartments();
    this.formGroup.controls.province_id.disable();
    this.formGroup.controls.district_id.disable();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  private getDocumentType() {
    this.userService.getDocumenType().then((res: any) => {
      this.documentTypes = res;
    });
  }

  private getListMaritalStatus() {
    this.userService.getMaritalStatus().then((res: any) => {
      this.maritalStatus = res;
    });
  }

  private getDepartments() {
    this.ubigeoService.getDepartments().then((res: any) => {
      this.departments = res;
    });
  }

  private getProvinces(id: number) {
    this.ubigeoService.getProvinces(id).subscribe((res: any) => {
      this.provinces = res.data;
    });
  }

  private getDistricts(id: number) {
    this.ubigeoService.getDistricts(id).subscribe((res: any) => {
      this.districts = res.data;
    });
  }

  onChangeUbigeo(event, type: string) {
    if (type === 'department') {
      this.getProvinces(event.id);
      this.formGroup.controls.province_id.enable();
    }
    if (type === 'province') {
      this.getDistricts(event.id);
      this.formGroup.controls.district_id.enable();
    }
  }

  onClickSave() {
    this.reactiveFormsService.markFormGroupTouched(this.formGroup);
    if (this.formGroup.valid) {
      setTimeout(() => { this.globals.setLoadingVisible(true); });
      const formGroupValues = this.formGroup.getRawValue();
      const params = {
        document_type: {
          id: formGroupValues.document_type.id,
          is_private: formGroupValues.document_type ? formGroupValues.document_private : null
        },
        document: {
          document: formGroupValues.document,
          is_private: formGroupValues.document_private
        },
        phone: {
          phone: formGroupValues.phone,
          is_private: formGroupValues.phone_private
        },
        annexed: {
          annexed: formGroupValues.annexed,
          is_private: formGroupValues.annexed_private
        },
        marital_status: {
          id: formGroupValues.marital_status,
          is_private: formGroupValues.marital_status_private
        },
        children: {
          children: formGroupValues.children,
          is_private: formGroupValues.children_private
        },
        hobby: {
          hobby: formGroupValues.hobby || formGroupValues.hobby !== '' ? formGroupValues.hobby : null,
          is_private: formGroupValues.hobby ? formGroupValues.hobby_private : null
        },
        ubigeo: {
          coordinates: formGroupValues.address || formGroupValues.address !== '' ? formGroupValues.coordinates : null,
          department_id: formGroupValues.address || formGroupValues.address !== '' ? formGroupValues.department_id : null,
          province_id: formGroupValues.address || formGroupValues.address !== '' ? formGroupValues.province_id : null,
          district_id: formGroupValues.address || formGroupValues.address !== '' ? formGroupValues.district_id : null,
          address: formGroupValues.address || formGroupValues.address !== '' ? formGroupValues.address : null,
          is_private: formGroupValues.address ? formGroupValues.address_private : null
        }
      };

      if (!params.hobby.hobby)
        delete(params.hobby)

      this.userService.postUserProfile(params)
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => {
            setTimeout(() => { this.globals.setLoadingVisible(false); });
          }),
        )
        .subscribe((res: any) => {
          Swal.fire({ title: 'Información Personal', text: 'Tu información se guardó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
            .then(()=>{
              this.userService.updateUserProfileData(res.data)
              this.router.navigate(['/perfil']);
            })
        }, error => {
          Swal.fire({ title: 'Información Personal', text: 'Ocurrió un error al guardar la información', icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  increaseValue() {
    this.ChildrenCounts++;
    this.formGroup.get('children').setValue(this.ChildrenCounts);
  }

  decreaseValue() {
    if (this.ChildrenCounts >= 1) {
      let temp = this.ChildrenCounts;
      temp--;
      this.ChildrenCounts = temp < 1 ? 1 : temp;
      this.formGroup.get('children').setValue(this.ChildrenCounts);
    }
  }

  onClickHaveChildren() {
    if (this.formGroup.controls.children_question.value === 'si') {
      this.childrenQuestion = true;
      this.ChildrenCounts = 1;
      this.formGroup.controls.children.setValue(this.ChildrenCounts);
    } else {
      this.childrenQuestion = false;
      this.ChildrenCounts = 0;
      this.formGroup.controls.children.setValue(this.ChildrenCounts);
    }
  }

  showDataProfile(event) {
    if (event) {
      console.log(event);
      this.typeOfDocument = event.documentType ? event.documentType.name : 'Nro. Doc.';
      this.ChildrenCounts = event.children ? event.children.number_children : this.ChildrenCounts;
      if (event.children && event.children.number_children >= 1) {
        this.childrenQuestion = true;
      }
      if (event.ubigeo && event.ubigeo.department_id) {
        this.getProvinces(event.ubigeo.department_id);
        this.formGroup.controls.province_id.enable();
      }
      if (event.ubigeo && event.ubigeo.province_id) {
        this.getDistricts( event.ubigeo.province_id);
        this.formGroup.controls.district_id.enable();
      }
      if (event.ubigeo && event.ubigeo.coordinates) {
        this.coords = [parseFloat(event.ubigeo.coordinates.split(',')[0]), parseFloat(event.ubigeo.coordinates.split(',')[1])];
      }
      this.formGroup.patchValue( {
        document_type: event.documentType ? { id: event.documentType.id, name: event.documentType.name } : null,
        document_type_private: event.documentType ? event.documentType.is_private : false,
        document_private: event.document ? event.document.is_private : false,
        document: event.document ? event.document.document : null,
        phone: event.phone ? event.phone.number : null,
        phone_private: event.phone ? event.phone.is_private : false,
        annexed: event.annexed ? event.annexed.number : null,
        annexed_private: event.annexed ? event.annexed.is_private : false,
        marital_status: event.maritalStatus ? event.maritalStatus.id : 1,
        marital_status_private: event.maritalStatus ? event.maritalStatus.is_private : false,
        children_question: event.children ? event.children.number_children === 0 ? 'no' : 'si' : 'no',
        children: this.ChildrenCounts,
        children_private: event.children ? event.children.is_private : false,
        hobby: event.hobby ? event.hobby.hobbies : null,
        hobby_private: event.hobby ? event.hobby.is_private : false,
        birth_date: event.birth_date ? event.birth_date.date : null,
        birth_date_private: event.birth_date ? event.birth_date.is_private : false,
        address: event.ubigeo ? event.ubigeo.address : null,
        address_private: event.ubigeo ? event.ubigeo.is_private : false,
        department_id: event.ubigeo ? event.ubigeo.department_id : null,
        province_id: event.ubigeo ? event.ubigeo.province_id : null,
        district_id: event.ubigeo ? event.ubigeo.district_id : null,
        coordinates: event.ubigeo ? event.ubigeo.coordinates : null,
      });
    }
  }

  searchAddress(address: Address) {
    const coordenadas = address.geometry.location.toJSON().lat + ',' + address.geometry.location.toJSON().lng;
    this.formGroup.get('address').setValue(address.formatted_address);
    this.formGroup.get('coordinates').setValue(coordenadas);
  }

  private createForm() {
    this.formGroup = this.formBuilder.group( {
      document_type: [null, null],
      document_type_private: [null, null],
      document_private: [null, Validators.required],
      document: [null, Validators.required],
      marital_status: [null, Validators.required],
      marital_status_private: [null, Validators.required],
      phone: [null, [Validators.required, Validators.minLength( 9 ), Validators.maxLength( 9 )]],
      annexed: [null, Validators.required],
      annexed_private: [null, Validators.required],
      phone_private: [null, Validators.required],
      children: [this.ChildrenCounts, Validators.required],
      children_question: [null, Validators.required],
      children_private: [null, Validators.required],
      hobby: [null, null],
      hobby_private: [null, null],
      birth_date: [null, null],
      birth_date_private: [null, null],
      address: [null, Validators.required],
      address_private: [null, Validators.required],
      department_id: [null, Validators.required],
      province_id: [null, Validators.required],
      district_id: [null, Validators.required],
      coordinates: [null, Validators.required],
    });
    this.changeForm();
    if (navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        const coordsLalo = position.coords;
        this.coords = [coordsLalo.latitude, coordsLalo.longitude];
      });
    }
  }

  changeForm() {
    this.formGroup.valueChanges.subscribe(() => {
      if (this.formGroup.value.coordinates) {
        this.coords = [
          parseFloat(this.formGroup.value.coordinates.split(',')[ 0 ]),
          parseFloat( this.formGroup.value.coordinates.split(',')[ 1 ])
        ];
      }
    });
  }
}
