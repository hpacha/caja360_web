import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InforPersonalComponent } from './infor-personal.component';

describe('InforPersonalComponent', () => {
  let component: InforPersonalComponent;
  let fixture: ComponentFixture<InforPersonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforPersonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InforPersonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
