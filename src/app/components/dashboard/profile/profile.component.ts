import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userProfile: any;
  loading: boolean;
  constructor() {
    console.log("profile.component");
  }

  ngOnInit(): void {}

  showDataProfile(event) {
    if (event) {
      this.userProfile = event;
    }
  }
}
