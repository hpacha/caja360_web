import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { InforPersonalComponent } from './infor-personal/infor-personal.component';
import { InforAcademicComponent } from './infor-academic/infor-academic.component';
import { InforGeneralComponent } from './infor-general/infor-general.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { InforPhysicaldataComponent } from './infor-physicaldata/infor-physicaldata.component';
import { InforContactComponent } from './infor-contact/infor-contact.component';
import { SpinnerModule } from '../../../shared/spinner/spinner.module';
import { ControlMessagesModule } from '../../../shared/control-messages/control-messages.module';
import { TagInputModule } from 'ngx-chips';
import { NgxMaskModule } from 'ngx-mask';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    ProfileComponent,
    InforPersonalComponent,
    InforAcademicComponent,
    InforGeneralComponent,
    InforPhysicaldataComponent,
    InforContactComponent
  ], imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    FormsModule,
    GooglePlaceModule,
    AgmCoreModule,
    ReactiveFormsModule,
    ButtonsModule,
    NgSelectModule,
    SpinnerModule,
    ControlMessagesModule,
    TagInputModule,
    NgxMaskModule.forRoot(),
    PerfectScrollbarModule
  ]
})
export class ProfileModule { }
