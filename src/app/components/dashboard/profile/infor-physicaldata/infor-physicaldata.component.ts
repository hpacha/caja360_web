import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsService } from '../../../../shared/services/reactive-forms.service';
import { UserService } from '../../../../services/user.service';
import { Router } from '@angular/router';
import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import 'rxjs/add/operator/finally';
import Swal from 'sweetalert2';
import * as Globals from '../../../../utils/globals';

@Component({
  selector: 'app-infor-physicaldata',
  templateUrl: './infor-physicaldata.component.html',
  styleUrls: ['./infor-physicaldata.component.scss']
})
export class InforPhysicaldataComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  formGroup: FormGroup;
  userProfile: any;
  config = [{id: 'public', name: 'Todos',  icon: './assets/img/svg/icon-users.svg'}, {id: 'private', name: 'Solo yo', icon: './assets/img/svg/icon-candado.svg'}];
  haveMedications: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private reactiveFormsService: ReactiveFormsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onClickSave() {
    this.reactiveFormsService.markFormGroupTouched(this.formGroup);
    if (this.formGroup.valid) {
      setTimeout(() => { this.globals.setLoadingVisible(true); });
      const value = this.formGroup.getRawValue();
      const params = {
        height: value.height ? value.height : null,
        weight: value.weight ? value.weight : null,
        imc: value.imc ? value.imc : null,
        medications: value.medications_question && value.medications_question === 'si' ? value.medications : null,
        is_private: true
      };

      this.userService.postPhysicalData( params)
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => {
            setTimeout(() => { this.globals.setLoadingVisible(false); });
          }),
        )
        .subscribe((res: any) => {
          Swal.fire({ title: 'Datos Físicos', text: 'Tu información se guardó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
          .then(()=>{
            this.userService.updateUserProfileData(res.data)
            this.router.navigate(['/perfil']);
          })
        }, error => {
          Swal.fire({ title: 'Datos Físicos', text: 'Ocurrió un error al guardar la información', icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  onClickHaveMedications() {
    if (this.formGroup.controls.medications_question.value === 'si') {
      this.haveMedications = true;
      this.formGroup.controls.medications.setValidators(Validators.required);
    } else {
      this.haveMedications = false;
      this.formGroup.controls.medications.reset();
      this.formGroup.controls.medications.setValidators(null);
    }
    this.formGroup.controls.medications.updateValueAndValidity();
  }

  showDataProfile(event) {
    if (event) {
      this.formGroup.patchValue( {
        height: event.physical_data && event.physical_data.height ? event.physical_data.height : null,
        weight: event.physical_data && event.physical_data.weight ? event.physical_data.weight : null,
        imc: event.physical_data && event.physical_data.imc
          ? event.physical_data.imc : null,
        medications_question: (event.physical_data && event.physical_data.medications) ?  'si' : 'no',
        medications: event.physical_data && (event.physical_data.medications || event.physical_data.medications !== '') ? event.physical_data.medications : null
      } );
      this.formGroup.controls.imc.disable();
      if (event.physical_data && (event.physical_data.medications !== null && event.physical_data.medications.length >= 1)) {
        this.haveMedications = true;
      }
    }
  }

  private createForm() {
    this.formGroup = this.formBuilder.group({
      height: [null, Validators.required],
      weight: [null, Validators.required],
      imc: [null, null],
      medications_question: [null, Validators.required],
      medications: [null, null],
    });
  }
}
