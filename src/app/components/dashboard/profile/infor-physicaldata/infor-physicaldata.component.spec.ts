import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InforPhysicaldataComponent } from './infor-physicaldata.component';

describe('InforPhysicaldataComponent', () => {
  let component: InforPhysicaldataComponent;
  let fixture: ComponentFixture<InforPhysicaldataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforPhysicaldataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InforPhysicaldataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
