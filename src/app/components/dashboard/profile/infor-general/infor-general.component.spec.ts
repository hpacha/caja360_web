import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InforGeneralComponent } from './infor-general.component';

describe('InforGeneralComponent', () => {
  let component: InforGeneralComponent;
  let fixture: ComponentFixture<InforGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InforGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
