import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { BusinessCardComponent } from '@components/modals/business-card/business-card.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SweetalertService } from '../../../../shared/services/sweetalert.service';
import { settings } from '../../../../shared/common/config';
import * as Globals from '../../../../utils/globals';
@Component( {
  selector: 'app-infor-general',
  templateUrl: './infor-general.component.html',
  styleUrls: ['./infor-general.component.scss']
})
export class InforGeneralComponent implements OnInit, OnChanges {
  width = false;
  userProfile: any;
  pictureProfile: any;
  photo: any;
  loading: boolean;
  @Input() userProfileUpdate: any;
  @Output() dataProfile: EventEmitter<any> = new EventEmitter();
  @ViewChild('fileInputBenefit') fileInputBenefit: ElementRef;

  constructor(
    private router: Router,
    private user: UserService,
    private modalService: BsModalService,
    private sweetalertService: SweetalertService
  ) {
    console.log("info.general");
    Globals.setChatAdviserVisible(true);
  }

  ngOnInit(): void {
    if (
      this.router.url === '#/perfil/informacion-personal' ||
      this.router.url === '#/perfil/informacion-academica' ||
      this.router.url === '#/perfil/datos-fisicos' ||
      this.router.url === '#/perfil/datos-de-contacto' ||
      this.router.url === '/perfil/informacion-personal' ||
      this.router.url === '/perfil/informacion-academica' ||
      this.router.url === '/perfil/datos-fisicos' ||
      this.router.url === '/perfil/datos-de-contacto'
    ) {
      this.width = true;
    }
    this.getUserProfile();
  }

  ngOnChanges(): void {
    if (this.userProfileUpdate) {
      this.userProfile = this.userProfileUpdate;
    }
  }

  onChangeFile(evt: any) {
    const file = evt.target.files[0];
    if (file.name.split('.').length > 2) {
      this.sweetalertService
        .swal({
          title: "Error",
          text: 'El archivo tiene más de una extensión',
        })
      return false;
    }
    if (!(/\.(jpg|jpeg|png)$/i).test(file.name)) {
      this.sweetalertService
        .swal({
          title: "Error",
          text: 'Solo se permiten archivos con extensión: *.jpg, *.jpeg, *.png',
        })
      return false;
    }
    if (( /(\/|;|\/|\\|<|>|%|\$|,|\*)/gm).test(file.name) ) {
      this.sweetalertService
        .swal({
          title: "Error",
          text: 'El nombre de archivo contiene caracteres no permitidos: “;”, “:”, “>”, “<”, “/” ,”\”,“.”, “*”, “%”, “$”',
        })
      return false;
    }
    this.loading = true;
    const thisIn = this;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const formData = new FormData();
      formData.append("profile_picture",  thisIn.fileInputBenefit.nativeElement.files[0]);
      this.user.postProfilePicture(formData).subscribe((res: any) => {
        this.success(res);
      }, error => {
        this.sweetalertService
        .swal({
          title: "Error",
          text: error.message,
        })
        this.loading = false;
      });
    };
  }

  success(res?: any) {
    this.loading = false;
    this.sweetalertService
    .swal({
      title: settings.messages.savePicture.title,
      text: settings.messages.savePicture.text,
    })
    .then(() => {
      this.getUserProfile();
      this.user.updateImage(res);
      window.location.reload();
    });
  }

  onClickBusinessCard() {
    const initialState = {
      userProfile: this.userProfile
    };
    const modal = this.modalService.show(BusinessCardComponent, {
      class: 'ModalQr modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    });
  }

  getUserProfile() {
    this.loading = true;
    console.log("info general getUserProfile");
    this.user.getUserProfile().then((res: any) => {
      this.userProfile = res;
      this.loading = false;
      this.dataProfile.emit(this.userProfile);
    });
    // this.user.getUserProfile().subscribe((res: any) => {
    //   this.loading = false;
    //   this.userProfile = res.data;
    //   if (this.userProfileUpdate) {
    //     this.userProfile = this.userProfileUpdate;
    //   }
    //   this.dataProfile.emit(this.userProfile);
    // });
  }
}
