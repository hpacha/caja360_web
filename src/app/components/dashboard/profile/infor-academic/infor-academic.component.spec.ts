import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InforAcademicComponent } from './infor-academic.component';

describe('InforAcademicComponent', () => {
  let component: InforAcademicComponent;
  let fixture: ComponentFixture<InforAcademicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InforAcademicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InforAcademicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
