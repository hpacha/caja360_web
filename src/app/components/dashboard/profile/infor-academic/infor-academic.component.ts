import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsService } from '../../../../shared/services/reactive-forms.service';
import { UserService } from '../../../../services/user.service';
import { Router } from '@angular/router';
import { ReplaySubject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';
import 'rxjs/add/operator/finally';
import Swal from 'sweetalert2';
import * as Globals from '../../../../utils/globals';

@Component({
  selector: 'app-infor-academic',
  templateUrl: './infor-academic.component.html',
  styleUrls: ['./infor-academic.component.scss']
})
export class InforAcademicComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  formGroup: FormGroup;
  profession = [];
  professionValue = [];
  otherProfessional: boolean;
  academicDegree = [];
  boss = [{id: 1, name: 'Jefe 1'}, {id: 2, name: 'Jefe 2'}, {id: 3, name: 'Jefe 3'}, {id: 4, name: 'Jefe 4'}, {id: 5, name: 'Jefe 5'}, {id: 6, name: 'Otros'}];
  arraySkills: string[] = [];
  userProfile: any;

  constructor(
    private formBuilder: FormBuilder,
    private reactiveFormsService: ReactiveFormsService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getListProfessions();
    this.getListAcademicDegrees();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onClickSave() {
    this.reactiveFormsService.markFormGroupTouched(this.formGroup);
    if (this.formGroup.valid) {
      setTimeout(() => { this.globals.setLoadingVisible(true); });
      const value = this.formGroup.value;
      const params = {
        profession: this.professionValue.filter(x => x.id !== 'Otros').map(x => ({id: x.id, is_private: true})),
        other_profession: value.other_profession ? { profession: value.other_profession, is_private: true } : null,
        academic_degree: { id: value.academic_degree, is_private: true },
        study_institution: { institution: value.study_institution, is_private: true },
        academic_training: { academic_training: value.academic_training, is_private: true },
        achievements: { achievement: value.achievements && value.achievements !== '' ? value.achievements : null, is_private: value.achievements && value.achievements !== '' ? true : null },
        skills: this.arraySkills.map(x => ({ skill: x, is_private: true }))
      };

      if (!params.achievements.achievement)
        delete params.achievements;
      if (!params.skills)
        delete params.skills;

      this.userService.postInfoAcademic(params)
        .pipe(
          takeUntil(this.destroyed$),
          finalize(() => {
            setTimeout(() => { this.globals.setLoadingVisible(false); });
          }),
        )
        .subscribe((res: any) => {
          Swal.fire({ title: 'Información Académica', text: 'Tu información se guardó exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
          .then(()=>{
            this.userService.updateUserProfileData(res.data)
            this.router.navigate(['/perfil']);
          })
        }, error => {
          Swal.fire({ title: 'Información Académica', text: 'Ocurrió un error al guardar la información', icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  onChangeProfession(event) {
    const value = event.map(x => x.id).sort();
    this.professionValue = event.map(x => ({ id: x.id, name: x.name }));
    this.formGroup.controls.profession.setValue(value);
    if (value.includes('Otros')) {
      this.otherProfessional = true;
      this.formGroup.get('other_profession').setValidators(Validators.required);
    } else {
      this.otherProfessional = false;
      this.formGroup.get('other_profession').setValidators(null);
      this.formGroup.controls.other_profession.reset();
    }
    this.formGroup.controls.other_profession.updateValueAndValidity();
  }

  onAdd(event) {
    if (event) {
      const i = this.arraySkills.indexOf(event.value);
      if (i === -1) {
        this.arraySkills.push(event.value);
      } else {
        this.arraySkills.splice(i, 1);
      }
    }
  }

  onClickDeleteSkill(item: string) {
    const i = this.arraySkills.indexOf(item);
    if (i === -1) {
      this.arraySkills.push(item);
    } else {
      this.arraySkills.splice(i, 1);
    }
  }

  showDataProfile(event) {
    if (event) {
      this.arraySkills = event.skills ? event.skills.map(x => x.skill) : null;
      this.professionValue = event.profession.map(x => ({ id: x.id, name: x.name }));
      this.otherProfessional = event.other_profession && event.other_profession.profession ? event.other_profession.profession : false;
      if (event.other_profession && event.other_profession.profession) {
        event.profession.push({id: 'Otros', name: 'Otros'});
        this.professionValue.push({id: 'Otros', name: 'Otros'});
      }
      this.onChangeProfession(event.profession);
      this.formGroup.patchValue({
        profession: event.profession ? event.profession.map(x => x.id) : null,
        other_profession: event.other_profession ? event.other_profession.profession : null,
        academic_degree: event.academicDegree ? event.academicDegree.id : null,
        study_institution: event.study_institution ? event.study_institution.institution : null,
        academic_training: event.academic_training ? event.academic_training.academic_training : null,
        achievements: event.achievements ? event.achievements.achievement : null,
        skills: this.arraySkills,
      });
    }
  }

  private getListProfessions() {
    this.userService.getProfessions().then((res: any) => {
      this.profession = [...res];
      this.profession.push({id: 'Otros', name: 'Otros'});
    });
  }

  private getListAcademicDegrees() {
    this.userService.getAcademicDegrees().then((res: any) => {
      this.academicDegree = [...res];
    });
  }

  private createForm() {
    this.formGroup = this.formBuilder.group( {
      profession: [null, Validators.required],
      other_profession: [null, null],
      academic_degree: [null, Validators.required],
      study_institution: [null, null],
      academic_training: [null, Validators.required],
      achievements: [null, null],
      skills: [null, null],
    });
  }
}
