import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from '@components/dashboard/profile/profile.component';
import { InforPersonalComponent } from '@components/dashboard/profile/infor-personal/infor-personal.component';
import { InforAcademicComponent } from '@components/dashboard/profile/infor-academic/infor-academic.component';
import { InforPhysicaldataComponent } from '@components/dashboard/profile/infor-physicaldata/infor-physicaldata.component';
import { InforContactComponent } from '@components/dashboard/profile/infor-contact/infor-contact.component';


const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  },
  {
    path: 'informacion-personal',
    component: InforPersonalComponent
  },
  {
    path: 'informacion-academica',
    component: InforAcademicComponent
  },
  {
    path: 'datos-fisicos',
    component: InforPhysicaldataComponent
  },
  {
    path: 'datos-de-contacto',
    component: InforContactComponent
  }
];

@NgModule( {
  imports: [RouterModule.forChild( routes )],
  exports: [RouterModule]
} )
export class ProfileRoutingModule {}
