import { ChangeDetectorRef, Component, ElementRef, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { NotificationService } from '../../../services/notification.service';
import { Router } from '@angular/router';
import { ReplaySubject, Subscription } from 'rxjs';
import { BirthdayComponent } from '@components/modals/birthday/birthday.component';
import { HappyBirthdayComponent } from '@components/modals/happy-birthday/happy-birthday.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';
import { SlugifyPipe } from '@shared/pipes/slugify.pipe';

@Component( {
  selector: 'app-popup-notificaciones',
  templateUrl: './popup-notificaciones.component.html',
  styleUrls: ['./popup-notificaciones.component.scss'],
  providers: [SlugifyPipe],
} )
export class PopupNotificacionesComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  loading: boolean;
  notifications: any;
  suscription: Subscription;
  closeAttempt = 0;
  urlPage: string;
  @Input() userProfileUpdate: any;
  @Output() closePopupNotif: EventEmitter<any> = new EventEmitter();

  @HostListener( 'document:keydown.escape', ['$event'] ) onKeydownHandler( event: KeyboardEvent ) {
    this.closePopupNotif.emit( true );
  }

  @HostListener( 'document:click', ['$event'] )
  clickout( event ) {
    if ( !this.eRef.nativeElement.contains( event.target ) && this.closeAttempt ) {
      this.closePopupNotif.emit( true );
    }
    this.closeAttempt++;
  }

  constructor(
    private eRef: ElementRef,
    private modalService: BsModalService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private notificationService: NotificationService,
    private slugifyPipe: SlugifyPipe,
  ) {}

  ngOnInit() {
    this.getListNotification();
  }

  private getListNotification() {
    this.loading = true;
    /* const parameters: any = {};
     parameters.page = pageNumber ? pageNumber : 1;*/
    this.suscription = this.notificationService.getNotification().subscribe( ( res: any ) => {
      console.log( res );
      this.loading = false;
      this.changeDetectorRef.detectChanges();
      this.notifications = res.data;
      console.log( this.notifications.data );
    } );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
    this.suscription.unsubscribe();
  }

  onClickAll() {
    this.closePopupNotif.emit( true );
    this.router.navigate( ['/notificaciones'] );
  }

  onClickModalBirthday( id: any ) {
    const initialState = {
      id
    };
    const modal = this.modalService.show( BirthdayComponent, {
      class: 'ModalBirthday modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickModalMyBirthday() {
    const initialState = {
      // title: '',
    };
    const modal = this.modalService.show( HappyBirthdayComponent, {
      class: 'ModalHappyBirthday modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  showChallengeMessage( notification: any, urlPage: string ) {
    const initialState = { notification, urlPage };
    const modal = this.modalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  onClickNotification( item: any ) {
    console.log( item );
    this.loading = true;
    this.notificationService.postSendViewed( item.id ).subscribe( ( res: any ) => {
      this.loading = false;
      console.log( 'postSendViewed', res );
      switch ( item.data.action ) {
        case 'news':
          console.log( 'news' );
          this.closePopupNotif.emit( true );
          break;
        case 'new_activity':
          this.router.navigate( [`actualidad/actividades/${item.data.id}/${this.slugifyPipe.transform( item.data.resource_title )}`] ).then( () => {
            this.closePopupNotif.emit( true );
            this.notificationService.fetchNotification( { page: 1 } );
            this.getListNotification();
          } );
          /*  this.router.navigate( [
           'actualidad/details/' + item.data.id
           ], { queryParams: { data: btoa( JSON.stringify( { type: 'activity' } ) ) } } );*/
          break;
        case 'activity':
          console.log( 'activity' );
          this.closePopupNotif.emit( true );
          break;
        case 'new_news':
          console.log( item.data.action );
          this.router.navigate( [`actualidad/noticias/${item.data.id}/${this.slugifyPipe.transform( item.data.resource_title )}`] ).then( () => {
            this.closePopupNotif.emit( true );
            this.notificationService.fetchNotification( { page: 1 } );
            this.getListNotification();
          } );
          /*this.router.navigate( [
           'actualidad/details/' + Number(item.data.id)
           ], { queryParams: { data: btoa( JSON.stringify( { type: 'news' } ) ) } } );*/
          break;
        case 'friend_birthday_card':
          console.log( 'friend_birthday_card' );
          this.onClickModalBirthday( item.data.id );
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
        case 'my_birthday_card':
          console.log( 'my_birthday_card' );
          this.onClickModalMyBirthday();
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
        case 'attended_caja_with_sense':
          console.log( 'attended_caja_with_sense' );
          this.urlPage = 'caja-con-sentido';
          this.showChallengeMessage( item, this.urlPage );
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
        case 'attended_forum':
          console.log( 'attended_forum' );
          this.urlPage = 'foros-charlas-y-conferencias';
          this.showChallengeMessage( item, this.urlPage );
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
        case 'recognition_received':
          console.log( 'recognition_received' );
          this.urlPage = 'recibir-conocimiento';
          this.showChallengeMessage( item, this.urlPage );
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
        default:
          this.closePopupNotif.emit( true );
          this.notificationService.fetchNotification( { page: 1 } );
          this.getListNotification();
          break;
      }
    } );

  }


}
