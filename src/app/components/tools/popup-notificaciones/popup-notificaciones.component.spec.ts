import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupNotificacionesComponent } from './popup-notificaciones.component';

describe('PopupNotificacionesComponent', () => {
  let component: PopupNotificacionesComponent;
  let fixture: ComponentFixture<PopupNotificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupNotificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupNotificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
