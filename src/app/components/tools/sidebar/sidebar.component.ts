import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  user_is_boss: boolean;
  listMenu: any;

  constructor() {}

  ngOnInit(): void {
    this.user_is_boss = JSON.parse( localStorage.getItem( 'user_is_boss' ) );
    this.listMenu = [
      { name: 'Actualidad', icon: './assets/img/svg/icon-novedades.svg', icon2: './assets/img/svg/icon-novedades-cw.svg', link: '/actualidad' },
      { name: 'Comunidad', icon: './assets/img/svg/icon-comunidad.svg', icon2: './assets/img/svg/icon-comunidad-cw.svg', link: '/comunidad' },
      { name: 'Beneficios', icon: './assets/img/svg/icon-beneficios.svg', icon2: './assets/img/svg/icon-beneficios-cw.svg', link: '/beneficios' },
      { name: 'Mi Agenda', icon: './assets/img/svg/icon-calendario.svg', icon2: './assets/img/svg/icon-calendario-cw.svg', link: '/mi-agenda' },
      { name: 'Gestiona tu turno presencial', icon: './assets/img/svg/icon-vacaciones.svg', icon2: './assets/img/svg/icon-vacaciones-cw.svg', link: '/gestiona-tu-turno-presencial' },
      { name: 'Cultura - Caja con Sentido', icon: './assets/img/svg/icon-logros.svg', icon2: './assets/img/svg/icon-logros-cw.svg', link: '/cultura-caja-con-sentido' },
      // {name: 'Consulta y solicita Vacaciones', icon: './assets/img/svg/icon-vacaciones.svg', icon2: './assets/img/svg/icon-vacaciones-cw.svg', link: '/consulta-solicita-vacaciones'},
      { name: 'Consulta y descarga Boletas de Pago', icon: './assets/img/svg/icon-boletas.svg', icon2: './assets/img/svg/icon-boletas-cw.svg', link: '/consulta-y-descarga-boletas-de-pago' },
      this.user_is_boss ? { name: 'Contratos', icon: './assets/img/svg/icon-contratos.svg', icon2: './assets/img/svg/icon-contratos-cw.svg', link: '/contratos' } : null,
      // this.user_is_boss ? {name: 'Políticas y reglamentos de RRHH', icon: './assets/img/svg/icon-descargas.svg', icon2: './assets/img/svg/icon-descargas-cw.svg', link: '/politicas-reglamentos'} : {},
      { name: 'Políticas y Reglamentos de RRHH', icon: './assets/img/svg/icon-descargas.svg', icon2: './assets/img/svg/icon-descargas-cw.svg', link: '/politicas-reglamentos' },
      { name: 'COVID-19', icon: './assets/img/svg/icon-preguntas.svg', icon2: './assets/img/svg/icon-preguntas-cw.svg', link: '/covid-19' },
      { name: 'Sistema de Reconocimientos', icon: './assets/img/svg/icon-sr.svg', icon2: './assets/img/svg/icon-sr-cw.svg', link: '/sistema-de-reconocimientos' },
    ];

    this.listMenu = this.listMenu.filter( x => x !== null );
  }
}
