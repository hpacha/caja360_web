import { Component, ElementRef, EventEmitter, HostListener, OnDestroy, OnInit, Output } from '@angular/core';

import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { AdviserService } from '@services/adviser.service';

@Component( {
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.scss']
} )
export class ChatBoxComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  preload: boolean = true;
  listChatAdviserOptions: any;
  levelAnswer: boolean = false;
  lastQuestion: any;
  closeAttempt: number = 0;
  @Output() closeChat: EventEmitter<any> = new EventEmitter();

  /*@HostListener( 'document:keydown.escape', ['$event'] ) onKeydownHandler( event: KeyboardEvent ) {
    this.closeChat.emit( true );
  }*/

 /* @HostListener( 'document:click', ['$event'] )*/
  clickout( event ) {
    if ( !this.elementRef.nativeElement.contains( event.target ) && this.closeAttempt ) {
      this.closeChat.emit( true );
    }
    this.closeAttempt++;
  }

  constructor(
    private elementRef: ElementRef,
    private adviserService: AdviserService
  ) {}

  ngOnInit(): void {
    this.getChatAdviserOptions();
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getChatAdviserOptions() {
    this.lastQuestion = null;
    this.levelAnswer = false;
    this.preload = true;
    this.adviserService.getChatAdviserOptions().pipe(
      finalize( () => {
        this.preload = false;
      } ),
      takeUntil( this.destroyed$ )
    ).subscribe( ( res: any ) => {
      this.listChatAdviserOptions = res.data;
    }, error => {
      this.listChatAdviserOptions = [];
    } );
  }

  getChatAdviserOptionsChild( adviser: any ) {
    const thisIn = this;
    thisIn.lastQuestion = adviser;
   /* if ( adviser.type !== 'answer' ) {
      this.preload = true;
      this.adviserService.getChatAdviserOptionsChild( adviser.id ).subscribe( ( res: any ) => {
        this.preload = false;
        this.listChatAdviserOptions = res.data;
        this.listChatAdviserOptions.map( ( item ) => {
          if ( item.type === 'answer' ) {
            thisIn.levelAnswer = true;
            this.preload = false;
          }
        } );
        console.log(this.listChatAdviserOptions);
      }, error => {
        this.preload = false;
        this.listChatAdviserOptions = [];
      } );
    }*/
    if ( adviser.type !== 'answer' ) {
      this.preload = true;
      this.adviserService.getChatAdviserOptionsChild( adviser.id ).pipe(
        finalize( () => {
          this.preload = false;
        } ),
        takeUntil( this.destroyed$ )
      ).subscribe( ( res: any ) => {
        this.listChatAdviserOptions = res.data;
        this.listChatAdviserOptions.map( ( item ) => {
          if ( item.type === 'answer' ) {
            thisIn.levelAnswer = true;
            finalize;
          }
        } );
        console.log(this.listChatAdviserOptions);
      }, error => {
        this.listChatAdviserOptions = [];
      } );
    }
  }
}
