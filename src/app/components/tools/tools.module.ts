import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NavComponent } from './nav/nav.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { RouterModule } from '@angular/router';
import { PopupNotificacionesComponent } from './popup-notificaciones/popup-notificaciones.component';
import { SharedModule } from '../../shared/shared.module';
import { SpinnerModule } from '../../shared/spinner/spinner.module';
import { ChatBoxComponent } from './chat-box/chat-box.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';



@NgModule({
  declarations: [SidebarComponent, NavComponent, PopupNotificacionesComponent, ChatBoxComponent],
  exports: [
    SidebarComponent,
    NavComponent,
    PopupNotificacionesComponent,
    ChatBoxComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    RouterModule,
    SharedModule,
    SpinnerModule,
    ReactiveFormsModule,
    ButtonsModule,
  ],
})
export class ToolsModule { }
