import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthenticationService } from '@services/authentication.service';
import { NotificationService } from '@services/notification.service';
import { UserService } from '@services/user.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SosComponent } from '@components/modals/sos/sos.component';

@Component( {
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
} )
export class NavComponent implements OnInit {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  shownotifications: boolean;
  loading: boolean;
  userProfile: any;
  pending: boolean;
  iconColor = true;
  // tslint:disable-next-line:variable-name
  user_is_boss: boolean;
  menu: any;
  @Output() logout: EventEmitter<any> = new EventEmitter();

  constructor(
    public router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private notificationService: NotificationService,
    private bsModalService: BsModalService,
  ) {}

  ngOnInit(): void {
    this.user_is_boss = JSON.parse( localStorage.getItem( 'user_is_boss' ) );
    this.menu = [
      {
        name: 'Actualidad', icon: './assets/img/svg/icon-novedades.svg', icon2: './assets/img/svg/icon-novedades-cw.svg', link: '/actualidad'
      },
      {
        name: 'Comunidad', icon: './assets/img/svg/icon-comunidad.svg', icon2: './assets/img/svg/icon-comunidad-cw.svg', link: '/comunidad'
      },
      {
        name: 'Beneficios', icon: './assets/img/svg/icon-beneficios.svg', icon2: './assets/img/svg/icon-beneficios-cw.svg',
        link: '/beneficios'
      },
      {
        name: 'Mi Agenda', icon: './assets/img/svg/icon-calendario.svg', icon2: './assets/img/svg/icon-calendario-cw.svg',
        link: '/mi-agenda'
      },
      {
        name: 'Gestiona tu turno presencial', icon: './assets/img/svg/icon-vacaciones.svg',
        icon2: './assets/img/svg/icon-vacaciones-cw.svg', link: '/gestiona-tu-turno-presencial'
      },
      {
        name: 'Cultura - Caja con Sentido', icon: './assets/img/svg/icon-logros.svg', icon2: './assets/img/svg/icon-logros-cw.svg',
        link: '/cultura-caja-con-sentido'
      },
      {
        name: 'Consulta y solicita Vacaciones', icon: './assets/img/svg/icon-vacaciones.svg',
        icon2: './assets/img/svg/icon-vacaciones-cw.svg',
        link: '/consulta-solicita-vacaciones'
      },
      {
        name: 'Consulta y descarga Boletas de Pago', icon: './assets/img/svg/icon-boletas.svg', icon2: './assets/img/svg/icon-boletas-cw.svg',
        link: '/consulta-y-descarga-boletas-de-pago',
      },
      this.user_is_boss ? {
        name: 'Contratos', icon: './assets/img/svg/icon-contratos.svg', icon2: './assets/img/svg/icon-contratos-cw.svg', link: '/contratos'
      } : null,
      /* this.user_is_boss ? {
       name: 'Políticas y reglamentos de RRHH', icon: './assets/img/svg/icon-descargas.svg', icon2: './assets/img/svg/icon-descargas-cw.svg',
       link: '/politicas-reglamentos'
       } : {},*/
      {
        name: 'Políticas y Reglamentos de RRHH', icon: './assets/img/svg/icon-descargas.svg',
        icon2: './assets/img/svg/icon-descargas-cw.svg', link: '/politicas-reglamentos'
      },
      {
        name: 'COVID-19', icon: './assets/img/svg/icon-preguntas.svg', icon2: './assets/img/svg/icon-preguntas-cw.svg',
        link: '/covid-19'
      },
      {
        name: 'Sistema de Reconocimientos', icon: './assets/img/svg/icon-sr.svg', icon2: './assets/img/svg/icon-sr-cw.svg',
        link: '/sistema-de-reconocimientos'
      },
    ];
    this.menu = this.menu.filter( x => x !== null );
    this.getUserProfile();
    this.getPendingNotifications();
    this.router.events.subscribe( () => {
      this.iconColor = this.router.url !== '/notificaciones';
    } );
  }

  getUserProfile() {
    this.userService.getUserProfile().then( ( res: any ) => {
      this.userProfile = res;
    } );
  }

  getPendingNotifications() {
    this.notificationService.getPending().subscribe( ( res: any ) => {
      console.log( res );
      this.pending = res;
    } );
  }

  onClickNotif() {
    this.shownotifications = !this.shownotifications;
  }

  onClickSos() {
    const initialState: any = {};
    const modal = this.bsModalService.show( SosComponent, {
      class: 'ModalSos modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  showEventClose( event ) {
    this.shownotifications = !( event && event === true );
  }

  onClickLogout() {
    this.logout.emit( true );
    this.authenticationService.postLogout( { origin: 'web' } ).pipe(
      takeUntil( this.destroyed$ )
    ).subscribe( ( res: any ) => {
      this.logout.emit( false );
      localStorage.removeItem( 'fcm' );
      localStorage.removeItem( 'user_is_boss' );
      localStorage.removeItem( 'user' );
      localStorage.removeItem( 'xser' );
      localStorage.removeItem( 'tk' );
      localStorage.removeItem( 'cart' );
      localStorage.removeItem( 'cart-products' );
      localStorage.removeItem( 'cart-index' );
      window.location.reload();
      // this.router.navigate(['/auth']).then(() => window.location.reload());
      // this.router.navigateByUrl('/auth');
    } );
  }

  onClickRouter( url: string ) {
    this.closeNav();
    return this.router.navigate( [url] );
  }

  openNav() {
    document.getElementById( 'mySidenav' ).style.width = '100%';
    document.getElementById( 'body' ).style.overflow = 'auto';
  }

  closeNav() {
    document.getElementById( 'mySidenav' ).style.width = '0';
    document.getElementById( 'body' ).style.overflow = 'block';
  }
}
