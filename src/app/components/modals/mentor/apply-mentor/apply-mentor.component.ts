import { Component, EventEmitter, NgZone, OnDestroy, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { UserService } from '@services/user.service';
import { CommunityService } from '@services/community.service';
import * as moment from 'moment';
import { ConfirmMentorComponent } from '@components/modals/mentor/confirm-mentor/confirm-mentor.component';

@Component( {
  selector: 'app-apply-mentor',
  templateUrl: './apply-mentor.component.html',
  styleUrls: ['./apply-mentor.component.scss'],
  providers: [DatePipe]
} )
export class ApplyMentorComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  id: any;
  loading: boolean;
  formGroup: FormGroup;
  dataCard: any;
  listUsers: any;
  listSetUsers: Array<any> = [];
  listSelectedUsers: any;
  typeAheadListUsers = new EventEmitter<string>();
  selectedDate = new Date( new Date() );
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  userInfo: any;
  menetorRequest: any;
  competenceId: number;
  addListUser: Array<any> = [];
  selectUser = [];
  tabUsers: any = { preload: false, preloadPage: false, current_page: 1, last_page: 0, per_page: 1000, listUser: null };

  constructor(
    private zone: NgZone,
    public bsModalRef: BsModalRef,
    private userService: UserService,
    private communityService: CommunityService,
    private formBuilder: FormBuilder,
    private bsModalService: BsModalService,
  ) {}

  ngOnInit(): void {
    console.log( this.menetorRequest );
    const thisIn = this;
    thisIn.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    thisIn.createForm();
    this.typeAheadListUsers
    .debounceTime( 250 )
    .throttleTime( 250 )
    .distinctUntilChanged()
    .switchMap( term => ( term && term.length ) >= 3 ? this.getUsers( term ) : Observable.of( [] ) )
    .takeUntil( this.destroyed$ )
    .subscribe( ( results: any ) => {
      console.log( results );
      if ( results.data ) {
        thisIn.tabUsers.last_page = results.data.last_page;
        thisIn.listUsers = ( results.data.data ).map( ( user ) => {
          if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, add: false
            };
          } else {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, add: false
            };
          }
        } );
        thisIn.listSetUsers = ( results.data.data ).map( ( user ) => {
          if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, add: false
            };
          } else {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, add: false
            };
          }
        } );
        console.log( this.listUsers );
        thisIn.listSetUsers.forEach( ( item ) => {
          /*  thisIn.listUsers =*/
          thisIn.listUsers.filter( el => {
            return el.id !== item.id;
          } );
        } );
        console.log( this.listSetUsers );
      }
    } );
    this.onClose = new Subject();
    this.getListUsers();
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getUsers( term: string ): Observable<any> {
    const parameters: any = {};
    parameters.hint = term;
    return this.communityService.postSearchMentors( this.competenceId || this.menetorRequest.id, parameters );
  }

  clearListUsers() {
    this.listUsers = [];
  }

  onClickClose() {
    console.log( 'onclose' );
    /*  setTimeout( () => {
     this.tabUsers.current_page = 1;
     this.getListUsers();
     }, 3000 );*/
  }

  addListUsers( event: any ) {
    /* this.listSetUsers = [];
     this.listSetUsers.push( event );*/
    this.addListUser.push( event );
  }

  removeListUsers( item ) {
    console.log( item );
    this.listSetUsers = this.listSetUsers.map( ( el ) => {
      console.log( el );
      if ( el.id === item.id ) {
        el.add = !el.add;
      } else {
        el.add = false;
        this.formGroup.controls.mentor_id.reset();
        this.addListUser = [];
      }
      return el;
    } );
    setTimeout( () => {
      this.selectUser = this.listSetUsers.filter( ( a => a.add === true ) );
      console.log( this.selectUser );
    }, 1001 );
    console.log( this.listSetUsers, this.formGroup.value );
    /* setTimeout( () => {
     const b = this.listSetUsers.filter( x => x.add === item.add );
     console.log( 'b', b );
     if ( b.length === 1 ) {
     this.addListUser = b;
     this.formGroup.controls.mentor_id.setValue( b[ 0 ].id );
     } else {
     this.formGroup.reset();
     this.addListUser = [];
     }
     }, 1001 );*/
  }

  public compareFn( a, b ): boolean {
    return a == b;
  }

  onShowOCupied( item ) {
    this.bsModalRef.setClass( 'ModalApplyMentor modal-dialog modal-dialog-centered hide' );
    const initialState = { mentor: item, occupied: true };
    const modal = this.bsModalService.show( ConfirmMentorComponent, {
      class: 'ModalConfirmMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      this.bsModalRef.setClass( 'ModalApplyMentor modal-dialog modal-dialog-centered' );
    } );
  }


  onSubmit() {
    console.log( this.formGroup.value );
    const thisIn = this;
    if ( thisIn.formGroup.invalid ) {
      thisIn.formGroup.markAllAsTouched();
      Swal.fire( {
        title: 'Solicitar mentor', html: `Debe seleccionar un mentor de la lista y escribir un motivo del cambio `, icon: 'warning',
        iconHtml: null, timer: 4000
      } );
      return false;
    } else {
      if ( this.formGroup.value.reason !== null && this.selectUser && this.selectUser.length !== 0 ) {
        const parameters: any = {};
        parameters.reason = thisIn.formGroup.value.reason;
        parameters.mentor_id = this.selectUser[ 0 ].id;
        thisIn.communityService.postApplyOtherMentor( parameters, thisIn.menetorRequest.mentor_requests[ 0 ].id ).pipe(
          takeUntil( thisIn.destroyed$ ),
          finalize( () => {
            thisIn.loading = false;
          } ),
        ).subscribe( ( res: any ) => {
          console.log( res );
          thisIn.formGroup.reset();
          Swal.fire( {
            title: 'Solicitar mentor', text: 'La solicitud fue enviada exitosamente', icon: 'success', iconHtml: null, timer: 4000
          } ).then( () => {
            thisIn.onClose.next( true );
            thisIn.bsModalRef.hide();
            // thisIn.eventClosed.emit( res );
            thisIn.eventClosed.emit( { new: res, old: thisIn.menetorRequest } );
          } );
        }, error => {
          Swal.fire( {
            title: 'Solicitar mentor', html: `Ocurrió un error al enviar la solicitud<br/>${error.error.message}`, icon: 'error',
            iconHtml: null, timer: 4000
          } );
        } );
      } else {
        thisIn.formGroup.markAllAsTouched();
        Swal.fire( {
          title: 'Solicitar mentor', html: `Debe seleccionar un mentor de la lista y escribir un motivo del cambio `, icon: 'warning',
          iconHtml: null, timer: 4000
        } );
        return false;
      }

    }
  }

  getListUsers() {
    return new Promise( ( resolve, reject ) => {
      if ( this.tabUsers.current_page === 1 ) {
        this.tabUsers.preload = true;
        this.loading = true;
      } else {
        this.loading = true;
        this.tabUsers.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = null;
      parameters.per_page = this.tabUsers.per_page;
      parameters.current_page = this.tabUsers.current_page;
      this.communityService.postSearchMentors( this.competenceId || this.menetorRequest.id, parameters )
      .pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          if ( this.tabUsers.current_page === 1 ) {
            this.tabUsers.preload = false;
            this.loading = false;
          } else {
            this.tabUsers.preloadPage = false;
            this.loading = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.listSetUsers = ( this.listSetUsers ) ? ( this.listSetUsers ).concat( res.data.data ) : res.data.data;
        this.tabUsers.last_page = res.data.last_page;

        if ( res.data ) {
          this.listUsers = ( res.data.data ).map( ( user ) => {
            if ( user.id !== this.userInfo.id && user.is_mentor === true ) {
              return {
                id: user.id, name: user.name, last_name: user.last_name,
                gender: user.gender, photo: user.photo, add: false, is_occupied: user.is_occupied,
                vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
              };
            }
          } );
          this.listSetUsers = ( res.data.data ).map( ( user ) => {
            if ( user.id !== this.userInfo.id && user.is_mentor === true ) {
              if ( this.addListUser.length === 1 ) {
                if ( this.addListUser[ 0 ].id === user.id ) {
                  return {
                    id: user.id, name: user.name, last_name: user.last_name,
                    gender: user.gender, photo: user.photo, add: true, is_occupied: user.is_occupied,
                    vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                  };
                } else {
                  return {
                    id: user.id, name: user.name, last_name: user.last_name,
                    gender: user.gender, photo: user.photo, add: false, is_occupied: user.is_occupied,
                    vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                  };
                }
              } else {
                return {
                  id: user.id, name: user.name, last_name: user.last_name,
                  gender: user.gender, photo: user.photo, add: false, is_occupied: user.is_occupied,
                  vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                };
              }
            }
          } );
          /* thisIn.listSetUsers.forEach( ( item ) => {
           console.log( item );
           /!* thisIn.listUsers =*!/
           thisIn.listUsers.filter( el => {
           console.log( el );
           return el.id !== item.id;
           } );
           } );*/
          // console.log( thisIn.listSetUsers );
        }
      }, error => {
        this.listSetUsers = [];
        this.loading = false;
      } );
    } );
  }

  onScrolledDownEnd() {
    if ( this.tabUsers.current_page <= this.tabUsers.last_page && this.tabUsers.preloadPage === false ) {
      this.zone.run( async () => {
        this.tabUsers.current_page = this.tabUsers.current_page + 1;
        if ( this.tabUsers.current_page <= this.tabUsers.last_page ) {
          await this.getListUsers();
        }
      } );
    }
  }

  createForm() {
    this.formGroup = this.formBuilder.group( {
      reason: [null, Validators.required],
      mentor_id: [null, null],
    } );
  }
}
