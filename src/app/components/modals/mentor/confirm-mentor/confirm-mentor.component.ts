import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component( {
  selector: 'app-confirm-mentor',
  templateUrl: './confirm-mentor.component.html',
  styleUrls: ['./confirm-mentor.component.scss'],
} )
export class ConfirmMentorComponent implements OnInit {
  mentor: any;
  occupied: boolean;
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  res?: any;

  constructor(
    public bsModalRef: BsModalRef,
    public router: Router,
  ) {}

  ngOnInit(): void {
    console.log( this.res );
    console.log( this.mentor );
    this.onClose = new Subject();
  }

  onCloseModal() {
    this.onClose.next( true );
    this.bsModalRef.hide();
    if ( this.res ) {
      this.eventClosed.emit( { value: true, res: this.res } );
    } else {
      this.eventClosed.emit( true );
    }
  }

  onClickChat() {
    this.bsModalRef.hide();
    localStorage.setItem( 'chatId', JSON.stringify( { id: this.mentor.id } ) );
    this.router.navigate(['/chat']);
  }
}
