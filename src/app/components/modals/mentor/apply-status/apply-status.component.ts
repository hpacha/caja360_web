import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, Subject, ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import 'moment/locale/es';

import { ApplyMentorComponent } from '@components/modals/mentor/apply-mentor/apply-mentor.component';
import { ConfirmMentorComponent } from '@components/modals/mentor/confirm-mentor/confirm-mentor.component';
import { CommunityService } from '@services/community.service';

@Component({
  selector: 'app-apply-status',
  templateUrl: './apply-status.component.html',
  styleUrls: ['./apply-status.component.scss'],
  providers: [DatePipe]
})
export class ApplyStatusComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  loading: boolean;
  formGroup: FormGroup;
  listUsers: any;
  listSetUsers: Array<any> = [];
  typeAheadListUsers = new EventEmitter<string>();
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  userInfo: any;
  menetorRequest: string;
  competence: any;
  showData: Promise<boolean>;
  res: any;

  constructor(
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private communityService: CommunityService,
    private formBuilder: FormBuilder,
  ) {}

  async ngOnInit() {
    console.log(this.res);
    if (this.res) {
      this.competence = this.res.new.data;
    }
    const thisIn = this;
    thisIn.userInfo = JSON.parse(localStorage.getItem('user'));
    this.onClose = new Subject();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  delMentorRequest() {
    const thisIn = this;
    thisIn.communityService.delMentorRequest(thisIn.competence.mentor_requests[0].id).pipe(
      takeUntil(thisIn.destroyed$),
      finalize(() => {
      }),
    ).subscribe((res: any ) => {
      Swal.fire({ title: 'Cancelar solicitud', text: 'La solicitud fue cancelada exitosamente', icon: 'success', iconHtml: null, timer: 4000 }).then(() => {
        thisIn.onClose.next(true);
        thisIn.bsModalRef.hide();
        thisIn.eventClosed.emit({type: 'delete', competence: thisIn.competence});
      });
    }, error => {
      Swal.fire({ title: 'Cancelar solicitud', html: `Ocurrió un error al cancelar la solicitud<br/>${error.error.message}`, icon: 'error', iconHtml: null, timer: 4000 })
    });
  }

  openApplyMentorModal() {
    const thisIn = this;
    let initialState: any = { menetorRequest: this.competence };
    let modal = this.bsModalService.show(ApplyMentorComponent, {
      class: 'ModalApplyMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    });
    modal.content.eventClosed.subscribe(async (res: any) => {
      this.competence = res.new.data;
      this.bsModalRef.hide();
      if (res) {
        initialState = { mentor: this.competence.mentor_requests[0].mentor, res  };
        this.bsModalService.show(ConfirmMentorComponent, {
          class: 'ModalConfirmMentor modal-dialog-centered',
          initialState,
          backdrop: 'static',
          keyboard: false
        });
      }
    });
  }

  postMentorAcceptSuggest() {
    const thisIn = this;
    thisIn.communityService.postMentorAcceptSuggest(thisIn.competence.mentor_requests[0].id).pipe(
      takeUntil(thisIn.destroyed$),
      finalize(() => {
      }),
    ).subscribe((res: any ) => {
      Swal.fire({ title: 'Aceptar solicitud', text: 'La solicitud fue aceptada exitosamente', icon: 'success', iconHtml: null, timer: 4000 }).then(() => {
        thisIn.onClose.next(true);
        thisIn.bsModalRef.hide();
        thisIn.eventClosed.emit({type: 'accept', competence: thisIn.competence});
      });
    }, error => {
      Swal.fire({ title: 'Aceptar solicitud', html: `Ocurrió un error al aceptar la solicitud<br/>${error.error.message}`, icon: 'error', iconHtml: null, timer: 4000 })
    });
  }
}
