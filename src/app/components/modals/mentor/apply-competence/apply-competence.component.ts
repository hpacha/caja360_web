import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import 'moment/locale/es';

import { ConfirmMentorComponent } from '@components/modals/mentor/confirm-mentor/confirm-mentor.component';
import { CommunityService } from '@services/community.service';

@Component( {
  selector: 'app-apply-competence',
  templateUrl: './apply-competence.component.html',
  styleUrls: ['./apply-competence.component.scss'],
  providers: [DatePipe]
} )
export class ApplyCompetenceComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  loading: boolean;
  formGroup: FormGroup;
  listUsers: any;
  listSetUsers: Array<any> = [];
  typeAheadListUsers = new EventEmitter<string>();
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  userInfo: any;
  menetorRequest: string;
  competence: any;
  selectUser = [];
  addListUser: Array<any> = [];

  tabUsers: any = { preload: false, preloadPage: false, current_page: 1, last_page: 0, listUser: null };

  constructor(
    private bsModalService: BsModalService,
    public bsModalRef: BsModalRef,
    private communityService: CommunityService,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    console.log( this.competence );
    const thisIn = this;
    thisIn.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    thisIn.createForm();
    this.typeAheadListUsers
    .debounceTime( 250 )
    .throttleTime( 250 )
    .distinctUntilChanged()
    .switchMap( term => ( term && term.length ) >= 3 ? this.getUsers( term ) : Observable.of( [] ) )
    .takeUntil( this.destroyed$ )
    .subscribe( ( results: any ) => {
      if ( results.data ) {
        thisIn.listUsers = ( results.data.data ).map( ( user ) => {
          if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, checked: false,
              is_occupied: user.is_occupied, vacate_date: moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' )
            };
          }
        } );
        thisIn.listSetUsers = ( results.data.data ).map( ( user ) => {
          if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
            return {
              id: user.id, name: user.name, last_name: user.last_name, gender: user.gender, photo: user.photo, checked: false,
              is_occupied: user.is_occupied, vacate_date: moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' )
            };
          }
        } );
        thisIn.listSetUsers.forEach( ( item ) => {
          /*thisIn.listUsers =*/
          thisIn.listUsers.filter( el => {
            return el.id !== item.id;
          } );
        } );
      }
    } );
    this.onClose = new Subject();

    this.getListUsers();
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  getUsers( term: string ): Observable<any> {
    console.log( 'term', term );
    const parameters: any = {};
    parameters.hint = term;
    return this.communityService.postSearchMentors( this.competence.id, parameters );
  }

  clearListUsers() {
    console.log( 'clear' );
    this.listUsers = [];
    // this.getListUsers();
  }

  onClickClose() {
    console.log( 'onclose' );
    setTimeout( () => {
      this.getListUsers();
    }, 3000 );
  }

  addListUsers( event: any ) {
    console.log( 'addlistusers', event );
    /*this.listSetUsers = [];
     this.listSetUsers.push( event );*/
    /*  this.listSetUsers = [];
     this.listSetUsers.push( event );*/
    this.addListUser.push( event );
    console.log( this.addListUser );
    /* this.getListUsers();*/
  }

  removeListUsers( item ) {
    console.log( item );
    this.listSetUsers = this.listSetUsers.map( ( el ) => {
      if ( el.id === item.id ) {
        el.checked = !el.checked;
      } else {
        el.checked = false;
        this.formGroup.controls.mentor_id.reset();
        this.addListUser = [];
      }
      return el;
    } );
    setTimeout( () => {
      this.selectUser = this.listSetUsers.filter( ( a => a.checked === true ));
      console.log(this.selectUser);
    }, 1001 );
    /* setTimeout( () => {
     const b = this.listSetUsers.filter( x => x.checked === item.checked );
     console.log( 'b', b );
     if ( b.length === 1 ) {
     this.formGroup.controls.mentor_id.setValue( b[ 0 ].id );
     } else {
     this.formGroup.reset();
     this.addListUser = [];
     }
     }, 1001 );*/
  }

  public compareFn( a, b ): boolean {
    return a == b;
  }

  onShowOCupied( item ) {
    this.bsModalRef.setClass( 'ModalApplyMentor modal-dialog modal-dialog-centered hide' );
    const initialState = { mentor: item, occupied: true };
    const modal = this.bsModalService.show( ConfirmMentorComponent, {
      class: 'ModalConfirmMentor modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
    modal.content.eventClosed.subscribe( async ( res: any ) => {
      this.bsModalRef.setClass( 'ModalApplyMentor modal-dialog modal-dialog-centered' );
    } );
  }

  onSubmit() {
    const thisIn = this;
    if ( thisIn.selectUser.length === 0 ) {
      thisIn.formGroup.markAllAsTouched();
      Swal.fire( {
        title: 'Solicitar mentor', html: `Debe seleccionar un mentor de la lista`, icon: 'warning', iconHtml: null, timer: 4000
      } );
      return false;
    } else {
      /*  console.log( 'entra-valid', this.listSetUsers );
       const mentorId: any = this.listSetUsers ? this.listSetUsers.filter( item => item.checked === true ) : null;
       console.log( mentorId );
       if ( !mentorId ) {
       Swal.fire( {
       title: 'Solicitar mentor', html: `Debe seleccionar un mentor de la lista`, icon: 'warning', iconHtml: null, timer: 4000
       } );
       return false;
       } mentorId[ 0 ].id */
      thisIn.communityService.postApplyMentor( this.competence.id, this.selectUser[0].id ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          thisIn.loading = false;
        } ),
      ).subscribe( ( res: any ) => {
        console.log( 'adf', res );
        thisIn.formGroup.reset();
        Swal.fire( {
          title: 'Solicitar mentor', text: 'La solicitud fue enviada exitosamente', icon: 'success', iconHtml: null, timer: 4000
        } ).then( () => {
          thisIn.onClose.next( true );
          thisIn.bsModalRef.hide();
          thisIn.eventClosed.emit( { new: res, old: thisIn.competence } );
        } );
      }, error => {
        Swal.fire( {
          title: 'Solicitar mentor', html: `Ocurrió un error al enviar la solicitud<br/>${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        } );
      } );
    }
  }

  getListUsers() {
    const thisIn = this;
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( thisIn.tabUsers.current_page === 1 ) {
        thisIn.tabUsers.preload = true;
      } else {
        thisIn.tabUsers.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = '';
      parameters.current_page = thisIn.tabUsers.current_page;
      thisIn.loading = true;
      thisIn.communityService.postSearchMentors( this.competence.id, parameters ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( thisIn.tabUsers.current_page === 1 ) {
            thisIn.tabUsers.preload = false;
          } else {
            thisIn.tabUsers.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        // console.log( res.data.data );
        thisIn.loading = false;
        if ( res.data ) {
          thisIn.listUsers = ( res.data.data ).map( ( user ) => {
            if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
              return {
                id: user.id, name: user.name, last_name: user.last_name,
                gender: user.gender, photo: user.photo, checked: false, is_occupied: user.is_occupied,
                vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
              };
            }
          } );
          thisIn.listSetUsers = ( res.data.data ).map( ( user ) => {
            if ( user.id !== thisIn.userInfo.id && user.is_mentor === true ) {
              if ( thisIn.addListUser.length === 1 ) {
                if ( thisIn.addListUser[ 0 ].id === user.id ) {
                  return {
                    id: user.id, name: user.name, last_name: user.last_name,
                    gender: user.gender, photo: user.photo, checked: true, is_occupied: user.is_occupied,
                    vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                  };
                } else {
                  return {
                    id: user.id, name: user.name, last_name: user.last_name,
                    gender: user.gender, photo: user.photo, checked: false, is_occupied: user.is_occupied,
                    vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                  };
                }
              } else {
                return {
                  id: user.id, name: user.name, last_name: user.last_name,
                  gender: user.gender, photo: user.photo, checked: false, is_occupied: user.is_occupied,
                  vacate_date: user.vacate_date ? moment.utc( user.vacate_date ).format( 'DD/MM/YYYY' ) : user.vacate_date
                };
              }
            }
          } );
          /* thisIn.listSetUsers.forEach( ( item ) => {
           console.log( item );
           /!* thisIn.listUsers =*!/
           thisIn.listUsers.filter( el => {
           console.log( el );
           return el.id !== item.id;
           } );
           } );*/
          // console.log( thisIn.listSetUsers );
        }
        // thisIn.tabUsers.listUser = ( thisIn.tabUsers.listUser ) ? ( thisIn.tabUsers.listUser ).concat( res.data.data ) : res.data.data;
        thisIn.tabUsers.lastPage = res.data.last_page;
      }, error => {
        thisIn.loading = false;
        thisIn.tabUsers.listUser = [];
        thisIn.tabUsers.lastPage = 0;
      } );
    } );
  }

  createForm() {
    this.formGroup = this.formBuilder.group( {
      mentor_id: [null, [Validators.required]],
    } );
  }
}
