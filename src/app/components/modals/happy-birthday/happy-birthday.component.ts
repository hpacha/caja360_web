import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-happy-birthday',
  templateUrl: './happy-birthday.component.html',
  styleUrls: ['./happy-birthday.component.scss']
})
export class HappyBirthdayComponent implements OnInit {
  userProfile: any;

  constructor(
    public bsModalRef: BsModalRef,
    private user: UserService
  ) {}

  ngOnInit(): void {
    this.getUser();
  }

  private getUser() {
    this.user.getUserProfile().then((res: any) => {
      this.userProfile = res;
    });
  }

}
