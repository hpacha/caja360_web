import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-challenge-message',
  templateUrl: './challenge-message.component.html',
  styleUrls: ['./challenge-message.component.scss']
} )
export class ChallengeMessageComponent implements OnInit {
  eventClosed: EventEmitter<any> = new EventEmitter();
  data: any;
  user: any;
  type: string;

  total: any;
  points: any;
  score: any;

  urlPage: string;
  notification: any;
  typeNtf: string;

  constructor( public bsModalRef: BsModalRef, private activatedRoute: ActivatedRoute, private router: Router,
               private challengeService: ChallengesService ) { }

  ngOnInit(): void {

    console.log( this.urlPage );
    console.log(this.data, this.data.data, this.data.progress);
    if ( this.data ) {
      this.total = this.data.progress.total_points;
      this.points = this.data.progress.challenge_points; // challenge_points;
      this.score = this.data.score; // challenge_points;
    }
    console.log( 'modal-message', this.user, this.type, this.data );
    if ( this.notification ) {
      console.log( 'notification', this.notification );
      const id = Number( this.notification.data.id );
      const resourceId = Number( this.notification.data.resource_id );
      this.typeNtf = this.notification.data.action;
      this.challengeService.getPointsReceived( id, resourceId ).subscribe( ( res: any ) => {
        console.log( res );
        this.total = res.data.progress.total_points;
        this.points = res.data.progress.challenge_points;
        this.score = res.data.points_received;
      } );
    }
  }

  onClickProgress() {
    this.bsModalRef.hide();
    this.router.navigateByUrl( '/cultura-caja-con-sentido' );
  }

  onClickRouter() {
    if ( this.urlPage === 'caja-con-sentido' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/caja-con-sentido' );
    } else if ( this.urlPage === 'foros-charlas-y-conferencias' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/foros-charlas-y-conferencias' );
    } else {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/recibir-conocimiento/5' );
    }
  }

  onClickClose() {
    if ( this.urlPage === 'discusion-de-videos-detail' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-videos/2' );
    } else if ( this.urlPage === 'discusion-de-lectura-detail' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-lectura/3' );
    } else if ( this.urlPage === 'trivias-detail' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/trivias/4' );
    } else if ( this.urlPage === 'dar-conocimiento' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/dar-conocimiento/6' );
    } else if ( this.urlPage === 'compartir-experiencias' ) {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/compartir-experiencias' );
    } else {
      this.bsModalRef.hide();
      this.router.navigateByUrl( '/cultura-caja-con-sentido/compartir-experiencias' );
    }
  }

  onClickCloseOpacity() {
    console.log('xxxxxxxxxxxxx-close-xxxxxxxxxxxxxxxxx');
    this.eventClosed.emit( true );
    this.bsModalRef.hide();
  }

}
