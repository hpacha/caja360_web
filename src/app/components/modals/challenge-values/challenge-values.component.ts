import { Component, OnInit } from '@angular/core';
import Swiper from 'swiper';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-challenge-values',
  templateUrl: './challenge-values.component.html',
  styleUrls: ['./challenge-values.component.scss']
} )
export class ChallengeValuesComponent implements OnInit {
  galleryThumbs: any;
  values: any;

  constructor( public bsModalRef: BsModalRef, private challengesService: ChallengesService ) { }

  ngOnInit(): void {
    this.getChallengesValues();
    this.galleryThumbs = new Swiper( '.gallery-thumbs', {
      slidesPerView: 1,
      spaceBetween: 10,
      allowTouchMove: false,
      preventInteractionOnTransition: false,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,

      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        360: {
          spaceBetween: 0,
          slidesPerView: 1,
        },
        576: {
          spaceBetween: 0,
          slidesPerView: 1,
        },
        768: {
          spaceBetween: 0,
          slidesPerView: 1,
        },
        992: {
          slidesPerView: 1,
        },
        1200: {
          slidesPerView: 1,
        }
      }
    } );

  }

  renderValues(num: number) {
    let data: any;
    switch ( num ) {
      case 1:
        return  data = { image: './assets/img/svg/valor1.svg', color: '#8dc646'};
      case 2:
        return  data = { image: './assets/img/svg/valor2.svg', color: '#f27065'};
      case 3:
        return  data = { image: './assets/img/svg/valor3.svg', color: '#faaa40'};
      case 4:
        return data = { image: './assets/img/svg/valor4.svg', color: '#31a5ed'};
      case 5:
        return data = { image: './assets/img/svg/valor5.svg', color: '#23a455'};
    }
  }

  private CarouseGalleryThumbs() {
    setTimeout( () => {
      this.galleryThumbs = new Swiper( '.gallery-thumbs', {
        slidesPerView: 1,
        spaceBetween: 10,
        allowTouchMove: false,
        preventInteractionOnTransition: false,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,

        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        breakpoints: {
          360: {
            spaceBetween: 0,
            slidesPerView: 1,
          },
          576: {
            spaceBetween: 0,
            slidesPerView: 1,
          },
          768: {
            spaceBetween: 0,
            slidesPerView: 1,
          },
          992: {
            slidesPerView: 1,
          },
          1200: {
            slidesPerView: 1,
          }
        }
      } );
    }, 1000 );
  }

  private getChallengesValues() {
    this.challengesService.getChallengesValues().subscribe( ( res: any ) => {
      this.values = res.data;
      this.CarouseGalleryThumbs();
    } );
  }

}
