import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDeleteCommentComponent } from './news-delete-comment.component';

describe('NewsDeleteCommentComponent', () => {
  let component: NewsDeleteCommentComponent;
  let fixture: ComponentFixture<NewsDeleteCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsDeleteCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDeleteCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
