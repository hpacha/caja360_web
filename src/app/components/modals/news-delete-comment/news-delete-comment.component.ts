import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NewnessActivityService } from '../../../services/newness-activity.service';
import { NewnessNewsService } from '../../../services/newness-news.service';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-news-delete-comment',
  templateUrl: './news-delete-comment.component.html',
  styleUrls: ['./news-delete-comment.component.scss']
} )
export class NewsDeleteCommentComponent implements OnInit {
  id: number;
  type: string;
  loading: boolean;
  eventClosed: EventEmitter<any> = new EventEmitter();
  typeOfService: any;

  deleteComment: boolean;
  idComment: number;

  constructor( public bsModalRef: BsModalRef,
               private newnessActivityService: NewnessActivityService,
               private newnessNewsService: NewnessNewsService,
               private challengesService: ChallengesService ) { }

  ngOnInit(): void {
    this.typeOfService = this.type === 'activity' ? this.newnessActivityService : this.newnessNewsService;
    console.log( this.deleteComment, this.idComment );
  }

  onClickDelete() {
    if ( !this.idComment ) {
      this.loading = true;
      // return console.log(this.typeOfService);
      this.typeOfService.deleteComment( this.id ).subscribe( ( res: any ) => {
          console.log( res );
          this.bsModalRef.hide();
          this.eventClosed.emit( true );
          this.loading = false;
        },
        error => (
          ( this.loading = false )
        ) );
    }
    if ( this.idComment ) {
      this.loading = true;
      this.challengesService.putDeleteCommentExp( { id: this.idComment } ).subscribe( ( res: any ) => {
        console.log( 'putDeleteCommentExp', res );
        this.bsModalRef.hide();
        this.eventClosed.emit( true );
        this.loading = false;
      }, error => {
        this.loading = false;
      } );

      // this.challengesService.eventComment.next( { id: this.idComment } );
    }
  }

}
