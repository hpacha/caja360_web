import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-challenge-info',
  templateUrl: './challenge-info.component.html',
  styleUrls: ['./challenge-info.component.scss']
} )
export class ChallengeInfoComponent implements OnInit {
  challengeItem: any;

  constructor(
    public bsModalRef: BsModalRef
  ) {}

  ngOnInit(): void {
    console.log( this.challengeItem );
  }
}
