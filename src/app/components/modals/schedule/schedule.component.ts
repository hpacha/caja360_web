import { Component, EventEmitter, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatCalendar, MatCalendarCellCssClasses } from '@angular/material/datepicker';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import 'moment/locale/es';

import { UserService } from '@services/user.service';
import { MeetingService } from '@services/meeting.service';

@Component( {
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
  providers: [DatePipe]
} )
export class ScheduleComponent implements OnInit, OnDestroy {
  @ViewChild( MatCalendar ) calendarSchedule: MatCalendar<Date>;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  id: any;
  loading: boolean;
  formGroup: FormGroup;
  dataCard: any;
  listUsers: any;
  listSetUsers: Array<any> = [];
  listSelectedUsers: any;
  typeAheadListUsers = new EventEmitter<string>();
  selectedDate = new Date( new Date() );
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  activity: any;
  mentor: any;
  listMonths = moment.localeData( 'es' ).months();
  selectedDateView: any;
  userInfo: any;

  constructor(
    public bsModalRef: BsModalRef,
    private userService: UserService,
    private meetingService: MeetingService,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    const thisIn = this;
    thisIn.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    thisIn.createForm();
    if ( thisIn.activity ) {
      thisIn.setData();
    }
    if ( thisIn.mentor ) {
      thisIn.setParticipant();
    }
    console.log(this.typeAheadListUsers);
    thisIn.typeAheadListUsers
    .debounceTime( 250 )
    .throttleTime( 250 )
    .distinctUntilChanged()
    .switchMap( term => ( term && term.length ) >= 3 ? thisIn.getUsers( term ) : Observable.of( [] ) )
    .takeUntil( thisIn.destroyed$ )
    .subscribe( ( results: any ) => {
      console.log(results);
      if ( results.data ) {
        thisIn.listUsers = ( results.data.data ).map( function( user ) {
          if ( user.id !== thisIn.userInfo.id ) {
            return { 'id': user.id, 'name': user.last_name + ' ' + user.name, 'add': true };
          }
        } );
        console.log(this.listUsers);
        thisIn.listSetUsers.forEach( function( item ) {
          thisIn.listUsers = thisIn.listUsers.filter( el => {
            return el.id !== item;
          } );
        } );
      }
    } );
    setTimeout( function() {
      thisIn.onCalendarView();
    }, 0 );
    thisIn.onClose = new Subject();
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  createForm() {
    this.formGroup = this.formBuilder.group( {
      name: [null, Validators.required],
      date: [null],
      start: [null, [Validators.required]],
      end: [null, [Validators.required]],
      users: [null, [Validators.required]],
    } );
  }

  setData() {
    const thisIn = this;
    thisIn.formGroup.get( 'name' ).setValue( thisIn.activity.name );
    thisIn.formGroup.get( 'start' ).setValue( thisIn.activity.start );
    thisIn.formGroup.get( 'end' ).setValue( thisIn.activity.end );
    thisIn.selectedDate = new Date( thisIn.activity.date + 'T00:00:00' );
    thisIn.listSelectedUsers = thisIn.activity.users.map( function( user ) {
      thisIn.listSetUsers.push( user.id );
      return { 'id': user.id, 'name': user.last_name + ' ' + user.name };
    } );
    thisIn.formGroup.get( 'users' ).setValue( thisIn.listSelectedUsers );
    Object.keys( thisIn.formGroup.controls ).forEach( key => {
      thisIn.formGroup.controls[ key ].markAsDirty();
    } );
  }

  setParticipant() {
    const thisIn = this;
    thisIn.listSelectedUsers = [{ 'id': thisIn.mentor.id, 'name': thisIn.mentor.last_name + ' ' + thisIn.mentor.name }];
    thisIn.listSetUsers.push( thisIn.mentor.id );
    thisIn.formGroup.get( 'users' ).setValue( thisIn.listSelectedUsers );
    this.formGroup.controls[ 'users' ].disable();
  }

  public compareFn( a, b ): boolean {
    return a == b;
  }

  onChangeCalendar( event ) {
    this.selectedDate = event;
  }

  onSelectedChange( event ) {
    this.selectedDate = event;
  }

  onMonthSelected( event ) {
    this.selectedDateView = this.listMonths[ event.getMonth() ] + ' ' + event.getFullYear();
  }

  myDateFilter = ( d: Date ): boolean => {
    const day = d.getDay();
    return day !== -1;
  };

  dateClass() {
    return ( date: Date ): MatCalendarCellCssClasses => {
      return ( new Date( new Date().getFullYear(), new Date().getMonth(), new Date().getDate() ) <= new Date( date.getFullYear(), date.getMonth(), date.getDate() ) ) ? 'future-date' : 'pass-date';
    };
  }

  onCalendarView() {
    const thisIn = this;
    thisIn.calendarSchedule.updateTodaysDate();
    thisIn.onSelectedChange( thisIn.selectedDate );
    thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendarSchedule.activeDate.getMonth() ] + ' ' + thisIn.calendarSchedule.activeDate.getFullYear();
    const buttonsPrev = document.querySelector( '.mat-schedule .mat-calendar-previous-button' );
    const buttonsNext = document.querySelector( '.mat-schedule .mat-calendar-next-button' );
    thisIn.renderer.listen( buttonsPrev, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendarSchedule.activeDate.getMonth() ] + ' ' + thisIn.calendarSchedule.activeDate.getFullYear();
    } );
    thisIn.renderer.listen( buttonsNext, 'click', () => {
      thisIn.selectedDateView = thisIn.listMonths[ thisIn.calendarSchedule.activeDate.getMonth() ] + ' ' + thisIn.calendarSchedule.activeDate.getFullYear();
    } );
  }

  clearListUsers() {
    this.listUsers = [];
  }

  addListUsers( event: any ) {
    this.listSetUsers.push( event.id );
  }

  removeListUsers( event ) {
    const index = this.listSetUsers.indexOf( event.value.id );
    if ( index > -1 ) {
      this.listSetUsers.splice( index, 1 );
    }
  }

  getUsers( term: string ): Observable<any> {
    const parameters: any = {};
    parameters.hint = term;
    return this.userService.getSearchUsers( parameters );
  }

  onSubmit() {
    if ( this.formGroup.invalid ) {
      this.formGroup.markAllAsTouched();
    } else {
      const parameters: any = {};
      parameters.name = this.formGroup.value.name;
      parameters.date = this.datepipe.transform( this.selectedDate, 'yyyy-MM-dd' );
      parameters.start = this.formGroup.value.start;
      parameters.end = this.formGroup.value.end;
      parameters.users = this.listSetUsers;
      if ( this.activity ) {
        this.updateMeeting( parameters );
      } else {
        this.putMeeting( parameters );
      }
    }
  }

  onDelete() {
    Swal.fire( {
      title: 'Desagendar Reunión',
      text: '¿Estás seguro que deseas desagendar esta reunión?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Desagendar`,
      cancelButtonText: `Cancelar`,
    } ).then( ( result ) => {
      if ( result.isConfirmed ) {
        this.deleteMeeting();
      }
    } );
  }

  putMeeting( parameters ) {
    this.loading = true;
    this.meetingService.putMeeting( parameters ).pipe( takeUntil( this.destroyed$ ) ).subscribe( ( res: any ) => {
      this.onClose.next( true );
      this.bsModalRef.hide();
      this.eventClosed.emit( res );
      this.loading = false;
    }, error => {
      this.loading = false;
      Swal.fire( { title: 'Agendar Reunión', text: 'Ocurrió un error al agendar reunión', icon: 'error', iconHtml: null, timer: 4000 } );
    } );
  }

  updateMeeting( parameters ) {
    this.loading = true;
    this.meetingService.updateMeeting( this.activity.id, parameters ).pipe( takeUntil( this.destroyed$ ) ).subscribe( ( res: any ) => {
      this.onClose.next( true );
      this.bsModalRef.hide();
      this.eventClosed.emit( res );
      this.loading = false;
    }, error => {
      this.loading = false;
      Swal.fire( {
        title: 'Actualizar Reunión', text: 'Ocurrió un error al actualizar reunión', icon: 'error', iconHtml: null, timer: 4000
      } );
    } );
  }

  deleteMeeting() {
    this.loading = true;
    this.meetingService.deleteMeeting( this.activity.id ).pipe( takeUntil( this.destroyed$ ) ).subscribe( ( res: any ) => {
      this.onClose.next( true );
      this.bsModalRef.hide();
      this.eventClosed.emit( 'delete' );
      this.loading = false;
      Swal.fire( {
        title: 'Desagendar Reunión', text: 'La reunión se desagendó exitosamente', icon: 'success', iconHtml: null, timer: 4000
      } );
    }, error => {
      this.loading = false;
      Swal.fire( {
        title: 'Desagendar Reunión', text: 'Ocurrió un error al desagendar reunión', icon: 'error', iconHtml: null, timer: 4000
      } )
    } );
  }
}
