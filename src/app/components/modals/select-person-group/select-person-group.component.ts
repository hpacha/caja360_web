import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RecognitionSystemService } from '@services/recognition-system.service';
import { Observable, Subject } from 'rxjs';
import { AuthenticationService } from '@services/authentication.service';


@Component( {
  selector: 'app-select-person-group',
  templateUrl: './select-person-group.component.html',
  styleUrls: ['./select-person-group.component.scss']
} )
export class SelectPersonGroupComponent implements OnInit {

  items: any;
  type: string;
  typeSelect: string;
  ngModelselectedPeople: any;
  selectedPeople: any;
  states: any;
  loading: boolean;
  users = [];
  eventClosed: EventEmitter<any> = new EventEmitter();
  destroy$: Subject<boolean> = new Subject<boolean>();

  dataSource: Observable<any[]>;
  searchText: any;
  typeAheadListUsers = new EventEmitter<string>();

  constructor( public bsModalRef: BsModalRef, private recognitionService: RecognitionSystemService ,
               private authenticationService: AuthenticationService) {
    // console.log( this.type );
  }

  ngOnInit(): void {
    console.log( this.type );
    if ( this.items && this.items.length >= 1 ) {
      this.selectedPeople = this.items;
      this.ngModelselectedPeople = this.items.filter( x => this.type === 'person' ? x.username : x.groupname ).map( x => this.type === 'person' ? x.username : x.groupname );
    }
    const userInfo = this.authenticationService.getUser();
    console.log(  userInfo);
    this.typeAheadListUsers
    .debounceTime( 250 )
    .throttleTime( 250 )
    .distinctUntilChanged()
    .switchMap( term => ( term && term.length ) >= 3 ? this.getListUsers( term ) : Observable.of( [] ) )
    .takeUntil( this.destroy$ )
    .subscribe( ( results: any ) => {
      if ( results.data ) {
        this.loading = false;
       //  this.users = results.data;
        this.users = ( results.data ).filter( user => {
          console.log(user);
          if ( user.email !== userInfo?.data?.user?.email?.email ) {
            return user;
          }
        } );
        console.log(this.users);
      }

    }, error => this.loading = false );
  }

  clearItem( item ) {
    console.log( this.ngModelselectedPeople );
    const i = this.selectedPeople.indexOf( item );
    this.selectedPeople.splice( i, 1 );
    this.ngModelselectedPeople = this.selectedPeople.filter( x => this.type === 'person' ? x.username : x.groupname ).map( x => this.type === 'person' ? x.username : x.groupname );
    console.log( this.ngModelselectedPeople );
  }

  onSelectItems( event ) {
    console.log( event );
    this.selectedPeople = event;
    console.log( this.selectedPeople );
    // this.selectedPeople.push(event);
  }

  onSearch( event ) {
    console.log( event );
    if ( event.term.length >= 3 ) {
      this.searchText = event.term;
      this.getListUsers( event.term );
    }
  }

  onClickSave() {
    this.eventClosed.emit( { value: true, items: this.selectedPeople, type: this.typeSelect } );
    this.bsModalRef.hide();
  }

  private getListUsers( search?: string ): Observable<any> {
    const params = { q: search };
    this.loading = true;
    return this.type === 'person' ? this.recognitionService.getSearchUsers( params ) : this.recognitionService.getSearchGroups( params );
    /* urlService.subscribe( ( res: any ) => {
     this.loading = false;
     console.log( res );
     this.users = res.data;
     }, error => this.loading = false );*/
  }

}
