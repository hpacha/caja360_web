import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengesService } from '@services/challenges.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ChallengeMessageComponent } from '@components/modals/challenge-message/challenge-message.component';

@Component( {
  selector: 'app-challenge-create-publication',
  templateUrl: './challenge-create-publication.component.html',
  styleUrls: ['./challenge-create-publication.component.scss']
} )
export class ChallengeCreatePublicationComponent implements OnInit {
  user: any;
  date = new Date( new Date() );
  form: FormGroup;
  loading: boolean;
  type: string;
  eventClosed: EventEmitter<any> = new EventEmitter();

  constructor( private fb: FormBuilder, private challengesService: ChallengesService,
               public bsModalRef: BsModalRef , private bsModalService: BsModalService) {
    this.createForm();
  }

  ngOnInit(): void {
    console.log( this.user, this.date, this.type );
  }

  onClickSave() {
    if ( this.form.valid ) {
      this.loading = true;
      this.challengesService.postSavePublicationExperience( this.form.value ).subscribe( ( res: any ) => {
        console.log( res );
        this.loading = false;
        this.eventClosed.emit( { value: true, type: this.type } );
        this.showChallengeMessage(this.type, {progress: res.data.progress, score: res.data.score});
        this.bsModalRef.hide();
      }, error => {
        this.loading = false;
      } );
    }
  }

  showChallengeMessage( type: string , data: any) {
    const initialState = { user: this.user, type, data };
    const modal = this.bsModalService.show( ChallengeMessageComponent, {
      class: 'ModalChallengeMessage modal-dialog-centered',
      initialState,
      backdrop: 'static',
      keyboard: false
    } );
  }

  createForm() {
    this.form = this.fb.group( {
      title: [null, Validators.required],
      detail: [null, Validators.required],
      challenge_id: [1, null]
    } );
  }

}
