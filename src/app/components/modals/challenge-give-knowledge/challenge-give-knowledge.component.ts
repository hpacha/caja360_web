import { Component, EventEmitter, OnInit } from '@angular/core';
import { ChallengesService } from '@services/challenges.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChallengeChooseKnowledgeComponent } from '@components/modals/challenge-choose-knowledge/challenge-choose-knowledge.component';

@Component( {
  selector: 'app-challenge-give-knowledge',
  templateUrl: './challenge-give-knowledge.component.html',
  styleUrls: ['./challenge-give-knowledge.component.scss']
} )
export class ChallengeGiveKnowledgeComponent implements OnInit {
  eventClosed: EventEmitter<any> = new EventEmitter();
  form: FormGroup;
  user: any;
  categories: any;
  events: any;
  loading: boolean;
  loadingCategories: boolean;
  loadingEvents: boolean;
  photos: any[] = [];
  Arrphotos: any[] = [];
  total = 0;
  images = [];
  showImages = [];
  authUser: any;
  urlPage: string;

  constructor( private fb: FormBuilder,
               private challengesService: ChallengesService,
               public bsModalRef: BsModalRef,
               private bsModalService: BsModalService ) {
  }

  ngOnInit(): void {
    console.log( this.user );
    this.getListCategories();
    this.getListEvents();
    this.createForm();
  }

  onDeselect() {
    this.bsModalRef.hide();
    const modal = this.bsModalService.show( ChallengeChooseKnowledgeComponent, {
      class: 'ModalChallengeGive modal-dialog-centered',
      backdrop: 'static',
      keyboard: false
    } );
  }

  onChangeFiles( event ) {
    console.log( 'dfd', this.images, event.target.files, event.target.files.length, 3 );
    this.total = this.images.length;
    for ( let i = 0; i < event.target.files.length; i++ ) {
      if ( i < ( 3 - this.total ) ) {
        this.images.push( event.target.files[ i ] );
        const imgTemp = event.target.files[ i ].name;
        const imgName = imgTemp.split( '.jpg' ).join( '' ).split( '.jpeg' ).join( '' ).split( '.png' );
        const reader = new FileReader();
        // tslint:disable-next-line:no-shadowed-variable
        reader.onload = ( event: any ) => {
          console.log( event );
          // @ts-ignore
          const tempVar = event.target.result;
          const splitedVar = tempVar.split( ',' );

          const arrayStructure = {
            original_name: imgName[ 0 ],
            resource: splitedVar,
            size: event.loaded
          };
          this.showImages.push( arrayStructure );
          console.log( arrayStructure );
        };
        reader.readAsDataURL( event.target.files[ i ] );
      }
    }
    // console.log( this.images, this.showImages );
    this.form.controls.images.setValue( this.images );
  }

  removeImage( idImage ) {
    /* const tempImage = this.showImages;
     tempImage.splice( idImage, 1 );*/
    this.images.splice( idImage, 1 );
    this.showImages.splice( idImage, 1 );
    /*  this.images = tempImage;
     this.showImages = tempImage;*/
    console.log( this.images, this.showImages );
  }

  onClickSave() {
    this.form.markAllAsTouched();
    if ( this.form.valid ) {
      this.loading = true;
      console.log( this.form.value );
      const formData: FormData = new FormData();
      formData.append( 'user_id', this.form.controls.user_id.value );
      formData.append( 'category_id', this.form.controls.category_id.value );
      formData.append( 'event_id', this.form.controls.event_id.value );
      formData.append( 'message', this.form.controls.message.value );

      for ( const file of this.images ) {
        formData.append( 'images[]', file );
      }
      // formData.append( 'images', this.form.controls.images.value );

      console.log( formData );
      this.challengesService.postSaveRecognition( formData ).subscribe( ( res: any ) => {
        console.log( res );
        this.eventClosed.emit( { value: true, data: res.data } );
        this.bsModalRef.hide();
        this.loading = false;
      }, error => this.loading = false );
    }
  }

  private getListCategories() {
    this.loadingCategories = true;
    this.challengesService.getKnowledgeCategories().subscribe( ( res: any ) => {
      console.log( res );
      this.categories = res.data;
      this.loadingCategories = false;
    }, error => { this.loadingCategories = false; } );
  }

  private getListEvents() {
    this.loadingEvents = true;
    this.challengesService.getKnowledgeEvents().subscribe( ( res: any ) => {
      console.log( res );
      this.events = res.data;
      this.loadingEvents = false;
    }, error => { this.loadingEvents = false; } );
  }

  createForm() {
    this.form = this.fb.group( {
      user_id: [this.user?.id, Validators.required],
      category_id: [null, Validators.required],
      event_id: [null, Validators.required],
      message: [null, Validators.required],
      images: [null, Validators.required]
    } );
  }

}
