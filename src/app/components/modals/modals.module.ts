import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';

import { NewsEditDateComponent } from './news-edit-date/news-edit-date.component';
import { NewsDeleteCommentComponent } from './news-delete-comment/news-delete-comment.component';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';

import { SpinnerModule } from '@shared/spinner/spinner.module';
import { ServicesModule } from '@services/services.module';
import { ControlMessagesModule } from '@shared/control-messages/control-messages.module';
import { SharedModule } from '@shared/shared.module';

import { HappyBirthdayComponent } from './happy-birthday/happy-birthday.component';
import { BirthdayComponent } from './birthday/birthday.component';
import { VtRequestsToApproveComponent } from './vt-requests-to-approve/vt-requests-to-approve.component';
import { BusinessCardComponent } from './business-card/business-card.component';
import { DeleteChatComponent } from './chat/delete-chat/delete-chat.component';
import { NewGroupComponent } from './chat/new-group/new-group.component';
import { LeaveGroupComponent } from './chat/leave-group/leave-group.component';
import { EditGroupComponent } from './chat/edit-group/edit-group.component';
import { DeleteGroupComponent } from './chat/delete-group/delete-group.component';
import { SharedFilesComponent } from './chat/shared-files/shared-files.component';
import { MeetingComponent } from './meeting/meeting.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { NewContractComponent } from './contracts/new-contract/new-contract.component';
import { ApplyMentorComponent } from './mentor/apply-mentor/apply-mentor.component';
import { ApplyCompetenceComponent } from './mentor/apply-competence/apply-competence.component';
import { ApplyStatusComponent } from './mentor/apply-status/apply-status.component';
import { ConfirmMentorComponent } from './mentor/confirm-mentor/confirm-mentor.component';
import { DeleteContractComponent } from './contracts/delete-contract/delete-contract.component';
import { ChallengeInfoComponent } from '@components/modals/challenge-info/challenge-info.component';
import { ChallengeCategorieComponent } from './challenge-categorie/challenge-categorie.component';
import { ChallengeCreatePublicationComponent } from './challenge-create-publication/challenge-create-publication.component';
import { ChallengeMessageComponent } from './challenge-message/challenge-message.component';
import { ChallengeValuesComponent } from './challenge-values/challenge-values.component';
import { ChallengeGiveKnowledgeComponent } from './challenge-give-knowledge/challenge-give-knowledge.component';
import { ChallengeChooseKnowledgeComponent } from './challenge-choose-knowledge/challenge-choose-knowledge.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DetailCarouselComponent } from './detail-carousel/detail-carousel.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { MentorRequestComponent } from './mentor-request/mentor-request.component';
import { MentorRejectRequestComponent } from './mentor-reject-request/mentor-reject-request.component';
import { SuggestMentorComponent } from './suggest-mentor/suggest-mentor.component';
import { SendFeedbackComponent } from './send-feedback/send-feedback.component';
import { SosComponent } from './sos/sos.component';
import { ConstancyComponent } from './constancy/constancy.component';
import { DefaultImagesComponent } from './default-images/default-images.component';
import { PreviewComponent } from './preview/preview.component';
import { SelectPersonGroupComponent } from './select-person-group/select-person-group.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
defineLocale('es', esLocale);

@NgModule({
  declarations: [
    ApplyMentorComponent,
    ApplyCompetenceComponent,
    ApplyStatusComponent,
    ConfirmMentorComponent,
    NewsEditDateComponent,
    NewsDeleteCommentComponent,
    HappyBirthdayComponent,
    BirthdayComponent,
    VtRequestsToApproveComponent,
    BusinessCardComponent,
    DeleteChatComponent,
    NewGroupComponent,
    LeaveGroupComponent,
    EditGroupComponent,
    DeleteGroupComponent,
    SharedFilesComponent,
    ScheduleComponent,
    MeetingComponent,
    NewContractComponent,
    DeleteContractComponent,
    ChallengeInfoComponent,
    ChallengeCategorieComponent,
    ChallengeCreatePublicationComponent,
    ChallengeMessageComponent,
    ChallengeValuesComponent,
    ChallengeGiveKnowledgeComponent,
    ChallengeChooseKnowledgeComponent,
    DetailCarouselComponent,
    MentorRequestComponent,
    MentorRejectRequestComponent,
    SuggestMentorComponent,
    SendFeedbackComponent,
    SosComponent,
    ConstancyComponent,
    DefaultImagesComponent,
    PreviewComponent,
    SelectPersonGroupComponent
  ], imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        FormsModule,
        ButtonsModule.forRoot(),
        TabsModule.forRoot(),
        PerfectScrollbarModule,
        ProgressbarModule.forRoot(),
        SpinnerModule,
        ServicesModule,
        SharedModule,
        ControlMessagesModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatGridListModule,
        NgSelectModule,
        BsDatepickerModule.forRoot(),
        PaginationModule,
        CarouselModule.forRoot(),
        LazyLoadImageModule,
    ],
})
export class ModalsModule { }
