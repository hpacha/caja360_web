import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { BsModalRef } from 'ngx-bootstrap/modal';
import Swal from 'sweetalert2';

import { UserService } from '@services/user.service';
import { ReactiveFormsService } from '@shared/services/reactive-forms.service';

@Component({
  selector: 'app-business-card',
  templateUrl: './business-card.component.html',
  styleUrls: ['./business-card.component.scss']
})
export class BusinessCardComponent implements OnInit {
  formGroup: FormGroup;
  alert: any;
  qr: any;
  loading: boolean;
  sharedWhatsapp: boolean;
  userProfile: any;

  constructor(
    public bsModalRef: BsModalRef,
    private userService: UserService,
    private domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private reactiveFormsService: ReactiveFormsService,
  ) {}

  ngOnInit() {
    this.createForm();
    this.getQr();
  }

  onClickSharedEmail() {
    this.sharedWhatsapp = !this.sharedWhatsapp;
  }

  onClickSend() {
    this.reactiveFormsService.markFormGroupTouched(this.formGroup);
    if (this.formGroup.valid) {
      this.loading = true;
      const params = {emails: this.formGroup.controls.emails.value.map(item => item.email)};
      this.userService.postBusinessCardShare(params).subscribe((res: any) => {
        this.loading = false;
        this.bsModalRef.hide();
        Swal.fire({ title: 'Tarjeta de Presentación', text: 'Se envió correctamente', icon: 'success', iconHtml: null, timer: 4000 })
      });
    }
  }

  onClickSharedWs() {
    const data = 'Hola, te comparto mi tarjeta de presentación, mis datos son: '
      + 'Nombre: ' + this.userProfile.name + ' ' + this.userProfile.last_name + ', ' + '\n'
      + 'Organización: ' + 'Caja Arequipa' + ', ' + '\n'
      + (this.userProfile && this.userProfile.headquarter && this.userProfile.headquarter.title ? 'Sede: ' + this.userProfile.headquarter.title : '') + ', ' + '\n'
      + (this.userProfile && this.userProfile.occupation && this.userProfile.occupation.title ? 'Cargo: ' + this.userProfile.occupation.title : '') + ', ' + '\n'
      + (this.userProfile && this.userProfile.phone && this.userProfile.phone.number ? 'Celular: ' + this.userProfile.phone.number : '') + ', ' + '\n'
      + (this.userProfile && this.userProfile.annexed && this.userProfile.annexed.number ? 'Anexo: ' + this.userProfile.annexed.number : '') + ', ' + '\n'
      + (this.userProfile && this.userProfile.email && this.userProfile.email.email ?  'Correo: ' + this.userProfile.email.email : '');
    return 'https://wa.me/' + '?text=' + data;
  }

  getArrayEmail() {
    const formarray: any = this.formGroup.get('emails') as FormArray;
    return formarray.controls;
  }

  onClickEmailAdd() {
    const items = this.formGroup.get('emails') as FormArray;
    items.push(this.emailCreateItem());
  }

  onClickEmailRemove(index: number) {
    const formarray = this.formGroup.get('emails') as FormArray;
    formarray.removeAt(index);
  }

  private getQr() {
    this.loading = true;
    this.userService.getQR().subscribe((res: any) => {
      const qp = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/svg+xml;base64,' + res.data);
      this.qr = qp;
      this.loading = false;
    });
  }

  private createForm(){
    this.formGroup = this.formBuilder.group({
      emails: this.formBuilder.array([this.emailCreateItem()])
    });
  }

  private emailCreateItem(): FormGroup {
    return this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
    });
  }
}
