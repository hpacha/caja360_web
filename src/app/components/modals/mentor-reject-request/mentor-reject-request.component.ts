import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CommunityService } from '@services/community.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component( {
  selector: 'app-mentor-reject-request',
  templateUrl: './mentor-reject-request.component.html',
  styleUrls: ['./mentor-reject-request.component.scss']
} )
export class MentorRejectRequestComponent implements OnInit {
  data: any;
  loading: boolean;
  form: FormGroup;
  eventClosed: EventEmitter<any> = new EventEmitter();

  constructor( public bsModalRef: BsModalRef,
               private fb: FormBuilder,
               private communityService: CommunityService ) {
    this.createForm();
  }

  ngOnInit(): void {
    console.log( this.data );
  }

  onclickSuggestedMentor() {
    this.form.markAllAsTouched();
    if ( this.form.valid ) {
      console.log( this.form.value );
      this.eventClosed.emit( { status: true, data: this.data, reason_rejection: this.form.controls.reason_rejection.value } );
      this.bsModalRef.hide();
    }
  }

  onClickClose() {
    this.eventClosed.emit( { status: false } );
    this.bsModalRef.hide();
  }

  private createForm() {
    this.form = this.fb.group( {
      reason_rejection: [null, Validators.required],
    } );
  }

}
