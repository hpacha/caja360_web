import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorRejectRequestComponent } from './mentor-reject-request.component';

describe('MentorRejectRequestComponent', () => {
  let component: MentorRejectRequestComponent;
  let fixture: ComponentFixture<MentorRejectRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorRejectRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorRejectRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
