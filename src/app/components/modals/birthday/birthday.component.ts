import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChatService } from '@services/chat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.scss']
})
export class BirthdayComponent implements OnInit {
  activity: any;

  constructor(
    public bsModalRef: BsModalRef,
    private chat: ChatService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onClickChat() {
    this.bsModalRef.hide();
    localStorage.setItem( 'chatId', JSON.stringify( { id: this.activity.user.id } ) );
    this.router.navigate(['/chat']);
  }
}
