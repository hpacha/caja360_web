import { Component, EventEmitter, OnInit } from '@angular/core';
import { finalize, takeUntil } from 'rxjs/operators';
import { UserService } from '@services/user.service';
import { ReplaySubject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ChallengeGiveKnowledgeComponent } from '@components/modals/challenge-give-knowledge/challenge-give-knowledge.component';

@Component( {
  selector: 'app-challenge-choose-knowledge',
  templateUrl: './challenge-choose-knowledge.component.html',
  styleUrls: ['./challenge-choose-knowledge.component.scss']
} )
export class ChallengeChooseKnowledgeComponent implements OnInit {
  timeout = null;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  page: number;
  tabUsers: any = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, listUser: null };
  tabUsersResponse: any;
  queryField: FormControl = new FormControl();
  urlPage: string;
  eventClosed: EventEmitter<any> = new EventEmitter();

  constructor( private  userService: UserService,
               public bsModalRef: BsModalRef, ) { }

  async ngOnInit() {
    await this.getUserSearch();
  }

  openModalChallengeGiveKnowledge( item ) {
    this.eventClosed.emit( { value: true, item } );
    this.bsModalRef.hide();
  }

  getUserSearch( queryText = null ) {
    // const thisIn = this;
    return new Promise<any[]>( ( resolve, reject ) => {
      if ( this.tabUsers.currentPage === 1 ) {
        this.tabUsers.preload = true;
      } else {
        this.tabUsers.preloadPage = true;
      }
      const parameters: any = {};
      parameters.hint = ( queryText ) ? queryText : null;
      /* parameters.headquarter = [this.selectedHeadquarter];
       parameters.area = [this.selectedArea];
       parameters.occupation = [this.selectedOccupation];*/
      const currentPage = this.page ? this.page : this.tabUsers.currentPage; // this.tabUsers.currentPage
      this.userService.getSearchUsers( parameters, currentPage ).pipe(
        takeUntil( this.destroyed$ ),
        finalize( () => {
          if ( this.tabUsers.currentPage === 1 ) {
            this.tabUsers.preload = false;
          } else {
            this.tabUsers.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        console.log( res );
        if ( res ) {
          this.tabUsersResponse = res;
          this.tabUsers.listUser = res.data.data;
          // ( this.tabUsers.listUser ) ? ( this.tabUsers.listUser ).concat( res.data.data ) : res.data.data;
          console.log( this.tabUsers.listUser );
          this.tabUsers.lastPage = res.data.last_page;
        }
      }, error => {
        this.tabUsers.listUser = [];
        this.tabUsers.lastPage = 0;
      } );
    } );
  }

  onSearchUsers( queryText ) {
    // const thisIn = this;
    if ( this.timeout ) {
      window.clearTimeout( this.timeout );
    }
    this.timeout = window.setTimeout( () => {
      this.timeout = null;
      if ( queryText != null && queryText.length >= 3 ) {
        this.tabUsers.preload = true;
        const parameters: any = {};
        parameters.hint = ( queryText ) ? queryText : null;
        /*  parameters.headquarter = [this.selectedHeadquarter];
         parameters.area = [this.selectedArea];
         parameters.occupation = [this.selectedOccupation];*/
        parameters.hint = queryText;
        this.userService.getSearchUsers( parameters, 1 )
        .pipe( takeUntil( this.destroyed$ ) )
        .subscribe( ( res: any ) => {
          console.log( res );
          if ( res ) {
            this.tabUsersResponse = res;
            this.tabUsers.listUser = ( res.data.data );
            this.tabUsers.preload = false;
            console.log( this.tabUsers.listUser );
          }
        }, ( err: any ) => {
          this.tabUsers.preload = false;
        } );
      } else if ( queryText != null && queryText.length === 0 ) {
        this.tabUsers.listUser = [];
        this.tabUsers.currentPage = 1;
        this.getUserSearch();
      }
    }, 250 );
  }

  pageChanged( e: any ) {
    console.log( e );
    this.page = e.page;
    console.log( this.page );
    this.getUserSearch();
  }

}
