import { Component, OnInit, ViewChild, NgZone, OnDestroy, EventEmitter } from '@angular/core';

import { Subject, ReplaySubject} from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import * as moment from 'moment';
import 'moment/locale/es';
import Swal from 'sweetalert2';

import { CalendarService } from '@services/calendar.service';
import * as Globals from '@utils/globals';

@Component({
  selector: 'app-vt-requests-to-approve',
  templateUrl: './vt-requests-to-approve.component.html',
  styleUrls: ['./vt-requests-to-approve.component.scss']
})
export class VtRequestsToApproveComponent implements OnInit, OnDestroy {
  @ViewChild('staticTabs', {static: true}) staticTabs: TabsetComponent;
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  loading: boolean;
  preload: boolean;
  preloadPage: boolean = false;
  type: string;
  listItems: any;
  listCurrentPage = 1;
  listLastPage = 0;
  listMonths = moment.localeData('es').months();
  listMonthsShort = moment.localeData('es').monthsShort();
  entity: string;
  entityModify: boolean = false;

  constructor(
    private zone: NgZone,
    private calendarService: CalendarService,
    public bsModalRef: BsModalRef
  ) {}

  async ngOnInit() {
    const thisIn = this;
    thisIn.onClose = new Subject();
    if (thisIn.entity === 'vacation') {
      thisIn.staticTabs.tabs[1].disabled = !this.staticTabs.tabs[1].disabled;
    } else if (thisIn.entity === 'telecommuting') {
      thisIn.staticTabs.tabs[0].disabled = !this.staticTabs.tabs[0].disabled;
    }
    await thisIn.getSlopes();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  getSlopes() {
    return new Promise((resolve, reject) => {
      const thisIn = this;
      if(thisIn.listCurrentPage === 1) thisIn.preload = true; else thisIn.preloadPage = true;
      thisIn.calendarService.getSlopes(thisIn.listCurrentPage, thisIn.entity)
        .pipe(
          finalize(() => {
            if(thisIn.listCurrentPage === 1) thisIn.preload = false; else thisIn.preloadPage = false;
            resolve();
          }),
          takeUntil(this.destroyed$),
        ).subscribe((res: any) => {
          res.data.data = thisIn.formatData(res.data.data);
          thisIn.listItems = (thisIn.listItems) ? (thisIn.listItems).concat(res.data.data) : res.data.data;
          thisIn.listLastPage = res.data.data.last_page;
        }, error => {
          thisIn.listItems = [];
          thisIn.listLastPage = 0;
        });
    });
  }

  onScrolledDownEnd() {
    if(this.listCurrentPage <= this.listLastPage && this.preloadPage === false) {
      this.zone.run(() => {
        this.listCurrentPage = this.listCurrentPage + 1;
        if(this.listCurrentPage <= this.listCurrentPage)
          this.getSlopes();
      });
    }
  }

  formatData(listItems: any) {
    if(this.entity === 'vacation') {
      return listItems = listItems.map((item) => {
        item.start_day = moment(item.start+ 'T00:00:00').format("DD");
        item.start_month = this.listMonthsShort[new Date(item.start+ 'T00:00:00').getMonth()];
        item.end_day = moment(item.end+ 'T00:00:00').format("DD");
        item.end_month = this.listMonthsShort[new Date(item.end+ 'T00:00:00').getMonth()];
        return item;
      });
    } else {
      return listItems = listItems.map((item) => {
        item.months = item.dates.reduce((acc: any, item) => {
          let key = moment(item.date + 'T00:00:00').format("MM/YYYY");
          acc[key] = acc[key] || [];
          acc[key].push({
            day: moment(item.date + 'T00:00:00').format("DD"),
            month: this.listMonths[new Date(item.date + 'T00:00:00').getMonth()],
          });
          return acc;
        }, []);
        item.start = item.dates[0].date;
        item.status_type = (item.status === 1) ? 'pending' : (item.status === 2) ? 'approve' : 'not_approve';
        return item;
      });
    }

  }

  onApprove(item: any) {
    const thisIn = this;
    const parameters: any = {}
    parameters.id = item.id;
    Swal.fire({
      title: `<span class="positive">Aprobar solicitud<span>`,
      text: '¿Estás seguro que deseas aprobar la solicitud?',
      icon: 'question',
      iconHtml: null,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Aprobar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      if (result.isConfirmed) {
        thisIn.loading = true;
        thisIn.calendarService.approveApply(parameters, thisIn.entity)
        .pipe(finalize(() => {setTimeout(() => { this.loading = false; });}), takeUntil(thisIn.destroyed$)).subscribe((res: any) => {
          thisIn.listItems = thisIn.listItems.filter(function(el){
            return (el.id !== res.data.id)
          });
          thisIn.entityModify = true;
          setTimeout(function () { if(!thisIn.listItems.length) { thisIn.onCloseModal(); }}, 1500);
          Swal.fire({ title: 'Aprobar solicitud', text: 'La solicitud fue aprobada exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Aprobar solicitud', text: 'Ocurrió un error al aprobar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
        });
      }
    })
  }

  async onReject(item: any) {
    const thisIn = this;
    const { value: subject } = await Swal.fire({
      title: `Rechazar solicitud`,
      text: '¿Estás seguro que deseas rechazar la solicitud?',
      icon: 'question',
      iconHtml: null,
      input: 'text',
      inputPlaceholder: '¿Cuál es el motivo?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: `Rechazar`,
      cancelButtonText: `Cancelar`,
      inputValidator: (value) => {
        if (!value) {
          return 'Necesitas ingresar el motivo'
        }
      }
    })

    if (subject) {
      const parameters: any = {}
      parameters.id = item.id;
      parameters.motivo = subject;
      thisIn.loading = true;
      thisIn.calendarService.rejectApply(parameters, thisIn.entity)
        .pipe(finalize(() => {setTimeout(() => { this.loading = false; });}), takeUntil(thisIn.destroyed$)).subscribe((res: any) => {
          thisIn.listItems = thisIn.listItems.filter(function(el){
            return (el.id !== res.data.id)
          });
          thisIn.entityModify = true;
          setTimeout(function () { if(!thisIn.listItems.length) { thisIn.onCloseModal(); }}, 1500);
          Swal.fire({ title: 'Rechazar solicitud', text: 'La solicitud fue rechazada exitosamente', icon: 'success', iconHtml: null, timer: 4000 })
        }, error => {
          Swal.fire({ title: 'Rechazar solicitud', text: 'Ocurrió un error al rechazar la solicitud', icon: 'error', iconHtml: null, timer: 4000 })
        });
    }
  }

  onCloseModal() {
    this.onClose.next(true);
    this.bsModalRef.hide();
    this.eventClosed.emit(this.entityModify);
  }

  trackBy(index: number, item: any) {
    return item.id;
  }

  returnZero() {
    return 0
  }
}
