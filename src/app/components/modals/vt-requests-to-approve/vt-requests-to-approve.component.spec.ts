import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VtRequestsToApproveComponent } from './vt-requests-to-approve.component';

describe('VtRequestsToApproveComponent', () => {
  let component: VtRequestsToApproveComponent;
  let fixture: ComponentFixture<VtRequestsToApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VtRequestsToApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VtRequestsToApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
