import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { ChallengesService } from '@services/challenges.service';

@Component( {
  selector: 'app-challenge-categorie',
  templateUrl: './challenge-categorie.component.html',
  styleUrls: ['./challenge-categorie.component.scss']
} )
export class ChallengeCategorieComponent implements OnInit {
  challengeCategorie: any;
  color: string;

  constructor(
    public bsModalRef: BsModalRef,
    private router: Router,
    private challengesService: ChallengesService
  ) {}

  ngOnInit(): void {
    console.log( this.challengeCategorie, this.color );
  }

  onClickNoModal() {
    this.challengesService.putNoModal({id_challenges: this.challengeCategorie.id}).subscribe((res: any) => {
      this.onClickRouter();
    }, error => {});
  }

  onClickRouter() {
    switch ( this.color ) {
      case 'turquoise':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/compartir-experiencias' );
      case 'coral':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-videos/2' );
      case 'lightgreen':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/discusion-de-lectura/3' );
      case 'orange':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/trivias/4' );
      case 'red':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/recibir-conocimiento/5' );
      case 'lightblue':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/dar-conocimiento/6' );
      case 'green':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/foros-charlas-y-conferencias' );
      case 'blue':
        this.bsModalRef.hide();
        return this.router.navigateByUrl( '/cultura-caja-con-sentido/caja-con-sentido' );
    }
  }
}
