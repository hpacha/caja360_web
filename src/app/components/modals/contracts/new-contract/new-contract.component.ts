import { Component, OnInit, OnDestroy, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import 'moment/locale/es';

import { ContractService } from '@services/contracts.service'

@Component({
  selector: 'app-new-contract',
  templateUrl: './new-contract.component.html',
  styleUrls: ['./new-contract.component.scss']
})
export class NewContractComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  loading: boolean;
  formGroup: FormGroup;
  bsValue = new Date();
  locale = 'es';
  filename: string = 'Adjuntar contrato';
  contractId: string;
  title: string;
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService,
    private contractService: ContractService
  ) {
    this.localeService.use(this.locale);
  }

  ngOnInit(): void {
    this.createForm();
    this.onClose = new Subject();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.unsubscribe();
  }

  onFileChange(fileInput: any) {
    const thisIn = this;
    var file = fileInput.target.files[0];
    if(file['type'] !== 'application/pdf') {
      Swal.fire({ title: 'Adjuntar contrato', text: 'Solo se admiten archivos .pdf', icon: 'error', iconHtml: null, timer: 4000 })
      return;
    }
    if (file) {
      thisIn.filename = file.name;
      const myReader = new FileReader();
      myReader.onloadend = (e) => {
        let file = <string>myReader.result;
        thisIn.formGroup.get('file').setValue(file);
      };
      myReader.readAsDataURL(file);
    }
  }

  onSubmit() {
    const thisIn = this;
    if(thisIn.formGroup.invalid) {
      thisIn.formGroup.markAllAsTouched();
    } else {
      thisIn.loading = true;
      const formData = new FormData();
      formData.append("contracts_id", thisIn.contractId);
      formData.append("file", thisIn.fileInput.nativeElement.files[0]);
      formData.append("date_start", moment.utc(thisIn.formGroup.value.date_start).format("YYYY-MM-DD"));
      formData.append("date_end", moment.utc(thisIn.formGroup.value.date_end).format("YYYY-MM-DD"));
      if (thisIn.formGroup.value.trialperiod) {
        formData.append("is_test", thisIn.formGroup.value.trialperiod);
        formData.append("date_end_test", moment.utc(thisIn.formGroup.value.date_trial).format("YYYY-MM-DD"));
      }
      thisIn.contractService.postStoreContract(formData).pipe(
        takeUntil(thisIn.destroyed$),
        finalize(() => {
          thisIn.loading = false;
        }),
      ).subscribe((res: any) => {
        thisIn.formGroup.reset();
        (<HTMLInputElement>document.getElementById('uploadFile')).value = "";
        Swal.fire({ title: 'Adjuntar Contrato', text: 'El contrato fue adjuntado exitosamente', icon: 'success', iconHtml: null, timer: 4000 }).then(() => {
          thisIn.onClose.next(true);
          thisIn.bsModalRef.hide();
          thisIn.eventClosed.emit(res);
        });
      }, error => {
        Swal.fire({ title: 'Adjuntar contrato', html: `Ocurrió un error al adjuntar el contrato<br/>${error.error.message}`, icon: 'error', iconHtml: null, timer: 4000 })
      });
    }
  }

  createForm() {
    this.formGroup = this.formBuilder.group( {
      date_start: [null, [Validators.required]],
      date_end: [null],
      trialperiod: [null],
      date_trial: [null],
      file: [null, [Validators.required]],
    });
    this.formGroup.get('trialperiod').valueChanges
      .subscribe(value => {
        if(value) {
          this.formGroup.get('date_trial').setValidators(Validators.required)
        } else {
          this.formGroup.get('date_trial').clearValidators();
          this.formGroup.get('date_trial').updateValueAndValidity();
        }
      }
    );
  }
}
