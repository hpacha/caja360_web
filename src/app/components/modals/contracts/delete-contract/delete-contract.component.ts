import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component( {
  selector: 'app-delete-contract',
  templateUrl: './delete-contract.component.html',
  styleUrls: ['./delete-contract.component.scss']
} )
export class DeleteContractComponent implements OnInit {
  loading: boolean;

  constructor( public bsModalRef: BsModalRef ) { }

  ngOnInit(): void {
  }
}
