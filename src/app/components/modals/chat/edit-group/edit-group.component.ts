import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChatService } from '@services/chat.service';
import { Subject } from 'rxjs';

@Component( {
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.scss']
} )
export class EditGroupComponent implements OnInit {
  loading: boolean;
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  chatRoom: any;
  chatRoomFiles: any;
  chatRoomFilesTotal: any;
  listSetUsers: Array<any> = [];
  createdBy: any;
  userInfo: any;
  userId: any;

  constructor(
    public bsModalRef: BsModalRef,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    this.getChatRoomsFiles();
    this.showChatRooms();
    this.onClose = new Subject();
  }

  getChatRoomsFiles() {
    this.loading = true;
    this.chatService.getChatRoomsFiles( this.chatRoom.id ).subscribe( ( res: any ) => {
      this.chatRoomFiles = res.data.data;
      this.chatRoomFilesTotal = res.data.total;
      this.chatRoomFilesTotal && this.chatRoomFiles.map( function( item ) {
        switch ( true ) {
          case ( ( /image/.exec( item.mime ) ) !== null ): {
            item.icon = 'image';
            item.footer = false;
          }
            break;
          case ( ( /video/.exec( item.mime ) ) !== null ): {
            item.icon = 'video';
            item.footer = false;
          }
            break;
          case ( ( /word/.exec( item.mime ) ) !== null ): {
            item.icon = 'icon-word.svg';
            item.footer = true;
          }
            break;
          case ( ( /presentation/.exec( item.mime ) ) !== null ): {
            item.icon = 'icon-power.svg';
            item.footer = true;
          }
            break;
          case ( ( /pdf/.exec( item.mime ) ) !== null ): {
            item.icon = 'icon-filer.svg';
            item.footer = true;
          }
            break;
          case ( ( /sheet/.exec( item.mime ) ) !== null ): {
            item.icon = 'icon-excel.svg';
            item.footer = true;
          }
            break;
          default: {
            item.icon = 'upload-file.svg';
            item.footer = true;
          }
            break;
        }
        return item;
      } );
      this.loading = false;
    }, error => {
      this.loading = false;
    } );
  }

  showChatRooms() {
    const thisIn = this;
    this.chatService.showChatRooms( this.chatRoom.id ).subscribe( ( res: any ) => {
      thisIn.createdBy = res.data.created_by;
      res.data.users.forEach( function( item ) {
        if ( item.id !== thisIn.createdBy.id ) {
          thisIn.listSetUsers.push( { 'id': item.id, 'name': item.last_name + ' ' + item.name, 'photo': item.photo } );
        }
      } );
    }, error => {
      console.log( error );
    } );
  }

  onClickNewGroup( event, picture: boolean ) {
    this.onClose.next( true );
    this.bsModalRef.hide();
    if (picture === true ) {
    this.eventClosed.emit( { chatRoom: this.chatRoom, picture: true } );
    } else {
      this.eventClosed.emit( { chatRoom: this.chatRoom} );
    }
    // event.preventdefault();
  }
}
