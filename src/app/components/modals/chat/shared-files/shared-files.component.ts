import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChatService } from '../../../../services/chat.service';

@Component({
  selector: 'app-shared-files',
  templateUrl: './shared-files.component.html',
  styleUrls: ['./shared-files.component.scss']
})
export class SharedFilesComponent implements OnInit {
  loading: boolean;
  eventClosed: EventEmitter<any> = new EventEmitter();
  chatRoom: any;
  chatRoomFiles: any;
  chatRoomFilesTotal: any;

  constructor(
    public bsModalRef: BsModalRef,
    private chatService: ChatService
  ) { }

  ngOnInit(): void {
    this.getChatRoomsFiles();
  }

  getChatRoomsFiles() {
    this.loading = true;
    this.chatService.getChatRoomsFiles(this.chatRoom.id).subscribe((res: any) => {
      this.chatRoomFiles = res.data.data;
      this.chatRoomFilesTotal = res.data.total;
      this.chatRoomFilesTotal && this.chatRoomFiles.map(function(item) {
        switch (true) {
          case ((/image/.exec(item.mime)) !== null): {item.icon = "image"; item.footer = false;} break;
          case ((/video/.exec(item.mime)) !== null): {item.icon = "video"; item.footer = false;} break;
          case ((/word/.exec(item.mime)) !== null): {item.icon = "icon-word.svg"; item.footer = true;} break;
          case ((/presentation/.exec(item.mime)) !== null): {item.icon = "icon-power.svg"; item.footer = true;} break;
          case ((/pdf/.exec(item.mime)) !== null): {item.icon = "icon-filer.svg"; item.footer = true;} break;
          case ((/sheet/.exec(item.mime)) !== null): {item.icon = "icon-excel.svg"; item.footer = true;} break;
          default: {item.icon = "upload-file.svg"; item.footer = true;} break;
        }
        return item;
      });
      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }
}
