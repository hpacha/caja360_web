import { Component, ElementRef, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user.service';
import { ChatService } from '../../../../services/chat.service';
import { SweetalertService } from '../../../../shared/services/sweetalert.service';
import 'rxjs-compat/add/operator/debounceTime';
import 'rxjs-compat/add/operator/throttleTime';
import 'rxjs-compat/add/operator/distinctUntilChanged';
import 'rxjs-compat/add/operator/switchMap';
import 'rxjs-compat/add/operator/takeUntil';
import 'rxjs-compat/add/observable/of';
import { Observable, Subject } from 'rxjs';

@Component( {
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
} )
export class NewGroupComponent implements OnInit {
  edit: string;
  loading: boolean;
  formGroup: FormGroup;
  onClose: Subject<boolean>;
  eventClosed: EventEmitter<any> = new EventEmitter();
  typeAheadListUsers = new EventEmitter<string>();
  destroy$: Subject<boolean> = new Subject<boolean>();
  listUsers: any;
  listSetUsers: Array<any> = [];
  chatRoom: any;
  chatUser: any;
  is_group: boolean;
  userInfo: any;
  picture: string;
  photo: any;
  @ViewChild('fileInputPhotoChat') fileInputPhotoChat: ElementRef;

  constructor(
    public bsModalRef: BsModalRef,
    private userService: UserService,
    private chatService: ChatService,
    private sweetalertService: SweetalertService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    const thisIn = this;
    console.log('chatRoom', this.chatRoom, this.is_group, this.picture);
    thisIn.userInfo = JSON.parse( localStorage.getItem( 'user' ) );
    this.createForm();
    if ( this.chatRoom ) {
      this.setFormData();
    }
    this.typeAheadListUsers
    .debounceTime( 250 )
    .throttleTime( 250 )
    .distinctUntilChanged()
    .switchMap( term => ( term && term.length ) >= 3 ? this.getUsers( term ) : Observable.of( [] ) )
    .takeUntil( this.destroy$ )
    .subscribe( ( results: any ) => {
      if ( results.data ) {
        thisIn.listUsers = ( results.data.data ).map( function( user ) {
          if ( user.id !== thisIn.userInfo.id ) {
            return { 'id': user.id, 'name': user.last_name + ' ' + user.name, 'photo': user.photo, 'add': true };
          }
        } );
        thisIn.listSetUsers.forEach( function( item ) {
          thisIn.listUsers = thisIn.listUsers.filter( el => {
            return el.id !== item.id;
          } );
        } );
      }
    } );
    this.onClose = new Subject();
  }

  createForm() {
    const thisIn = this;
    thisIn.formGroup = thisIn.formBuilder.group( {
      name: [null, Validators.required],
      photo: [null, null],
    } );
  }

  onChangeFiles(event) {
    console.log('event', event);
    console.log('event', event.target.files[0]);

    console.log( 'dfd',  event.target.files[0] );
    const imgTemp = event.target.files[ 0 ].name;
    const imgName = imgTemp.split( '.jpg' ).join( '' ).split( '.jpeg' ).join( '' ).split( '.png' );
    const reader = new FileReader();
    // tslint:disable-next-line:no-shadowed-variable
    reader.onload = ( event: any ) => {
      console.log( event );
      // @ts-ignore
      const tempVar = event.target.result;
      const splitedVar = tempVar.split( ',' );

      const arrayStructure = {
        original_name: imgName[ 0 ],
        resource: event.target.result,
        size: event.loaded
      };
      this.photo = arrayStructure;
      console.log( arrayStructure );
    };
    reader.readAsDataURL( event.target.files[ 0 ] );
    // console.log( this.images, this.showImages );
    this.formGroup.controls.photo.setValue( event.target.files[ 0 ] );
  }

  setFormData() {
    const thisIn = this;
    this.chatRoom.users.forEach( function( item ) {
      if ( thisIn.userInfo.id !== item.id ) {
        thisIn.listSetUsers.push( { 'id': item.id, 'name': item.last_name + ' ' + item.name, 'photo': item.photo, 'add': true } );
      }
    } );
    thisIn.formGroup.setValue( {
      name: thisIn.chatRoom.name,
      photo: thisIn.chatRoom.photo
    } );
  }

  getUsers( term: string ): Observable<any> {
    const parameters: any = {};
    parameters.hint = term;
    return this.userService.getSearchUsers( parameters );
  }

  clearListUsers() {
    this.listUsers = [];
  }

  addListUsers( event: any ) {
    this.listSetUsers.push( event );
  }

  removeListUsers( item ) {
    this.listSetUsers = this.listSetUsers.map( function( el ) {
      if ( el.id === item ) {
        el.add = !el.add;
      }
      return el;
    } );
  }

  public compareFn( a, b ): boolean {
    return a == b;
  }

  newChat( userId ) {
    this.chatUser = userId;
    if ( !this.is_group ) {
      this.putChatRooms();
    } else {
      console.log( 'newGroup' );
    }
  }

  onSubmit() {
    if ( this.formGroup.invalid || !this.listSetUsers.length ) {
      this.formGroup.markAllAsTouched();
    } else {
      const resultado = this.listSetUsers.find( item => item.add === true );
      if ( resultado ) {
        if ( this.chatRoom ) {
          this.updateChatRooms();
        } else {
          this.putChatRooms();
        }
      }
    }
  }

  updateChatRooms() {
    //return console.log(this.formGroup.value)
    this.loading = true;
    const formData = new FormData();
    formData.append( 'name', this.formGroup.value.name );
    if ( this.formGroup.value.photo !== null ) {
      formData.append( 'photo', this.formGroup.value.photo );
    }
    formData.append( '_method', 'patch' );
    let index = 0;
    this.listSetUsers.forEach( function( item ) {
      if ( item.add ) {
        formData.append( `users[${index}]`, item.id );
        index = index + 1;
      }
    } );
    this.chatService.patchChatRooms( this.chatRoom.id, formData ).subscribe( ( res: any ) => {
      this.loading = false;
      this.onClose.next( true );
      this.bsModalRef.hide();
      this.eventClosed.emit( res.data );
      this.sweetalertService
      .swal( {
        title: 'Editar Grupo',
        text: 'Grupo actualizado',
      } );
    }, error => {
      this.loading = false;
      this.sweetalertService
      .swal( {
        title: 'Editar Grupo',
        text: 'ha ocurrido un error',
      } );
    } );
  }

  putChatRooms() {
    this.loading = true;
    const parameters: any = {};
    if ( this.is_group ) {
      parameters.name = this.formGroup.value.name;
    }
    let usersAdd = [];
    if ( this.is_group ) {
      this.listSetUsers.forEach( function( user ) {
        if ( user.add ) {
          usersAdd.push( user.id );
        }
      } );
    } else {
      usersAdd.push( this.chatUser );
    }
    parameters.users = usersAdd;
    parameters.is_group = this.is_group;
    this.chatService.putChatRooms( parameters ).subscribe( ( res: any ) => {
      this.loading = false;
      this.onClose.next( true );
      this.bsModalRef.hide();
      this.eventClosed.emit( res.data );
      this.sweetalertService
      .swal( {
        title: ( this.is_group ) ? 'Nuevo Grupo' : 'Nuevo Chat',
        text: ( this.is_group ) ? 'Grupo creado' : 'Chat creado',
      } );
    }, ( error: any ) => {
      this.loading = false;
      this.sweetalertService
      .swal( {
        title: ( this.is_group ) ? 'Nuevo Grupo' : 'Nuevo Chat',
        text: ( this.is_group ) ? 'El grupo ya existe' : 'El chat ya existe',
      } );
      if ( !this.is_group && ( error.data && error.data.id ) ) {
        this.onClose.next( true );
        this.bsModalRef.hide();
        this.eventClosed.emit( error.data );
      }
    } );
  }
}
