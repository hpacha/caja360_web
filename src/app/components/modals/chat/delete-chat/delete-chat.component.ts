import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChatService } from '../../../../services/chat.service';
import { SweetalertService } from '../../../../shared/services/sweetalert.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-delete-chat',
  templateUrl: './delete-chat.component.html',
  styleUrls: ['./delete-chat.component.scss']
})
export class DeleteChatComponent implements OnInit {
  loading: boolean;
  eventClosed: EventEmitter<any> = new EventEmitter();
  onClose: Subject<boolean>;
  chatRoom: any;

  constructor(
    public bsModalRef: BsModalRef,
    private chatService: ChatService,
    private sweetalertService: SweetalertService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  onSubmit() {
    this.delChatRooms();
  }

  delChatRooms() {
    const thisIn = this;
    this.loading = true;
    this.chatService.delChatRooms(this.chatRoom.id).subscribe((res: any) => {
      this.loading = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
      this.eventClosed.emit(true);
      this.sweetalertService
        .swal( {
          title: 'Borrar Chat',
          text: 'Chat eliminado',
        })
    }, error => {
      this.loading = false;
      this.sweetalertService
        .swal( {
          title: 'Borrar Chat',
          text: "ha ocurrido un error",
        })
    });
  }
}
