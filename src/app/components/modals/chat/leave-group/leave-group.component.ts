import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ChatService } from '../../../../services/chat.service';
import { SweetalertService } from '../../../../shared/services/sweetalert.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-leave-group',
  templateUrl: './leave-group.component.html',
  styleUrls: ['./leave-group.component.scss']
})
export class LeaveGroupComponent implements OnInit {
  loading: boolean;
  eventClosed: EventEmitter<any> = new EventEmitter();
  onClose: Subject<boolean>;
  chatRoom: any;

  constructor(
    public bsModalRef: BsModalRef,
    private chatService: ChatService,
    private sweetalertService: SweetalertService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  onSubmit() {
    this.deleteChatRooms();
  }

  deleteChatRooms() {
    const thisIn = this;
    this.loading = true;
    this.chatService.leaveChatRooms(this.chatRoom.id).subscribe((res: any) => {
      this.loading = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
      this.eventClosed.emit(true);
      this.sweetalertService
        .swal( {
          title: 'Salir del grupo',
          text: 'Acabas de dejar el grupo',
        })
    }, error => {
      this.loading = false;
      this.sweetalertService
        .swal( {
          title: 'Salir del grupo',
          text: "ha ocurrido un error",
        })
    });
  }

}
