import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthenticationService } from '@services/authentication.service';

@Component( {
  selector: 'app-constancy',
  templateUrl: './constancy.component.html',
  styleUrls: ['./constancy.component.scss']
} )
export class ConstancyComponent implements OnInit {
  user: any;
  data: any;

  constructor( public bsModalRef: BsModalRef, private auth: AuthenticationService ) { }

  ngOnInit(): void {
    this.user = this.auth.getUser()?.data.user;
    console.log( this.user );
    console.log( this.data );
  }

}
