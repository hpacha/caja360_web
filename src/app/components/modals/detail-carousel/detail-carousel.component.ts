import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-detail-carousel',
  templateUrl: './detail-carousel.component.html',
  styleUrls: ['./detail-carousel.component.scss']
})
export class DetailCarouselComponent implements OnInit {
  images: any;
  videos: any;
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
    console.log(this.images, this.videos);
  }

}
