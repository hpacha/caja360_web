import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CommunityService } from '@services/community.service';
import { finalize, takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import * as moment from 'moment';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-mentor-request',
  templateUrl: './mentor-request.component.html',
  styleUrls: ['./mentor-request.component.scss']
} )
export class MentorRequestComponent implements OnInit {
  currentDay = moment().format( 'DD/MM/YYYY' );
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  mentoringId: any;
  loading: boolean;
  requests: any = { preload: false, preloadPage: false, currentPage: 1, lastPage: 0, items: null };
  mentors = [];
  eventClosed: EventEmitter<any> = new EventEmitter();
  acceptReject: any;

  constructor( public bsModalRef: BsModalRef,
               private communityService: CommunityService ) { }

  ngOnInit(): void {
    console.log( this.mentoringId, this.requests );
    if ( this.mentoringId ) {
      this.getUserCompetencesMentoringRequest( this.mentoringId, this.requests );
    }
  }

  onClickClose() {
    this.bsModalRef.hide();
    this.eventClosed.emit( {status: 'aceptado'} );
  }

  getUserCompetencesMentoringRequest( mentoringId, tabOption ) {
    return new Promise<any[]>( ( resolve, reject ) => {
      const thisIn = this;
      if ( tabOption.currentPage === 1 ) {
        tabOption.preload = true;
      } else {
        tabOption.preloadPage = true;
      }
      // const parameters: any = {};
      this.loading = true;
      thisIn.communityService.getUserCompetencesMentoringRequest( mentoringId, tabOption.currentPage ).pipe(
        takeUntil( thisIn.destroyed$ ),
        finalize( () => {
          if ( tabOption.currentPage === 1 ) {
            tabOption.preload = false;
          } else {
            tabOption.preloadPage = false;
          }
          resolve();
        } ),
      ).subscribe( ( res: any ) => {
        this.loading = false;
        console.log(this.mentors);
        tabOption.items = [];
        if (res.data.data && res.data.data.length >= 1 ) {
          // tabOption.items = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
          const data = ( tabOption.items ) ? ( tabOption.items ).concat( res.data.data ) : res.data.data;
          this.mentors = [];
          tabOption.items = [];
          console.log(this.mentors);
          data.forEach( message => {
            const date = this.mentors.find( x => x.date === moment( message.created_at ).format( 'DD/MM/YYYY' ) );
            if ( date ) {
              date.mentors.push( message );
            } else {
              this.mentors.push( {
                date: moment( message.created_at ).format( 'DD/MM/YYYY' ),
                actualName: ( moment( message.created_at ).format( 'DD/MM/YYYY' ) === this.currentDay ?
                  'Hoy' : moment( message.created_at ).format( 'DD/MM/YYYY' ) ), mentors: [message]
              } );
            }
          } );
          tabOption.items = this.mentors;
          console.log( this.mentors );
          console.log( tabOption.items );
          tabOption.lastPage = res.data.last_page;
        }
      }, error => {
        this.loading = false;
        tabOption.items = [];
        tabOption.currentPage = 1;
        tabOption.lastPage = 0;
      } );
    } );
  }

  onClickAcceptReject( item, type: string ) {
    console.log( item, type );
    if ( type === 'aceptar' ) {
      const parameters: any = {};
      // parameters.suggested_mentor_id = item.id;
      parameters.status = 'Aceptado';
      this.loading = true;
      this.communityService.patchRequestAcceptReject( this.mentoringId, item.id, parameters ).subscribe( ( res: any ) => {
        this.loading = false;
        console.log( res );
        Swal.fire( {
          title: 'Solicitar mentor', text: 'La solicitud fue aceptada exitosamente', icon: 'success', iconHtml: null, timer: 4000
        } ).then( () => {
          this.getUserCompetencesMentoringRequest( this.mentoringId, this.requests );
          /* this.onClose.next( true );
           this.bsModalRef.hide();
           this.eventClosed.emit( true );*/
        } );
      }, error => {
        this.loading = false;
        Swal.fire( {
          title: 'Solicitar mentor', html: `Ocurrió un error al aceptar la solicitud<br/>${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        } );
      } );
    } else {
      this.eventClosed.emit( {status: 'rechazado', data: {mentoringId: this.mentoringId, requestId: item.id }} );
      this.bsModalRef.hide();
    }
  }
}
