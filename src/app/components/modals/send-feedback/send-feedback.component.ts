import { Component, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CommunityService } from '@services/community.service';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-send-feedback',
  templateUrl: './send-feedback.component.html',
  styleUrls: ['./send-feedback.component.scss']
} )
export class SendFeedbackComponent implements OnInit {
  competenceId: any;
  menteesId: any;
  evaluationId: any;
  loading: boolean;
  form: FormGroup;
  eventClosed: EventEmitter<any> = new EventEmitter();

  constructor( public bsModalRef: BsModalRef,
               private fb: FormBuilder,
               private communityService: CommunityService ) {
    this.createForm();
  }

  ngOnInit(): void {
    console.log( this.competenceId, this.menteesId, this.evaluationId );
  }

  onclickSuggestedMentor() {
    this.form.markAllAsTouched();
    if ( this.form.valid ) {
      console.log( this.form.value );
      const parameters: any = {};
      parameters.status = this.form.controls.status.value;
      parameters.feedback = this.form.controls.feedback.value;
      this.loading = true;
      this.communityService.patchUpdateEvaluations( this.competenceId, this.menteesId, this.evaluationId, parameters ).subscribe( ( res: any ) => {
        console.log( res );
        this.loading = false;
        Swal.fire( {
          title: 'Evaluación', text: 'La evaluación fue actualizada', icon: 'success', iconHtml: null, timer: 4000
        } ).then( () => {
          this.bsModalRef.hide();
          this.eventClosed.emit( true );
        } );
      }, error => {
        Swal.fire( {
          title: 'Evaluación', html: `Ocurrió un error al actualizar la evaluación<br/>${error.error.message}`, icon: 'error',
          iconHtml: null, timer: 4000
        } );
        this.loading = false;
      } );
      /*
       this.eventClosed.emit( { status: true, data: this.data, reason_rejection: this.form.controls.reason_rejection.value } );
       */
    }
  }

  onClickClose() {
    this.bsModalRef.hide();
  }

  private createForm() {
    this.form = this.fb.group( {
      status: ['desaprobado', Validators.required],
      feedback: [null, Validators.required],
    } );
  }

}
