import { Component, EventEmitter, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RecognitionSystemService } from '@services/recognition-system.service';

@Component({
  selector: 'app-default-images',
  templateUrl: './default-images.component.html',
  styleUrls: ['./default-images.component.scss']
})
export class DefaultImagesComponent implements OnInit {

  eventId: number;
  images = [];
  loading: boolean;
  activeImage: null;
  defaultImage: string;
  eventClosed: EventEmitter<any> = new EventEmitter();
  template: any;

  constructor( public bsModalRef: BsModalRef, private recognitionService: RecognitionSystemService ) { }

  ngOnInit(): void {
    if ( this.eventId && !this.template ) {
      this.getImages();
    } else {
      this.images = [this.template];
      this.activeImage = this.template;
    }
  }

  onClickSelect( item ) {
    this.activeImage = item;
  }

  onClickSave() {
    this.eventClosed.emit( { value: true, template: this.activeImage } );
    this.bsModalRef.hide();
  }

  getImages() {
    this.loading = true;
    this.recognitionService.getEventByPhoto( this.eventId ).subscribe( ( res: any ) => {
      this.loading = false;
      console.log( res.data );
      this.images = res.data;
      this.defaultImage = './assets/img/placeholder-h.png';
    }, error => this.loading = true );
  }
}
