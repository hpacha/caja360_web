import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '@services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component( {
  selector: 'app-sos',
  templateUrl: './sos.component.html',
  styleUrls: ['./sos.component.scss']
} )
export class SosComponent implements OnInit {
  loading: boolean;
  types: any;
  form: FormGroup;

  constructor( private fb: FormBuilder, public bsModalRef: BsModalRef,
               private userService: UserService, ) { }

  ngOnInit(): void {
    this.createForm();
    this.getListPanic();
  }

  onClickSos() {
    const params = {
      emergency_type_id: this.form.value.emergency_type_id,
      cordinates: this.form.value.cordinates,
    };
    this.loading = true;
    this.userService.postPanic( params ).subscribe( ( res: any ) => {
      console.log( 'is a panic', res );

      Swal.fire( {
        title: 'Emergencia Enviada',
        text: 'Gracias por comunicarse, se envió un correo a las áreas notificando que ha ocurrido una emergencia.', icon: 'success',
        customClass: { container: 'container-class' }, iconHtml: '<img src="./assets/img/svg/panicsend.svg" class="img-fluid" alt="">',
        timer: 6000
      }).then(() => {
        this.bsModalRef.hide();
      });
      this.loading = false;
    }, error => {
      this.loading = false;
      Swal.fire( {
        title: 'Emergencia no enviada', text: 'Ocurrió un error al enviar la emergencia', icon: 'error', iconHtml: null, timer: 4000
      } ).then(() => {
        this.bsModalRef.hide();
      });
    } );
  }

  private getListPanic() {
    this.loading = true;
    this.userService.getListPanic().subscribe( ( res: any ) => {
      this.loading = false;
      console.log( res );
      this.types = res.data.filter(x => x.show_map === false);
    }, error => this.loading = false );
  }

  private createForm() {
    this.form = this.fb.group({
      emergency_type_id: [null, Validators.required],
      cordinates: [null, null]
    });
  }
}
