import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit {
  activity: any;
  mentor: any;
  current: boolean = false;
  start: any;
  end: any;

  constructor(
    public bsModalRef: BsModalRef,
  ) {}

  ngOnInit(): void {}
}
