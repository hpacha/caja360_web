import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BsModalRef } from 'ngx-bootstrap/modal';
import { ReplaySubject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

import { NewnessActivityService } from '@services/newness-activity.service';
import * as Globals from '@utils/globals';

@Component( {
  selector: 'app-news-edit-date',
  templateUrl: './news-edit-date.component.html',
  styleUrls: ['./news-edit-date.component.scss']
} )
export class NewsEditDateComponent implements OnInit, OnDestroy {
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  globals = Globals;
  form: FormGroup;
  loading: boolean;
  id: number;
  schedule: any;
  typebtn: any;
  type: string;
  arrIdSchedule = [];
  eventClosed: EventEmitter<any> = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    public bsModalRef: BsModalRef,
    private newnessActivityService: NewnessActivityService,
  ) {}

  ngOnInit(): void {
    this.createForm();
    console.log( this.schedule, this.typebtn );
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  onChangeValue( id, type: string ) {
    const i = this.arrIdSchedule.indexOf( id );
    if ( i === -1 ) {
      this.arrIdSchedule.push( id );
    } else {
      this.arrIdSchedule.splice( i, 1 );
    }
  }

  onClickChoose() {
    if ( this.arrIdSchedule.length ) {
      this.putScheduleActivity();
    }
  }

  onClickChooseDelete() {
   /* this.newnessActivityService.deleteSchedule(this.iDelete).pipe(
      takeUntil( this.destroyed$ ),
      finalize( () => {
        this.loading = false;
      } ),
    ).subscribe( ( res: any ) => {
      Swal.fire( {
        title: 'Desagendar Actividad', text: `La actividad se guardo correctamente`, icon: 'success', iconHtml: null, timer: 4000
      } ).then( () => {
        this.bsModalRef.hide();
        this.eventClosed.emit( true );
      } );
    }, error => {
      console.log( error );
      Swal.fire( {
        title: 'Agendar Actividad', html: `Ocurrió un error al desagendar actividad. <br/> ${error.error.message}`, icon: 'error',
        iconHtml: null, timer: 4000
      } ).then( () => {
        this.bsModalRef.hide();
        this.eventClosed.emit( true );
      } );
    } );*/
  }

  private putScheduleActivity() {
    this.loading = true;
    const params: any = {};
    params.activity_id = this.type === 'actividades' ? this.id : null;
    params.schedules = this.arrIdSchedule;
    this.newnessActivityService.putSchedulePost( params ).pipe(
      takeUntil( this.destroyed$ ),
      finalize( () => {
        this.loading = false;
      } ),
    ).subscribe( ( res: any ) => {
      Swal.fire( {
        title: 'Agendar Actividad', text: `La actividad se agendó exitosamente`, icon: 'success', iconHtml: null, timer: 4000
      } ).then( () => {
        this.bsModalRef.hide();
        this.eventClosed.emit( true );
      } );
    }, error => {
      console.log( error );
      Swal.fire( {
        title: 'Agendar Actividad', html: `Ocurrió un error al agendar actividad. <br/> ${error.error.message}`, icon: 'error',
        iconHtml: null, timer: 4000
      } ).then( () => {
        this.bsModalRef.hide();
        this.eventClosed.emit( true );
      } );
    } );
  }

  private createForm() {
    this.form = this.fb.group( {} );
  }
}
