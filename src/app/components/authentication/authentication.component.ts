import { Component, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ReCaptcha2Component } from 'ngx-captcha';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { finalize } from 'rxjs/internal/operators';

import { environment } from '@environments/environment';
import { AuthenticationService } from '@services/authentication.service';
import { MessagingService } from '@services/messaging.service';
import { CipherUtil } from '@utils/cipher.util';

@Component( {
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
} )
export class AuthenticationComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;
  destroyed$: ReplaySubject<boolean> = new ReplaySubject();
  @ViewChild( 'captchaElem' ) captchaElem: ReCaptcha2Component;
  messageError: any;
  loading: boolean;
  isTextFieldType: boolean;
  siteKey = environment.recaptcha_key;

  constructor(
    private formBuilder: FormBuilder,
    private renderer: Renderer2,
    private router: Router,
    private authenticationService: AuthenticationService,
    private messagingService: MessagingService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.renderer.selectRootElement( '#workerCode' ).focus();
    this.messagingService.requestPermission();
  }

  ngOnDestroy() {
    this.destroyed$.next( true );
    this.destroyed$.unsubscribe();
  }

  togglePasswordFieldType() {
    this.isTextFieldType = !this.isTextFieldType;
  }

  handleExpire() {
    this.captchaElem.resetCaptcha();
    this.formGroup.get( 'recaptcha' ).reset();
  }

  handleSuccess( event: any ) {
    this.formGroup.get( 'recaptcha' ).setValue( event );
  }

  async onClickLogin() {
    this.formGroup.markAllAsTouched();
    if ( this.formGroup.valid ) {
      this.loading = true;
      const fcmtoken = localStorage.getItem( 'fcm' ) ? JSON.parse( atob( localStorage.getItem( 'fcm' ) ) ) : null;
      const params = {
        username: await CipherUtil.encrypt( this.formGroup.value.username ),
        password: await CipherUtil.encrypt( this.formGroup.value.password ),
        worker_code: await CipherUtil.encrypt( this.formGroup.value.worker_code ),
        fcm_token: fcmtoken !== null ? fcmtoken : null,
        origin: 'web',
      };

      const paramst = await CipherUtil.encrypt( this.formGroup.value.username + ':' + this.formGroup.value.worker_code + ':' + this.formGroup.value.password );
      const login = await this.authenticationService.postLogin( params )
      .pipe(
        finalize( () => {
          this.loading = false;
        } ),
        takeUntil( this.destroyed$ )
      ).subscribe( async ( res: any ) => {
        this.loading = false;
        localStorage.setItem( 'tk', btoa( JSON.stringify( paramst ) ) );
        localStorage.setItem( 'user', JSON.stringify( { 'id': res.data.user.id } ) );
        localStorage.removeItem( 'fcm' );
        this.isBoss();
      }, error => {
        this.loading = false;
        this.captchaElem.resetCaptcha();
        this.formGroup.get( 'recaptcha' ).reset();
        this.messageError = { msg: error.error.message };
      } );
    }
  }

  async isBoss() {
    await this.authenticationService.getBoos().subscribe( ( res: any ) => {
      localStorage.setItem( 'user_is_boss', res.data.is_boss );
      this.router.navigate( ['/perfil'] );
    } );
  }

  private createForm() {
    this.formGroup = this.formBuilder.group( {
      username: [null, Validators.required],
      password: [null, [Validators.required]],
      worker_code: [null, [Validators.required, Validators.maxLength( 11 )]],
      recaptcha: [null, [Validators.required]],
    } );
  }
}
