import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxCaptchaModule } from 'ngx-captcha';

import { ControlMessagesModule } from '@shared/control-messages/control-messages.module';
import { ServicesModule } from '@services/services.module';
import { SharedModule } from '@shared/shared.module';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponent } from './authentication.component';

@NgModule({
  declarations: [AuthenticationComponent],
  imports: [
    AuthenticationRoutingModule,
    ControlMessagesModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
    ServicesModule,
    SharedModule,
  ]
})
export class AuthenticationModule {}
